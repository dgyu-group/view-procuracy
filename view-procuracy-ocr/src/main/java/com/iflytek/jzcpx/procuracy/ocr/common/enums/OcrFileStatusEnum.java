package com.iflytek.jzcpx.procuracy.ocr.common.enums;

/**
 * ocr 识别状态枚举
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-12 14:36
 */
public enum OcrFileStatusEnum {
    /** 初始状态 */
    INIT,

    /** 获取到文件流 */
    GOT_FILE_STREAM,

    /** 请求ocr引擎结束 */
    REQUEST_OCR_ENGINE,

    /** ocr引擎结果存储完成 */
    ENGINE_RESULT_STORE,

    /** 合成pdf完成 */
    COMPOSITE_PDF,

    /** 推送给上游完成 */
    PUSH_TO_UPSTREAM,

    /** 任务结束 */
    END,
    ;

    OcrFileStatusEnum() {
    }
}