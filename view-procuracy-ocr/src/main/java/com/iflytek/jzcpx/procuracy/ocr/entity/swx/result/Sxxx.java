package com.iflytek.jzcpx.procuracy.ocr.entity.swx.result;

import java.util.List;

import lombok.Data;

/**
 * 手写信息
 */
@Data
public class Sxxx {

    /** 文本 */
    private String wb;

    /** 手写文本坐标 , 依次为左上 右上 右下 左下 */
    private List<Wbzb> wbzb;

    /** 字符信息 */
    private List<Zfxx> zfxx;
}
