package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import java.util.List;

import lombok.Data;

@Data
public class JzInfoResult {

    /**
     * #标识编号
     */
    private String bsbh;

    /**
     * #任务ID
     */
    private String taskid;

    /**
     * 文件信息列表
     */
    private List<Jzmlwj> jzmlwjlist;


}
