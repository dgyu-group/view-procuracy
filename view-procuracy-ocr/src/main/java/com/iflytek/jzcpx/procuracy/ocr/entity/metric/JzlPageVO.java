package com.iflytek.jzcpx.procuracy.ocr.entity.metric;

import java.util.Date;

public class JzlPageVO extends BasePageVO {

	// 单位编码，时间区间（年月日时分秒），部门受案号，标识编号，任务编号，卷宗编号，卷宗名称，操作类别
	private String dwbm;
	private Date startDate;
	private Date endDate;
	private String bmsah;
	private String bsbh;
	private String taskId;
	private String jzbh;
	private String jzmc;
	private String czlb;

	public String getDwbm() {
		return dwbm;
	}

	public void setDwbm(String dwbm) {
		this.dwbm = dwbm;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getBmsah() {
		return bmsah;
	}

	public void setBmsah(String bmsah) {
		this.bmsah = bmsah;
	}

	public String getBsbh() {
		return bsbh;
	}

	public void setBsbh(String bsbh) {
		this.bsbh = bsbh;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getJzbh() {
		return jzbh;
	}

	public void setJzbh(String jzbh) {
		this.jzbh = jzbh;
	}

	public String getJzmc() {
		return jzmc;
	}

	public void setJzmc(String jzmc) {
		this.jzmc = jzmc;
	}

	public String getCzlb() {
		return czlb;
	}

	public void setCzlb(String czlb) {
		this.czlb = czlb;
	}

}
