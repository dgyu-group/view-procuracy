package com.iflytek.jzcpx.procuracy.ocr.entity.swx.result;

import lombok.Data;

/**
 * 行文本坐标
 */
@Data
public class Wbzb {
    private int x;

    private int y;

    public Wbzb(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Wbzb() {
    }
}
