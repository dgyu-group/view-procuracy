package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import javax.validation.constraints.NotEmpty;

import lombok.Data;

/**
 * {
 * "WJBH": "24E6C7DB70E441888CA0E6AD40850157",  #文件编号
 * "WJHZ": ".pdf", #文件后缀
 * "WJNR": "dnjnciemkcaskm…" #文件内容（文件字节流）
 * }
 */
@Data
public class OcrParamVO {

    /**
     * 文件编号
     */
    private @NotEmpty String wjbh;

    /**
     * #文件后缀
     */
    private @NotEmpty String wjhz;

    /**
     * 文件内容（文件字节流）
     */
    private @NotEmpty String wjnr;

}
