package com.iflytek.jzcpx.procuracy.ocr.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

@Data
@TableName("t_metric")
public class Metric implements Serializable {
    /**
    * 
    */
    private static final long serialVersionUID = 2490510087863981535L;

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 指标类型ocr:图文识别 card:案卡回填 cont：编目
     */
    @TableField("mrtric_type")
    private String mrtricType;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;

    /**
     * 数据ID
     */
    @TableField("data_id")
    private String dataId;

    /**
     * 调用方IP
     */
    @TableField("caller_ip")
    private String callerIp;

    /**
     * 服务提供方IP
     */
    @TableField("service_ip")
    private String serviceIp;

    /**
     * 应用和系统唯一id
     */
    @TableField("systemid")
    private String systemId;

    /**
     * 接口名
     */
    @TableField("interface_name")
    private String interfaceName;

    /**
     * 文件编号
     */
    @TableField("wjxh")
    private String wjxh;

    /**
     * 文件id 存储ocrFileId或者cardFileId
     */
    @TableField("file_id")
    private Long fileId;

    /**
     * 调用OCR开始时间
     */
    @TableField("ocr_start_time")
    private Date ocrStartTime;

    /**
     * 调用OCR结束时间
     */
    @TableField("ocr_end_time")
    private Date ocrEndTime;

    /**
     * 开始时间 单位毫秒
     */
    @TableField("start_millis")
    private Long startMillis;

    /**
     * 结束时间 单位毫秒
     */
    @TableField("end_millis")
    private Long endMillis;

    /**
     * 耗时 单位毫秒
     */
    @TableField("cost_time")
    private Long costTime;

    /**
     * 调用OCR结果 succes或者fail
     */
    @TableField("ocr_result")
    private String ocrResult;

    /**
     * 预留字段
     */
    @TableField("remark")
    private String remark;

    /**
     * 单位编码
     */
    @TableField("dwbm")
    private String dwbm;

    /**
     * 部门受案号
     */
    @TableField("bmsah")
    private String bmsah;

    /**
     * 案件类别
     */
    @TableField("ajlbbm")
    private String ajlbbm;

    /**
     * 标识编号
     */
    @TableField("bsbh")
    private String bsbh;
    /**
     * 任务编号
     */
    @TableField("taskid")
    private String taskid;
    /**
     * 卷宗编号
     */
    @TableField("jzbh")
    private String jzbh;
    /**
     * 卷宗名称
     */
    @TableField("jzmc")
    private String jzmc;
    /**
     * 目录编号
     */
    @TableField("mlbh")
    private String mlbh;
    
    /**
     * 文书类型 0:起诉意见书 1：提请批准逮捕书
     */
    @TableField("wslx")
    private String wslx;

}
