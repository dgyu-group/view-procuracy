package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import lombok.Data;

@Data
public class Jzmlwj {

    /**
     * 部门受案号
     */
    private String bmsah;

    /**
     * 标识编号
     */
    private String bsbh;

    /**
     * 文件类型
     */
    private String filetype;

    /**
     * 文件在存储上的路径
     */
    private String wjlj;

    /**
     * 文件名称
     */
    private String wjmc;

    /**
     * 文件识别路径
     */
    private String wjsblj;

    /**
     * 自定义字段，在此处代表，文件是否已经识别为双层pdf
     * <p>
     * Y ：已经识别
     * N ：未识别
     */
    private String wjzdy;

    /**
     * 文件序号
     */
    private String wjxh;
    
    /**
     * 图片ocr结果
     */
    private String ocrResult;


}
