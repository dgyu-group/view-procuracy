package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 表格里的行
 */
@Data
public class WjOcrsbjgResDataWjsbxxBgqyTableTrs {

    public WjOcrsbjgResDataWjsbxxBgqyTableTrs()
    {
        tds=new ArrayList<>();
    }

    /**
     * 1
     */
    private int hxh;//#行序号

    private String zbwz;//坐标位置

    /**
     * 列数组
     */
    private List<WjOcrsbjgResDataWjsbxxBgqyTableTrsTds> tds;
}
