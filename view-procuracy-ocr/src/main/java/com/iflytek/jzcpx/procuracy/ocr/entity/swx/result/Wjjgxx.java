package com.iflytek.jzcpx.procuracy.ocr.entity.swx.result;

import java.util.List;

import lombok.Data;

/**
 * OCR返回带有文字坐标信息
 */
@Data
public class Wjjgxx {

    /** 文本数量 为0时wbxx节点不存在 */
    private Integer wbsl;

    /** 手写数量 为0时sxxx节点不存在 */
    private Integer sxsl;

    /** 结构化文本信息 */
    private List<Wbxx> wbxx;

    /** 手写信息 */
    private List<Sxxx> sxxx;

}
