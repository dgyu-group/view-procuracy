package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import lombok.Data;

@Data
public class Jzml {

    /**
     * 部门受案号
     */
    private String bmsah;
    
    /**
     * #卷宗编号
     */
    private String jzbh;
    
    /**
     * #单位编码
     */
    private String dwbm;
    
    /**
     * #目录顺序号
     */
    private String mlsxh;
    
    /**
     * #所属类别编码
     */
    private String sslbbm;
    
    /**
     * #目录显示名称
     */
    private String mlxsmc;
    
    /**
     * #所属类别名称
     */
    private String sslbmc;
    
    /**
     * #目录编码
     */
    private String mlbm;
    
    /**
     * #目录编号
     */
    private String mlbh;
    
    /**
     * #目录类型
     */
    private String mllx;
    
    /**
     * 父目录编号
     */
    private String fmlbh;
    
    /**
     * 目录详细信息
     */
    private String mlxx;
    
}
