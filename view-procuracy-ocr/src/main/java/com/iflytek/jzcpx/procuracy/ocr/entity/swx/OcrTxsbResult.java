package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import lombok.Data;

@Data
public class OcrTxsbResult {

    private boolean success = false;

    private String code;


    private String message;


    private OcrTxsbData data;


}
