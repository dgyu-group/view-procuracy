package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import java.util.List;

import lombok.Data;

/**
 *
 */
@Data
public class OcrWJsbMultiResult {

    private String bmsah;//"汉东省院起诉受[2019]77000000034号",#部门受案号

    private List<Wjsbxx> wjxhlist;//文件序号集合
}
