package com.iflytek.jzcpx.procuracy.ocr.common.enums;

/**
 * ocr批量任务状态
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-14 09:34
 */
public enum OcrTaskStatusEnum {

    /** 初始状态 */
    INIT,

    /** 处理中 */
    PROCESSING,

    /** 处理完成, 推送消息提交中 */
    SUBMITTING,

    /** 推送中 */
    PUSHING,

    /** 任务结束 */
    END
}
