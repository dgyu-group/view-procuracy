package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import lombok.Data;

/**
 * 推送OCR识别请求
 */
@Data
public class BatchOcrVO {

    /**
     * #应用和单位的唯一标识
     */
    private String systemid;

    /**
     * #标识编号
     */
    private String bsbh;

    /**
     * #单位编码
     */
    private String dwbm;

    /**
     * #任务ID应用和单位的唯一标识
     */
    private String taskid;
    
    /**
     * #操作类型 ocr-1 图像识别-2
     */
    private String type;
    /**
     * 部门受案号
     */
    private String bmsah;

}
