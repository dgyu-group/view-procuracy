package com.iflytek.jzcpx.procuracy.ocr.common.constant;

/**
 * ocr常量信息
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-13 09:20
 */
public final class OcrConstants {

    /** ocr 批量任务结果推送任务队列 */
    public static final String QUEUE_OCR_RESULT_PUSH = "queue_ocr_result_push";
    /** 图像识别批量任务结果推送任务队列 */
    public static final String QUEUE_RECOGNIZE_RESULT_PUSH = "queue_recognize_result_push";

    /** ocr 批量任务队列 */
    public static final String QUEUE_OCR_TASK = "queue_ocr_task";
    /** 图像识别批量任务队列 */
    public static final String QUEUE_RECOGNIZE_TASK = "queue_recognize_task";

    /** ocr识别结果上传fdfs使用的默认后缀名 */
    public static final String OCR_ENGINE_FDFS_SUFFIX = "txt";

    /** 图像识别功能启用状态 */
    public static final class RecognizeFuncFlag {
        public static final String ENABLE = "Y";
        public static final String DISABLE = "N";

        private RecognizeFuncFlag() {
        }
    }


}
