package com.iflytek.jzcpx.procuracy.ocr.common.constant;

public interface Constants {
    String CARDFILE_OCRPATH_SEPERATOR = ",";
    String OCR_METRIC_DATA_PREFIX = "ocr_batch_";
    String RECOG_METRIC_DATA_PREFIX = "recog_batch_";

    /**
     * 文件转换支持的格式类型
     */
    class SupportExt {
        /**
         * 支持转图片的源文件格式
         */
        public static final String[] SOURCE = {"pdf", "doc", "xls", "ppt", "docx", "xlsx", "pptx", "txt", "html", "tif", "tiff", "jpg", "jpeg", "bmp", "png"};
        /**
         * 支持转图片的office文件格式
         */
        public static final String[] OFFICE = {"doc", "xls", "ppt", "docx", "xlsx", "pptx", "txt", "html"};
        /**
         * 支持转图片的pdf文件格式
         */
        public static final String PDF = "pdf";
        /**
         * 支持转图片的word文件格式
         */
        public static final String DOC = "doc";
        /**
         * 支持转图片的word文件格式
         */
        public static final String DOCX = "docx";
        /**
         * 支持转图片的excel文件格式
         */
        public static final String XLS = "xls";
        /**
         * 支持转图片的excel文件格式
         */
        public static final String XLSX = "xlsx";
        /**
         * 支持转图片的ppt文件格式
         */
        public static final String PPT = "ppt";
        /**
         * 支持转图片的ppt文件格式
         */
        public static final String PPTX = "pptx";
        /**
         * 支持转目标图片格式
         */
        public static final String[] IMAGE = {"tif", "tiff", "jpg", "jpeg", "bmp", "png"};
        /**
         * 支持转目标图片格式
         */
        public static final String TIF = "tif";
        /**
         * 支持转图片的tiff文件格式
         */
        public static final String TIFF = "tiff";
        /**
         * 支持jpg格式，但不做转换
         */
        public static final String JPG = "jpg";
        /**
         * 支持jpg格式，但不做转换
         */
        public static final String JPEG = "jpeg";
        /**
         * 支持转图片的bmp文件格式
         */
        public static final String BMP = "bmp";
        /**
         * 支持转图片的png文件格式
         */
        public static final String PNG = "png";
        /**
         * 支持转目标图片格式
         */
        public static final String[] TARGET = {"jpg", "jpeg"};
    }

    /**
     * 字符串 0
     */
    String STATE_NOT_DELETED = "0";
    /**
     * 字符串 1
     */
    String STATE_DELETED = "1";
    /**
     * 字符串 2
     */
    String STR_TWO = "2";

    /**
     * ocr 结果结构
     * <p>
     * this is description
     * </p>
     *
     * @author mcxu2
     * @version 1.0
     * @since 2019年2月27日下午2:29:10
     */
    class ResultStruct {
        /**
         * 任务结果
         */
        public static final String TASK_RESULT = "task_result";
        /**
         * page
         */
        public static final String PAGE = "page";
        /**
         * 文本区域
         */
        public static final String TEXT_REGION = "TextRegion";
        /**
         * 表格
         */
        public static final String TABLE = "Table";
        /**
         * 单元格
         */
        public static final String CELL = "cell";
        /**
         * 签章
         */
        public static final String SEAL = "Seal";
        /**
         * 手印
         */
        public static final String FINGER_PRINT = "Fingerprint";
        /**
         * 插图Illustration
         */
        public static final String ILLUSTRATION = "Illustration";
        /**
         * 图片Graphics
         */
        public static final String GRAPHICS = "Graphics";
        /**
         * textline
         */
        public static final String TEXT_LINE = "textline";
        /**
         * sent
         */
        public static final String SENT = "sent";
        /**
         * value
         */
        public static final String VALUE = "value";
        /**
         * 手写体
         */
        public static final String HANDWRITE = "HANDWRITE";
        /**
         * 签名
         */
        public static final String SIGNATURE = "SIGNATURE";
    }

    class PDFCompose {

        public static final String SUCCESS = "1";
        public static final String FAILURE = "0";
        public static final String DATE_FORMATE = "yyyy-MM-dd_HH-mm-ss-SSS";
        public static final String NEW_OCR_VERSION = "new";
        public static final String OLD_OCR_VERSION = "old";
        /**
         * 服务最大排队数量
         */
        public static final int QUEUE_LENGTH = 20;
        /**
         * 单次请求最大文件个数
         */
        public static final int FILE_SIZE = 300;

        /**
         * 上传文件大小限制
         */
        public static final int FILE_LIMITET_CONTENT_SIZE = 1024 * 1024 * 10;
        /**
         * 文件需要压缩的临界值
         */
        public static final int FILE_COMPRESS_CRITICAL_SIZE = 1024 * 1024 * 5;

        public static final String PDF_FORMATE = "pdf";
        public static final String JPG_FORMATE = "jpg";
        public static final String PNG_FORMATE = "png";
        /**
         * 印章类型
         */
        public static final String OCR_SEAL_TYPE = "SEAL";
        /**
         * 打印体
         */
        public static final String OCR_MACHINEPRINT_TYPE = "MACHINEPRINT";
        /**
         * 手写体
         */
        public static final String OCR_HANDWRITE_TYPE = "HANDWRITE";

        public static final String DOUBLE_PDF_LIST = "doublePdfList";

        public static final String DOUBLE_PDF_MERGE = "doublePdfMerge";
    }

    class WjsbRequiredType {
        public static final String Y = "y";
        public static final String N = "n";
    }

    class WjsbItemType {
        public static final String SBBT= "sbbt";
    }

}
