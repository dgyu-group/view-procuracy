package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class WjOcrsbjgResDataWjjgxxWbxx {

    public WjOcrsbjgResDataWjjgxxWbxx()
    {
        hwbzb=new ArrayList<>();
    }

    private String hwb;//#行文本

    private int hxh;//#行序号

    /**
     * 段落怎么办，可能搞不清楚段落，暂用行序号来处理
     */
    private int dlxh;// #段落序号

    /**
     * 一行的坐标
     */
    private List<WjOcrsbjgResDataWjjgxxWbxxHwbzb> hwbzb;//#行文本坐标
}
