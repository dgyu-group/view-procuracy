package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import lombok.Data;

/**
 * 获取案件目录文件信息
 */
@Data
public class JzmlInfo {


    /**
     * true 成功
     */
    private boolean success;


    /**
     * 成功状态码
     */
    private String code;

    /**
     * 消息
     */
    private String message;

    /**
     * 数据消息
     */
    private JzmlInfoResult data;


}
