package com.iflytek.jzcpx.procuracy.ocr.recognize.model;

import lombok.Data;

/**
 * 图像识别结果 - 文件特征
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-13 10:19
 */
@Data
public class Wjtz {

    /** 特征类型 */
    private Integer tzlx;

    /** 坐标信息 */
    private Zbxx zbxx;

    public Wjtz(int tzlx, int x, int y, int w, int h) {
        this.tzlx = tzlx;
        this.zbxx = new Zbxx(x, y, w, h);
    }

    /** 坐标信息 */
    @Data
    public class Zbxx {
        /** x坐标 */
        private Integer x;
        /** y坐标 */
        private Integer y;
        /** WIDTH坐标 */
        private Integer w;
        /** HEIGHT坐标 */
        private Integer h;

        public Zbxx(Integer x, Integer y, Integer w, Integer h) {
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
        }
    }

}
