package com.iflytek.jzcpx.procuracy.ocr.recognize.model;

/**
 *
 */
public class OcrResultQueryDto {

    private String bmsah;

    private String[] wjxhlist;

    public String getBmsah() {
        return bmsah;
    }

    public String[] getWjxhlist() {
        return wjxhlist;
    }

    public void setBmsah(String bmsah) {
        this.bmsah = bmsah;
    }

    public void setWjxhlist(String[] wjxhlist) {
        this.wjxhlist = wjxhlist;
    }
}
