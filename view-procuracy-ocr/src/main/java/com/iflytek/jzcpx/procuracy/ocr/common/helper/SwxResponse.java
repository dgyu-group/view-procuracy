package com.iflytek.jzcpx.procuracy.ocr.common.helper;

import com.iflytek.jzcpx.procuracy.ocr.entity.swx.JzInfoResult;
import lombok.Data;


/**
 * 赛威讯接口返回值
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-13 14:38
 */
@Data
public class SwxResponse {

    /**
     * true 成功
     */
    private boolean success;


    /**
     * 响应码
     */
    private String code;

    /**
     * 响应描述
     */
    private String message;

    /**
     * 数据消息
     */
    private JzInfoResult data;


    public static void main(String[] args) {

    }
}
