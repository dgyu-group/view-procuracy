package com.iflytek.jzcpx.procuracy.ocr.common.enums;

import com.iflytek.jzcpx.procuracy.common.enums.ResultType;

/**
 * 图像识别接口结果枚举, 12xxx
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-07 09:15
 */
public enum RecognizeResultEnum implements ResultType {
    RECOGNIZE_EXCEPTION(12001, "图像识别异常"),
    GET_DZJD_JPG_REQUEST_FAILED(12101, "获取电子卷宗文件流失败"),
    SAVEPDF_REQUEST_FAILED(12102, "提交OCR识别文件（双层pdf）失败"),

    ;

    private Integer code;
    private String desc;

    RecognizeResultEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static RecognizeResultEnum getByCode(Integer code) {
        if (code == null) {
            return null;
        }

        for (RecognizeResultEnum anEnum : values()) {
            if (anEnum.getCode().equals(code)) {
                return anEnum;
            }
        }

        return null;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getDesc() {
        return desc;
    }}