/**
  * Copyright 2020 bejson.com 
  */
package com.iflytek.jzcpx.procuracy.ocr.entity.txsb;
import java.util.List;

import com.iflytek.jzcpx.procuracy.ocr.recognize.model.Wjtz;

/**
 * Auto-generated: 2020-08-10 15:13:42
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Jzmlwj {

    private String jzbh;
    private String mlbh;
    private String wjxh;
    private String sfkby;
    private Wjlx wjlx;
    private List<Wjtz> wjtz;
    public void setJzbh(String jzbh) {
         this.jzbh = jzbh;
     }
     public String getJzbh() {
         return jzbh;
     }

    public void setMlbh(String mlbh) {
         this.mlbh = mlbh;
     }
     public String getMlbh() {
         return mlbh;
     }

    public void setWjxh(String wjxh) {
         this.wjxh = wjxh;
     }
     public String getWjxh() {
         return wjxh;
     }

    public void setSfkby(String sfkby) {
         this.sfkby = sfkby;
     }
     public String getSfkby() {
         return sfkby;
     }

    public void setWjlx(Wjlx wjlx) {
         this.wjlx = wjlx;
     }
     public Wjlx getWjlx() {
         return wjlx;
     }

    public void setWjtz(List<Wjtz> wjtz) {
         this.wjtz = wjtz;
     }
     public List<Wjtz> getWjtz() {
         return wjtz;
     }

}