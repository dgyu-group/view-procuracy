package com.iflytek.jzcpx.procuracy.ocr.recognize.model;

import lombok.Data;

@Data
public class OcrResultRequestVo {

    /** 文件编号 */
    private String wjbh;

    /** 文件后缀 */
    private String wjhz;

    /** 文件内容（文件字节数组base64） */
    private String wjnr;

    /** 是否输出文件内容（双层pdf） */
    private boolean sfwjnr;

    /** 是否输出文件文本 */
    private boolean sfwjwb;

    /** 是否输出文件结构化信息（带坐标） */
    private boolean sfwjxx;

    /** 是否对图片纠偏（非必要） */
    private boolean sfjp;

    /** 是否对图片旋转（非必要） */
    private boolean sfxz;

    /** 不需要传文件，传true的场景是应用监听了mq的ocr识别消息，得到消息直接获取ocr结果（非必要） */
    private boolean nofile;

    /** 源文件md5,  #md5和wjxh至少一个必填（非必要） */
    private String md5;

    /** #文件序号，文件唯一标识（非必要） */
    private String wjxh;

    /**  #优先级  (high/low)，默认值low（非必要） */
    private String yxj;

    /** 应用名称 */
    private String yylx;
}
