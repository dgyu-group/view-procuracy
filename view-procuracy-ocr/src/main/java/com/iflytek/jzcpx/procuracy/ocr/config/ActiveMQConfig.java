package com.iflytek.jzcpx.procuracy.ocr.config;

import javax.jms.Queue;

import org.apache.activemq.ActiveMQPrefetchPolicy;
import org.apache.activemq.RedeliveryPolicy;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.jms.JmsAutoConfiguration;
import org.springframework.boot.autoconfigure.jms.JmsProperties;
import org.springframework.boot.autoconfigure.jms.activemq.ActiveMQConnectionFactoryCustomizer;
import org.springframework.boot.autoconfigure.jms.activemq.ActiveMQProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;

/**
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2022/6/17
 */
@Configuration
@EnableJms
@EnableConfigurationProperties({ActiveMQProperties.class, JmsProperties.class})
@AutoConfigureBefore(JmsAutoConfiguration.class)
public class ActiveMQConfig {

    @Value("${mq.queue.ocrTask}")
    private String ocrTaskQueueName;
    @Value("${mq.queue.ocrResult}")
    private String ocrResultQueueName;
    @Value("${mq.queue.recognizeTask}")
    private String recognizeTaskQueueName;
    @Value("${mq.queue.recognizeResult}")
    private String recognizeResultQueueName;

    @Bean
    public Queue ocrTaskQueue() {
        return new ActiveMQQueue(ocrTaskQueueName);
    }
    @Bean
    public Queue ocrResultPushQueue() {
        return new ActiveMQQueue(ocrResultQueueName);
    }
    @Bean
    public Queue recognizeTaskQueue() {
        return new ActiveMQQueue(recognizeTaskQueueName);
    }
    @Bean
    public Queue recognizeResultPushQueue() {
        return new ActiveMQQueue(recognizeResultQueueName);
    }

    @Bean
    public ActiveMQConnectionFactoryCustomizer configureRedeliveryPolicy() {
        return connectionFactory -> {
            // 自定义重试机制
            RedeliveryPolicy redeliveryPolicy = new RedeliveryPolicy();
            //是否在每次尝试重新发送失败后,增长这个等待时间
            redeliveryPolicy.setUseExponentialBackOff(true);
            //重发次数,默认为6次
            redeliveryPolicy.setMaximumRedeliveries(3);
            //重发时间间隔,默认为1秒 ！！！！
            redeliveryPolicy.setInitialRedeliveryDelay(2000);
            //第一次失败后重新发送之前等待2000毫秒,第二次失败再等待2000 * 2毫秒,这里的2就是value
            redeliveryPolicy.setBackOffMultiplier(2);
            //是否避免消息碰撞
            redeliveryPolicy.setUseCollisionAvoidance(false);
            //设置重发最大拖延时间-1 表示没有拖延只有UseExponentialBackOff(true)为true时生效
            redeliveryPolicy.setMaximumRedeliveryDelay(-1);

            //
            ActiveMQPrefetchPolicy prefetchPolicy = new ActiveMQPrefetchPolicy();
            prefetchPolicy.setQueuePrefetch(1);
            connectionFactory.setPrefetchPolicy(prefetchPolicy);

            connectionFactory.setRedeliveryPolicy(redeliveryPolicy);
        };
    }


    // @Bean
    // public JmsTemplate jmsTemplate(ConnectionFactory connectionFactory) {
    //     ActiveMQConnectionFactory connetionFactory = null;
    //     if (connectionFactory instanceof ActiveMQConnectionFactory) {
    //         connetionFactory = (ActiveMQConnectionFactory) connectionFactory;
    //     }
    //     else if (connectionFactory instanceof PooledConnectionFactory) {
    //         connetionFactory = (ActiveMQConnectionFactory) ((PooledConnectionFactory) connectionFactory).getConnectionFactory();
    //     }
    //
    //     ActiveMQPrefetchPolicy activeMQPrefetchPolicy = new ActiveMQPrefetchPolicy();
    //     activeMQPrefetchPolicy.setQueuePrefetch(1);
    //     connetionFactory.setPrefetchPolicy(activeMQPrefetchPolicy);
    //     JmsTemplate jmsTemplate = new JmsTemplate();
    //     jmsTemplate.setConnectionFactory(connectionFactory);
    //     // jmsTemplate.setSessionTransacted(true);
    //     return jmsTemplate;
    // }

}
