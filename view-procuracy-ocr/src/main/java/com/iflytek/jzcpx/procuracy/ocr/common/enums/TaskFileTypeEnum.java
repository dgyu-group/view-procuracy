package com.iflytek.jzcpx.procuracy.ocr.common.enums;

/**
 * ocr/图像识别子任务类型
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-14 10:17
 */
public enum TaskFileTypeEnum {
    /** 批量任务 */
    BATCH,

    /** 单文件任务 */
    SINGLE
}

