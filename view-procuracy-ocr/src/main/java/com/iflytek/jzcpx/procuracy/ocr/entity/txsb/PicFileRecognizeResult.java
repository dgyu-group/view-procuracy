package com.iflytek.jzcpx.procuracy.ocr.entity.txsb;

import java.util.List;

import com.iflytek.jzcpx.procuracy.ocr.recognize.model.Wjtz;

import lombok.Data;

@Data
public class PicFileRecognizeResult {
	private String sfkby;
	private Wjlx wjlx;
	private List<Wjtz> wjtz;
}
