package com.iflytek.jzcpx.procuracy.ocr.common.enums;

/**
 * 图像识别批量任务状态
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-14 09:34
 */
public enum RecognizeTaskStatusEnum {

    /** 初始状态 */
    INIT,

    /** 处理中 */
    PROCESSING,

    /** 提交中 */
    SUBMITTING,

    /** 处理完成 */
    DONE,

    /** 推送中 */
    PUSHING,

    /** 任务结束 */
    END
}
