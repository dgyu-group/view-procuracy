package com.iflytek.jzcpx.procuracy.ocr.entity.txsb;

import com.iflytek.jzcpx.procuracy.ocr.common.constant.Constants;

import lombok.Data;

@Data
public class PicFileRecognizeReqVo {
	// 文件序号
	private String wjxh = Constants.WjsbRequiredType.N;
	// MD5值用来获取文件
	private String md5 = Constants.WjsbRequiredType.N;
	// 是否识别印章
	private String sfyz = Constants.WjsbRequiredType.N;
	// 是否识别指纹
	private String sfzw = Constants.WjsbRequiredType.N;
	// 是否手写
	private String sfsx = Constants.WjsbRequiredType.N;
	// 是否识别标题
	private String sfbt = Constants.WjsbRequiredType.N;
	// 是否指纹和印章
	private String sfzwjyz = Constants.WjsbRequiredType.N;
	// 文件内容(base64)
	private String wjnr = Constants.WjsbRequiredType.N;
}
