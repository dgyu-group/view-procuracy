package com.iflytek.jzcpx.procuracy.ocr.common.enums;

/**
 * 图像识别结果推送枚举
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-12 14:36
 */
public enum RecognizeResultPushStatusEnum {
    /** 未准备好 */
    NOT_READY,

    /** 可以推送 */
    READY,

    /** 正在推送 */
    PUSHING,

    /** 任务完成 */
    FINISHED,
    ;

    RecognizeResultPushStatusEnum() {
    }
}