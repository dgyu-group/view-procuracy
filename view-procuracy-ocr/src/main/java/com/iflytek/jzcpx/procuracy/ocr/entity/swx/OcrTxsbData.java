package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import java.util.List;

import lombok.Data;

/**
 * #文件图像识别特征
 */
@Data
public class OcrTxsbData {

    private List<OcrTxsbWjtz> wjtz;

}
