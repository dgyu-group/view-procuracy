package com.iflytek.jzcpx.procuracy.ocr.common.enums;

/**
 * @Author: 刘洪奇
 * @Date: 2019/7/30 17:18
 * @Description: 状态码
 */
public enum StateEnums {

    /**
     * 成功
     */
    SUCCESS("0",1),
    /**
     * 失败
     */
    FAIL("1000",0);

    private final String code;
    private final int desc;

    StateEnums(String code, int desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public int getDesc() {
        return desc;
    }

    public static StateEnums getStateEnums(String code) {
        for (StateEnums t : StateEnums.values()) {
            if (code.equals(t.code)) {
                return t;
            }
        }
        throw new RuntimeException("找不到对应的StateEnums: " + code);
    }
}
