package com.iflytek.jzcpx.procuracy.ocr.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.iflytek.jzcpx.procuracy.common.enums.CommonResultEnum;
import com.iflytek.jzcpx.procuracy.ocr.entity.Metric;

/**
 * 指标统计接口
 * 
 * @author dgyu
 *
 */
public interface MetricService extends IService<Metric> {
  boolean updateByDataId(String dataId,CommonResultEnum commonResultEnum);
}
