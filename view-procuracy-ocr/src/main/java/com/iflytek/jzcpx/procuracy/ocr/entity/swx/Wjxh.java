package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import java.util.List;

import lombok.Data;

/**
 * 文件序号 - 封装ocr识别结果
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019/8/20 9:00
 */
@Data
public class Wjxh {
    /** 文件序号 */
    private String wjxh;

    /** 文件高度 */
    private String wjgd;

    /** 文件宽度 */
    private String wjkd;

    /** 文件倾斜角度 */
    private String wjqxjd;

    /** 文件文本 */
    private String wjwb;

    /** 文件结构信息 */
    private List<Wjjgxx> wjjgxx;

    @Data
    public class Wjjgxx {
        /**  结构化文本信息 */
        private List<Wbxx> wbxx;
    }

    @Data
    public class Wbxx {
        /** 行文本 */
        private String hwb;

        /** 行序号 */
        private int hxh;

        /** 段落序号 */
        private int dlxh;

        /** 行文本坐标 */
        private List<Position> hwbzb;
    }

    @Data
    public class Position {
        private int x;

        private int y;
    }
}
