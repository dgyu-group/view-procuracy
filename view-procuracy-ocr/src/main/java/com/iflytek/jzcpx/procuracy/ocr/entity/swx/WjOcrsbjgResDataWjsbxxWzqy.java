package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 文字区域
 */
@Data
public class WjOcrsbjgResDataWjsbxxWzqy {

    public  WjOcrsbjgResDataWjsbxxWzqy()
    {
        h=new ArrayList<>();
    }
    /**
     * sb，又套了一层
     */
    private List<WjOcrsbjgResDataWjsbxxWzqyH> h;//
}
