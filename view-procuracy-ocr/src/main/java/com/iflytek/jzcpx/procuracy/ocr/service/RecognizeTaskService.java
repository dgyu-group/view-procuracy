package com.iflytek.jzcpx.procuracy.ocr.service;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.iflytek.jzcpx.procuracy.ocr.common.enums.RecognizeTaskStatusEnum;
import com.iflytek.jzcpx.procuracy.ocr.entity.RecognizeTask;

/**
 * 图像识别批量任务接口
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-12 19:25
 */
public interface RecognizeTaskService extends IService<RecognizeTask> {

    /**
     * 创建图像识别批量任务
     *
     * @param systemid 应用和单位的唯一标识
     * @param dwbm     单位编码
     * @param bsbh     标识编号
     * @param taskid   taskid
     *
     * @return 成功保存后的数据库id
     */
    Long create(String systemid, String dwbm, String bsbh, String taskid,String bmsah);

    /**
     * 任务异常结束
     *
     * @param taskId    文件 id
     * @param errorInfo 错误信息
     *
     * @return 更新成功与否
     */
    boolean abnormalEnd(Long taskId, String errorInfo);

    /**
     * 更新任务状态
     *
     * @param taskId           任务 id
     * @param targetStatusEnum 要更新成的状态
     *
     * @return 更新成功与否
     */
    boolean updateStatus(Long taskId, RecognizeTaskStatusEnum targetStatusEnum);

    /**
     * 查询需要推送结果的任务数据
     *
     * @return
     */
    List<Long> findPushableRecognizeTaskId();

    /**
     * 推送识别成功消息
     * @return
     */
    boolean pushSuccessMessage(RecognizeTask recognizeTask);

    /**
     * 更新失败次数
     *
     * @param recognizeTaskId           任务 id
     *
     * @return 更新成功与否
     */
    boolean updateTimes(Long recognizeTaskId);

    int cleanData(Date startDatetime, Date endDatetime);

    List<RecognizeTask> listByBsbh(String bsbh);
}
