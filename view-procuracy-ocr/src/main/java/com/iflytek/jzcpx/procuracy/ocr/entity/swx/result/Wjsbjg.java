package com.iflytek.jzcpx.procuracy.ocr.entity.swx.result;

import java.util.List;

import lombok.Data;

/**
 * 文件OCR识别结果
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2020/1/17 09:32
 */
@Data
public class Wjsbjg {

    /** 文件编号 */
    private String wjbh;

    /** 文件后缀 */
    private String wjhz;

    /** 传入文件的页数，如果为pdf文件时，页数可能是多页 */
    private Integer wjys;

    /** 双层pdf 文件内容（文件字节数组base64） */
    private String pdfnr;

    /** 文本内容 */
    private String txtnr;

    /** 文件MD5, 这个md5值用来校验生成的pdf内容 */
    private String wjmd5;

    /**
     * 文件识别信息（输入文件为多页，输出转化多页信息）
     */
    private List<Wjsbxx> wjsbxx;

}
