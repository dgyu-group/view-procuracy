package com.iflytek.jzcpx.procuracy.ocr.common.helper;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import com.alibaba.fastjson.JSONObject;
import com.iflytek.jzcpx.procuracy.common.result.Result;
import com.iflytek.jzcpx.procuracy.common.util.JSONUtil;
import com.iflytek.jzcpx.procuracy.ocr.common.enums.OcrResultEnum;
import com.iflytek.jzcpx.procuracy.ocr.entity.swx.JzInfoResult;
import com.iflytek.jzcpx.procuracy.ocr.entity.swx.JzmlInfo;
import com.iflytek.jzcpx.procuracy.tools.common.PropUtils;
import com.iflytek.jzcpx.procuracy.tools.common.enums.ToolsContstants;
import com.iflytek.jzcpx.procuracy.tools.csb.CsbUtil;
import com.iflytek.jzcpx.procuracy.tools.roma.RomaUtil;
import lombok.Getter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

/**
 * 赛威讯接口请求工具类
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-13 14:36
 */
public class SwxRequestHelper {
    private static final Logger logger = LoggerFactory.getLogger(SwxRequestHelper.class);

    private static final int REQUEST_TIMEOUT = 30000;

    @Value("${csb.enabled}")
    private boolean isCsb;
    
    @Value("${roma.enabled}")
    private boolean isHwApi;

    @Getter
    private String swxServerUrl;

    @Value("${swx.server.ocr.dzjz.un_ocr.api}")
    private String unOcrFilesApi;

    /**
     * 从电子卷宗系统获取图片文件
     */
    @Value("${swx.server.ocr.dzjz.jpg.api}")
    private String ocrFileJpgApi;

    /**
     * 请求电子卷宗JPG文件
     */
    @Value("${swx.server.ocr.dzjz.jpg.wj.api}")
    private String ocrFileJpgWjApi;

    @Value("${swx.server.cont.jzmlwj.jpg.api}")
    private String contFileJpgApi;

    @Value("${swx.server.ocr.dzjz.savepdf.api}")
    private String saveOcrPdfApi;

    @Value("${swx.server.ocr.dzjz.saveOcrStatus.api}")
    private String saveOCRResultStatusApi;

    @Value("${swx.server.cont.jzmlwj.api}")
    private String swx_dzmlwjUrl;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private CsbUtil csbUtil;
    
    @Autowired
    private RomaUtil romaUtil;

    public SwxRequestHelper(String serverUrl) {
        swxServerUrl = serverUrl;
    }

    /**
     * 获取电子卷宗待识别文件信息接口地址
     *
     * @param bsbh   标识编号
     * @param taskid 任务编号
     * @return
     */
    public Result<JzInfoResult> getOCRFileInfo(String bsbh, String taskid) {
        Map<String, Object> params = new HashMap<>();
        params.put("bsbh", bsbh);
        params.put("taskid", taskid);

        String resp = null;
        String url = swxServerUrl + unOcrFilesApi;

        String bodyStr= JSONUtil.toStrDefault(params);
        logger.debug("获取电子卷宗待识别文件信息访问地址：{}，输入参数： {}",url, bodyStr);
        long start = System.currentTimeMillis();
        try {
            // resp = HttpUtil.post(url, params, REQUEST_TIMEOUT);
            if (isCsb) {
                logger.debug("获取电子卷宗待识别文件信息接口走CSB中台");
                resp = csbUtil.doPostCsbApiNameOcrDzjzUn_ocr(bodyStr);
            }else if(isHwApi) {
                logger.debug("获取电子卷宗待识别文件信息接口走华为中台");
            	resp = romaUtil.doPostRomaApiNameOcrDzjzUn_ocr(bodyStr);
            } else {
                logger.debug("获取电子卷宗待识别文件信息接口走HTTP直调");
                resp = HttpRequest.post(url).body(bodyStr)
                        .header("Content-Type", "application/json; charset=UTF-8")
                        .timeout(ToolsContstants.ENGINE_REQUEST_TIMEEOUT).execute().body();
            }
        } catch (Exception e) {
            logger.warn("请求[获取电子卷宗待识别文件信息接口]异常, url: {}", url, e);
            if (!PropUtils.getBool("swx_request_debug", false)) {
                return Result.failed(OcrResultEnum.GETOCRFILEINFO_REQUEST_EXCEPTION, JzInfoResult.class);
            }
        } finally {
            logger.info("获取电子卷宗待识别文件信息接口耗时: {}ms", System.currentTimeMillis() - start);
            logger.debug("获取电子卷宗待识别文件信息接口返回结果：{}",resp);
        }

        if (PropUtils.getBool("swx_request_debug", false)) {
            logger.info(">>>>> 调试 <<<<<<");
            try {
                resp = FileUtils.readFileToString(
                        new File(PropUtils.getProp("swx_request_debug_path", "/") + "/getOCRFileInfo.json"));
            }
            catch (IOException e) {
                //
            }
        }

        if (StringUtils.isBlank(resp)) {
            return Result.failed(OcrResultEnum.GETOCRFILEINFO_REQUEST_FAILED, "获取电子卷宗待识别文件信息接口无响应内容",
                    JzInfoResult.class);
        }

        try {
            return parseSwxResponse(resp);
        } catch (IOException e) {
            logger.warn("解析请求[获取电子卷宗待识别文件信息接口]返回值异常, url: {}", url, e);
            return Result.failed(OcrResultEnum.GETOCRFILEINFO_PARSE_EXCEPTION, JzInfoResult.class);
        }
    }

    /**
     * 获取卷宗目录(图像识别)
     *
     * @param bsbh
     * @return
     */
    public Result<JzmlInfo> getDZJZInfo(String bsbh) {

        JSONObject object = new JSONObject();
        object.put("bsbh", bsbh);
        String url = swxServerUrl + swx_dzmlwjUrl;
        String result = "";
        long start = System.currentTimeMillis();
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("Accept", "*/*");
            headers.add("systemid", "");
            headers.add("dzjz_token", "");
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> formEntity = new HttpEntity<String>(object.toString(), headers);

            String  bodyStr= object.toString();
            logger.debug("获取电子卷宗目录文件接口访问地址：{}，输入参数： {}", url, bodyStr);

            if (isCsb) {
                logger.debug("获取电子卷宗扫描文件接口走CSB中台");
                result = csbUtil.doPostCsbApiNameContJzmlwj(bodyStr);
            }else if(isHwApi) {
                logger.debug("获取电子卷宗扫描文件接口走华为中台");
            	result = romaUtil.doPostRomaApiNameContJzmlwj(bodyStr);
            } else {
                logger.debug("获取电子卷宗扫描文件接口走HTTP直调");
                result = restTemplate.postForObject(url, formEntity, String.class);
            }
        } catch (Exception ex) {
            logger.warn("获取电子卷宗目录文件接口异常", ex);
            if (!PropUtils.getBool("swx_request_debug", false)) {
                return Result.failed(OcrResultEnum.REQUEST_FAILED, "获取电子卷宗目录文件接口请求异常");
            }
        }finally {
            logger.info("获取电子卷宗目录文件接口耗时: {}ms", System.currentTimeMillis() - start);
            logger.info("获取电子卷宗目录文件接口响应内容: {}", result);
        }

        if (PropUtils.getBool("swx_request_debug", false)) {
            logger.info(">>>>> 调试 <<<<<<");
            try {
                result = FileUtils.readFileToString(
                        new File(PropUtils.getProp("swx_request_debug_path", "/") + "/getDzjzMlwj.json"));
            }
            catch (IOException e) {
                //
            }
        }

        if (StringUtils.isEmpty(result)) {
            logger.warn("获取电子卷宗目录文件接口返回结果为空");
            return Result.failed(OcrResultEnum.REQUEST_FAILED, "获取电子卷宗目录文件接口返回结果为空");
        }

        JzmlInfo jzmlInfo = null;
        try {
            jzmlInfo = JSONUtil.toBean(result, JzmlInfo.class);
        }
        catch (Exception e) {
            logger.warn("获取电子卷宗目录文件接口响应内容转换异常", e);
            return Result.failed(OcrResultEnum.REQUEST_FAILED, "获取电子卷宗目录文件接口响应内容转换异常");
        }
        if (jzmlInfo == null || !jzmlInfo.isSuccess()) {
            logger.warn("获取电子卷宗目录文件接口返回失败响应, 内容: {}", result);
            String msg = "获取电子卷宗目录文件接口请求失败" + jzmlInfo == null ? "" : (jzmlInfo.getCode() + jzmlInfo.getMessage());
            return Result.failed(OcrResultEnum.REQUEST_FAILED, msg);
        }

        return Result.success(jzmlInfo);
    }


    /**
     * 请求电子卷宗JPG文件
     * @param wjxh
     * @return
     */
    public Result<DzjzWjDTO> ocrFileJpgWjApi(String wjxh) {
        Map<String, Object> params = new HashMap<>();
        params.put("wjxh", wjxh);

        String url = swxServerUrl + ocrFileJpgWjApi;
        String bodyStr= JSONUtil.toStrDefault(params);
        byte[] respBytes = null;
        long start = System.currentTimeMillis();
        try {
            logger.info("获取电子卷宗扫描文件接口访问地址：{}，输入参数： {}", url, bodyStr);
            if (isCsb) {
                logger.debug("获取电子卷宗扫描文件接口走CSB中台");
                respBytes = csbUtil.doPostCsbApiNameOcrDzjzJpg(bodyStr).getBytes();
            }
            else if (isHwApi) {
                logger.debug("获取电子卷宗扫描文件接口走华为中台");
                respBytes = romaUtil.doPostRomaApiNameOcrDzjzJpg(bodyStr).getBytes();
            }
            else {
                logger.debug("获取电子卷宗扫描文件接口走HTTP直调");
                respBytes = HttpRequest.post(url).body(bodyStr).header("Content-Type",
                                                                       "application/json; charset=UTF-8").timeout(
                        ToolsContstants.ENGINE_REQUEST_TIMEEOUT).execute().bodyBytes();
            }
        }
        catch (Exception e) {
            logger.warn("请求[电子卷宗JPG文件]接口异常. url: {}", url, e);
            return Result.failed(OcrResultEnum.GETDZJZJPG_REQUEST_EXCEPTION);
        }
        finally {
            logger.info("获取电子卷宗扫描文件接口耗时: {}ms", System.currentTimeMillis() - start);
        }

        if (ArrayUtils.isEmpty(respBytes)) {
            logger.warn("获取电子卷宗扫描文件接口返回结果为空, 输入参数: {}", bodyStr);
            return Result.failed(OcrResultEnum.GETDZJZJPG_REQUEST_FAILED, "获取电子卷宗扫描文件接口无响应内容");
        }

        DzjzWjDTO dataDTO = new DzjzWjDTO(respBytes);
        logger.info("获取电子卷宗扫描文件（jpg格式） 结果：{}", dataDTO);

        if (dataDTO.isSuccess()) {
            if (ArrayUtils.isEmpty(dataDTO.getData())) {
                logger.warn("获取电子卷宗扫描文件接口返回成功, 但是图片数据为空，输入参数： {}", bodyStr);
                return Result.failed(OcrResultEnum.GETDZJZJPG_REQUEST_FAILED, "获取电子卷宗扫描文件接口无图片数据");
            }
            return Result.success(dataDTO);
        }
        else {
            logger.warn("获取电子卷宗扫描文件接口返回失败响应, 输入参数： {}, 结果：{}", bodyStr, dataDTO);
            return buildDzjjFailedResult(dataDTO, OcrResultEnum.GETDZJZJPG_REQUEST_FAILED);
        }
    }


    /**
     * 获取电子卷宗扫描文件(单个JPG)
     *
     * @param bsbh   标识编号
     * @param taskid 任务编号
     * @param wjxh   文件序号
     * @return
     */
    public Result<DzjzDataDTO> getDZJZJPG(String bsbh, String taskid, String wjxh) {
        Map<String, Object> params = new HashMap<>();
        params.put("bsbh", bsbh);
        params.put("taskid", taskid);
        params.put("wjxh", wjxh);

        //String url = swxServerUrl + "/api/ajDzjz/GetDZJZJPG";
        String url = swxServerUrl + ocrFileJpgApi;
        String bodyStr= JSONUtil.toStrDefault(params);
        byte[] respBytes = null;
        long start = System.currentTimeMillis();
        try {
            logger.info("获取电子卷宗图片接口访问地址：{}，输入参数： {}", url, bodyStr);
            if (isCsb) {
                logger.debug("获取电子卷宗图片接口走CSB状态");
                //此处可能有坑，获取字节数据
                respBytes = csbUtil.doPostCsbApiNameOcrDzjzJpg(bodyStr).getBytes();
            }else if(isHwApi) {
                logger.debug("获取电子卷宗图片接口走华为中台");
            	respBytes = romaUtil.doPostRomaApiNameOcrDzjzJpg(bodyStr).getBytes();
            }  else {
                logger.debug("获取电子卷宗图片接口走HTTP直调");
                respBytes = HttpRequest.post(url).body(bodyStr)
                        .header("Content-Type", "application/json; charset=UTF-8")
                        .timeout(ToolsContstants.ENGINE_REQUEST_TIMEEOUT).execute().bodyBytes();
            }
        } catch (Exception e) {
            logger.warn("请求[获取电子卷宗图片]接口异常. url: {}", url, e);
            if (!PropUtils.getBool("swx_request_debug", false)) {
                return Result.failed(OcrResultEnum.GETDZJZJPG_REQUEST_EXCEPTION, e.getMessage());
            }
        }
        finally {
            logger.info("获取电子卷宗扫描文件接口耗时: {}ms, 数据大小: {} bytes", System.currentTimeMillis() - start,
                        respBytes == null ? 0 : respBytes.length);
        }

        if (PropUtils.getBool("swx_request_debug", false)) {
            logger.info(">>>>> 调试 <<<<<<");
            try {
                respBytes = FileUtils.readFileToByteArray(
                        new File(PropUtils.getProp("swx_request_debug_path", "/") + "/getDzjzJpg.bin"));
            }
            catch (IOException e) {
                //
            }
        }

        if (ArrayUtils.isEmpty(respBytes)) {
            logger.warn("获取电子卷宗图片接口返回结果为空, 输入参数: {}", bodyStr);
            return Result.failed(OcrResultEnum.GETDZJZJPG_REQUEST_FAILED, "获取电子卷宗图片接口无响应内容");
        }

        DzjzDataDTO dataDTO = new DzjzDataDTO(respBytes);
        logger.info("获取电子卷宗图片结果：{}", dataDTO);

        if (dataDTO.isSuccess()) {
            if (ArrayUtils.isEmpty(dataDTO.getData())) {
                logger.warn("获取电子卷宗图片接口返回成功, 但是图片数据为空，输入参数： {}", bodyStr);
                return Result.failed(OcrResultEnum.GETDZJZJPG_REQUEST_FAILED, "获取电子卷宗图片接口无图片数据");
            }
            return Result.success(dataDTO);
        } else {
            logger.warn("获取电子卷宗图片接口返回失败响应, 输入参数： {}, 结果：{}", bodyStr, dataDTO);
            return buildDzjjFailedResult(dataDTO, OcrResultEnum.GETDZJZJPG_REQUEST_FAILED);
        }
    }
    
    /**
     * 请求电子卷宗扫描文件(1.2.5 接口4-1 请求电子卷宗JPG文件api-04)
     *
     * @param bsbh   标识编号
     * @param wjxh   文件序号
     * @return
     */
    public Result<DzjzWjDTO> getDZJZWJPG(String bsbh, String wjxh) {
        Map<String, Object> params = new HashMap<>();
        params.put("bsbh", bsbh);
        params.put("wjxh", wjxh);

        //String url = swxServerUrl + "/api/ajDzjz/GetDZJZJPG";
        String url = swxServerUrl + contFileJpgApi;
        byte[] respBytes = null;
        try {

            String bodyStr= JSONUtil.toStrDefault(params);
            logger.debug("1.2.5.接口4-1：请求电子卷宗JPG文件（api-04）接口访问地址：{}，输入参数： {}", url, bodyStr);
            if (isCsb) {
                //此处可能有坑，获取字节数据
                respBytes = csbUtil.doPostCsbApiNameOcrDzjzJpg(bodyStr).getBytes();
            }else if(isHwApi) {
            	respBytes = romaUtil.doPostRomaApiNameOcrDzjzJpg(bodyStr).getBytes();
            }  else {
                respBytes = HttpRequest.post(url).body(bodyStr)
                        .header("Content-Type", "application/json; charset=UTF-8")
                        .timeout(ToolsContstants.ENGINE_REQUEST_TIMEEOUT).execute().bodyBytes();
            }

            if (respBytes == null || respBytes.length <= 500) {
                String errorStr = new String(respBytes, "UTF-8");
                logger.error("1.2.5.接口4-1：请求电子卷宗JPG文件（api-04）接口报错信息：" + errorStr);
            }

            if (ArrayUtils.isEmpty(respBytes)) {
                return Result.failed(OcrResultEnum.GETDZJZJPG_REQUEST_FAILED, "1.2.5.接口4-1：请求电子卷宗JPG文件（api-04）接口无响应内容");
            }
        } catch (Exception e) {
            logger.warn("请求[1.2.5.接口4-1：请求电子卷宗JPG文件（api-04）接口]接口异常. url: {}", url, e);
            return Result.failed(OcrResultEnum.GETDZJZJPG_REQUEST_EXCEPTION);
        }

        DzjzWjDTO dzjzWjDTO = new DzjzWjDTO(respBytes);
        logger.info("1.2.5.接口4-1：请求电子卷宗JPG文件（api-04）接口 结果：" +dzjzWjDTO.printMessage());

        if (dzjzWjDTO.isSuccess()) {
            return Result.success(dzjzWjDTO);
        } else {
            return buildDzjjFailedResult(dzjzWjDTO, OcrResultEnum.GETDZJZJPG_REQUEST_FAILED);
        }
    }

    /**
     * 提交OCR识别文件（双层pdf）
     *
     * @param dataDTO 数据对象
     * @return
     */
    public Result savePDF(DzjzDataDTO dataDTO) {
        String url = swxServerUrl + "/api/ajDzjz/SavePDF";
        url = swxServerUrl + saveOcrPdfApi;
        //url = "http://192.168.2.151:8111/api/ajDzjz/SavePdf";
        byte[] respBytes = null;

        long start = System.currentTimeMillis();
        try {
            String bodyStr= dataDTO.getParams();
            logger.debug("提交OCR识别结果PDF文件访问地址：{}，输入参数： {}", url, bodyStr);

            if (isCsb) {
                logger.debug("获取电子卷宗图片接口走CSB状态");
                //字节数组作为参数，可能有坑，注意
                respBytes = csbUtil.doPostCsbApiNameOcrDzjzSavepdf(dataDTO.getDatas().toString()).getBytes();
            } else if(isHwApi) {
                logger.debug("获取电子卷宗图片接口走华为中台");
            	respBytes = romaUtil.doPostRomaApiNameOcrDzjzSavepdf(dataDTO.getDatas().toString()).getBytes();
            }else {
                byte[] sendBodyBytes = dataDTO.getDatas();
                logger.debug("获取电子卷宗图片接口走HTTP直调, byteSize: {}", sendBodyBytes == null ? 0: sendBodyBytes.length);
                respBytes = restTemplate.postForObject(url, sendBodyBytes, byte[].class);
            }
        } catch (Exception e) {
            logger.warn("请求[提交OCR识别结果PDF文件]接口异常. url: {}", url, e);
            if (!PropUtils.getBool("swx_request_debug", false)) {
                return Result.failed(OcrResultEnum.SAVEPDF_REQUEST_EXCEPTION);
            }
        }
        finally {
            logger.info("提交OCR识别结果PDF文件接口耗时: {}ms", System.currentTimeMillis() - start);
        }

        if (PropUtils.getBool("swx_request_debug", false)) {
            logger.info(">>>>> 调试 <<<<<<");
            try {
                respBytes = FileUtils.readFileToByteArray(
                        new File(PropUtils.getProp("swx_request_debug_path", "/") + "/savePdf.bin"));
            }
            catch (IOException e1) {
                //
            }
        }

        if (ArrayUtils.isEmpty(respBytes)) {
            return Result.failed(OcrResultEnum.SAVEPDF_REQUEST_FAILED, "提交OCR识别结果PDF文件接口无响应内容");
        }

        DzjzDataDTO resp = new DzjzDataDTO(respBytes);
        logger.debug("提交OCR识别结果PDF文件接口返回结果： {}", resp.getMessage());
        if (resp.isSuccess()) {
            return Result.success();
        } else {
            return buildDzjjFailedResult(resp, OcrResultEnum.SAVEPDF_REQUEST_FAILED);
        }
    }

    /**
     * 构造电子卷宗接口失败响应
     */
    private Result buildDzjjFailedResult(DzjzDataDTO resp, OcrResultEnum savepdfRequestFailed) {
        Result<byte[]> result = Result.failed(savepdfRequestFailed);
        Map<String, Object> errorDetail = result.getError().getData();
        errorDetail.put("code", resp.getCode());
        errorDetail.put("message", resp.getMessage());
        errorDetail.put("params", resp.getParams());
        return result;
    }
    
    /**
     * 构造电子卷宗接口失败响应
     */
    private Result buildDzjjFailedResult(DzjzWjDTO resp, OcrResultEnum savepdfRequestFailed) {
        Result<byte[]> result = Result.failed(savepdfRequestFailed);
        Map<String, Object> errorDetail = result.getError().getData();
        errorDetail.put("code", resp.getCode());
        errorDetail.put("message", resp.getMessage());
        return result;
    }

    /**
     * 提交OCR识别状态接口
     *
     * @param jzInfoResult 卷宗信息
     * @return
     */
    public Result saveOCRResult(JzInfoResult jzInfoResult) {
        String url = swxServerUrl + "/api/ajDzjz/SaveOCRResult";
        url = swxServerUrl + saveOCRResultStatusApi;
        String resp = null;

        long start = System.currentTimeMillis();
        try {
            String bodyStr= JSONUtil.toStrDefault(jzInfoResult);
            logger.debug("提交OCR识别状态接口访问地址：{}，输入参数： {}", url, bodyStr);

            if (isCsb) {
                logger.debug("获取电子卷宗图片接口走CSB状态");
                resp = csbUtil.doPostCsbApiNameOcrDzjzSaveOcrStatus(bodyStr);
            }else if(isHwApi) {
                logger.debug("获取电子卷宗图片接口走华为中台");
            	resp = romaUtil.doPostRomaApiNameOcrDzjzSaveOcrStatus(bodyStr);
            } else {
                logger.debug("获取电子卷宗图片接口走HTTP直调");
                HttpRequest request = HttpRequest.post(url).body(bodyStr)
                                                 .header("Content-Type", "application/json; charset=UTF-8")
                                                 .timeout(30000);
                try (HttpResponse response = request.execute()) {
                    resp = response.body();
                }
            }
        } catch (Exception e) {
            logger.warn("提交OCR识别状态接口异常, url: {}", url, e);
            if (!PropUtils.getBool("swx_request_debug", false)) {
                return Result.failed("提交OCR识别状态接口异常");
            }
        } finally {
            logger.info("提交OCR识别状态接口返回结果：{}", resp);
            logger.info("提交OCR识别状态接口耗时: {}ms", System.currentTimeMillis() - start);
        }

        if (PropUtils.getBool("swx_request_debug", false)) {
            logger.info(">>>>> 调试 <<<<<<");
            try {
                resp = FileUtils.readFileToString(
                        new File(PropUtils.getProp("swx_request_debug_path", "/") + "/saveOCRResult.json"));
            }
            catch (IOException e) {
                //
            }
        }

        if (StringUtils.isBlank(resp)) {
            return Result.failed("提交OCR识别状态接口无响应");
        }

        try {
            return parseSwxResponse(resp);
        } catch (IOException e) {
            logger.warn("解析请求[提交OCR识别状态接口异常]返回值异常, url: {}", url, e);
            return Result.failed(OcrResultEnum.GETOCRFILEINFO_PARSE_EXCEPTION);
        }
    }

    private Result parseSwxResponse(String resp) throws IOException {
        SwxResponse swxResponse = JSONUtil.toBean(resp, SwxResponse.class);
        if (swxResponse.isSuccess()) {
            return Result.success(swxResponse.getData());
        } else {
            Result result = Result.failed(OcrResultEnum.GETOCRFILEINFO_REQUEST_FAILED);
            Map<String, Object> errorDetail = result.getError().getData();
            errorDetail.put("code", swxResponse.getCode());
            errorDetail.put("message", swxResponse.getMessage());
            return result;
        }
    }
}
