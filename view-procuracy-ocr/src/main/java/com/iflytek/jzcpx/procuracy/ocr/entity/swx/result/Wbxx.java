package com.iflytek.jzcpx.procuracy.ocr.entity.swx.result;

import java.util.List;

import lombok.Data;

/**
 * 结构化文本信息
 */
@Data
public class Wbxx {

    /** 行文本 */
    private String hwb;

    /** 行序号 */
    private Integer hxh;

    /** 段落序号 */
    private Integer dlxh;

    /** 分栏序号 */
    private Integer flxh;

    /** 行文本坐标, 左上 右上 右下 左下 */
    private List<Wbzb> hwbzb;

    /** 行字符信息, 按行内字符顺序组成有序数组 */
    private List<Zfxx> zfxx;
}
