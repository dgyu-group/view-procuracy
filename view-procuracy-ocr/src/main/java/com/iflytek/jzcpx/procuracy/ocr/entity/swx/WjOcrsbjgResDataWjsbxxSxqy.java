package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 手写区域
 */
@Data
public class WjOcrsbjgResDataWjsbxxSxqy {

    public WjOcrsbjgResDataWjsbxxSxqy()
    {
        zf=new ArrayList<>();
    }

    /**
     * 1731,2637,1842,2742
     */
    private String zbwz;//坐标位置
    private List<String> zf;//字符
}
