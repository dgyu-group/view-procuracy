package com.iflytek.jzcpx.procuracy.ocr.common.enums;

import com.iflytek.jzcpx.procuracy.common.enums.ResultType;

/**
 * 图文识别接口结果枚举, 11xxx
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-07 09:15
 */
public enum OcrResultEnum implements ResultType {
    REQUEST_FAILED(11000, "请求异常"),
    OCR_RECOGNIZE_EXCEPTION(11001, "ocr识别异常"),
    OCR_RECOGNIZE_FAILED(11002, "ocr识别异常"),
    UPDATE_ENGINE_RESULT_FAILED(11003, "上传 ocr 引擎识别结果失败"),
    COMPOSE_PDF_EXCEPTION(11004, "合成PDF文件异常"),

    GETOCRFILEINFO_REQUEST_EXCEPTION(11101, "获取电子卷宗待识别文件信息接口请求异常"),
    GETOCRFILEINFO_REQUEST_FAILED(11102, "获取电子卷宗待识别文件信息接口请求失败"),
    GETOCRFILEINFO_PARSE_EXCEPTION(11103, "获取电子卷宗待识别文件信息接口结果解析异常"),

    GETDZJZJPG_REQUEST_EXCEPTION(11111, "获取电子卷宗扫描文件接口请求异常"),
    GETDZJZJPG_REQUEST_FAILED(11112, "获取电子卷宗扫描文件接口请求异常"),

    SAVEPDF_REQUEST_EXCEPTION(11121, "提交OCR识别文件接口请求异常"),
    SAVEPDF_REQUEST_FAILED(11121, "提交OCR识别文件接口请求失败"),



    ;

    private Integer code;
    private String desc;

    OcrResultEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static OcrResultEnum getByCode(Integer code) {
        if (code == null) {
            return null;
        }

        for (OcrResultEnum anEnum : values()) {
            if (anEnum.getCode().equals(code)) {
                return anEnum;
            }
        }

        return null;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getDesc() {
        return desc;
    }}