package com.iflytek.jzcpx.procuracy.ocr.common.enums;

/**
 * 图像识别状态枚举
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-12 14:36
 */
public enum RecognizeFileStatusEnum {
    /** 初始状态 */
    INIT,

    /** 获取到文件内容 */
    GOT_FILE_CONTENT,

    /** 请求ocr引擎结束 */
    REQUEST_OCR_ENGINE,

    /** ocr引擎结果存储完成 */
    ENGINE_RESULT_STORE,

    /** 推送给上游完成 */
    PUSH_TO_UPSTREAM,

    /** 任务结束 */
    END,
    ;

    RecognizeFileStatusEnum() {
    }
}