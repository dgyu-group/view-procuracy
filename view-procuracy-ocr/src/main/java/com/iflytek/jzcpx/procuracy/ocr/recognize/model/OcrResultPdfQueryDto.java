package com.iflytek.jzcpx.procuracy.ocr.recognize.model;

/**
 *
 */
public class OcrResultPdfQueryDto {
    private String wjbh;//文件编号
    private String wjhz;//文件后缀
    private String wjnr;//文件内容（文件字节数组base64）
    private boolean sfwjnr;//是否输出文件内容（双层pdf）
    private boolean sfwjwb;//是否输出文件文本
    private boolean sfwjxx;//是否输出文件结构化信息（带坐标）

    public boolean isSfwjnr() {
        return sfwjnr;
    }

    public void setSfwjwb(boolean sfwjwb) {
        this.sfwjwb = sfwjwb;
    }

    public boolean isSfwjwb() {
        return sfwjwb;
    }

    public boolean isSfwjxx() {
        return sfwjxx;
    }

    public String getWjbh() {
        return wjbh;
    }

    public String getWjhz() {
        return wjhz;
    }

    public String getWjnr() {
        return wjnr;
    }

    public void setSfwjnr(boolean sfwjnr) {
        this.sfwjnr = sfwjnr;
    }

    public void setSfwjxx(boolean sfwjxx) {
        this.sfwjxx = sfwjxx;
    }

    public void setWjbh(String wjbh) {
        this.wjbh = wjbh;
    }

    public void setWjhz(String wjhz) {
        this.wjhz = wjhz;
    }

    public void setWjnr(String wjnr) {
        this.wjnr = wjnr;
    }
}
