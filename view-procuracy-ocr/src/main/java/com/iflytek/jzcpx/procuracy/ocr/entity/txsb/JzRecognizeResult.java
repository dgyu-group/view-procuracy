/**
  * Copyright 2020 bejson.com 
  */
package com.iflytek.jzcpx.procuracy.ocr.entity.txsb;
import java.util.List;

/**
 * Auto-generated: 2020-08-10 15:13:42
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class JzRecognizeResult {

    private String bmsah;
    private List<Jzmlwj> jzmlwj;
    public void setBmsah(String bmsah) {
         this.bmsah = bmsah;
     }
     public String getBmsah() {
         return bmsah;
     }

    public void setJzmlwj(List<Jzmlwj> jzmlwj) {
         this.jzmlwj = jzmlwj;
     }
     public List<Jzmlwj> getJzmlwj() {
         return jzmlwj;
     }

}