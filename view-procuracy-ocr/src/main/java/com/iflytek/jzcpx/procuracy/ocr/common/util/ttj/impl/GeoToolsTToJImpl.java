package com.iflytek.jzcpx.procuracy.ocr.common.util.ttj.impl;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.iflytek.jzcpx.procuracy.ocr.common.util.ttj.TToJ;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class GeoToolsTToJImpl implements TToJ {
    private static final Logger LOGGER = LoggerFactory.getLogger(GeoToolsTToJImpl.class);

    private static final String GEO_TOOLS_WORK_DIR = String.format("%s", new Object[] { System.getProperty("user.dir") });

    private ThreadFactory threadFactory = (new ThreadFactoryBuilder()).setNameFormat("gt-ttoj-Pool-%d").build();

    private ExecutorService gtTToJPool = new ThreadPoolExecutor(5, 200, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingDeque<>(1024), this.threadFactory, new ThreadPoolExecutor.AbortPolicy());


    public List<byte[]> convertTiffToJpg(File tiffFile) throws Exception { return null; }

    public List<byte[]> convertTiffToJpg(InputStream ins) throws Exception { return null; }

    public static boolean writeBaToFile(byte[] fileBa, File file) {
        boolean ret = true;
        try (OutputStream os = new FileOutputStream(file)) {
            os.write(fileBa);
        } catch (IOException e) {
            ret = false;
        }
        return ret;
    }
}
