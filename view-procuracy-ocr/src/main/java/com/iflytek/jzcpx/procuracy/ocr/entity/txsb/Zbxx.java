/**
  * Copyright 2020 bejson.com 
  */
package com.iflytek.jzcpx.procuracy.ocr.entity.txsb;

/**
 * Auto-generated: 2020-08-10 15:13:42
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Zbxx {

    private int x;
    private int y;
    private int w;
    private int h;
    private String zxd;
    public void setX(int x) {
         this.x = x;
     }
     public int getX() {
         return x;
     }

    public void setY(int y) {
         this.y = y;
     }
     public int getY() {
         return y;
     }

    public void setW(int w) {
         this.w = w;
     }
     public int getW() {
         return w;
     }

    public void setH(int h) {
         this.h = h;
     }
     public int getH() {
         return h;
     }

    public void setZxd(String zxd) {
         this.zxd = zxd;
     }
     public String getZxd() {
         return zxd;
     }

}