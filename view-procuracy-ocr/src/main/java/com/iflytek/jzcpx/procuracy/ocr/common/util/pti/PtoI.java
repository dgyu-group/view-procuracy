package com.iflytek.jzcpx.procuracy.ocr.common.util.pti;

import java.util.List;

public interface PtoI {
    List<byte[]> pdfToImages(byte[] paramArrayOfbyte) throws Exception;
}
