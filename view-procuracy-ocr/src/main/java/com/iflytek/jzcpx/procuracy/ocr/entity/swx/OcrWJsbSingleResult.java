package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *
 */
@JsonIgnoreProperties({"fileId"})
public class OcrWJsbSingleResult {

    private String wjbh;//: "24E6C7DB70E441888CA0E6AD40850157",  #文件编号
    private String wjhz;//: ".pdf", #文件后缀

    private List<Wjsbxx> wjsbxx;
    
    /**
     * fileId 指标收集用
     */
    private Long fileId;

    public void setWjhz(String wjhz) {
        this.wjhz = wjhz;
    }

    public void setWjbh(String wjbh) {
        this.wjbh = wjbh;
    }

    public String getWjhz() {
        return wjhz;
    }

    public String getWjbh() {
        return wjbh;
    }

    public List<Wjsbxx> getWjsbxx() {
        return wjsbxx;
    }

    public void setWjsbxx(List<Wjsbxx> wjsbxx) {
        this.wjsbxx = wjsbxx;
    }

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}
}
