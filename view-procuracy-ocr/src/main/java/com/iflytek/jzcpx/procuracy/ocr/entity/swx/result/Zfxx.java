package com.iflytek.jzcpx.procuracy.ocr.entity.swx.result;

import lombok.Data;

/**
 * 字符信息
 */
@Data
public class Zfxx {

    /** 字 */
    private String z;

    /** 高 */
    private Integer g;

    /** 宽 */
    private Integer k;

    /** 左上横坐标 */
    private Integer x;

    /** 左上纵坐标 */
    private Integer y;
}
