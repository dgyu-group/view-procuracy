package com.iflytek.jzcpx.procuracy.ocr.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 自动编目配置
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-07 09:48
 */
@ConfigurationProperties(prefix = "skynet.cont")
@Data
public class ContServerProps {

    private String server;

    private String path;

}
