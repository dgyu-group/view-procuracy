package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import lombok.Data;
import java.util.ArrayList;
import java.util.List;

@Data
public class WjOcrsbjgResDataWjsbxx {

    public WjOcrsbjgResDataWjsbxx()
    {
        wzqy=new ArrayList<>();
        bgqy=new ArrayList<>();
        sxqy=new ArrayList<>();
    }

    private String wjnr;//文件内容
    private String wjwb;//文件文本
    private int wjgd;//文件高度
    private int wjkd;//文件宽度
    private int wjqxjd;//文件倾斜角度
    /**
     * 与文件倾斜角有啥区别不？？？
     */
    private int wjpzj;//文件偏转角

    /**
     * "1" 值为字符1时，表示要做压缩识别
     */
    private String yssb;//#图像超过长边2500px，要做压缩识别

    private List<WjOcrsbjgResDataWjsbxxWzqy> wzqy;//文字区域

    private List<WjOcrsbjgResDataWjsbxxBgqy> bgqy;//表格区域

    /**
     * 手写区域
     */
     private List<WjOcrsbjgResDataWjsbxxSxqy> sxqy;
}
