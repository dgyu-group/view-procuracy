package com.iflytek.jzcpx.procuracy.ocr.service;

import java.util.List;

import com.iflytek.jzcpx.procuracy.ocr.entity.swx.*;
import com.alibaba.fastjson.JSONObject;
import com.iflytek.jzcpx.procuracy.common.result.Result;
import com.iflytek.jzcpx.procuracy.common.result.WebResult;
import com.iflytek.jzcpx.procuracy.ocr.common.enums.RecognizeFuncEnum;
import com.iflytek.jzcpx.procuracy.ocr.entity.swx.result.Wjsbjg;
import com.iflytek.jzcpx.procuracy.ocr.entity.txsb.JzRecognizeResult;
import com.iflytek.jzcpx.procuracy.ocr.recognize.model.OcrResultQueryDto;
import com.iflytek.jzcpx.procuracy.ocr.recognize.model.OcrResultRequestVo;
import com.iflytek.jzcpx.procuracy.ocr.recognize.model.Wjtz;

/**
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-06 19:49
 */
public interface OcrService {

	/**
	 *
	 * @param bmsah
	 * @param param
	 * @return
	 */
	OcrWJsbMultiResult findOcrResult(String bmsah, OcrResultQueryDto param);

	/**
	 *
	 * @param fileBytes
	 * @param wjbh
	 * @param wjhz
	 * @return
	 */
	Result<OcrWJsbSingleResult> ocrToPdfDetail(byte[] fileBytes, String wjbh, String wjhz);

	/**
	 * 提交批量图文识别任务
	 *
	 * @param batchOcrVO
	 *            识别请求参数
	 *
	 * @return
	 */
	Result submitBatchOcrToPdf(BatchOcrVO batchOcrVO,JSONObject obj);

	/**
	 * 提交批量图像识别任务
	 *
	 * @param batchOcrVO
	 *            识别请求参数
	 *
	 * @return
	 */
	Result submitBatchRecognize(BatchOcrVO batchOcrVO,JSONObject obj);

	/**
	 * 图像识别
	 *
	 *
	 * @param fileBytes
	 * @param fileSuffix
	 * @param funcList
	 * @return
	 */
	Result<List<Wjtz>> recognize(byte[] fileBytes, String fileSuffix, List<RecognizeFuncEnum> funcList);
	
	/**
	 * 图像识别
	 *
	 *
	 * @param bmsah 部门受案号
	 * @return
	 */
	WebResult<JzRecognizeResult> recognize(String bmsah);

	/**
	 * 提交OCR识别状态
	 *
	 * @param ocrTaskId
	 *            ocr 识别任务 id
	 *
	 * @return
	 */
	Result pushOcrResult(Long ocrTaskId);

	/**
	 * 提交图像识别状态
	 *
	 * @param recognizeTaskId
	 *            图像识别任务 id
	 *
	 * @return
	 */
	Result pushRecognizeResult(Long recognizeTaskId);

	/**
	 * 异步处理单个 ocr 识别任务
	 *
	 * @param ocrFileId
	 *            ocr 任务文件 id
	 *
	 * @return
	 */
	void processOcrTask(long ocrFileId);

	/**
	 * 异步处理单个图像识别任务
	 *
	 * @param recognizeFileId
	 *            图像识别任务文件 id
	 *
	 * @return
	 */
	void processRecognizeTask(long recognizeFileId);

	/**
	 *
	 * @param bmsah
	 * @param bsbhlist
	 * @param dwbm
	 * @param jzbh
	 */
	void relationShipTaskAndBMSAH(String bmsah, String[] bsbhlist, String dwbm,String jzbh);

	/**
	 *
	 * @param params
	 * @return
	 */
	WjOcrsbjgRes fileRecognize(OcrResultRequestVo params) throws Exception;

	/**
	 *
	 * @param bmsah
	 * @param bsbhlist
	 * @param dwbm
	 * @param jzbh
	 */
	void unRelationShipTaskAndBMSAH(String bmsah, String[] bsbhlist, String dwbm, String jzbh);

    /**
     * 将传入需要识别的图片或pdf进行OCR识别并返回识别结果
     *
     * @param ocrParam
     *
     * @return
     */
    Wjsbjg recognize(OcrResultRequestVo ocrParam);
}
