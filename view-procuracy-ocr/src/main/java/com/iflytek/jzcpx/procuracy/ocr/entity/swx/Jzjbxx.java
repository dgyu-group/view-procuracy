package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import lombok.Data;

/**
 * 获取案件目录文件信息
 */
@Data
public class Jzjbxx {


    /**
     * #部门受案号
     */
    private String bmsah;

    /**
     * #统一受案号
     */
    private String tysah;

    /**
     * #卷宗名称
     */
    private String jzmc;

    /**
     * #卷宗存储路径
     */
    private String jzlj;

    /**
     * #卷宗编号
     */
    private String jzbh;

    /**
     * #卷宗描述
     */
    private String jzms;

    /**
     * #单位编码
     */
    private String dwbm;
    
}
