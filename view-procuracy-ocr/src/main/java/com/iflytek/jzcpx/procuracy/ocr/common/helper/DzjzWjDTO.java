package com.iflytek.jzcpx.procuracy.ocr.common.helper;

import java.nio.charset.StandardCharsets;

import com.google.common.primitives.Longs;
import com.iflytek.jzcpx.procuracy.ocr.common.ByteUtil;
import com.iflytek.jzcpx.procuracy.ocr.common.enums.StateEnums;

import lombok.Data;
import org.apache.commons.lang3.ArrayUtils;

/**
 * 电子卷宗扫描文件数据传输对象
 * <p>
 * 0字节 成功失败标识  bool
 * 1-8字节  数据区长度long
 * 9-99字节 code 内容 string
 * 100-299字节  message string
 * 300-结束  数据区长度 byte[]
 */
@Data
public class DzjzWjDTO {
    /**
     * 1 表示成功
     */
    private boolean success;

    /**
     * 返回数据部分的长度
     */
    private long dataLength;

    /**
     * 成功状态码
     */
    private String code;
    /**
     * 提示消息
     */
    private String message;
    
    /**
     * byte[]文件数据流
     */
    private byte[] data;
    

    /**
     * @return
     */
    public byte[] getDatas() {
        try {
            if (message==null)
            {
                message="";
            }
            byte[] resultDats= ByteUtil.getBytes(StateEnums.SUCCESS, data.length, message, data);
            return  resultDats;
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * 打印日志
     * @return
     */
    public  String printMessage()
    {
        return  "success"+success+",message="+message+",code="+code;
    }


    public DzjzWjDTO() {
    }

    public DzjzWjDTO(byte[] respBytes) {
        this.decode(respBytes);
    }


    public void decode(byte[] respBytes) {
        success = respBytes[0] == 1;

        if (success == false) {
            return;
        }
        dataLength = Longs.fromByteArray(ArrayUtils.subarray(respBytes, 1, 9));

        code = new String(respBytes, 9, 90, StandardCharsets.UTF_8).trim();
        message = new String(respBytes, 100, 199, StandardCharsets.UTF_8).trim();

        data = new byte[(int) dataLength];
        System.arraycopy(respBytes, 300, data, 0, (int) dataLength);
    }
}
