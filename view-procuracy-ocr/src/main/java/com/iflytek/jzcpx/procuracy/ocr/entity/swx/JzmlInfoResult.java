package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import java.util.List;

import lombok.Data;

@Data
public class JzmlInfoResult {

    /**
     * #标识编号
     */
    private String bsbh;

    /**
     * #卷宗基本信息
     */
    private Jzjbxx jzjbxx;
    
    /**
     * 文件目录列表
     */
    private List<Jzml> jzml;
    
    /**
     * 文件信息列表
     */
    private List<Jzmlwj> jzmlwj;


}
