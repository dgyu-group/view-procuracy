package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 表格类型的
 */
@Data
public class WjOcrsbjgResDataWjsbxxBgqyTable extends WjOcrsbjgResDataWjsbxxBgqy {

    public WjOcrsbjgResDataWjsbxxBgqyTable()
    {
        trs=new ArrayList<>();
    }

    /**
     * 186,435,1124,1497
     */
    private String zbwz;//坐标位置

    /**
     * 表格行
     */
    private List<WjOcrsbjgResDataWjsbxxBgqyTableTrs> trs;
}
