package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class WjOcrsbjgResDataWjjgxx {

    public WjOcrsbjgResDataWjjgxx()
    {
        wbxx=new ArrayList<>();
    }

    /**
     * 文本信息
     */
    private List<WjOcrsbjgResDataWjjgxxWbxx> wbxx;
}
