package com.iflytek.jzcpx.procuracy.ocr.entity.txsb;

import lombok.Data;

/**
 * 获取 卷宗图像识别结果（api-06）请求类
 * 
 * @author dgyu
 *
 */
@Data
public class JzRecognizeVo {
	private String bmsah;// 部门受案号 汉东省院起诉受[2018]10000100001号
}
