package com.iflytek.jzcpx.procuracy.ocr.entity.swx.result;

import lombok.Data;

/**
 * 单页 ocr 识别结果
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2020/1/17 16:35
 */
@Data
public class PageOcrInfo {
    /** 高度 */
    private int gd;

    /** 宽度 */
    private int kd;

    /** 原始高度 */
    private int ysgd;

    /** 原始宽度 */
    private int yskd;

    /** ocr 识别结果 */
    private String ocrData;

    /** 图片二进制内容 */
    private byte[] imgBytes;
}
