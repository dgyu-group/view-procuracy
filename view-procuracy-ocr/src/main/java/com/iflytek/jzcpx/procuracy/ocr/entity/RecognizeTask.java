package com.iflytek.jzcpx.procuracy.ocr.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 图像识别任务表
 * 
 * @author 
 * @date 2019/08/12
 */
@Data
@TableName("t_recognize_task")
public class RecognizeTask implements Serializable {
    private static final long serialVersionUID = 1;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;

    /**
     * 应用和系统唯一id
     */
    @TableField("systemid")
    private String systemid;

    /**
     * 单位编号
     */
    @TableField("dwbm")
    private String dwbm;

    /**
     * 标识编号
     */
    @TableField("bsbh")
    private String bsbh;

    /**
     * 任务id
     */
    @TableField("taskid")
    private String taskid;

    /**
     * 部门受案号
     */
    @TableField("bmsah")
    private String bmsah;

    /**
     * 卷宗编号
     */
    @TableField("jzbh")
    private String jzbh;

    /**
     * 任务状态
     */
    @TableField("status")
    private String status;

    /**
     * 结果推送信息
     */
    @TableField("result_push_info")
    private String resultPushInfo;

    /**
     * 预留字段
     */
    @TableField("remark")
    private String remark;

    /**
     * 失败次数
     */
    @TableField("fail_nums")
    private Integer failNums;
}