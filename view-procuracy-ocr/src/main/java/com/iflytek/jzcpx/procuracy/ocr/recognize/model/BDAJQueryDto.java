package com.iflytek.jzcpx.procuracy.ocr.recognize.model;

import lombok.Data;

/**
 *
 */
@Data
public class BDAJQueryDto {

    /**
     * 部门受案号
     */
    private String bmsah;

    /**
     * 单位编码
     */
    private  String dwbm;

    /**
     * 增加卷宗编号
     */
    private String jzbh;

    /**
     * 标志编号
     */
    private String[] bsbhlist;

    public void setBmsah(String bmsah) {
        this.bmsah = bmsah;
    }

    public String getBmsah() {
        return bmsah;
    }

    public String getDwbm() {
        return dwbm;
    }

    public String[] getBsbhlist() {
        return bsbhlist;
    }

    public void setBsbhlist(String[] bsbhlist) {
        this.bsbhlist = bsbhlist;
    }

    public void setDwbm(String dwbm) {
        this.dwbm = dwbm;
    }
}
