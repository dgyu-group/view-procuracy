package com.iflytek.jzcpx.procuracy.ocr.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * ocr识别任务单元表
 * 
 * @author 
 * @date 2019/08/12
 */
@Data
@TableName("t_ocr_file")
public class OcrFile implements Serializable {
    private static final long serialVersionUID = 1;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;

    /**
     * 图文识别批量任务 id
     */
    @TableField("ocr_task_id")
    private Long ocrTaskId;

    /**
     * 任务编号
     */
    @TableField("taskid")
    private String taskid;

    /**
     * 标识编号
     */
    @TableField("bsbh")
    private String bsbh;

    /**
     * 文件序号
     */
    @TableField("wjxh")
    private String wjxh;

    /**
     * 部门受案号
     */
    @TableField("bmsah")
    private String bmsah;

    /**
     * ocr识别结果存储路径
     */
    @TableField("ocr_result_path")
    private String ocrResultPath;

    /**
     * 引擎识别结果存储路径
     */
    @TableField("engine_result_path")
    private String engineResultPath;

    /**
     * 识别类型，单文件或批量任务
     */
    @TableField("type")
    private String type;

    /**
     * 状态
     */
    @TableField("status")
    private String status;

    /**
     * 任务失败信息
     */
    @TableField("fail_info")
    private String failInfo;

    /**
     * 预留字段
     */
    @TableField("remark")
    private String remark;

    /**
     * 状态：默认0，超过一个月被删除1
     */
    @TableField("state")
    private String state;
}