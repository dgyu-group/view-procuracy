package com.iflytek.jzcpx.procuracy.ocr.common.enums;

/**
 * 图像识别功能列表
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-13 10:00
 */
public enum RecognizeFuncEnum {
    /** 指纹 */
	Fingerprint(0, "指纹"),
    /** 印章 */
	Seal(1, "印章"),
    /** 签名 */
    SIGNATURE(2, "签名"),
    /** 标题 */
    TITLE(3, "标题"),
    ;

    private int code;
    private String name;

    RecognizeFuncEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }}

