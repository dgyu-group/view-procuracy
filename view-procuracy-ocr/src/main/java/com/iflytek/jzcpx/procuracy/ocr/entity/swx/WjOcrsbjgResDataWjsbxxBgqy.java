package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import lombok.Data;

/**
 * 表格区域
 */
@Data
public class WjOcrsbjgResDataWjsbxxBgqy {

    /**
     * 有text和table两种类型
     */
    private String lx;//类型
}
