package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

import lombok.Data;

@Data
/**
 * 二进制流文件包格式
 * 0字节 成功失败标识  bool
 * 1-8字节  数据区长度long
 * 9-99字节 code 内容 string
 * 100-299字节  message内容 string
 * 300-结束  数据区长度 byte[]
 */
public class BinaryData {
    boolean success;
    long dataLength;
    String code;
    String message;
    byte[] data;

    public byte[] encode() {
        byte[] resultData = new byte[300 + data.length];
        resultData[0] = (byte) (success ? 1 : 0);
        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.putLong(0, dataLength);
        byte[] dataLengthArray = buffer.array();
        System.arraycopy(dataLengthArray, 0, resultData, 1, 8);
        byte[] codeBytes = code.getBytes(StandardCharsets.UTF_8);
        System.arraycopy(codeBytes, 0, resultData, 9, codeBytes.length);
        byte[] messageBytes = message.getBytes(StandardCharsets.UTF_8);
        System.arraycopy(messageBytes, 0, resultData, 100, messageBytes.length);
        System.arraycopy(data, 0, resultData, 300, data.length);
        return resultData;
    }

    public void decode(byte[] inData) {
    	success = inData[0] == 1;

        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.put(inData, 1, 8);
        buffer.flip();
        dataLength = buffer.getLong();
        code = new String(inData, 9, 90, StandardCharsets.UTF_8).trim();
        message = new String(inData, 100, 199, StandardCharsets.UTF_8).trim();
        data = new byte[(int) dataLength];
        System.arraycopy(inData, 300, data, 0, (int) dataLength);
    }
}
