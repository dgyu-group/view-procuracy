package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import lombok.Data;

/**
 * 特征类型
 * 0：指纹
 * 1：印章
 * 2: 签名
 * 3：标题
 */
@Data
public class OcrTxsbZbxx {

    /**
     * x坐标
     */
    private int x;

    /**
     * y坐标
     */
    private int y;

    /**
     * WIDTH坐标
     */
    private int w;

    /**
     * HEIGHT坐标
     */
    private int h;
}
