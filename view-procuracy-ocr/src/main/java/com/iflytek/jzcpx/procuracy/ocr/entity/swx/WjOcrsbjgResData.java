package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class WjOcrsbjgResData {

    /**
     *
     */
    public WjOcrsbjgResData()
    {
        wjjgxx=new WjOcrsbjgResDataWjjgxx();
        wjsbxx=new ArrayList<>();
    }

    private String wjbh;//#文件编号
    private String wjhz;//#文件后缀

    /**
     * 为啥要返回这个
     */
    private String pdfnr;//#双层pdf 文件内容（文件字节数组base64）

    /**
     * 输出文件文本
     */
    private String txtnr;

    /**
     * 这个md5值用来校验生成的pdf内容
     */
    private String md5;

    /**
     * #OCR返回带有文字坐标信息
     */
    private WjOcrsbjgResDataWjjgxx wjjgxx;

    /**
     * #文件识别信息（输入文件为多页，输出转化多页信息）
     */
    private List<WjOcrsbjgResDataWjsbxx> wjsbxx;

    /**
     * ocr识别结果json结构
     */
    private String ocrResultJson;
}
