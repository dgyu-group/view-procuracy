package com.iflytek.jzcpx.procuracy.ocr.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.iflytek.jzcpx.procuracy.ocr.entity.OcrFile;

public interface OcrFileDao extends BaseMapper<OcrFile> {

}