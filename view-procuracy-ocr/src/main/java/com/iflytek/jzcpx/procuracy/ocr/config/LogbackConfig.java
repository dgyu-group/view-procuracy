package com.iflytek.jzcpx.procuracy.ocr.config;

import ch.qos.logback.classic.LoggerContext;
import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.impl.StaticLoggerBinder;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 *
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2021/8/25
 */
@Configuration
public class LogbackConfig implements InitializingBean {
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private Environment environment;

    @Override
    public void afterPropertiesSet() throws Exception {
        // 如果使用java -jar方式启动, 则认为是在服务器启动, 就会把控制台输出关闭
        if (System.getProperty("sun.java.command").endsWith("jar")) {
            ILoggerFactory factory = StaticLoggerBinder.getSingleton().getLoggerFactory();
            LoggerContext lc = (LoggerContext) factory;
            ch.qos.logback.classic.Logger rootLogger = lc.getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);
            rootLogger.detachAppender("CONSOLE");
        }
    }
}
