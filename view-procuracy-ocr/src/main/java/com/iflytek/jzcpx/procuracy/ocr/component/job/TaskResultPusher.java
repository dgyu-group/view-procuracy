package com.iflytek.jzcpx.procuracy.ocr.component.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * 批量任务结果推送器
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-13 17:30
 */
@Component
public class TaskResultPusher {
    private static final Logger logger = LoggerFactory.getLogger(TaskResultPusher.class);

    @Async("taskResultPushExecutor")
    public void push() {
        //扫描到做完图像识别的任务，推送消息 by ssju
    }

}
