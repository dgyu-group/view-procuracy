package com.iflytek.jzcpx.procuracy.ocr.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 图像识别任务单元表
 * 
 * @author 
 * @date 2019/08/12
 */
@Data
@TableName("t_recognize_file")
public class RecognizeFile implements Serializable {
    private static final long serialVersionUID = 1;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;

    /**
     * 图像识别批量任务 id
     */
    @TableField("recognize_task_id")
    private Long recognizeTaskId;

    /**
     * 标识编号
     */
    @TableField("bsbh")
    private String bsbh;

    /**
     * 任务编号
     */
    @TableField("taskid")
    private String taskid;

    /**
     * 部门受案号
     */
    @TableField("bmsah")
    private String bmsah;

    /**
     * 卷宗编号
     */
    @TableField("jzbh")
    private String jzbh;

    /**
     * 目录编号
     */
    @TableField("mlbh")
    private String mlbh;

    /**
     * 文件序号
     */
    @TableField("wjxh")
    private String wjxh;

    /**
     * ocr引擎结果存储路径
     */
    @TableField("engine_result_path")
    private String engineResultPath;

    /**
     * 识别结果存储路径
     */
    @TableField("ocr_result_path")
    private String ocrResultPath;

    /**
     * 识别状态
     */
    @TableField("status")
    private String status;

    /**
     * 任务类型，单文件或批量任务
     */
    @TableField("type")
    private String type;

    /**
     * 待提取数据类型
     */
    @TableField("func_list")
    private String funcList;

    /**
     * 失败信息
     */
    @TableField("fail_info")
    private String failInfo;

    /**
     * 预留字段
     */
    @TableField("remark")
    private String remark;

    /**
     * 状态：默认0，超过一个月被删除1
     */
    @TableField("state")
    private String state;
}