package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import com.iflytek.jzcpx.procuracy.ocr.common.constant.Constants;
import lombok.Data;

/**
 * {
 * "sfyz": "y/n", #是否识别印章
 * "sfzw": "y/n", #是否识别指纹
 * "sfsx": "y/n", #是否签名
 * "sfbt": "y/n", #是否识别标题
 * "wjnr": bytes    #文件内容
 * }
 */
@Data
public class OcrTxsbVo {

    private String sfyz = Constants.WjsbRequiredType.N;

    private String sfzw = Constants.WjsbRequiredType.N;

    private String sfsx = Constants.WjsbRequiredType.N;

    private String sfbt = Constants.WjsbRequiredType.N;

    private String wjnr = Constants.WjsbRequiredType.N;

    /**
     * #文件后缀
     */
    private String wjhz;

}