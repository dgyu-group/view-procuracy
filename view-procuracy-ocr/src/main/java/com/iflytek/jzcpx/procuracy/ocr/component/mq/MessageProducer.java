package com.iflytek.jzcpx.procuracy.ocr.component.mq;

import javax.annotation.Resource;
import javax.jms.Queue;
import java.util.Collection;
import java.util.List;

import com.iflytek.jzcpx.procuracy.common.util.JSONUtil;
import com.iflytek.jzcpx.procuracy.ocr.common.enums.RecognizeTaskStatusEnum;
import com.iflytek.jzcpx.procuracy.ocr.service.OcrTaskService;
import com.iflytek.jzcpx.procuracy.ocr.service.RecognizeTaskService;
import com.yomahub.tlog.core.mq.TLogMqWrapBean;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;

/**
 * 消息生产者
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-14 18:29
 */
@Component
public class MessageProducer {
    private static final Logger logger = LoggerFactory.getLogger(MessageProducer.class);

    @Resource(name = "ocrResultPushQueue")
    private Queue ocrResultPushQueue;

    @Resource(name = "recognizeResultPushQueue")
    private Queue recognizeResultPushQueue;

    @Resource(name = "ocrTaskQueue")
    private Queue ocrTaskQueue;

    @Resource(name = "recognizeTaskQueue")
    private Queue recognizeTaskQueue;

    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    @Autowired
    private OcrTaskService ocrTaskService;

    @Autowired
    private RecognizeTaskService recognizeTaskService;

    /**
     * 发送 ocr 结果推送消息
     *
     * @param taskId ocr任务id
     */
    public void sendOcrResultPushMessage(Long taskId) {
        if (taskId != null) {
            //ocrTaskService.updateStatus(taskId, OcrTaskStatusEnum.PUSHING);
            TLogMqWrapBean<Long> payload = new TLogMqWrapBean<>(taskId);
            String data = JSONUtil.toStrDefault(payload);
            logger.info("发送OCR处理结果推送消息, 内容: {}", data);
            jmsMessagingTemplate.convertAndSend(ocrResultPushQueue, data);
            logger.info("发送OCR处理结果推送消息完成");
        }
    }

    /**
     * 批量发送 ocr 结果推送消息
     *
     * @param taskIds ocr任务id集合
     */
    public void sendOcrResultPushMessage(Collection<Long> taskIds) {
        if (CollectionUtils.isNotEmpty(taskIds)) {
            for (Long taskId : taskIds) {
                sendOcrResultPushMessage(taskId);
            }
        }
    }

    /**
     * 批量发送待处理 ocr 文件id消息
     *
     * @param fileIdList 待处理 ocr 文件id集合
     */
    public void sendOcrFileProcessMessage(List<Long> fileIdList) {
        if (CollectionUtils.isNotEmpty(fileIdList)) {
            fileIdList.forEach(id -> {
                TLogMqWrapBean<Long> payload = new TLogMqWrapBean<>(id);
                String data = JSONUtil.toStrDefault(payload);
                logger.info("发送待处理的OCR文件ID消息, 内容: {}", data);
                jmsMessagingTemplate.convertAndSend(ocrTaskQueue, data);
                logger.info("发送待处理的OCR文件ID消息完成");
            });
        }
    }

    /**
     * 批量发送待处理图象识别文件id消息
     *
     * @param fileIdList
     */
    public void sendRecognizeFileProcessMessage(List<Long> fileIdList) {
        if (CollectionUtils.isNotEmpty(fileIdList)) {
            // fileIdList.forEach(id -> recognizeTaskQueue.pushFromHead(id.toString()));
            fileIdList.forEach(id -> {
                TLogMqWrapBean<Long> payload = new TLogMqWrapBean<>(id);
                String data = JSONUtil.toStrDefault(payload);
                logger.info("发送待处理的图像识别文件ID消息, 内容: {}", data);
                jmsMessagingTemplate.convertAndSend(recognizeTaskQueue, data);
                logger.info("发送待处理的图像识别文件ID消息完成");
            });
        }
    }

    /**
     * 批量推送图像识别完成消息
     *
     * @param recognizeTaskIds
     */
    public void sendRecognizeResultPushMessage(List<Long> recognizeTaskIds) {
        //推送图像识别完成消息
        if (CollectionUtils.isNotEmpty(recognizeTaskIds)) {
            for (Long recognizeTaskId : recognizeTaskIds) {
                sendRecognizeResultPushMessage(recognizeTaskId);
            }
        }
    }

    /**
     * 推送图像识别完成消息
     *
     * @param recognizeTaskId
     */
    private void sendRecognizeResultPushMessage(Long recognizeTaskId) {
        if (recognizeTaskId != null) {
            recognizeTaskService.updateStatus(recognizeTaskId, RecognizeTaskStatusEnum.PUSHING);
            TLogMqWrapBean<Long> payload = new TLogMqWrapBean<>(recognizeTaskId);
            String data = JSONUtil.toStrDefault(payload);
            logger.info("发送图像识别结果推送消息, 内容: {}", data);
            jmsMessagingTemplate.convertAndSend(recognizeResultPushQueue, data);
            logger.info("发送图像识别结果推送消息完成");
        }
    }
}
