package com.iflytek.jzcpx.procuracy.ocr.service;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.iflytek.jzcpx.procuracy.ocr.common.enums.OcrTaskStatusEnum;
import com.iflytek.jzcpx.procuracy.ocr.entity.OcrTask;

/**
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-12 19:25
 */
public interface OcrTaskService extends IService<OcrTask> {
    /**
     * 创建 ocr 批量任务
     *
     * @param systemid 应用和单位的唯一标识
     * @param dwbm     单位编码
     * @param bsbh     标识编号
     * @param taskid   taskid
     *
     * @return 成功保存后的数据库id
     */
    Long create(String systemid, String dwbm, String bsbh, String taskid,String bmsah);

    /**
     * 任务非正常结束
     *
     * @param taskId    任务 id
     * @param errorInfo 错误信息
     *
     * @return 更新成功与否
     */
    boolean abnormalEnd(Long taskId, String errorInfo);

    /**
     * 更新任务状态
     *
     * @param taskId           任务 id
     * @param targetStatusEnum 要更新成的状态
     *
     * @return 更新成功与否
     */
    boolean updateStatus(Long taskId, OcrTaskStatusEnum targetStatusEnum);

    /**
     * 更新任务状态和推送结果
     *
     * @param taskId           任务 id
     * @param targetStatusEnum 要更新成的状态
     *
     * @return 更新成功与否
     */
    boolean updateStatusAndPushInfo(Long taskId, OcrTaskStatusEnum targetStatusEnum, String resultPushInfo);

    /**
     * 查询需要推送结果的任务数据
     *
     * @return
     */
    List<Long> findPushableTaskId();

    /**
     * 根据部门受案号查询ocr任务
     *
     * @param bmsah 部门受案号
     *
     * @return
     */
    OcrTask findByBmsah(String bmsah);

    /**
     * 更新失败次数
     *
     * @param taskId           任务 id
     *
     * @return 更新成功与否
     */
    boolean updateTimes(Long taskId);

    int cleanData(Date startDatetime, Date endDatetime);

    List<OcrTask> listByBsbh(String bsbh);
}
