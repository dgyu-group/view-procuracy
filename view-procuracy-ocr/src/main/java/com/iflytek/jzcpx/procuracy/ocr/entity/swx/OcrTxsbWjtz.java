package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import lombok.Data;

@Data
public class OcrTxsbWjtz {

    /**
     * 特征类型 0：指纹 1：印章 2:签名 3：标题
     */
    private Integer tzlx;

    private OcrTxsbZbxx zbxx;
}
