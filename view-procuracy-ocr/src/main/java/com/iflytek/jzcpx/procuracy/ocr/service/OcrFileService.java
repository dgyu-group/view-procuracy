package com.iflytek.jzcpx.procuracy.ocr.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;
import com.iflytek.jzcpx.procuracy.ocr.common.enums.OcrFileStatusEnum;
import com.iflytek.jzcpx.procuracy.ocr.entity.OcrFile;

/**
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-12 19:25
 */
public interface OcrFileService extends IService<OcrFile> {

    /**
     * 文件识别异常结束
     *
     * @param id        任务文件 id
     * @param errorInfo 错误信息
     * @return 关系成功与否
     */
    boolean abnormalEnd(Long id, String errorInfo);

    /**
     * 更新识别文件状态
     *
     * @param fileId           文件 id
     * @param targetStatusEnum 要更新成的状态
     * @return 更新成功与否
     */
    boolean updateStatus(Long fileId, OcrFileStatusEnum targetStatusEnum);

    /**
     * 更新 ocr 引擎识别结果存储路径
     *
     * @param fileId    文件 id
     * @param storePath 存储路径
     * @return 更新成功与否
     */
    boolean updateEngineResultPath(Long fileId, String storePath);

    /**
     * 创建单文件识别记录
     *
     * @param wjbh 文件编号
     * @return 新增的记录 id
     */
    Long createSingle(String wjbh);

    /**
     * 根据 ocrTaskId 批量查询未完成的任务文件 id
     *
     * @param taskIdList 批量任务 id
     * @return 任务id : 未完成的任务文件id集合
     */
    Map<Long, List<Long>> findUndoneFileIdMap(List<Long> taskIdList);

    /**
     * 根据任务id查询所有ocr文件
     *
     * @param ocrTaskId ocr任务id
     * @return
     */
    List<OcrFile> findByOcrTaskId(Long ocrTaskId);

    /**
     * @param wjxhList
     * @return
     */
    List<OcrFile> findByWjxh(List<String> wjxhList);

    /**
     * 根据标识编号获取文件列表
     *
     * @param bsbh 识别任务的表示编号
     * @return
     */
    List<OcrFile> getOcrByBSBH(String bsbh);

    /**
     *补充一个下载图片的方法
     * @param bsbh
     * @param taskid
     * @param wjxh
     * @return
     */
    byte[] getDZJZJPG(String bsbh, String taskid, String wjxh);
    /**
     * 
     * @param bmsah
     * @return
     */
    public List<OcrFile> findBybmsah(String bmsah);
    /**
     * 根据标识编号 文件序号查找卷宗文件
     * @param bsbh
     */
    public OcrFile findOcrFileByWjxh(String wjxh);

    List<OcrFile> listByBsbhWjxh(String bsbh, List<String> wjxh);

    int cleanData(Date startDatetime, Date endDatetime, boolean cleanDB, boolean cleanFdfs);
}
