package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import lombok.Data;

/**
 * {
 * "WJBH": "24E6C7DB70E441888CA0E6AD40850157",  #文件编号
 * "WJHZ": ".pdf", #文件后缀
 * "WJNR_OCR": "dsnajfjdsncjdsnjs…", #转换后文件内容（文件流）
 * "WJWB_OCR": "受案登记表  张三…"  #文件文本
 * }
 */
@Data
public class OcrWjsbResult {


    private boolean success = false;

    /**
     * 状态码
     */
    private String code;

    /**
     * #文件后缀
     */
    private String message;

    /**
     * 文件内容（文件字节流）
     */
    private OcrWjsbData data;


}
