package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import lombok.Data;

@Data
public class WjOcrsbjgRes {

    public WjOcrsbjgRes()
    {
        data=new WjOcrsbjgResData();
    }

    /**
     * 识别是否成功
     */
    private boolean success = false;

    /**
     * 状态码
     */
    private String code;

    /**
     * #文件后缀
     */
    private String message;

    /**
     * 识别结果
     */
    private WjOcrsbjgResData data;
}
