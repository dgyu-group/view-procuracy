package com.iflytek.jzcpx.procuracy.ocr.entity.metric;

/**
 * 指标统计，分页请求基础类
 * @author dgyu
 *
 */
public class BasePageVO {
	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	/** 页码, 从 1 开始 */
	private int page=1;

	/** 分页条数 */
	private int size=10;
}
