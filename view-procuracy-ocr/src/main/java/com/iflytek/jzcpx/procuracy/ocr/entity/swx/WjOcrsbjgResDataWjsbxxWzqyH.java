package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class WjOcrsbjgResDataWjsbxxWzqyH {

    public WjOcrsbjgResDataWjsbxxWzqyH()
    {
        zf=new ArrayList<>();
        zfzbwz=new ArrayList<>();
    }

    private int xh;//序号

    /**
     * 556,418,2025,546
     */
    private String zbwz;//坐标位置

    /**
     * "湖",
     * "南"
     */
    private List<String> zf;//字符

    /**
     * 字符坐标位置
     *
     *  "666,420,776,546",
     *  "866,420,976,546"
     */
    private List<String> zfzbwz;//字符坐标位置
}
