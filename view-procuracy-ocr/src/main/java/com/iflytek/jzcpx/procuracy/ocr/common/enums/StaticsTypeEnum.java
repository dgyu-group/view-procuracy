package com.iflytek.jzcpx.procuracy.ocr.common.enums;

public enum StaticsTypeEnum {

	SUCCESSNUM(1, "成功数"), 
	FAILURENUM(2, "失败数"), SUCCESSRATE(3, "成功率");

	private final int type;
	private final String desc;

	StaticsTypeEnum(int type, String desc) {
		this.type = type;
		this.desc = desc;
	}

	public int getType() {
		return type;
	}

	public String getDesc() {
		return desc;
	}

	public static StaticsTypeEnum getStaticsTypeEnum(int type) {
		for (StaticsTypeEnum s : StaticsTypeEnum.values()) {
			int enumType = s.getType();
			if (enumType == type) {
				return s;
			}
		}
		throw new RuntimeException("找不到对应的StaticsTypeEnum: " + type);
	}

}
