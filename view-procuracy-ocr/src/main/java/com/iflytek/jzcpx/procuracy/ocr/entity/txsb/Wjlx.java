/**
  * Copyright 2020 bejson.com 
  */
package com.iflytek.jzcpx.procuracy.ocr.entity.txsb;

/**
 * Auto-generated: 2020-08-10 15:13:42
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Wjlx {

    private String cls;
    private String prob;
    public void setCls(String cls) {
         this.cls = cls;
     }
     public String getCls() {
         return cls;
     }

    public void setProb(String prob) {
         this.prob = prob;
     }
     public String getProb() {
         return prob;
     }

}