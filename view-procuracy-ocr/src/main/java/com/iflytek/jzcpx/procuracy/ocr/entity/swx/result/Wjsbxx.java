package com.iflytek.jzcpx.procuracy.ocr.entity.swx.result;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

/**
 * 文件识别信息
 */
@Data
public class Wjsbxx {

    /** 文件二进制内容, 不作为最终接口返回结果, 仅用于方便计算其 MD5 */
    @JsonIgnore
    @JSONField(serialize = false)
    private byte[] pdfBytes;

    /** 文件内容 */
    private String wjnr;

    /** 文件文本 */
    private String wjwb;

    /** 文件高度 */
    private Integer wjgd;

    /** 文件宽度 */
    private Integer wjkd;

    /** 原始图片文件高度 */
    private Integer wjysgd;

    /** 原始图片文件宽度 */
    private Integer wjyskd;

    /** 文件倾斜角度 */
    private Integer wjqxjd;

    /** 文件偏转角，顺时针为正，逆时针为负 */
    private Integer wjpzj;

    /** 压缩识别, 值为字符1时，表示图像超过长边2500px，要做压缩识别 */
    private String yssb;

    /**
     * OCR返回带有文字坐标信息
     */
    private Wjjgxx wjjgxx;

}
