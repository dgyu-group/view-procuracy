package com.iflytek.jzcpx.procuracy.ocr.common.helper;

import com.iflytek.jzcpx.procuracy.common.result.Result;
import com.iflytek.jzcpx.procuracy.ocr.entity.swx.JzInfoResult;
import com.iflytek.jzcpx.procuracy.ocr.entity.swx.JzmlInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

/**
 * 赛威讯请求装饰器
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2022/6/21
 */
public class SwxRequestHelperDecorator extends SwxRequestHelper {
    protected final Logger logger = LoggerFactory.getLogger(SwxRequestHelperDecorator.class);

    @Value("${swx.retry:3}")
    private int maxRetry;

    @Override
    public String getSwxServerUrl() {
        return super.getSwxServerUrl();
    }

    @Override
    public Result<JzInfoResult> getOCRFileInfo(String bsbh, String taskid) {
        int requestCount = 1;
        Result<JzInfoResult> result = null;
        do {
            logger.info("第{}次请求电子卷宗待识别文件信息接口", requestCount);
            result = super.getOCRFileInfo(bsbh, taskid);
        } while ((result == null || !result.isSuccess()) && requestCount++ <= maxRetry);
        return result;
    }

    @Override
    public Result<JzmlInfo> getDZJZInfo(String bsbh) {
        int requestCount = 1;
        Result<JzmlInfo> result = null;
        do {
            logger.info("第{}次请求电子卷宗目录文件接口", requestCount);
            result = super.getDZJZInfo(bsbh);
        } while ((result == null || !result.isSuccess()) && requestCount++ <= maxRetry);
        return result;
    }

    @Override
    public Result<DzjzWjDTO> ocrFileJpgWjApi(String wjxh) {
        int requestCount = 1;
        Result<DzjzWjDTO> result = null;
        do {
            logger.info("第{}次请求电子卷宗JPG文件接口", requestCount);
            result = super.ocrFileJpgWjApi(wjxh);
        } while ((result == null || !result.isSuccess()) && requestCount++ <= maxRetry);
        return result;
    }

    @Override
    public Result<DzjzDataDTO> getDZJZJPG(String bsbh, String taskid, String wjxh) {
        int requestCount = 1;
        Result<DzjzDataDTO> result = null;
        do {
            logger.info("第{}次请求电子卷宗图片接口", requestCount);
            result = super.getDZJZJPG(bsbh, taskid, wjxh);
        } while ((result == null || !result.isSuccess()) && requestCount++ <= maxRetry);
        return result;
    }

    @Override
    public Result<DzjzWjDTO> getDZJZWJPG(String bsbh, String wjxh) {
        int requestCount = 1;
        Result<DzjzWjDTO> result = null;
        do {
            logger.info("第{}次请求电子卷宗扫描文件接口", requestCount);
            result = super.getDZJZWJPG(bsbh, wjxh);
        } while ((result == null || !result.isSuccess()) && requestCount++ <= maxRetry);
        return result;
    }

    @Override
    public Result savePDF(DzjzDataDTO dataDTO) {
        int requestCount = 1;
        Result result = null;
        do {
            logger.info("第{}次请求提交OCR识别结果PDF文件接口", requestCount);
            result = super.savePDF(dataDTO);
        } while ((result == null || !result.isSuccess()) && requestCount++ <= maxRetry);
        return result;
    }

    @Override
    public Result saveOCRResult(JzInfoResult jzInfoResult) {
        int requestCount = 1;
        Result result = null;
        do {
            logger.info("第{}次请求提交OCR识别状态接口", requestCount);
            result = super.saveOCRResult(jzInfoResult);
        } while ((result == null || !result.isSuccess()) && requestCount++ <= maxRetry);
        return result;
    }

    public SwxRequestHelperDecorator(String serverUrl) {
        super(serverUrl);
    }
}
