package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import com.iflytek.jzcpx.procuracy.ocr.entity.swx.result.Wjjgxx;

import lombok.Data;

/**
 *文件识别信息
 */
@Data
public class Wjsbxx {

	private String wjxh;// 文件序号
	private String wjmd5;// 文件MD5码
	private int wjgd;// 1000 #文件高度
	private int wjkd;// 700 #文件宽度
	private int wjysgd;// 原始图片文件高度
	private int wjyskd;// 原始图片文件宽度
	private String wjqxjd;// 文件倾斜角度，顺时针为正，逆时针为负
	private int wjpzjd;// 文件偏转角度，0、90、180、270 可选参数
	private int yssb;// 是否压缩识别，为1时压缩识别，此时wjgd,wjkd,wjysgd,wjyskd 值不一致
	private String wjwb;// "受案登记表 张三…" #文件文本
	private String wjnr;// "dsnajfjdsncjdsnjs…", #转换后双层pdf文件（文件字节数组base64）
	private Wjjgxx wjjgxx;
}
