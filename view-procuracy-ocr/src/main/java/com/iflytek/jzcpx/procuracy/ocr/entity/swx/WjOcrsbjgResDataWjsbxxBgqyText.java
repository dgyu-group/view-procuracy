package com.iflytek.jzcpx.procuracy.ocr.entity.swx;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 表格区域 text类型
 */
@Data
public class WjOcrsbjgResDataWjsbxxBgqyText  extends  WjOcrsbjgResDataWjsbxxBgqy{

    public WjOcrsbjgResDataWjsbxxBgqyText()
    {
        h=new ArrayList<>();
    }

    /**
     * 表格区域里的文字部分
     */
    private List<WjOcrsbjgResDataWjsbxxWzqyH> h;
}
