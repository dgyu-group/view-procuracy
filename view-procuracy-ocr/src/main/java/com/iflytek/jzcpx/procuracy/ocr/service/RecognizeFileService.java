package com.iflytek.jzcpx.procuracy.ocr.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;
import com.iflytek.jzcpx.procuracy.ocr.common.enums.RecognizeFileStatusEnum;
import com.iflytek.jzcpx.procuracy.ocr.common.enums.RecognizeFuncEnum;
import com.iflytek.jzcpx.procuracy.ocr.entity.RecognizeFile;

/**
 * 图像识别接口
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-12 19:25
 */
public interface RecognizeFileService extends IService<RecognizeFile> {
    /**
     * 保存单文件识别任务
     *
     * @param funcs 识别结果类型
     *
     * @return 新增的数据 id
     */
    Long createSingle(List<RecognizeFuncEnum> funcs);

    /**
     * 识别任务异常结束
     *
     * @param fileId    文件 id
     * @param errorInfo 错误信息
     *
     * @return 更新成功与否
     */
    boolean abnormalEnd(Long fileId, String errorInfo);

    /**
     * 更新图像识别任务状态
     *
     * @param fileId           文件 id
     * @param targetStatusEnum 要更新成的状态
     *
     * @return 更新成功与否
     */
    boolean updateStatus(Long fileId, RecognizeFileStatusEnum targetStatusEnum);

    /**
     * 更新 ocr 引擎识别结果存储位置
     * @param fileId 文件 id
     * @param storePath 存储地址
     * @return 更新成功与否
     */
    boolean updateEngineResultPath(Long fileId, String storePath);

    /**
     * 根据 recognizeTaskId 批量查询未完成的任务文件 id
     *
     * @param recognizeTaskIdList 批量任务 id 集合
     *
     * @return 任务id : 未完成的任务文件id集合
     */
    Map<Long, List<Long>> findUndoneFileIdMap(List<Long> recognizeTaskIdList);


    /**
     * 根据标识编号 文件序号查找卷宗文件
     *
     * @param bsbh
     */
    List<RecognizeFile> listByBsbhWjxh(String bsbh, List<String> wjxh);


    int cleanData(Date startDatetime, Date endDatetime, boolean cleanDB, boolean cleanFdfs);
}
