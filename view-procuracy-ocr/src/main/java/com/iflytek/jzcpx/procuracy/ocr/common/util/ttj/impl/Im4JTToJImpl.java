package com.iflytek.jzcpx.procuracy.ocr.common.util.ttj.impl;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.iflytek.jzcpx.procuracy.ocr.common.util.pti.impl.GsPtoIImpl;
import com.iflytek.jzcpx.procuracy.ocr.common.util.ttj.TToJ;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class Im4JTToJImpl
        implements TToJ
{
    private static final Logger LOGGER = LoggerFactory.getLogger(Im4JTToJImpl.class);
    private static final String IMAGE_MAGICK_WORK_DIR = String.format("%s", new Object[] { System.getProperty("user.dir") });

    public static List<byte[]> readFilesBytes(File targetDirectory) throws Exception {
        //Preconditions.checkNotNull(targetDirectory, "targetDirectory = null");
        List<byte[]> ret = (List)new ArrayList<>();


        if (!targetDirectory.exists() || targetDirectory.isFile()) {
            return ret;
        }
        File[] allFiles = targetDirectory.listFiles();
        if (null == allFiles) {
            LOGGER.error("can't open targetDirectory, return null.");
            allFiles = new File[0];
        }
        GsPtoIImpl.numericSortFileName(allFiles);
        for (File file : allFiles) {
            ret.add(Files.readAllBytes(file.toPath()));
        }
        return ret;
    }
}

