
package com.iflytek.jzcpx.procuracy.tools.ocr.pdf;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
@Generated("http://www.stay4it.com")
public class TextRegion {

    private Integer id;
    private Rect rect;
    private List<Textline> textline = new ArrayList<Textline>();
    
    @JSONField(name="textline_count")
    private Integer textlineCount;
    private String type;

}
