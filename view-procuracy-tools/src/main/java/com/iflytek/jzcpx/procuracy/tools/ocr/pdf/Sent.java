
package com.iflytek.jzcpx.procuracy.tools.ocr.pdf;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
@Generated("http://www.stay4it.com")
public class Sent {
	
	@JSONField(name="char")
    private List<Char> _char = new ArrayList<Char>();
	
	@JSONField(name="char_count")
    private Integer charCount;
    private Double score;
    private String value;


    public List<Char> getChar() {
        return _char;
    }

    public void setChar(List<Char> _char) {
        this._char = _char;
    }



}
