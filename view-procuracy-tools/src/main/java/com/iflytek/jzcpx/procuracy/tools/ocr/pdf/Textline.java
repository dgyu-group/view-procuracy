
package com.iflytek.jzcpx.procuracy.tools.ocr.pdf;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
@Generated("http://www.stay4it.com")
public class Textline {

    private Integer id;
    private Rect rect;
    /**
     * 左下点坐标
     */
    private BL_point bl_point;
    /**
     * 右下坐标
     */
    private BR_point br_point;
    /**
     * 左上点坐标
     */
    private TL_point tl_point;
    /**
     * 右上点坐标
     */
    private TR_point tr_point;
    private List<Sent> sent = new ArrayList<Sent>();
    
    @JSONField(name="top_n")
    private Integer topN;
    private String type;


}
