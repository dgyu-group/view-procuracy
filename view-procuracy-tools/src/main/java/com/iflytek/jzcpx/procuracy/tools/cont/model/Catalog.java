package com.iflytek.jzcpx.procuracy.tools.cont.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Catalog {

    @JsonProperty("evidence_id")
    private int evidenceId;

    private String id;
    @JsonProperty("page_num")
    private int pageNum;

    private double score;

    private Object title;

    @JsonProperty("title_score")
    private double titleScore;

    private String type;

    private String content;

}