
package com.iflytek.jzcpx.procuracy.tools.ocr.pdf;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
@Generated("http://www.stay4it.com")
public class TaskResult {

    private List<Page> page = new ArrayList<Page>();
    
    @JSONField(name="page_count")
    private Integer pageCount;
    
    @JSONField(name="return")
    private Integer _return;


}
