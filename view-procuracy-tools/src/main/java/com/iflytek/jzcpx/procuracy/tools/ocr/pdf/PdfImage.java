/**
 * 
 */
package com.iflytek.jzcpx.procuracy.tools.ocr.pdf;

import com.lowagie.text.Image;
import lombok.Data;


/**
 * @author z4463
 * 
 *
 */
@Data
public class PdfImage {
    
    /**  图片的文件名*/
    private String imageName;

    /** ocr对应图片 */
    private Image image;
    /** ocr对应图片修正后的宽度 */
    private Float imageCurrentionWidth;
    /** ocr对应图片修正后的高度 */
    private Float imageCurrentionHeight;
    /** ocr对应图片原始宽度 */
    private Float iamgeOriginalWidth;
    /** ocr对应图片原始高度 */
    private Float iamgeOriginalHeight;
    /** ocr识别数据 */
    private OcrResult ocrResult;

}
