package com.iflytek.jzcpx.procuracy.tools.cont.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CatalogInfoDTO {

    @JsonProperty("catalog_info")
    private CatalogInfo catalogInfo;

    private int rc;

}