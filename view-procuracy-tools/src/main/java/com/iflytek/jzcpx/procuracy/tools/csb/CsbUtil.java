package com.iflytek.jzcpx.procuracy.tools.csb;


import com.alibaba.csb.sdk.ContentBody;
import com.alibaba.csb.sdk.HttpCaller;
import com.alibaba.csb.sdk.HttpCallerException;
import com.alibaba.csb.sdk.HttpParameters;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CsbUtil {

    private static final Logger logger = LoggerFactory.getLogger(CsbUtil.class);

    @Value("${csb.url}")
    private String requestURL;

    @Value("${csb.ak}")
    private String ak;

    @Value("${csb.sk}")
    private String sk;

    @Value("${csb.version}")
    private String csbVersion;

    @Value("${csb.apiName.file2DoublePdf}")
    private String apiNameFile2DoublePdf;

    @Value("${csb.version}")
    private String versionFile2DoublePdf;

    @Value("${csb.apiName.saveDZJZSBZT}")
    private String apiNameSaveDZJZSBZT;

    @Value("${csb.version}")
    private String versionSaveDZJZSBZT;


    @Value("${csb.apiName.cont.jzmlwj}")
    private String csb_apiName_cont_jzmlwj;//csb.apiName.cont.jzmlwj=dzjz-api-2003

    @Value("${csb.version}")
    private String csb_version_cont_jzmlwj; //csb.version.cont.jzmlwj=1.0.0

    @Value("${csb.apiName.cont.jzmlwj.jpg}")
    private String csb_apiName_cont_jzmlwj_jpg;////csb.apiName.cont.jzmlwj.jpg=dzjz-api-2004

    @Value("${csb.version}")
    private String csb_version_cont_jzmlwj_jpg; //csb.version.cont.jzmlwj.jpg=1.0.0

    @Value("${csb.apiName.cont.jzmlwj.save}")
    private String csb_apiName_cont_jzmlwj_save; //csb.apiName.cont.jzmlwj.save=dzjz-api-2005

    @Value("${csb.version}")
    private String csb_version_cont_jzmlwj_save; //csb.version.cont.jzmlwj.save=1.0.0

    @Value("${csb.apiName.ocr.dzjz.un_ocr}")
    private String csb_apiName_ocr_dzjz_un_ocr; //csb.apiName.ocr.dzjz.un_ocr=dzjz-api-2006

    @Value("${csb.version}")
    private String csb_version_ocr_dzjz_un_ocr; //csb.version.ocr.dzjz.un_ocr=1.0.0


    @Value("${csb.apiName.ocr.dzjz.jpg}")
    private String csb_apiName_ocr_dzjz_jpg; //csb.apiName.ocr.dzjz.jpg=dzjz-api-2007

    @Value("${csb.version}")
    private String csb_version_ocr_dzjz_jpg; //csb.version.ocr.dzjz.jpg=1.0.0

    @Value("${csb.apiName.ocr.dzjz.savepdf}")
    private String csb_apiName_ocr_dzjz_savepdf;  //csb.apiName.ocr.dzjz.savepdf=dzjz-api-2008

    @Value("${csb.version}")
    private String csb_version_ocr_dzjz_savepdf; //csb.version.ocr.dzjz.savepdf=1.0.0

    @Value("${csb.apiName.ocr.dzjz.saveOcrStatus}")
    private String csb_apiName_ocr_dzjz_saveOcrStatus; //csb.apiName.ocr.dzjz.saveOcrStatus=dzjz-api-2009

    @Value("${csb.version}")
    private String csb_version_ocr_dzjz_saveOcrStatus;  // csb.version.ocr.dzjz.saveOcrStatus=1.0.0

    @Value("${csb.apiName.card.ajxx}")
    private String apiName_GetAJXX;

    @Value("${csb.apiName.card.ajwswj}")
    private String apiName_GetAJWSWJ;

    @Value("${csb.apiName.card.ajwsjzwj}")
    private String apiName_GetAJWSJZWJ;

    /**
     * @param jsonString
     * @return
     */
    @SuppressWarnings("deprecation")
    public String doPostCsbApiNameContJzmlwj(String jsonString) {
        String result = null;
        String apiName = csb_apiName_cont_jzmlwj;
        String version = csb_version_cont_jzmlwj;
        try {
            result = HttpCaller.doPost(requestURL, apiName, version, new ContentBody(jsonString), ak, sk);
        } catch (HttpCallerException e) {
            logger.error("调用阿里云CSB错误：{}", e);
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @param jsonString
     * @return
     */
    @SuppressWarnings("deprecation")
    public String doPostCsbApiNameContJzmlwjJpg(String jsonString) {
        String result = null;

        String apiName = csb_apiName_cont_jzmlwj_jpg;
        String version = csb_version_cont_jzmlwj_jpg;
        try {
            result = HttpCaller.doPost(requestURL, apiName, version, new ContentBody(jsonString), ak, sk);
        } catch (HttpCallerException e) {
            logger.error("调用阿里云CSB错误：{}", e);
            e.printStackTrace();
        }
        return result;
    }

    @SuppressWarnings("deprecation")
    public String doPostCsbApiNameContJzmlwjSave(String jsonString) {
        String result = null;

        String apiName = csb_apiName_cont_jzmlwj_save;
        String version = csb_version_cont_jzmlwj_save;

        try {
            result = HttpCaller.doPost(requestURL, apiName, version, new ContentBody(jsonString), ak, sk);
        } catch (HttpCallerException e) {
            logger.error("调用阿里云CSB错误：{}", e);
            e.printStackTrace();
        }
        return result;
    }

    @SuppressWarnings("deprecation")
    public String doPostCsbApiNameOcrDzjzUn_ocr(String jsonString) {
        String result = null;

        String apiName = csb_apiName_ocr_dzjz_un_ocr;
        String version = csb_version_ocr_dzjz_un_ocr;

        try {
            result = HttpCaller.doPost(requestURL, apiName, version, new ContentBody(jsonString), ak, sk);
        } catch (HttpCallerException e) {
            logger.error("调用阿里云CSB错误：{}", e);
            e.printStackTrace();
        }
        return result;
    }

    @SuppressWarnings("deprecation")
    public String doPostCsbApiNameOcrDzjzJpg(String jsonString) {
        String result = null;
        String apiName = csb_apiName_ocr_dzjz_jpg;
        String version = csb_version_ocr_dzjz_jpg;
        try {
            result = HttpCaller.doPost(requestURL, apiName, version, new ContentBody(jsonString), ak, sk);
        } catch (HttpCallerException e) {
            logger.error("调用阿里云CSB错误：{}", e);
            e.printStackTrace();
        }
        return result;
    }

    @SuppressWarnings("deprecation")
    public String doPostCsbApiNameOcrDzjzSavepdf(String jsonString) {

        String apiName = csb_apiName_ocr_dzjz_savepdf;
        String version = csb_version_ocr_dzjz_savepdf;

        String result = null;
        try {
            result = HttpCaller.doPost(requestURL, apiName, version, new ContentBody(jsonString), ak, sk);
        } catch (HttpCallerException e) {
            logger.error("调用阿里云CSB错误：{}", e);
            e.printStackTrace();
        }
        return result;
    }


    @SuppressWarnings("deprecation")
    public String doPostCsbApiNameOcrDzjzSaveOcrStatus(String jsonString) {
        String result = null;
        String apiName = csb_apiName_ocr_dzjz_saveOcrStatus;
        String version = csb_version_ocr_dzjz_saveOcrStatus;
        try {
            result = HttpCaller.doPost(requestURL, apiName, version, new ContentBody(jsonString), ak, sk);
        } catch (HttpCallerException e) {
            logger.error("调用阿里云CSB错误：{}", e);
            e.printStackTrace();
        }
        return result;
    }

    /**
     * OCR mq接口中，获取批量ocr原始图片的接口
     *
     * @param jsonString 入参
     * @return
     */
    @SuppressWarnings("deprecation")
    public String getOCRFileInfo(String jsonString) {
        String result = null;
        try {
            result = HttpCaller.doPost(requestURL, apiNameFile2DoublePdf, versionFile2DoublePdf, new ContentBody(jsonString), ak, sk);
        } catch (HttpCallerException e) {
            logger.error("调用阿里云CSB错误：{}", e);
            e.printStackTrace();
        }
        return result;
    }

    /**
     * OCR mq接口中，保存OCR识别结果
     *
     * @param jsonString 入参
     * @return
     */
    @SuppressWarnings("deprecation")
    public String saveDZJZSBZT(String jsonString) {
        String result = null;
        try {
            result = HttpCaller.doPost(requestURL, apiNameSaveDZJZSBZT, versionSaveDZJZSBZT, new ContentBody(jsonString), ak, sk);
        } catch (HttpCallerException e) {
            logger.error("调用阿里云CSB错误：{}", e);
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 获取案件文书文件
     * @param body 请求体
     * @return 文件流
     */
    public byte[] getAJWSWJ(String body) {
        String resp = null;
        try {
            resp = HttpCaller.doPost(requestURL, apiName_GetAJWSWJ, csbVersion, new ContentBody(body), ak, sk);

            if (StringUtils.isBlank(resp)) {
                logger.info("获取案件文书文件响应结果为空, body: {}", body);
                return null;
            }
        }
        catch (Exception e) {
            logger.error("调用阿里云CSB, 获取案件文书卷宗文件错误, body: {}", body, e);
            return null;
        }

        try {
            JSONObject respJson = JSONObject.parseObject(resp);
            if (respJson.getBooleanValue("success")) {
                return respJson.getBytes("data");
            }
            else {
                logger.info("获取案件文书文件响应结果为失败. body: {}, resp: {}", body, resp);
            }
        }
        catch (Exception e) {
            logger.warn("获取案件文书文件响应内容解析错误, body: {}, resp: {}", body, resp, e);
        }
        return null;
    }

    /**
     * 获取案件文书卷宗文件
     *
     * @param body 请求体
     * @return wjmc: 文件名称; wjkzm: 文件扩展名;  wjl: 文件流(byte[])
     */
    public JSONObject getAJWSJZWJ(String body) {
        String resp = null;
        try {
            resp = HttpCaller.doPost(requestURL, apiName_GetAJWSWJ, csbVersion, new ContentBody(body), ak, sk);

            if (StringUtils.isBlank(resp)) {
                logger.info("获取案件文书卷宗文件响应结果为空, body: {}", body);
                return null;
            }
        }
        catch (Exception e) {
            logger.error("调用阿里云CSB, 获取案件文书卷宗文件错误, body: {}", body, e);
            return null;
        }

        try {
            JSONObject respJson = JSONObject.parseObject(resp);
            if (respJson.getBooleanValue("success")) {
                return respJson.getJSONObject("data");
            }
            else {
                logger.info("获取案件文书卷宗文件响应结果为失败. body: {}, resp: {}", body, resp);
            }
        }
        catch (Exception e) {
            logger.warn("获取案件文书卷宗文件响应内容解析错误, body: {}, resp: {}", body, resp, e);
        }
        return null;
    }

    /**
     * 获取案件信息
     *
     * @param body 请求体
     *
     */
    public JSONObject getAJXX(String body) {
        String resp = null;
        try {
            resp = HttpCaller.doPost(requestURL, apiName_GetAJXX, csbVersion, new ContentBody(body), ak, sk);
            if (StringUtils.isBlank(resp)) {
                logger.info("获取案件信息响应结果为空, body: {}", body);
                return null;
            }
        }
        catch (Exception e) {
            logger.error("调用阿里云CSB, 获取案件信息错误, body: {}", body, e);
            return null;
        }

        try {
            JSONObject respJson = JSONObject.parseObject(resp);
            if (respJson.getBooleanValue("success")) {
                return (JSONObject) respJson.getJSONArray("data").get(0);
            }
            else {
                logger.info("获取案件信息响应结果为失败. body: {}, resp: {}", body, resp);
            }
        }
        catch(Exception e){
            logger.warn("获取案件信息内容解析错误, body: {}, resp: {}", body, resp, e);
        }
        return null;
    }
    
    public String requestCsb(String body) {
	HttpParameters.Builder builder = new HttpParameters.Builder();
	builder.requestURL(requestURL).// 总部csb服务地址
		api(apiName_GetAJWSWJ).// 总部级联服务名
		version(csbVersion).method("post").accessKey(ak).// ak值
		secretKey(sk).// sk值
		putHeaderParamsMap("csbCasRouter", "");// 设置总部级联服务路由信息
	// 设置HTTP FORM表单请求参数
	//builder.putParamsMap("", "");
	builder.contentBody(new ContentBody(body));
	String ret = StringUtils.EMPTY;
	try {
	    ret = HttpCaller.invoke(builder.build());
	} catch (HttpCallerException e) {
	    logger.warn("调用csb异常, body: {}, ret: {}", body, ret, e);
	}
	return ret;
    }
}
