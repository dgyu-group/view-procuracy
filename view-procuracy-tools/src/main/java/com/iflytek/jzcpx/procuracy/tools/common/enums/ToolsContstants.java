package com.iflytek.jzcpx.procuracy.tools.common.enums;

/**
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-13 11:42
 */
public final class ToolsContstants {

    /** 引擎请求超时时间 */
    public static final int ENGINE_REQUEST_TIMEEOUT = 240000;
}
