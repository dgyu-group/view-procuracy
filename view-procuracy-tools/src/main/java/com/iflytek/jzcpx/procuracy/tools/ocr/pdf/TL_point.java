/* 
 *
 * Copyright (C) 1999-2012 IFLYTEK Inc.All Rights Reserved. 
 * 
 * FileName：BL_point.java
 * 
 * Description：
 * 
 * History：
 * Version   Author      Date            Operation 
 * 1.0	  zyzhang15   2018年4月3日下午5:31:49	       Create	
 */
package com.iflytek.jzcpx.procuracy.tools.ocr.pdf;

import javax.annotation.Generated;

import lombok.ToString;

/**
 * @author zyzhang15 
 *
 * @version 1.0
 * 
 */

@ToString
@Generated("http://www.stay4it.com")
public class TL_point {
    public Integer x;
    public Integer y;
    /** {@inheritDoc} */

}
