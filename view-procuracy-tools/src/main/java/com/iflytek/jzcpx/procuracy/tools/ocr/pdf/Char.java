
package com.iflytek.jzcpx.procuracy.tools.ocr.pdf;

import javax.annotation.Generated;

import lombok.Data;

@Generated("http://www.stay4it.com")
@Data
public class Char {

    private Rect rect;
    private Double score;
    private String value;

}
