package com.iflytek.jzcpx.procuracy.tools.cont.model;

public class ContCatalogResultNew {
    private CatalogState state;
    private CatalogInfoDTO tag;
	public CatalogState getState() {
		return state;
	}
	public void setState(CatalogState state) {
		this.state = state;
	}

    public CatalogInfoDTO getTag() {
        return tag;
    }

    public void setTag(CatalogInfoDTO tag) {
        this.tag = tag;
    }
}
