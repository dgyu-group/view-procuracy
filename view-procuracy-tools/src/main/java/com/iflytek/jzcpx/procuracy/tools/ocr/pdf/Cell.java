
package com.iflytek.jzcpx.procuracy.tools.ocr.pdf;

import javax.annotation.Generated;
import java.util.ArrayList;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
@Generated("http://www.stay4it.com")
public class Cell {

    private Integer col;
    private Integer colspan;
    private Rect rect;
    private Integer row;
    private Integer rowspan;
    
    private  ArrayList<Textline> textline=new ArrayList<>();
    @JSONField(name="textline_count")
    private Integer textlineCount;


}
