
package com.iflytek.jzcpx.procuracy.tools.ocr.pdf;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
@Generated("http://www.stay4it.com")
public class Page {

	@JSONField(name = "BlueSeal")
	private Object blueSeal;
	
	@JSONField(name = "Fingerprint")
	private Object finger;
	
	@JSONField(name = "Graphics")
	private Object graphics;
	
	@JSONField(name = "RedSeal")
	private Object redSeal;
	
	@JSONField(name = "Seal")
	private Object seal;
	
	@JSONField(name = "Table")
	private List<Table> table = new ArrayList<Table>();
	
	@JSONField(name = "TextRegion")
	private List<TextRegion> textRegion = new ArrayList<TextRegion>();
	
	private Double angle;
	@JSONField(name = "blue_seal_count")
	private Integer blueSealCount;
	
	@JSONField(name = "finger_count")
	private Integer fingerCount;
	
	@JSONField(name = "graphics_count")
	private Integer graphicsCount;
	private Integer id;
	private String label;
	private Rect rect;
	
	@JSONField(name = "red_seal_count")
	private Integer redSealCount;
	
	@JSONField(name = "seal_count")
	private Integer sealCount;
	
	@JSONField(name = "table_count")
	private Integer tableCount;
	
	@JSONField(name = "text_region_count")
	private Integer textRegionCount;

	@JSONField(name = "orientation")
	private Integer orientation;
}
