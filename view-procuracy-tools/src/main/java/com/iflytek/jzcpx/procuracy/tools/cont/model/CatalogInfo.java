package com.iflytek.jzcpx.procuracy.tools.cont.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CatalogInfo {

    private List<Catalog> catalog = new ArrayList<>();

    @JsonProperty("pages_num")
    private int pagesNum;

}