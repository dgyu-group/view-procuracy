package com.iflytek.jzcpx.procuracy.tools.ocr;

public interface OcrConstants {

    /**
     * 公用
     */
    class Common {
        /**
         * redis中key值分隔符
         */
        public static final String KEY_SEPARATOR = ":";
        /**
         * redis中 key过期时间
         */
        public static final Long KEY_EXPIRE_TIME = 30L;
    }

    /**
     * OCR 支持的图片格式
     */
    class OcrSupportExt {
        /**
         * OCR支持的图片格式
         */
        public static final String[] IMAGE_TYPES = {"bmp", "jpg", "png", "ppt", "tif"};
        /**
         * 支持bmp图片格式
         */
        public static final String BMP = "bmp";
        /**
         * 支持jpg图片格式
         */
        public static final String JPG = "jpg";
        /**
         * 支持png图片格式
         */
        public static final String PNG = "png";
        /**
         * 支持tif图片格式
         */
        public static final String TIF = "tif";
    }

    /**
     * 文件转换支持的格式类型
     */
    class SupportExt {
        /**
         * 支持转图片的源文件格式
         */
        public static final String[] SOURCE = {"pdf", "doc", "xls", "ppt", "docx", "xlsx", "pptx", "txt", "html", "tif", "tiff", "jpg", "jpeg", "bmp", "png"};
        /**
         * 支持转图片的office文件格式
         */
        public static final String[] OFFICE = {"doc", "xls", "ppt", "docx", "xlsx", "pptx", "txt", "html"};
        /**
         * 支持转图片的pdf文件格式
         */
        public static final String PDF = "pdf";
        /**
         * 支持转图片的word文件格式
         */
        public static final String DOC = "doc";
        /**
         * 支持转图片的word文件格式
         */
        public static final String DOCX = "docx";
        /**
         * 支持转图片的excel文件格式
         */
        public static final String XLS = "xls";
        /**
         * 支持转图片的excel文件格式
         */
        public static final String XLSX = "xlsx";
        /**
         * 支持转图片的ppt文件格式
         */
        public static final String PPT = "ppt";
        /**
         * 支持转图片的ppt文件格式
         */
        public static final String PPTX = "pptx";
        /**
         * 支持转目标图片格式
         */
        public static final String[] IMAGE = {"tif", "tiff", "jpg", "jpeg", "bmp", "png"};
        /**
         * 支持转目标图片格式
         */
        public static final String TIF = "tif";
        /**
         * 支持转图片的tiff文件格式
         */
        public static final String TIFF = "tiff";
        /**
         * 支持jpg格式，但不做转换
         */
        public static final String JPG = "jpg";
        /**
         * 支持jpg格式，但不做转换
         */
        public static final String JPEG = "jpeg";
        /**
         * 支持转图片的bmp文件格式
         */
        public static final String BMP = "bmp";
        /**
         * 支持转图片的png文件格式
         */
        public static final String PNG = "png";
        /**
         * 支持转目标图片格式
         */
        public static final String[] TARGET = {"jpg", "jpeg"};
    }

    class FileStoreType {
        /**
         * 本地存储
         */
        public static final String LOCAL = "local";
        /**
         * FastDFS存储
         */
        public static final String FAST_DFS = "FastDFS";
        /**
         * BYTES
         */
        public static final String BYTES = "BYTES";
    }

    /**
     * 度量指标名称
     */
    class Metrics {
        public static final String METER_BATCH_OCR_JK_TIME = "batchOcrJkTime";
        public static final String METER_OCR_PROCESS_TIME = "ocrProcessTime";
    }

    /**
     * OCR beanName
     */
    class OcrProcessBeanName {
        /**
         * 多图片处理
         */
        public static final String MULTI_IMAGEG_OCR_PROCESS = "multiImagesOcrProcessServiceImpl";
        /**
         * 批量文件
         */
        public static final String BATCH_FILES_OCR_PROCESS = "batchFilesOcrProcessServiceImpl";
    }

    /**
     * 字符串{trackId}
     */
    String STR_TRACK_ID = "{trackId}";
    /**
     * 图片文件临时保存路径
     */
    String STR_TEMP_PATH = "/temp/image/";

    /**
     * ocr结果临时保存路径
     */
    String STR_OCR_SAVE_PATH = "/temp/ocr/";

    /**
     * 字符串 picFile
     */
    String STR_PIC_FILE = "picFile";

    /**
     * 字符串 1
     */
    String STR_ONE = "1";
    /**
     * 字符串 2
     */
    String STR_TWO = "2";
    /**
     * 字符串 3
     */
    String STR_THREE = "3";
    /**
     * 字符串 4
     */
    String STR_FOUR = "4";
    /**
     * 字符串 5
     */
    String STR_FIVE = "5";
    /**
     * 字符串 6
     */
    String STR_SIX = "6";

    /**
     * 空字符串
     */
    String STR_EMPTY = "";


    String STR_TOKEN = "token:";

    String STR_LOCAL_HOST = "localhost";

    String STR_TOKEN_STAR = "token:*";

    String STR_MARK = ".";


    /**
     * ocr 结果结构
     * <p>
     * this is description
     * </p>
     *
     * @author mcxu2
     * @version 1.0
     * @since 2019年2月27日下午2:29:10
     */
    class ResultStruct {
        /**
         * 任务结果
         */
        public static final String TASK_RESULT = "task_result";
        /**
         * page
         */
        public static final String PAGE = "page";
        /**
         * 文本区域
         */
        public static final String TEXT_REGION = "TextRegion";
        /**
         * 表格
         */
        public static final String TABLE = "Table";
        /**
         * 单元格
         */
        public static final String CELL = "cell";
        /**
         * 签章
         */
        public static final String SEAL = "Seal";
        /**
         * 手印
         */
        public static final String FINGER_PRINT = "Fingerprint";
        /**
         * 插图Illustration
         */
        public static final String ILLUSTRATION = "Illustration";
        /**
         * 图片Graphics
         */
        public static final String GRAPHICS = "Graphics";
        /**
         * textline
         */
        public static final String TEXT_LINE = "textline";
        /**
         * sent
         */
        public static final String SENT = "sent";
        /**
         * value
         */
        public static final String VALUE = "value";
        /**
         * 手写体
         */
        public static final String HANDWRITE = "HANDWRITE";
        /**
         * 签名
         */
        public static final String SIGNATURE = "SIGNATURE";
    }

    class PDFCompose {

        public static final String SUCCESS = "1";
        public static final String FAILURE = "0";
        public static final String DATE_FORMATE = "yyyy-MM-dd_HH-mm-ss-SSS";
        public static final String NEW_OCR_VERSION = "new";
        public static final String OLD_OCR_VERSION = "old";
        /**
         * 服务最大排队数量
         */
        public static final int QUEUE_LENGTH = 20;
        /**
         * 单次请求最大文件个数
         */
        public static final int FILE_SIZE = 300;

        /**
         * 上传文件大小限制
         */
        public static final int FILE_LIMITET_CONTENT_SIZE = 1024 * 1024 * 10;
        /**
         * 文件需要压缩的临界值
         */
        public static final int FILE_COMPRESS_CRITICAL_SIZE = 1024 * 1024 * 5;

        public static final String PDF_FORMATE = "pdf";
        public static final String JPG_FORMATE = "jpg";
        public static final String PNG_FORMATE = "png";
        /**
         * 印章类型
         */
        public static final String OCR_SEAL_TYPE = "SEAL";
        /**
         * 打印体
         */
        public static final String OCR_MACHINEPRINT_TYPE = "MACHINEPRINT";
        /**
         * 手写体
         */
        public static final String OCR_HANDWRITE_TYPE = "HANDWRITE";

        public static final String DOUBLE_PDF_LIST = "doublePdfList";

        public static final String DOUBLE_PDF_MERGE = "doublePdfMerge";
    }

    class FileExportConstants {

        public static final String FILE_EXPORT_LIST = "fileExportList";

        public static final String FILE_EXPORT_MERGE = "fileExportMerge";

    }

    /**
     * 参数校验
     * <p>
     * this is description
     * </p>
     *
     * @author mcxu2
     * @version 1.0
     * @since 2019年2月27日下午6:43:56
     */
    class parameterVal {
        public static final String[] CONT_TYPES = {"1", "2", "3", "4"};
    }

    class BatchFile {

        /**
         * 批量文件消费队列--ocr
         */
        public static final String TOPIC = "BatchFileQueue";
        
        /**
         * 批量文件消费队列--图像识别
         */
        public static final String TOPIC_TXSB = "BatchFileQueueTXSB";
    }


    class StorageType {
        /**
         * 任务结果
         */
        public static final String OCR_FILE = "ocrFile";

        /**
         * 任务结果
         */
        public static final String OCR_BASE64 = "ocrBase64";
    }

    class WjsbRequiredType {
        public static final String Y = "y";
        public static final String N = "n";
    }

    class WjsbItemType {
        public static final String SBBT= "sbbt";
    }

}
