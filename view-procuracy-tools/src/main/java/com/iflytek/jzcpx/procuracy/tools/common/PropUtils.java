package com.iflytek.jzcpx.procuracy.tools.common;

import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.extra.spring.SpringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

public class PropUtils {
    protected static final Logger logger = LoggerFactory.getLogger(PropUtils.class);

    public static Boolean getBool(String key) {
        return getBool(key, null);
    }

    public static Boolean getBool(String key, Boolean defaultValue) {
        try {
            String value = getProp(key);
            if (CharSequenceUtil.isBlank(value)) {
                return defaultValue;
            }
            return Boolean.parseBoolean(value);
        }
        catch (Throwable e) {
            logger.warn("读取Boolean类型系统属性: {} 异常", key, e);
        }
        return defaultValue;
    }

    public static Integer getInt(String key) {
        return getInt(key, null);
    }

    public static Integer getInt(String key, Integer defaultValue) {
        try {
            String value = getProp(key);
            if (CharSequenceUtil.isBlank(value) || !CharSequenceUtil.isNumeric(value)) {
                return defaultValue;
            }
            return Integer.parseInt(value);
        }
        catch (Throwable e) {
            logger.warn("读取Int类型系统属性: {} 异常", key, e);
        }
        return defaultValue;
    }

    public static Float getFloat(String key) {
        return getFloat(key, null);
    }

    public static Float getFloat(String key, Float defaultValue) {
        try {
            String value = getProp(key);
            if (CharSequenceUtil.isBlank(value)) {
                return defaultValue;
            }
            return Float.parseFloat(value);
        }
        catch (Throwable e) {
            logger.warn("读取Float类型系统属性: {} 异常", key, e);
        }
        return defaultValue;
    }

    public static Long getLong(String key) {
        return getLong(key, null);
    }

    public static Long getLong(String key, Long defaultValue) {
        try {
            String value = getProp(key);
            if (CharSequenceUtil.isBlank(value) || !CharSequenceUtil.isNumeric(value)) {
                return defaultValue;
            }
            return Long.parseLong(value);
        }
        catch (Throwable e) {
            logger.warn("读取Long类型系统属性: {} 异常", key, e);
        }
        return defaultValue;
    }

    public static Double getDouble(String key) {
        return getDouble(key, null);
    }

    public static Double getDouble(String key, Double defaultValue) {
        try {
            String value = getProp(key);
            if (CharSequenceUtil.isBlank(value)) {
                return defaultValue;
            }
            return Double.parseDouble(value);
        }
        catch (Throwable e) {
            logger.warn("读取Double类型系统属性: {} 异常", key, e);
        }
        return defaultValue;
    }

    public static String[] getArray(String key) {
        return getArray(key, null);
    }

    public static String[] getArray(String key, String[] defaultValue) {
        try {
            String value = getProp(key);
            if (CharSequenceUtil.isBlank(value)) {
                return defaultValue;
            }
            return CharSequenceUtil.splitToArray(value, ",");
        }
        catch (Throwable e) {
            logger.warn("读取String[]类型系统属性: {} 异常", key, e);
        }
        return defaultValue;
    }

    public static String getProp(String key) {
        return getProp(key, null);
    }

    /**
     * 读取系统属性, 顺序: {@link Environment#getProperty(String, String)}
     * > {@link System#getProperty(String, String)} > {@link System#getenv(String) }
     *
     * @see Environment#getProperty(String, String)
     * @see System#getProperty(String, String)
     * @see System#getenv(String) 
     *
     * @param key          属性键
     * @param defaultValue 默认值
     *
     * @return
     */
    public static String getProp(String key, String defaultValue) {
        if (CharSequenceUtil.isBlank(key)) {
            return defaultValue;
        }

        String value = null;
        try {
            Environment environment = SpringUtil.getBean(Environment.class);
            value = null;
            if (environment != null) {
                value = environment.getProperty(key, defaultValue);
            }
            else {
                value = System.getProperty(key, defaultValue);
                value = value == null ? System.getenv(key) : defaultValue;
            }
        }
        catch (Throwable e) {
            logger.warn("系统属性读取异常, key: {}", key, e);
        }

        return value == null ? defaultValue : value;
    }
}