
package com.iflytek.jzcpx.procuracy.tools.ocr.pdf;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
@Generated("http://www.stay4it.com")
public class Table {

    private List<Cell> cell = new ArrayList<Cell>();
    private Integer id;
    private Rect rect;
    private String type;

  }
