package com.iflytek.jzcpx.procuracy.tools.common.enums;

import com.iflytek.jzcpx.procuracy.common.enums.ResultType;

/**
 * 引擎能力结果枚举，1xxx
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-07 09:10
 */
public enum EngineResultEnum implements ResultType {
    /* ========================= 11xx OCR能力错误 =========================*/
    OCR_REQUEST_EXCEPTION(1101, "请求ocr引擎异常"),

    /* ========================= 12xx 自动编目能力错误 =========================*/
    CONT_REQUEST_EXCEPTION(1201, "请求ocr引擎异常"),

    /* ========================= 13xx 要素抽取能力错误 =========================*/
    ELLE_REQUEST_EXCEPTION(1301, "请求ocr引擎异常"),

    ;

    private Integer code;
    private String desc;

    EngineResultEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static EngineResultEnum getByCode(Integer code) {
        if (code == null) {
            return null;
        }

        for (EngineResultEnum anEnum : values()) {
            if (anEnum.getCode().equals(code)) {
                return anEnum;
            }
        }

        return null;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getDesc() {
        return desc;
    }}