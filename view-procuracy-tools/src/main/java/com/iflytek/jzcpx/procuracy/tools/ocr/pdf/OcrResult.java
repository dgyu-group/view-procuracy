
package com.iflytek.jzcpx.procuracy.tools.ocr.pdf;

import javax.annotation.Generated;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
@Generated("http://www.stay4it.com")
public class OcrResult {
	
	@JSONField(name="task_result")
    private TaskResult taskResult;

}
