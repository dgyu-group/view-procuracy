package com.iflytek.jzcpx.procuracy.tools.cont.model;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

/**
 * 自动编目协议json对应实体
 * 
 * @author lli
 *
 * @version 1.0
 * 
 */
@Data
public class AcDto {

	/**
	 * 文件id
	 */
	private String id;
	/**
	 * 文件ocr识别结果
	 */
	private JSONObject ocr_result;

	public AcDto() {
		super();
	}

	public AcDto(String id, JSONObject ocr_result) {
		super();
		this.id = id;
		this.ocr_result = ocr_result;
	}
}
