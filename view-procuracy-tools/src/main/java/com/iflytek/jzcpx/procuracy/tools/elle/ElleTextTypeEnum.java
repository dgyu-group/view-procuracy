package com.iflytek.jzcpx.procuracy.tools.elle;

import org.apache.commons.lang3.StringUtils;

/**
 * 待抽取文书类型
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019/8/20 14:14
 */
public enum ElleTextTypeEnum {

    /** 起诉意见书 */
    qsyjs(1,"一审公诉案件"),

    /** 提请批准逮捕书 */
    tqpzdbs(2,"审查逮捕案件"),

    /** 判决书 */
    pjs(1,"一审公诉案件"),

    /** 批准逮捕决定书 */
    pzdbjds(2,"审查逮捕案件"),

    /** 不批准逮捕决定书 */
    bpzdbjds(2,"审查逮捕案件"),

    /** 起诉书 */
    qss(1,"一审公诉案件"),

    /** 不起诉决定书 */
    bqsjds(2,"审查逮捕案件"),
    
    /** 不起诉决定书 */
    unknown(3,"未知"),
    ;
	private int caseCatrgory;
	private String caseCatrgoryMean;
	
	public int getCaseCatrgory() {
		return caseCatrgory;
	}
	public String getCaseCatrgoryMean() {
		return caseCatrgoryMean;
	}
	ElleTextTypeEnum(int caseCatrgory,String caseCatrgoryMean){
		this.caseCatrgory=caseCatrgory;
		this.caseCatrgoryMean=caseCatrgoryMean;
	}
    public static ElleTextTypeEnum fromName(String name) {
        if (StringUtils.isBlank(name)) {
            return null;
        }

        for (ElleTextTypeEnum anEnum : values()) {
            if (anEnum.name().equals(name)) {
                return anEnum;
            }
        }

        return null;
    }

    /**
     * 根据赛威讯文书类型返回对应的 elle 文书类型
     *
     * @param wslx 赛威讯文书类型
     *
     * @return
     */
    public static ElleTextTypeEnum fromWslx(String wslx) {
        if (StringUtils.isBlank(wslx)) {
            return null;
        }

        switch (wslx) {
            case "0":
                return qsyjs;
            case "1":
                return tqpzdbs;
            default:
                return null;
        }
    }
}
