
package com.iflytek.jzcpx.procuracy.tools.ocr.pdf;

import javax.annotation.Generated;

import lombok.Data;

@Data
@Generated("http://www.stay4it.com")
public class Rect {

    public Integer h;
    public Integer w;
    public Integer x;
    public Integer y; 
    /** 左下坐标*/
    public BL_point bl_point;
    /** 右下坐标*/
    public BR_point br_point;
    /** 左上坐标*/
    public TL_point tl_point;
    /** 右上坐标*/
    public TR_point tr_point;


}
