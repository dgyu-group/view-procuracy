import javax.imageio.ImageIO;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.BiConsumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.thread.ThreadFactoryBuilder;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Sequence;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.iflytek.jzcpx.procuracy.card.common.FileUtils;
import com.iflytek.jzcpx.procuracy.common.util.IdWokerUtil;
import com.iflytek.jzcpx.procuracy.common.util.JSONUtil;
import com.iflytek.jzcpx.procuracy.common.util.StopWatch;
import com.iflytek.jzcpx.procuracy.ocr.common.helper.DzjzDataDTO;
import com.iflytek.jzcpx.procuracy.ocr.entity.OcrFile;
import com.iflytek.jzcpx.procuracy.ocr.recognize.model.OcrResultRequestVo;
import com.iflytek.jzcpx.procuracy.tools.ocr.OcrConstants;
import lombok.Data;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

/**
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019/8/19 21:00
 */
public class CommonTest {
    @Test
    public void round() {
        Lists.newArrayList(98, 99, 100, 101, 102).stream().forEach(integer -> {
            System.out.println(integer + " " + (int) Math.ceil(integer / 10f));
        });
    }

    @Test
    public void mod() {
        ArrayList<Integer> list = Lists.newArrayList(0, 1, 2, 3, 4, 5, 6);
        int size = list.size();
        int step = 2;
        int start, end = 0;
        for (int i = 0; i < Math.ceil(size / (float) step); i++) {
            start = i * step;
            end = Math.min(size, start + step);
            System.out.println(list.subList(start, end));
        }
        System.out.println("-----------");

        int lastIndex = 0;
        do {
            int endIndex = Math.min(lastIndex + step, size);
            System.out.println(lastIndex + " " + endIndex);
            System.out.println(list.subList(lastIndex, endIndex));

            lastIndex = endIndex;
        } while (lastIndex <= size - 1);
        System.out.println(lastIndex + " - " + size);
    }

    @Test
    public void function() {
        OcrFile file = new OcrFile();
        BiConsumer<OcrFile, Long> taskId = OcrFile::setOcrTaskId;
        Function<OcrFile, Long> getOcrTaskId = OcrFile::getOcrTaskId;
        taskId.accept(file, 1L);
        System.out.println(getOcrTaskId.apply(file));
    }

    @Test
    public void id() {
        Sequence sequence = new Sequence(0, 0);
        System.out.println(sequence.nextId());
        System.out.println(Long.MAX_VALUE);
    }

    @Test
    public void date() {
        LocalDateTime dateTime = LocalDateTime.of(2022, 1, 1, 0, 0, 0, 0);
        System.out.println(dateTime.atZone(ZoneOffset.ofHours(8)).toEpochSecond());
    }

    @Test
    public void test() throws Exception {
        FileInputStream input = new FileInputStream("/Users/yikaitang/work/讯飞/高检 2.0/现场问题排查/安徽/getDzjzJpg.raw");
        byte[] bytes = IOUtils.toByteArray(input);
        String string = IOUtils.toString(bytes, "UTF-8");

        org.apache.commons.io.FileUtils.write(
                new File("/Users/yikaitang/work/讯飞/高检 2.0/现场问题排查/安徽/getDzjzJpg.txt"), string);

        String resp = HttpUtil.post("http://mock.iflytek.com/http/mock/007YJ/GJ2.0/api/ajDzjz/GetDZJZJPG2", "{}");
        DzjzDataDTO dto = new DzjzDataDTO(resp.getBytes());
        System.out.println(dto.getMessage());
        DzjzDataDTO dto2 = new DzjzDataDTO(bytes);
        System.out.println(dto2.getMessage());
    }

    @Test
    public void stopWatch() throws InterruptedException {
        StopWatch w1 = new StopWatch();
        w1.start();
        TimeUnit.MILLISECONDS.sleep(RandomUtils.nextInt(10, 100));
        w1.start("123");
        TimeUnit.MILLISECONDS.sleep(RandomUtils.nextInt(10, 100));
        w1.start("321");
        TimeUnit.MILLISECONDS.sleep(RandomUtils.nextInt(10, 100));
        System.out.println(w1.prettyPrint(TimeUnit.MILLISECONDS));
        System.out.println(w1.shortSummary());
        System.out.println(w1);
    }

    @Test
    public void base64Card() throws IOException {
        String filepath = "/Users/tonnyyi/Downloads/demo/test/单人起诉意见书/起诉意见书汪金秀2.jpg";
        String base64Str = Base64.encodeBase64String(IOUtils.toByteArray(new FileInputStream(filepath)));
        JSONObject json = new JSONObject();
        json.put("base64", base64Str);
        org.apache.commons.io.FileUtils.write(new File(filepath + ".json"), json.toJSONString());
    }

    @Test
    public void getWJSBJG() {
        OcrResultRequestVo vo = new OcrResultRequestVo();
        vo.setSfwjnr(true);
        vo.setSfwjwb(true);
        vo.setSfwjxx(true);
        vo.setSfjp(true);
        vo.setSfxz(true);
        vo.setNofile(true);

        String path = "/Users/tonnyyi/Downloads/demo/test/02.png";
        try (FileInputStream fis = new FileInputStream(path); FileOutputStream pdfFos = new FileOutputStream(
                path + ".pdf"); FileOutputStream jsonFos = new FileOutputStream(path + ".json")) {

            byte[] bytes = IoUtil.readBytes(fis);
            vo.setWjhz("." + StringUtils.substringAfterLast(path, "."));
            vo.setWjnr(Base64.encodeBase64String(bytes));

            String respStr = HttpUtil.post("http://localhost:9192/procuracy-web/iflytek/api/GetWJOCRSBJG",
                                           JSONObject.toJSONString(vo));

            JSONObject resp = JSONObject.parseObject(respStr);
            String pdfnr = resp.getJSONObject("data").getString("pdfnr");
            byte[] pdfBytes = Base64.decodeBase64(pdfnr);
            IOUtils.write(pdfBytes, pdfFos);

            resp.getJSONObject("data").remove("pdfnr");
            resp.getJSONObject("data").getJSONArray("wjsbxx").getJSONObject(0).remove("wjnr");
            IOUtils.write(resp.toJSONString(), jsonFos);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    private DateTimeFormatter pattern = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

    class PriorityFuture<T> implements RunnableFuture<T> {

        private RunnableFuture<T> src;
        private int priority;

        public PriorityFuture(RunnableFuture<T> other, int priority) {
            this.src = other;
            this.priority = priority;
        }

        public int getPriority() {
            return priority;
        }

        public boolean cancel(boolean mayInterruptIfRunning) {
            return src.cancel(mayInterruptIfRunning);
        }

        public boolean isCancelled() {
            return src.isCancelled();
        }

        public boolean isDone() {
            return src.isDone();
        }

        public T get() throws InterruptedException, ExecutionException {
            return src.get();
        }

        public T get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
            return src.get();
        }

        public void run() {
            src.run();
        }
    }

    class PriorityFutureComparator implements Comparator<Runnable> {
        public int compare(Runnable o1, Runnable o2) {
            if (o1 == null && o2 == null) {
                return 0;
            }
            else if (o1 == null) {
                return -1;
            }
            else if (o2 == null) {
                return 1;
            }
            else {
                int p1 = ((PriorityFuture<?>) o1).getPriority();
                int p2 = ((PriorityFuture<?>) o2).getPriority();

                return p2 - p1;
            }
        }
    }

    private class requestTask<V> implements Callable<V> {
        private int priority;

        public int getPriority() {
            return priority;
        }

        public requestTask(int priority) {
            this.priority = priority;
        }

        @Override
        public V call() throws Exception {
            int millis = RandomUtils.nextInt(300, 500);
            Thread.sleep(millis);
            System.out.println(Thread.currentThread().getName() + " : " + this.priority);
            return null;
        }
    }

    @Test
    public void priorityQueue() throws ExecutionException, InterruptedException {
        ExecutorService exec = new ThreadPoolExecutor(2, 2, 0L, TimeUnit.MILLISECONDS,
                                                      new PriorityBlockingQueue<Runnable>(100,
                                                                                          new PriorityFutureComparator())) {

            protected <T> RunnableFuture<T> newTaskFor(Callable<T> callable) {
                RunnableFuture<T> newTaskFor = super.newTaskFor(callable);
                return new PriorityFuture<T>(newTaskFor, ((requestTask<Object>) callable).getPriority());
            }
        };

        ThreadPoolExecutor pool = new ThreadPoolExecutor(2, 2, 0, TimeUnit.MINUTES,
                                                         new PriorityBlockingQueue<>(100, new Comparator<Object>() {
                                                             @Override
                                                             public int compare(Object o1, Object o2) {
                                                                 return 0;
                                                             }
                                                         }), ThreadFactoryBuilder.create()
                                                                                 .setNamePrefix("OcrEngine-Requester-")
                                                                                 .build(),
                                                         new ThreadPoolExecutor.CallerRunsPolicy());

        for (int i = 0; i < 10; i++) {
            int priority = RandomUtils.nextInt(1, 30);
            System.out.println(priority);
            exec.submit(new requestTask<>(priority));
        }

        Thread.sleep(10000);
    }

    @Test
    public void stream() {
        int size = 100;
        Integer[] ints = new Integer[size];
        for (int i = 0; i < size; i++) {
            if (RandomUtils.nextBoolean()) {
                ints[i] = i;
            }
        }
        List<Integer> intList = Arrays.stream(ints).filter(Objects::nonNull).collect(Collectors.toList());
        System.out.println(JSONUtil.toStrDefault(intList));
        intList.stream().forEach(i -> System.out.print(i + ","));

        List<Integer> collect = intList.stream().filter(i -> i > 100).collect(Collectors.toList());
        System.out.println(collect);

        List<UserTestBean> users = Stream.generate(() -> {
            return new UserTestBean(RandomStringUtils.random(1, 'a', 'b', 'c', 'd', 'e', 'f', 'g'), null,
                                    RandomUtils.nextInt(1, 3));
        }).limit(10).collect(Collectors.toList());
        List<UserTestBean> sortedUser = users.stream().sorted((a, b) -> {
            String an = a.getName();
            int al = a.getLevel();
            String bn = b.getName();
            int bl = b.getLevel();
            return al - bl == 0 ? an.compareTo(bn) : al - bl;
        }).collect(Collectors.toList());
        System.out.println(JSONUtil.toStrDefault(sortedUser));

        List<List<String>> doubleList = Stream.generate(() -> {
            return Stream.generate(() -> RandomStringUtils.randomAlphanumeric(3)).limit(3).collect(Collectors.toList());
        }).limit(3).collect(Collectors.toList());
        System.out.println(JSONUtil.toStrDefault(doubleList));
        List<String> list = doubleList.stream().flatMap(l -> l.stream()).collect(Collectors.toList());
        System.out.println(JSONUtil.toStrDefault(list));
    }

    @Test
    public void md5() throws IOException {
        String pdfM = "/Users/tonnyyi/work/讯飞/图文平台/高检 2.0/文书/1000份文书/司法文书解析平台用例/5、起诉意见书30份/刑事案件30份/邗公（上）诉字〔2018〕200号" +
                      ".pdf";
        try (FileInputStream fis = new FileInputStream(pdfM)) {
            byte[] bytes = IOUtils.toByteArray(fis);
            String pdfBase64 = Base64.encodeBase64String(bytes);
            OcrResultRequestVo vo = new OcrResultRequestVo();
            vo.setWjnr(pdfBase64);
            System.out.println(JSONUtil.toStrDefault(vo));
        }
    }

    @Test
    public void ocrTest() throws IOException {
        JSONObject requestBody = new JSONObject();
        requestBody.put("wjbh", "ED50D584F3DA2179DED71253333F0B01");
        requestBody.put("sfwjnr", true);
        requestBody.put("sfwjwb", true);
        requestBody.put("sfwjxx", false);
        requestBody.put("sfjp", false);
        requestBody.put("sfxz", false);
        requestBody.put("nofile", false);

        String path = "/Users/tonnyyi/work/讯飞/图文平台/ocr开发/ocr/图片/test.jpg";
        // String path = "/Users/tonnyyi/work/讯飞/图文平台/ocr开发/ocr/多页 pdf/test.pdf";
        // String path = "/Users/tonnyyi/work/讯飞/图文平台/ocr开发/ocr/单页 pdf/test.pdf";
        File file = new File(path);
        String extension = FilenameUtils.getExtension(file.getName());
        requestBody.put("wjhz", "." + extension);
        byte[] bytes = org.apache.commons.io.FileUtils.readFileToByteArray(file);
        String md5 = DigestUtils.md5Hex(bytes);
        requestBody.put("md5", md5);
        requestBody.put("wjnr", Base64.encodeBase64String(bytes));
        System.out.println(requestBody.toJSONString());
    }

    @Test
    public void pattern() {
        Pattern pattern = Pattern.compile("([零壹贰叁肆伍陆柒捌玖拾0-9]+)[卷册张]?");

        List<String> strings = Lists.newArrayList("壹卷", "拾壹卷", "贰册", "拾贰册", "壹卷贰册", "壹卷拾贰册", "壹卷册", "卷贰册", "卷册",
                                                  "2卷15页", "2卷页", "3卷", "壹张", "张", "");
        for (String s : strings) {
            Matcher matcher = pattern.matcher(s);

            String r = "";
            System.out.print(s + "  ");
            while (matcher.find()) {
                String group = matcher.group(0);

                r = matcher.group(1);
                if (group.endsWith("册")) {
                    break;
                }
            }
            System.out.println(r);
        }
    }

    @Test
    public void wsmbbm() throws Exception {
        // 获取所有单位编码
        String allUnitResp = HttpUtil.post("http://143.176.22.167:8080/zzjg-service/api/org/unit/findAll", "");
        JSONObject allUnitJson = JSONObject.parseObject(allUnitResp);

        JSONArray result = new JSONArray();

        // 获取单位下的文书模板
        JSONArray units = allUnitJson.getJSONArray("data");
        for (Object unit : units) {
            JSONObject unitJson = (JSONObject) unit;
            String dwbm = unitJson.getString("dwbm");
            Object dwmc = unitJson.get("dwmc");

            System.out.println("获取 " + dwmc + " 下的文书模板");
            JSONObject param = new JSONObject();
            param.put("dwbm", dwbm);
            String mbResp = HttpUtil.post(
                    "http://143.176.22.167:8080/ajmx-service/api/modelExternal" + "/getDocumentFolderAndTemplateByDwbm",
                    param.toJSONString());
            JSONObject mbRespJson = JSONObject.parseObject(mbResp);
            JSONArray mbArray = mbRespJson.getJSONArray("data");
            if (!mbArray.isEmpty()) {
                JSONObject unitData = new JSONObject();
                unitData.put("dwmc", dwmc);
                unitData.put("dwbm", dwbm);
                JSONArray mbs = new JSONArray();
                unitData.put("wsmb", mbs);
                result.add(unitData);

                for (Object mb : mbArray) {
                    JSONObject mbJson = (JSONObject) mb;
                    JSONObject mbData = new JSONObject();
                    mbData.put("wsmbbh", mbJson.getString("wsmbbh"));
                    mbData.put("wsmbmc", mbJson.getString("wsmbmc"));
                    mbs.add(mbData);
                }
            }
        }

        IOUtils.write(result.toJSONString(),
                      new FileOutputStream("C:\\Users\\Administrator\\Desktop\\ktyi\\wmbm.json"));
    }

    @Test
    public void docContent() {
        try (InputStream is = new FileInputStream("/Users/tonnyyi/Downloads/demo/test/单人起诉意见书/多人_犯罪提请批准逮捕书.docx")) {
            String content = FileUtils.extractWordText(is);
            JSONObject json = new JSONObject();
            json.put("protocol_level", "2");
            JSONArray texts = new JSONArray();
            JSONObject text = new JSONObject();
            texts.add(text);
            System.out.println(content);
            text.put("content", content);
            text.put("type", "qss");
            json.put("texts", texts);
            json.put("trackId", String.valueOf(System.currentTimeMillis()));
            json.put("usrId", "aktq");
            json.put("rlt_switch", true);

            System.out.println(json.toJSONString());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void base64() {
        System.out.println(new String(Base64.decodeBase64("eb67e201ede343999ff31d389a031f84")));
    }

    @Test
    public void decimal() {
        BigDecimal bigDecimal = new BigDecimal("432.132");
        System.out.println(bigDecimal);
    }

    @Test
    public void allChineseNum() throws IOException {
        Files.walkFileTree(Paths.get("/Users/tonnyyi/work/讯飞/图文平台/高检 2.0/文书/1000份文书/司法文书解析平台用例"),
                           new SimpleFileVisitor<Path>() {
                               @Override
                               public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                                       throws IOException {
                                   System.out.println("正在访问：" + dir + " 目录");
                                   return FileVisitResult.CONTINUE;
                               }

                               @Override
                               public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
                                       throws IOException {
                                   if (!StringUtils.endsWithAny(file.toString().toLowerCase(), "doc", "docx")) {
                                       return FileVisitResult.CONTINUE;
                                   }

                                   if (StringUtils.endsWithAny(file.toString().toLowerCase(), "doc", "docx")) {
                                       try (FileInputStream fis = new FileInputStream(file.toFile())) {
                                           String content = FileUtils.extractWordText(fis);

                                           String[] strings = StringUtils.split(content, "元");
                                           if (ArrayUtils.isNotEmpty(strings)) {
                                               for (int i = 0; i < strings.length; i++) {
                                                   if (i == strings.length - 1) {
                                                       continue;
                                                   }
                                                   System.out.println(StringUtils.substring(strings[i], -10) + "元");
                                               }
                                           }
                                       }
                                       catch (Exception e) {
                                           System.out.println(file.toString() + " read failed: " + e.getMessage());
                                           return FileVisitResult.CONTINUE;
                                       }
                                   }

                                   return FileVisitResult.CONTINUE;
                               }
                           });
    }

    @Test
    public void extract() {
        String path = "/Users/tonnyyi/work/讯飞/图文平台/高检 2.0/引擎问题";
        try (Stream<Path> walk = Files.walk(Paths.get(path))) {
            walk.filter(Files::isRegularFile).filter(f -> StringUtils.endsWithAny(f.toString(), "doc", "docx")).forEach(
                    f -> {
                        System.out.println(f);

                        try (FileInputStream fis = new FileInputStream(f.toFile())) {
                            String content = FileUtils.extractWordText(fis);
                            IOUtils.write(content, new FileOutputStream(
                                    StringUtils.substringBeforeLast(f.toAbsolutePath().toString(), ".") + ".txt"));
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                    });
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void regUtil() {
        Matcher matcher = Pattern.compile("\\d+(\\.\\d+)?").matcher("42343");
        while (matcher.find()) {
            System.out.println(matcher.group());
        }
        System.out.println(
                RegExUtils.removeAll(RegExUtils.removeAll("山东省济南市历城  区郭店镇西枣园115号，", "^[：，。,.:]"), "[：，。,.:]$"));
        System.out.println(RegExUtils.removeAll("：    犯罪嫌疑人段立祥  1", "[：，。,.:  ]"));
    }

    @Test
    public void idGenTest() {
        int size = 1000_0000;
        long start1 = System.currentTimeMillis();
        for (int i = 0; i < size; i++) {
            IdWokerUtil.nextId();
        }
        System.out.println(System.currentTimeMillis() - start1);    //1万: 242ms, 10万: 291ms, 100万: 447ms, 1000万: 2809ms

        long start2 = System.currentTimeMillis();
        for (int i = 0; i < size; i++) {
            UUID.randomUUID();
        }
        System.out.println(System.currentTimeMillis() - start2);    //1万: 60ms, 10万: 205ms, 100万: 1148ms, 1000万: 10319ms
    }

    @Test
    public void reg() {
        System.out.println(RegExUtils.removeAll("340244198010204357.", "[^0-9a-zA-Z]"));
    }

    @Test
    public void getwswj() {
        String url = "http://192.168.2.119/ws-service/api/caseDoc/getfileInputStream";
        Map<String, Object> params = new HashMap<>();

        try (FileInputStream is = new FileInputStream("D:\\iflytek\\src\\图文平台\\入卷消息内容.txt")) {
            List<String> lines = IOUtils.readLines(is);
            for (String line : lines) {
                JSONObject lineJson = JSON.parseObject(line);
                JSONArray wslb = lineJson.getJSONArray("wslb");
                boolean hasData = false;
                for (Object ws : wslb) {
                    String wsslbh = ((JSONObject) ws).getString("wsslbh");
                    params.put("wsslbh", wsslbh);
                    String resp = HttpUtil.post(url, JSONUtil.toStrDefault(params), 30000);
                    if (StringUtils.isNotBlank(resp)) {
                        JSONObject respJSON = JSONObject.parseObject(resp);
                        String data = respJSON.getString("data");
                        hasData = respJSON.getBooleanValue("success") && StringUtils.isNotBlank(data);
                    }
                }

                if (hasData) {
                    System.out.println(line);
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void swxSwagger() {
        String apis = "{\n" + "\"电子卷宗\": \"http://192.168.2.119/dzjz-service/v2/api-docs\",\n" +
                      "\"电子卷宗2.0\": \"http://192.168.2.119/dzjzzz-service/v2/api-docs\",\n" +
                      "\"文书服务\": \"http://192.168.2.119/ws-service/v2/api-docs\",\n" +
                      "\"组织机构\": \"http://192.168.2.119/zzjg-service/v2/api-docs\",\n" +
                      "\"文书函数数据源\": \"http://192.168.2.119/ws-func-service/v2/api-docs\",\n" +
                      "\"消息系统\": \"http://192.168.2.119/mqxx-service/v2/api-docs\",\n" +
                      "\"元数据\": \"http://192.168.2.119/ysj-service/v2/api-docs\",\n" +
                      "\"任务服务\": \"http://192.168.2.119/rw-service/v2/api-docs\",\n" +
                      "\"刑事案件办理\": \"http://192.168.2.119/xsajbl-service/v2/api-docs\",\n" +
                      "\"流程配置\": \"http://192.168.2.119/lcpz-service/v2/api-docs\",\n" +
                      "\"流程引擎\": \"http://192.168.2.119/lcyq-service/v2/api-docs\",\n" +
                      "\"审批管理\": \"http://192.168.2.119/spgl-service/v2/api-docs\",\n" +
                      "\"轮案管理\": \"http://192.168.2.119/lagl-service/v2/api-docs\",\n" +
                      "\"案件管理\": \"http://192.168.2.119/ajgl-service/v2/api-docs\",\n" +
                      "\"要号管理\": \"http://192.168.2.119/yhgl-service/v2/api-docs\",\n" +
                      "\"表单服务\": \"http://192.168.2.119/bd-service/v2/api-docs\",\n" +
                      "\"检察内卷\": \"http://192.168.2.119/jcnj-service/v2/api-docs\",\n" +
                      "\"规则校验\": \"http://192.168.2.119/gzjy-service/v2/api-docs\",\n" +
                      "\"案件模型\": \"http://192.168.2.119/ajmx-service/v2/api-docs\",\n" +
                      "\"文件访问服务\": \"http://192.168.2.119/wjfw-service/v2/api-docs\",\n" +
                      "\"日志管理\": \"http://192.168.2.119/log-manager-service/v2/api-docs\",\n" +
                      "\"刑执服务\": \"http://192.168.2.119/xz-service/v2/api-docs\",\n" +
                      "\"数据读取\": \"http://192.168.2.119/sjdq-service/v2/api-docs\",\n" +
                      "\"数据读取(高检)\": \"http://192.168.2.119/sjdq-service-gj/v2/api-docs\",\n" +
                      "\"数据读取(省院)\": \"http://192.168.2.119/sjdq-service-sy/v2/api-docs\",\n" +
                      "\"数据交换(高检)\": \"http://192.168.2.119/sjjh-service-gj/v2/api-docs\",\n" +
                      "\"数据交换(省院)\": \"http://192.168.2.119/sjjh-service-sy1-dev/v2/api-docs\",\n" +
                      "\"专项活动\": \"http://192.168.2.119/zxhd-service/v2/api-docs\",\n" +
                      "\"文书提取服务\": \"http://192.168.2.119/wstq-service/v2/api-docs\",\n" +
                      "\"前端配置服务\": \"http://192.168.2.119/qdpz-service/v2/api-docs\",\n" +
                      "\"风险评估\": \"http://192.168.2.119/fxpg-service/v2/api-docs\",\n" +
                      "\"涉案财物\": \"http://192.168.2.119/sacw-service/v2/api-docs\",\n" +
                      "\"超期预警\": \"http://192.168.2.119/cqyj-service/v2/api-docs\",\n" +
                      "\"事项服务\": \"http://192.168.2.119/sx-service/v2/api-docs\",\n" +
                      "\"检务督察\": \"http://192.168.2.119/jwdc-service/v2/api-docs\",\n" +
                      "\"后台管理\": \"http://192.168.2.119/htgl-service/v2/api-docs\",\n" +
                      "\"自动编目\": \"http://192.168.2.119/zdbm-service/v2/api-docs\",\n" +
                      "\"辩护代理\": \"http://192.168.2.119/bhdl-service/v2/api-docs\",\n" +
                      "\"案件公开\": \"http://192.168.2.119/ajgk-service/v2/api-docs\",\n" +
                      "\"检委办\": \"http://192.168.2.119/jwb-service/v2/api-docs\",\n" +
                      "\"流程监控\": \"http://192.168.2.119/lcjk-service/v2/api-docs\",\n" + "}";
        JSONObject apiJSON = JSON.parseObject(apis);
        for (Map.Entry<String, Object> entry : apiJSON.entrySet()) {
            String name = entry.getKey();
            String url = (String) entry.getValue();
            File file = new File("D:\\iflytek\\src\\图文平台\\赛威讯接口JSON\\" + name + ".json");
            String resp = HttpUtil.get(url);

            if (resp.contains("获取案件文书卷宗文件")) {
                // /api/caseDoc/getWorkingDocList
                System.out.println(name + " " + url);
            }
        }
    }

    @Test
    public void wps() {
        File file = new File("D:\\iflytek\\src\\view-procuracy\\view-procuracy-web\\target\\classes\\wsmb.xlsx");
        try (InputStream is = new FileInputStream(file)) {
            Workbook wb = WorkbookFactory.create(is);
            Sheet sheetAt = wb.getSheetAt(0);
            System.out.println(sheetAt.getLastRowNum());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void bdJson() {
        File file;
        try (InputStream is = new FileInputStream("D:\\iflytek\\src\\bdjg.json")) {
            String jsonStr = IOUtils.toString(is);
            JSONObject obj = JSON.parseObject(jsonStr);
            JSONArray cbsj = obj.getJSONArray("cbsj");
            Object o = cbsj.get(0);
            ListIterator<Object> iterator = cbsj.listIterator();
            while (iterator.hasNext()) {
                Object next = iterator.next();
                System.out.println(next);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void flatMap() {
        Map<Integer, List<Integer>> map = new HashMap<>();
        for (int i = 0; i < 3; i++) {
            map.put(i, Lists.newArrayList(1 + i * 3, 2 + i * 3, 3 + i * 3));
        }
        List<Integer> list = map.values().stream().flatMap(l -> l.stream()).collect(Collectors.toList());
        System.out.println(list.size());
    }

    @Test
    public void join() {
        List<String> list = Lists.newArrayList();
        System.out.println(Joiner.on(",").join(list));
    }

    @Test
    public void classpathResource() {
        ClassPathResource resource = new ClassPathResource("fldmlb.xls");
        try (InputStream is = resource.getInputStream()) {
            DataFormatter formatter = new DataFormatter();
            Workbook wb = WorkbookFactory.create(is);
            Sheet sheet1 = wb.getSheetAt(0);
            for (Row row : sheet1) {
                for (Cell cell : row) {
                    // get the text that appears in the cell by getting the cell value and applying any data formats
                    // (Date, 0.00, 1.23e9, $1.23, etc)
                    String text = formatter.formatCellValue(cell);
                    System.out.print(text + "-");

                    // Alternatively, get the value and format it yourself
                    switch (cell.getCellType()) {
                        case STRING:
                            System.out.print(cell.getRichStringCellValue().getString());
                            break;
                        case NUMERIC:
                            if (DateUtil.isCellDateFormatted(cell)) {
                                System.out.print(cell.getDateCellValue());
                            }
                            else {
                                System.out.print(cell.getNumericCellValue());
                            }
                            break;
                        case BOOLEAN:
                            System.out.print(cell.getBooleanCellValue());
                            break;
                        case FORMULA:
                            System.out.print(cell.getCellFormula());
                            break;
                        case BLANK:
                            System.out.print("");
                            break;
                        default:
                            System.out.print("");
                    }
                    System.out.print("  ");
                }
                System.out.println();
            }
        }
        catch (Exception e) {

        }
    }

    @Test
    public void loadResource() {
        InputStream s1 = CommonTest.class.getResourceAsStream("./ViewProcuracyWebApplicatoin.java");
        InputStream s2 = this.getClass().getClassLoader().getResourceAsStream("./ViewProcuracyWebApplicatoin.java");
        InputStream s3 = ClassLoader.getSystemResourceAsStream("./ViewProcuracyWebApplicatoin.java");
        System.out.println(s1);
    }

    @Test
    public void extractWord() {
        try (InputStream is = new FileInputStream("D:\\iflytek\\src\\图文平台\\05 案卡填录\\test.wps")) {
            final String text = FileUtils.extractWordText(is);
            System.out.println(text);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("------------");
        String pjsPath =
                "C:\\Users\\Administrator\\Documents\\测试文书\\司法文书解析平台用例\\1、刑事裁判文书500份\\1、一审刑事判决书400份\\1" + "、危险驾驶罪40份\\";
        try (InputStream is = new FileInputStream(
                "C:\\Users\\Administrator\\Documents\\测试文书\\司法文书解析平台用例\\1、刑事裁判文书500份\\2、一审刑事裁定书100份\\1、危险驾驶罪10" +
                "份\\（2018）辽03刑初137号.docx")) {
            final String content = FileUtils.extractWordText(is);
            System.out.println(content);
            // {"texts":[{"content":"","type":"qss"}],"trackId":633,"usrId":"aktq","protocol_level":"1"}
            JSONObject json = new JSONObject();
            json.put("trackId", System.currentTimeMillis());
            json.put("usrId", "aktq");
            json.put("protocol_level", "2");
            Map<String, String> text = new HashMap<>();
            text.put("content", content);
            text.put("type", "qss");
            json.put("texts", Lists.newArrayList(text));
            System.out.println(json.toJSONString());
            String resp = HttpUtil.post("http://143.176.23.212:8088/tuling/elle/v2/interpret", json.toJSONString());
            System.out.println(resp);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void lambda() {
        ArrayList<UserTestBean> users = Lists.newArrayList(new UserTestBean("张三", LocalDate.of(2005, 07, 01), 3),
                                                           new UserTestBean("李四", LocalDate.of(2005, 11, 01), 3),
                                                           new UserTestBean("张五", LocalDate.of(1995, 07, 01), 1),
                                                           new UserTestBean("张三", LocalDate.of(1990, 11, 21), 2),
                                                           new UserTestBean("王二", LocalDate.of(1985, 01, 21), 3));

        // 排序
        Collections.sort(users, Comparator.comparing(UserTestBean::getBirthday));

        // 将level==3的用户按birthday排序后，输出用,拼接的name ==> 王二, 张三, 李四
        String names = users.stream().filter(u -> u.getLevel() == 3).sorted(
                Comparator.comparing(UserTestBean::getBirthday)).map(UserTestBean::getName).collect(
                Collectors.joining(", "));
        System.out.println(names);

    }

    @Data
    class UserTestBean {
        private String name;
        private LocalDate birthday;
        private int level;

        public UserTestBean(String name, LocalDate birthday, int level) {
            this.name = name;
            this.birthday = birthday;
            this.level = level;
        }
    }

    @Test
    public void base64File() throws IOException {
        String ip = "172.31.96.176";
        // String ip = "localhost";
        String name = "/Users/tonnyyi/work/讯飞/图文平台/高检 2.0/test/source/0.jpg";
        OcrResultRequestVo vo = new OcrResultRequestVo();
        vo.setWjhz("." + StringUtils.substringAfterLast(name, "."));
        vo.setSfwjnr(true);
        vo.setSfwjwb(true);
        vo.setWjxh("374BD0941D468847D1A20174482FEF5A");
        vo.setMd5("fa1db4712f3c2c651bbd6934f75a8b8a");

        try (FileInputStream is = new FileInputStream(name)) {
            byte[] bytes = IOUtils.toByteArray(is);
            String wjnr = Base64.encodeBase64String(bytes);
            vo.setWjnr(wjnr);
        }

        request(ip, vo, pattern, 2, 10);
    }

    private void request(String ip, OcrResultRequestVo vo, DateTimeFormatter pattern, int times, int interval) {
        for (int i = 0; i < times; i++) {
            LocalDateTime start = LocalDateTime.now();
            System.out.println("请求开始, " + start.format(pattern));
            String body = HttpRequest.post("http://" + ip + ":9192/procuracy-web/iflytek/api/GetWJOCRSBJG").body(
                    JSONUtil.toStrDefault(vo)).execute().body();
            System.out.println("请求结束, " + LocalDateTime.now().format(pattern));

            if (i <= times) {
                try {
                    Thread.sleep(interval * 1000);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Test
    public void json() {
        String path = "/Users/tonnyyi/work/讯飞/图文平台/ocr开发/response.json";
        try (FileInputStream fis = new FileInputStream(path)) {
            String string = IOUtils.toString(fis);
            String body = JSONUtil.getProp(JSONUtil.toJsonNode(string), "body", String.class);
            log("getTextByType start");
            JSONObject data = JSONObject.parseObject(body);
            log("getTextByType parseObject");
            JSONObject taskResult = JSONObject.parseObject(data.getString(OcrConstants.ResultStruct.TASK_RESULT));
            log("getTextByType taskResult");
            JSONArray pages = JSONObject.parseArray(taskResult.getString(OcrConstants.ResultStruct.PAGE));
            log("getTextByType parseArray");

            Thread.sleep(10000);
            log("getTextByType start");
            data = JSONObject.parseObject(body + "  ");
            log("getTextByType parseObject");
            taskResult = JSONObject.parseObject(data.getString(OcrConstants.ResultStruct.TASK_RESULT) + "  ");
            log("getTextByType taskResult");
            pages = JSONObject.parseArray(taskResult.getString(OcrConstants.ResultStruct.PAGE) + "  ");
            log("getTextByType parseArray");
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void log(String msg) {
        System.out.println(LocalDateTime.now().format(pattern) + " " + msg);
    }

    @Test
    public void scaleImg() throws IOException {
        String path = "/Users/tonnyyi/work/讯飞/图文平台/高检 2.0/test/source/input.jpg";
        FileInputStream fis = new FileInputStream(path);
        byte[] bytes = IOUtils.toByteArray(fis);

        log("start");
        // 获取原图尺寸
        int yskd = 0;
        int ysgd = 0;
        BufferedImage image = null;
        try (ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes)) {
            image = ImageIO.read(inputStream);
            yskd = image.getWidth();
            ysgd = image.getHeight();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        double scale = 2500.0 / Math.max(ysgd, yskd);

        log("1");
        for (int i = 0; i < 30; i++) {
            try (ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
                    ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream()) {
                Thumbnails.of(inputStream).scale(scale).outputFormat("jpg").toOutputStream(byteOutputStream);
                byte[] bytes1 = byteOutputStream.toByteArray();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }

        log("start");
        for (int i = 0; i < 30; i++) {
            try (ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
                    ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream()) {
                int width = (int) (yskd * scale);
                int height = (int) (ysgd * scale);
                BufferedImage newImage = new BufferedImage(width, height, image.getType());
                Graphics g = newImage.getGraphics();
                g.drawImage(image, 0, 0, width, height, null);
                g.dispose();
                ImageIO.write(newImage, "JPEG", byteOutputStream);
                byte[] bytes1 = byteOutputStream.toByteArray();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        log("out");

        fis.close();
    }

    public static BufferedImage zoomOutImage(BufferedImage originalImage, int width, int height) {
        BufferedImage newImage = new BufferedImage(width, height, originalImage.getType());
        Graphics g = newImage.getGraphics();
        g.drawImage(originalImage, 0, 0, width, height, null);
        g.dispose();
        return newImage;

    }
}
