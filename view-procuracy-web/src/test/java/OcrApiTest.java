import com.iflytek.jzcpx.procuracy.common.result.Result;
import com.iflytek.jzcpx.procuracy.common.util.JSONUtil;
import com.iflytek.jzcpx.procuracy.ocr.entity.swx.OcrWJsbSingleResult;
import com.iflytek.jzcpx.procuracy.ocr.recognize.model.OcrResultPdfQueryDto;
import com.iflytek.jzcpx.procuracy.web.ViewProcuracyWebApplication;
import com.iflytek.jzcpx.procuracy.web.ocr.bd.OcrBDService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ViewProcuracyWebApplication.class)
@Slf4j
public class OcrApiTest {

    @Autowired
    private OcrBDService ocrBDService;

    /**
     * 测试文件合成双层pdf接口
     */
    @Test
    public void picToPdf() {
        OcrResultPdfQueryDto ocrResultPdfQueryDto = new OcrResultPdfQueryDto();
        Result<OcrWJsbSingleResult> result = ocrBDService.ocrToPdf(ocrResultPdfQueryDto);
        log.debug(JSONUtil.toStrDefault(result.getData()));
    }

    /**
     * 测试证据识别接口
     */
    @Test
    public  void zhengjuRecognize()
    {}
}
