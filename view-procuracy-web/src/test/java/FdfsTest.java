import java.io.FileInputStream;
import java.io.FileOutputStream;

import com.iflytek.jzcpx.procuracy.web.ViewProcuracyWebApplication;
import com.iflytek.sxs.dfs.client.FdfsClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019/8/19 19:51
 */
@SpringBootTest(classes = ViewProcuracyWebApplication.class)
@RunWith(SpringRunner.class)
public class FdfsTest {

    @Autowired
    private FdfsClient client;

    @Test
    public void upload() {
        try (FileInputStream fis = new FileInputStream("D:\\iflytek\\src\\view-procuracy\\pom.xml");
                FileOutputStream fos = new FileOutputStream("./pom-fdfs.xml")) {
            String path = client.uploadFile(fis, fis.available(), "xml");
            System.out.println(path);

            client.downloadFile(fos, path);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

}
