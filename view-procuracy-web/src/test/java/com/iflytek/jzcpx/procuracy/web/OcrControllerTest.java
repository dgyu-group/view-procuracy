package com.iflytek.jzcpx.procuracy.web;

import cn.hutool.core.io.FileUtil;

import com.alibaba.fastjson.JSONObject;
import com.iflytek.jzcpx.procuracy.common.enums.MetricEnum;
import com.iflytek.jzcpx.procuracy.common.util.JSONUtil;
import com.iflytek.jzcpx.procuracy.common.util.MetricUtil;
import com.iflytek.jzcpx.procuracy.ocr.entity.swx.BatchOcrVO;
import com.iflytek.jzcpx.procuracy.ocr.recognize.model.OcrResultRequestVo;
import com.iflytek.jzcpx.procuracy.web.ocr.bd.OcrBDService;
import com.iflytek.sxs.dfs.client.FdfsClient;

import org.csource.common.MyException;
import org.hamcrest.core.IsNot;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureMockMvc
public class OcrControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void query() throws Exception {
        this.mvc.perform(
                get("/card/1").contentType(MediaType.APPLICATION_JSON_UTF8).accept(MediaType.APPLICATION_JSON_UTF8))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("200"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.id").value(1));
        // .andExpect(MockMvcResultMatchers.jsonPath("$.data[?(@=='312312')]").exists());
    }

    @Test
    @Transactional
    public void save() throws Exception {
        // 参数缺失
        this.mvc.perform(
                put("/card")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .content("{}"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value(IsNot.not(200)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data").isEmpty());
    }

    @Test
    @Transactional
    public void GetWJOCRSBJG() throws Exception {

        OcrResultRequestVo vo=new OcrResultRequestVo();
        //ClassPathResource classPathResource = new ClassPathResource("test/test01.png");//test2
        //ClassPathResource classPathResource = new ClassPathResource("test/handwrite.jpg");//test3

        ClassPathResource classPathResource = new ClassPathResource("test/table.jpg");//test3
        //ClassPathResource classPathResource = new ClassPathResource("test/90.png");
        //ClassPathResource classPathResource = new ClassPathResource("test/page1.pdf");//test1
        //ClassPathResource classPathResource = new ClassPathResource("test/2500more.jpg");
        File file = classPathResource.getFile();
       byte[] pngbytes= FileUtil.readBytes(file);
        String pngBase64Str=Base64.getEncoder().encodeToString(pngbytes);
        vo.setWjnr(pngBase64Str);
        //vo.setSfwjnr(true);
        vo.setSfwjwb(true);
        //vo.setWjhz(".png");//test2
       // vo.setWjhz(".pdf");//test1
        vo.setWjhz(".jpg");//test3
        vo.setSfwjnr(true);

        //
        String text=JSONUtil.toStrDefault(vo);



        this.mvc.perform(
                post("/iflytek/api/GetWJOCRSBJG").contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(text))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }
    
    @Autowired
    private FdfsClient fdfsClient;
    @Test
    public void  deleteFastDfs() throws IOException, MyException {
	this.fdfsClient.deleteFile("group1/M00/55/50/rB8ub1_r8MeAesIPABXXHhmowDU643.jpg");
    }
    
    @Autowired
    private OcrBDService ocrBDService;
    
    @Test
    public void contextLoads() {
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                try {
                    String param="{\n" + 
                    	"  \"bsbh\": \"3b7e2fb569d34a92848ea9f19ad4d111730\",\n" + 
                    	"  \"dwbm\": \"340000\",\n" + 
                    	"  \"systemid\": \"1\",\n" + 
                    	"  \"taskid\": \"3b7e2fb569d34a92848ea9f19ad4d730\",\n" + 
                    	"  \"type\": \"1\"\n" + 
                    	"}";
                    BatchOcrVO batchOcrVO=JSONObject.parseObject(param, BatchOcrVO.class);
                    
                        JSONObject obj = new JSONObject();
                	obj.put("systemid", batchOcrVO.getSystemid());
                	obj.put("bsbh", batchOcrVO.getBsbh());
                	obj.put("dwbm", batchOcrVO.getDwbm());
                	obj.put("taskid", batchOcrVO.getTaskid());
                	obj.put("remark", "批量文件图像识别接口:MQ消息");
                	
                    ocrBDService.batchRecognize(batchOcrVO, null);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            ).start();
        }
        try {
            Thread.sleep(6000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
 
}
