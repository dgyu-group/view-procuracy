package model;

import java.util.List;

public class ContResultModel {

    /**
     * 目录序号
     */
    public int num;

    /**
     * 目录名称
     */
    public String name;

    /**
     * 目录元素
     */
    public List<ContResultItem> chirdren;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setChirdren(List<ContResultItem> chirdren) {
        this.chirdren = chirdren;
    }

    public List<ContResultItem> getChirdren() {
        return chirdren;
    }

    public  void add(ContResultItem model)
    {
        if(chirdren!=null)
        {
            chirdren.add(model);
        }
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
