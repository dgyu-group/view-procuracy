import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import cn.hutool.core.io.FileUtil;
import com.alibaba.fastjson.JSONObject;
import com.iflytek.jzcpx.procuracy.common.result.Result;
import com.iflytek.jzcpx.procuracy.ocr.dao.RecognizeFileDao;
import com.iflytek.jzcpx.procuracy.ocr.entity.swx.OcrParamVO;
import com.iflytek.jzcpx.procuracy.ocr.entity.swx.result.Wjsbjg;
import com.iflytek.jzcpx.procuracy.ocr.recognize.model.OcrResultRequestVo;
import com.iflytek.jzcpx.procuracy.ocr.service.OcrService;
import com.iflytek.jzcpx.procuracy.ocr.service.RecognizeFileService;
import com.iflytek.jzcpx.procuracy.tools.ocr.OcrClient;
import com.iflytek.jzcpx.procuracy.web.ViewProcuracyWebApplication;
import com.iflytek.sxs.dfs.client.FdfsClient;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

/**
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-09 09:44
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ViewProcuracyWebApplication.class)
@ActiveProfiles("dev")
public class OcrTest {
    @Autowired
    private OcrService ocrService;
    @Autowired
    private OcrClient ocrClient;
    @Autowired
    private RecognizeFileService recognizeFileService;
    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private FdfsClient fdfsClient;

    @Test
    public void base64() {
        try (InputStream in = new FileInputStream("D:\\iflytek\\src\\图文平台\\test\\picAndTable.png")) {
            byte[] bytes = new byte[in.available()];
            in.read(bytes);

            String encode = new String(org.apache.commons.codec.binary.Base64.encodeBase64(bytes));

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.parseMediaType("text/plain; charset=UTF-8"));
            JSONObject param = new JSONObject();
            param.put("pagetype", "page");
            param.put("funclist", "ocr,er");
            param.put("resultlevel", "3");
            final String resp = restTemplate
                    .postForObject("http://192.168.203.128:8085/tuling/ocr/v2/base64/1234", new HttpEntity<>(encode, headers), String.class, param);
            System.out.println(resp);

            Result<String> result = ocrClient.request(encode);
            System.out.println(result.getData());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void ocrBase64() {
        try (FileInputStream fis = new FileInputStream("D:\\iflytek\\src\\图文平台\\test\\picAndTable.png")) {
            byte[] bytes = new byte[fis.available()];
            fis.read(bytes);
            String base64Img = new String(org.apache.commons.codec.binary.Base64.encodeBase64(bytes));
            Result<String> result = ocrClient.request(base64Img);
            System.out.println(result.getData());
        } catch (Exception e) {

        }
    }

    @Test
    public void subTask() throws InterruptedException {
        System.out.println(" ------------ begin");
        ocrService.submitBatchRecognize(null, null);
        Thread.sleep(7000);
        System.out.println(" ------------ begin");
    }

    @Test
    public void toPdf() throws Exception {
        OcrParamVO ocrParamVO = new OcrParamVO();
        ocrParamVO.setWjbh("123");
        ocrParamVO.setWjhz("png");
        ocrParamVO.setWjnr(Base64.getEncoder().encodeToString(IOUtils.toByteArray(new FileInputStream("/Users/tonnyyi/work/讯飞/图文平台/测试/base64.png"))));
        // OcrWjsbResult wjsb = batchOcrService.ocrWjsb(ocrParamVO);
    }

    @Test
    public void priorityOcr() throws InterruptedException {
        String body = FileUtil.readUtf8String(
                "/Users/tonnyyi/Library/Containers/com.tencent.xinWeChat/Data/Library/Application " +
                        "Support/com.tencent.xinWeChat/2.0b4.0" +
                        ".9/ba20b86f043250bb4f4c73ce9068de1d/Message/MessageTemp/f922d303ac09e895d2749c1ff75dc7df" +
                        "/OpenData/request_body.json");

        OcrResultRequestVo ocrParam = JSONObject.parseObject(body, OcrResultRequestVo.class);
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                Wjsbjg resp = ocrService.recognize(ocrParam);
                System.out.println(resp.getTxtnr().substring(0, 50));
            }).start();
        }

        Thread.sleep(10000);

        String base64Img = new String(org.apache.commons.codec.binary.Base64
                .encodeBase64(FileUtil.readBytes("/Users/tonnyyi/work/讯飞/图文平台/高检 2.0/test/source/1.jpg")));

        for (int i = 0; i < 3; i++) {
            new Thread(() -> {
                try {
                    ocrClient.request(base64Img, OcrClient.RequestPriority.HIGH);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }).start();
        }

        Thread.sleep(60000);
    }

    @Autowired
    RecognizeFileDao recognizeFileDao;

    @Test
    public void testExectorThread() {
        ExecutorService executorService = Executors.newCachedThreadPool();
        ArrayList<Object> stringList = new ArrayList<>();
        CopyOnWriteArrayList<Object> objects = new CopyOnWriteArrayList<>();
        CountDownLatch countDownLatch = new CountDownLatch(10);
        for (int i = 0; i < 10; i++) {
            int finalI = i;
            Future<String> submit = executorService.submit(() -> {
                System.out.println(Thread.currentThread().getName() + "->" + Integer.toString(finalI));
                return Thread.currentThread().getName() + "->" + Integer.toString(finalI);
            });

            if (submit.isDone()) {
                try {
                    stringList.add(submit.get());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

            }
            countDownLatch.countDown();
//            executorService.execute(() -> {
//                stringList.add(Thread.currentThread().getName()+"-->" + Integer.toString(finalI));
//                System.out.println(String.format("线程:%s",stringList.toString()));
//            });
        }
        if(countDownLatch.getCount()==0){
            System.out.println("结果："+stringList.toString());
        }
    }
}
