import com.google.common.collect.Lists;
import com.iflytek.jzcpx.procuracy.ocr.component.mq.MessageProducer;
import com.iflytek.jzcpx.procuracy.web.ViewProcuracyWebApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-14 19:00
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ViewProcuracyWebApplication.class)
public class MQTest {
    private static final Logger logger = LoggerFactory.getLogger(MQTest.class);

    @Autowired
    private MessageProducer producer;

    @Test
    public void send() throws InterruptedException {
        producer.sendOcrResultPushMessage(Lists.newArrayList(1L, 11L, 111L, 1111L, 11111L));
        Thread.sleep(30000);
        producer.sendOcrResultPushMessage(Lists.newArrayList(2L, 22L, 222L, 2222L, 22222L));
        Thread.sleep(30000);
        logger.info("退出");
    }

}
