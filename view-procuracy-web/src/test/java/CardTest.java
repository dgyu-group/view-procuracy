import java.io.FileInputStream;
import java.io.IOException;

import com.iflytek.jzcpx.procuracy.card.pojo.ElleResultData;
import com.iflytek.jzcpx.procuracy.common.result.Result;
import com.iflytek.jzcpx.procuracy.common.util.JSONUtil;
import com.iflytek.jzcpx.procuracy.ocr.service.OcrService;
import com.iflytek.jzcpx.procuracy.tools.elle.ElleClient;
import com.iflytek.jzcpx.procuracy.tools.elle.ElleTextTypeEnum;
import com.iflytek.jzcpx.procuracy.tools.ocr.OcrClient;
import com.iflytek.jzcpx.procuracy.tools.ocr.OcrUtil;
import com.iflytek.jzcpx.procuracy.web.ViewProcuracyWebApplication;
import org.apache.commons.codec.binary.Base64;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019/8/19 20:15
 */
@SpringBootTest(classes = ViewProcuracyWebApplication.class)
@RunWith(SpringRunner.class)
public class CardTest {

    @Autowired
    private OcrClient ocrClient;
    @Autowired
    private ElleClient elleClient;

    @Autowired
    private OcrService ocrService;

    @Test
    public void elleDBS() throws IOException {
        String text = ocrText("D:\\iflytek\\src\\图文平台\\05 案卡填录\\提请批准逮捕书+起诉意见书模版\\20_2020_文书_image0010.jpg");
        text += ocrText("D:\\iflytek\\src\\图文平台\\05 案卡填录\\提请批准逮捕书+起诉意见书模版\\20_2020_文书_image0011.jpg");

        Result<String> elleResult = elleClient.request(ElleTextTypeEnum.tqpzdbs, text);
        System.out.println(elleResult.getData());
    }
    @Test
    public void elleQS() throws IOException {
        String text = ocrText("/Users/tonnyyi/Downloads/demo/test/单人起诉意见书/起诉意见书汪金秀1.jpg");
        text += ocrText("/Users/tonnyyi/Downloads/demo/test/单人起诉意见书/起诉意见书汪金秀2.jpg");

        Result<String> elleResult = elleClient.request(ElleTextTypeEnum.qsyjs, text);
        System.out.println(elleResult.getData());
    }

    private String ocrText(String filePath) {
        try (FileInputStream fis = new FileInputStream(filePath)) {
            byte[] bytes = new byte[fis.available()];
            fis.read(bytes);
            final String base64Img = new String(Base64.encodeBase64(bytes));
            final Result<String> ocrResult = ocrClient.request(base64Img);
            return OcrUtil.getTextByType(ocrResult.getData(), "2");
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Test
    public void req() throws IOException {
        String content = "16\\n上海市公安局奉贤分局\\n提请批准逮捕书\\n沪公(奉)提捕字[2018]45号\\n犯罪嫌疑人李杰,男,1987年10月22日生,上海青浦县人,身份证号\\n码:310229198710220810,汉族,大专文化程度,无业," +
                "户籍地上海市青\\n浦区徐泾镇徐泾378号,现住上海市青浦区徐泾镇育才路451弄7号206室,\\n群众,手机号码:无,母亲:顾秀娥,现住上海市青浦区徐泾镇育才路45\\n1弄7号206室," +
                "联系电话:13472745584。\\n违法犯罪经历:犯罪嫌疑人李杰因犯有盗窃行为于2014年12月被处行\\n政拘留十日,2015年2月因犯盗窃罪被判处拘役4个月,2015年9月因犯盗\\n窃罪被判处有期徒刑1年5个月," +
                "2017年3月因犯有盗窃行为被处行政拘留\\n十日,2017年8月因犯盗窃罪被判处有期徒刑10个月,2018年1月20日因涉\\n嫌盗窃罪被我局依法刑事拘留。\\n被害人金群恩,男,1981年6月14日生,上海奉贤人," +
                "身份证号码:3\\n10226198106142618,汉族,初中文化程度,个体,户籍地上海市奉贤区\\n奉城镇陈桥村512号,现住上海市奉贤区展园路250弄4号1801室,群众,\\n手机号码:13020293180。\\n被害人吴国强,男," +
                "1981年3月28日生,上海奉贤人,身份证号码:3\\n10226198103281612,汉族,初中文化程度,个体,户籍地上海市奉贤区\\n拓林镇海湾村西湾208号,现住上海市奉贤区南桥镇育秀东区1117号501\\n至,群众," +
                "手机号码:13917839088。\\n被害人任香龙,男,1986年5月20日生,安徽六安市人,身份证号码:342401198605206138,汉族,初中文化程度,个体,户籍地安徽省六\\n安市金安区翁墩乡联合村小楼组," +
                "现住上海市奉贤区青村镇钱桥社区石海\\n村海边781号103室,群众,手机号码:18217298658。\\n犯罪嫌疑人李杰未聘请辩护律师。\\n犯罪嫌疑人李杰涉嫌盗窃一案,于2017年3月9日由被害人吴国强举报\\n至我局。我局经过审查," +
                "于2017年3月22日立案侦查。犯罪嫌疑人李杰于2\\n018年1月20日被抓获归案。\\n经依法侦查查明:\\n2017年3月9日凌晨,犯罪嫌疑人李杰窜至奉贤区南桥镇南奉公路9367\\n号海峡宝岛眼镜店内,用脚踢开玻璃门锁入内实施盗窃," +
                "窃得人民币现\\n金100元。\\n2017年3月9日凌晨,犯罪嫌疑人李杰窜至奉贤区南桥镇南奉公路8813\\n号悦和生前店内,用脚踢开玻璃门锁入内实施盗窃,窃得人民币现金400\\n元。\\n2017年3月9日凌晨," +
                "犯罪嫌疑人李杰窜至奉贤区南桥镇环城东路1398\\n号手机店内,用脚踢开玻璃门锁入内实施盗窃,窃得人民币现金200元。\\n认定上述犯罪事实的证据如下:被害人任香龙、吴国强、金群恩的陈\\n述,犯罪嫌疑人李杰的供述及辨认笔录,现场勘查材料," +
                "上海市公安局奉\\n贤分局南桥派出所出具的案发经过,犯罪嫌疑人李杰的户籍资料、前科资\\n料等。\\n综上所述,犯罪嫌疑人李杰以非法占有为目的,秘密窃取他人财物,\\n其行为触犯了《中华人民共和国刑法》第二百六十四条之规定,涉嫌盗窃\\n罪," +
                "符合逮捕条件。依照《中华人民共和国刑事诉讼法》第七十九条、第\\n八十五条之规定,特提请批准逮捕。\\n此致\\n\\n              " +
                "18\\n奉贤区人民检察院\\n二O一八年六月十九日\\n附:1、本案卷宗壹册:\\n2、犯罪嫌疑人李杰现羁押于奉贤区看守所。";
        Result<String> result = elleClient.request(ElleTextTypeEnum.tqpzdbs, content);
        System.out.println(result.getData());

        ElleResultData data = JSONUtil.toBean(result.getData(), ElleResultData.class);
        System.out.println(data.getExtractInfoVec().get(0).getExtractInfo().getSuspect());
    }

}
