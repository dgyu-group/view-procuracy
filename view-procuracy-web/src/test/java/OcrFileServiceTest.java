import java.util.ArrayList;
import java.util.List;

import com.iflytek.jzcpx.procuracy.ocr.entity.OcrFile;
import com.iflytek.jzcpx.procuracy.ocr.service.OcrFileService;
import com.iflytek.jzcpx.procuracy.ocr.service.RecognizeFileService;
import com.iflytek.jzcpx.procuracy.web.ViewProcuracyWebApplication;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-16 09:46
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ViewProcuracyWebApplication.class)
public class OcrFileServiceTest {

    @Autowired
    private OcrFileService ocrFileService;

    @Autowired
    private RecognizeFileService recognizeFileService;

    @Test
    public void shrink() {
        OcrFile file = new OcrFile();
        file.setId(15L);
        file.setRemark("123\r\n1231\r333\n4444");
        ocrFileService.updateById(file);
    }

    @Test
    public void selectUndo() {
        List<Long> ids = new ArrayList<>();
        for (int i = 0; i < 101; i++) {
            ids.add((long) i);
        }
        recognizeFileService.findUndoneFileIdMap(ids);
    }

    @Test
    public void selectUndone() {
        ocrFileService.findUndoneFileIdMap(Lists.newArrayList(1L, 2L, 3L));
    }

}
