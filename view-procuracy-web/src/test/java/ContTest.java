import com.alibaba.fastjson.JSONObject;
import com.iflytek.jzcpx.procuracy.cont.common.util.UUIDUtil;
import com.iflytek.jzcpx.procuracy.cont.model.ArchiveCatalogueResultDto;
import com.iflytek.jzcpx.procuracy.cont.model.JZML;
import com.iflytek.jzcpx.procuracy.cont.model.JZMLWJ;
import com.iflytek.jzcpx.procuracy.web.ViewProcuracyWebApplication;
import com.iflytek.jzcpx.procuracy.web.cont.bd.ContBDService;
import lombok.extern.slf4j.Slf4j;
import model.ContResultItem;
import model.ContResultModel;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ViewProcuracyWebApplication.class)
@Slf4j
public class ContTest {

    @Autowired
    private ContBDService contBDService;

    @Resource(name = "dzjzOcrContExecutor")
    private ThreadPoolTaskExecutor DZJZ_OCR_CONT_THREAD_POOL;

    @Test
    public void downloadBSBHTask() {
        String bsbh = "bc844442715a489fbd9b707498809f44";
        String path = "C:\\Users\\Administrator\\Desktop\\ssju\\mulu\\7";
        contBDService.downloadTaskFileFromSwx(bsbh, path);
    }

    @Test
    public void contTest() {
        try {
            List<JZML> mlList = new ArrayList<>();
            List<JZMLWJ> wjList = getJZMLWJs();


            String bsbh = "";
            // 提交编目结果到电子卷宗系统.
            ArchiveCatalogueResultDto dto = contBDService.ocrAndContFile(mlList, wjList, bsbh,null);
            dto.setBSBH(bsbh);

            List<ContResultModel> resultModels = convert(dto);

            String treeStrResult = JSONObject.toJSONString(resultModels);

            log.info(treeStrResult);

            String saveResultStr = JSONObject.toJSONString(dto);

            log.info(String.format("保存电子卷宗传入参数:%s", saveResultStr));

            //将结果写到临时文件里
            //File file = new File("C:\\Users\\Administrator\\Desktop\\ssju\\mulu\\result.json");

            //FileWriter fileWriter = new FileWriter(file);
            //fileWriter.write(saveResultStr);
            //fileWriter.flush();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }
    }

    private List<ContResultModel> convert(ArchiveCatalogueResultDto dto) {

        List<JZML> jzmls = dto.getJZML();
        List<JZMLWJ> jzmlwjs = dto.getJZMLWJ();

        List<ContResultModel> contResultModels = new ArrayList<>();

        int num = 1;

        for (JZML c : jzmls) {
            ContResultModel contResultModel = new ContResultModel();
            contResultModel.setNum(num);
            contResultModel.setChirdren(new ArrayList<>());


            contResultModel.setName(c.getMLXSMC());

            //由文件找到目录
            jzmlwjs.forEach(v -> {
                if (c.getMLBH().equals(v.getMLBH())) {

                    ContResultItem item = new ContResultItem();
                    item.setName(v.getWJXSMC());
                    contResultModel.add(item);
                }

            });

            contResultModels.add(contResultModel);

            num++;
        }

        return contResultModels;
    }

    /**
     * @return
     */
    private List<JZMLWJ> getJZMLWJs() {
        //读取本地目录文件
        String pathStr = "C:\\Users\\Administrator\\Desktop\\ssju\\mulu\\7";
        File file = new File(pathStr);
        File[] list = file.listFiles();

        JSONObject ocrParams = new JSONObject();
        ocrParams.put("pagetype", "page");
        ocrParams.put("resultlevel", "3");
        ocrParams.put("funclist", "ocr,er");
        List<JZMLWJ> resultList = new ArrayList<>();
        int wjsxh = 1;
        for (File fileTemp : list) {
            JZMLWJ item = new JZMLWJ();


            item.setWJSXH(wjsxh);
            item.setWJXH(UUIDUtil.getuuid());
            item.setWJXSMC("第" + wjsxh + "页");
            item.setWJMC(fileTemp.getName());
            item.setFile(fileTemp);
            resultList.add(item);


            wjsxh++;
        }


        final CountDownLatch cdl = new CountDownLatch(resultList.size());
        resultList.forEach(vo -> {
            try {

                DZJZ_OCR_CONT_THREAD_POOL.execute(() -> {
                    String result = "{}";
                    String recognizeResult = contBDService.recognize(vo.getFile(), ocrParams, "111");
                    vo.setOcrRes(recognizeResult);
                    cdl.countDown();
                });

            } catch (Exception e) {
                cdl.countDown();
            }
        });

        try {
            cdl.await();
        } catch (Exception ex) {
        }

        return resultList;
    }
}
