RELEASES LOG
============

* Project：view-procuracy-web
* Configuretion Manager
	- dgyu@iflytek.com
* Developers
	- dgyu@iflytek.com
	- ktyi@iflytek.com
* Modified by: CM


#### Dependencies
* JDK1.8+


## build-1001 对应CI构建号1054 2020-07-08
## New Features
修改案卡接口，适配elle 5024新引擎，规则按照小书案卡2.0规则回填高检2.0系统

1.POST /api/card/accept/bmsah
案件受理成功, 更新受理阶段的文件编码对应的部门受案号到数据库

2.POST /api/card/accept/file
受理阶段文书文件案卡提取

3.POST /api/card/accept/img
受理阶段高拍文件案卡提取

4.POST /api/card/handle/file
办理文书案卡提取-同步

5.POST /api/card/handle/full
全案卡提取

6.POST /api/card/handle/full/source
差异化页面字段数据

7.POST /api/card/handle/outer/async
外来文书案卡提取-异步

8.POST /api/card/handle/outer/sync
外来文书案卡提取-同步

9.POST /api/card/handle/rj
入卷后批量案卡提取-异步


## Known problems
*
OCR识别导出双层PDF可能再高检2.0系统存在复制坐标偏差，文字复制不全