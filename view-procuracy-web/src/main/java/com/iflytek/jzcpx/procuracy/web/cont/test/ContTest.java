package com.iflytek.jzcpx.procuracy.web.cont.test;

import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import com.iflytek.jzcpx.procuracy.cont.model.ArchiveCatalogueResultDto;
import com.iflytek.jzcpx.procuracy.cont.model.Catalog;
import com.iflytek.jzcpx.procuracy.cont.model.CellData;
import com.iflytek.jzcpx.procuracy.cont.model.JZML;
import com.iflytek.jzcpx.procuracy.cont.model.JZMLWJ;
import com.iflytek.jzcpx.procuracy.web.ViewProcuracyWebApplication;
import com.iflytek.jzcpx.procuracy.web.cont.bd.ContBDService;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ViewProcuracyWebApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Data
@Slf4j
public class ContTest {

    //private static final Logger logger = LoggerFactory.getLogger(ContTest.class);

    /**
     * @LocalServerPort 提供了 @Value("${local.server.port}") 的代替
     */
    @LocalServerPort
    private int port;

    private URL base;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private ContBDService contBDService;

    @Before
    public void setUp() throws Exception {
        String url = String.format("http://localhost:%d/", port);
        System.out.println(String.format("port is : [%d]", port));
        this.base = new URL(url);
    }

    /**
     * 向"/test"地址发送请求，并打印返回结果
     *
     * @throws Exception
     */
    @Test
    public void test1() throws Exception {
        
    }
}
