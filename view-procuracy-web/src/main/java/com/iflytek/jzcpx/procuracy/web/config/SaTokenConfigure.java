package com.iflytek.jzcpx.procuracy.web.config;

import cn.dev33.satoken.basic.SaBasicUtil;
import cn.dev33.satoken.exception.NotBasicAuthException;
import cn.dev33.satoken.filter.SaServletFilter;
import cn.dev33.satoken.interceptor.SaRouteInterceptor;
import cn.dev33.satoken.router.SaRouter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2022/1/12
 */
@Configuration
// @EnableWebMvc    // SpringBoot (≥2.6.x)需要额外添加
public class SaTokenConfigure implements WebMvcConfigurer {
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    private String[] excludePath = {"/swagger-ui.html", "/webjars/**", "/v2/api-docs", "/swagger-resources/**",
            "/error", "/favicon.ico"};

    @Value("${sa-token.swagger.user:admin}")
    private String swaggerUsername;
    @Value("${sa-token.swagger.password:Admin!@123}")
    private String swaggerPassword;

    @Bean
    public SaServletFilter getSaServletFilter() {
        return new SaServletFilter().addInclude("/v2/api-docs").setAuth(obj -> {
            SaRouter.match("/**", r -> {
                try {
                    SaBasicUtil.check(swaggerUsername + ":" + swaggerPassword);
                }
                catch (NotBasicAuthException e) {
                    r.back();
                }
            });
        });
    }

    // 注册Sa-Token的注解拦截器，打开注解式鉴权功能
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 注册注解拦截器，并排除不需要注解鉴权的接口地址 (与登录拦截器无关)
        registry.addInterceptor(new SaRouteInterceptor((req, res, handler) -> {
            // 登录认证 -- 拦截所有路由，并排除/user/doLogin 用于开放登录
            SaRouter.match("/**").notMatch(excludePath).check(() -> {
                try {
                    if (!"OPTIONS".equalsIgnoreCase(req.getMethod())) {
                        // StpUtil.checkLogin();
                    }
                }
                catch (Exception e) {
                    logger.info("用户登录校验异常, url: {}", req.getUrl());
                    throw e;
                }
            });

            // 角色认证 -- 拦截以 admin 开头的路由，必须具备 admin 角色或者 super-admin 角色才可以通过认证
            // SaRouter.match("/admin/**", r -> StpUtil.checkRoleOr("admin", "super-admin"));

            // 权限认证 -- 不同模块认证不同权限
            // SaRouter.match("/goods/**", r -> StpUtil.checkPermission("goods"));
            // SaRouter.match("/orders/**", r -> StpUtil.checkPermission("orders"));

            // 甚至你可以随意的写一个打印语句
            // SaRouter.match("/**", r -> System.out.println("----啦啦啦1----"));

            // 连缀写法
            // SaRouter.match("/**").check(r -> System.out.println("----啦啦啦2----"));

            // 多个条件一起使用
            // SaRouter.match(SaHttpMethod.GET).match("/user/**", "/goods/**", "/art/get/{id}")
            //         .notMatch("/**/*.js").notMatch("/**/*.css")
            //         .check(r -> System.out.println("----啦啦啦3----"));
        })).addPathPatterns("/**");
    }
}