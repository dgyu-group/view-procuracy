package com.iflytek.jzcpx.procuracy.web.card.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 字段来源信息
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019/9/16 15:40
 */
@Data
@ApiModel("字段来源信息")
public class FieldSourceVO {

    @ApiModelProperty("字段文本值")
    private String value;

    @ApiModelProperty("字段起始位置")
    private int startPosition;

    @ApiModelProperty("字段结束位置")
    private int endPosition;

    @ApiModelProperty("文件内容")
    private String fileContent;

    @ApiModelProperty("文件名")
    private String fileName;
}
