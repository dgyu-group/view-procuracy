package com.iflytek.jzcpx.procuracy.web.cont.dto;

import lombok.Data;

@Data
public class ContModel {

	/**
	 * 标识编号
	 */
	private String bsbh;

	/**
	 * 应用和单位的唯一标识
	 */
	private String systemid;

	/**
	 *单位编码
	 */
	private String dwbm;

	public String getBsbh() {
		return bsbh;
	}

	public void setBsbh(String bsbh) {
		this.bsbh = bsbh;
	}

	public void setDwbm(String dwbm) {
		this.dwbm = dwbm;
	}

	public String getDwbm() {
		return dwbm;
	}

	public String getSystemid() {
		return systemid;
	}

	public void setSystemid(String systemid) {
		this.systemid = systemid;
	}
}
