package com.iflytek.jzcpx.procuracy.web.cont.bd;

import java.io.File;
import java.util.List;
import com.alibaba.fastjson.JSONObject;
import com.iflytek.jzcpx.procuracy.common.model.HandContResult;
import com.iflytek.jzcpx.procuracy.cont.model.*;
import com.iflytek.jzcpx.procuracy.cont.vo.HandContModel;
import com.iflytek.jzcpx.procuracy.ocr.entity.OcrFile;

/**
 * 自动编目代理接口
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-06 19:55
 */
public interface ContBDService {

    List<OcrFile> getOcrByBSBH(String bsbh);

    void matchResult(List<JZMLWJ> wjs, String bsbh);

    /**
     * @param mlList
     * @param wjList
     * @param BSBH
     * @return
     * @throws Exception
     */
    ArchiveCatalogueResultDto ocrAndContFile(List<JZML> mlList, List<JZMLWJ> wjList, String BSBH,JZML leafJzml) throws Exception;

    /**
     * @param bsbh
     * @param files
     * @return
     */
    CatalogInfoDto catalogEngines(String bsbh, List<JZMLWJ> files);

    /**
     * @param catalog
     * @param list
     * @return
     */
    List<CellData> getCatalogCell(List<Catalog> catalog, List<JZMLWJ> list);


    /**
     * @param file
     * @param trackId
     * @return
     */
    String recognize(File file, JSONObject params, String trackId);

    /**
     * 下载任务图片到本地分析，此方法仅用于分析数据用
     * @param bsbh
     * @param path
     */
    void downloadTaskFileFromSwx(String bsbh,String path);

    /**
     *
     * @param handContModel
     * @return
     */
    HandContResult handCont(HandContModel handContModel);
}
