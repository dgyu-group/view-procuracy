package com.iflytek.jzcpx.procuracy.web.job;

import java.util.Date;

import com.iflytek.jzcpx.procuracy.card.service.CardFileService;
import com.iflytek.jzcpx.procuracy.card.service.CardFormFieldService;
import com.iflytek.jzcpx.procuracy.common.util.JSONUtil;
import com.iflytek.jzcpx.procuracy.ocr.service.OcrFileService;
import com.iflytek.jzcpx.procuracy.ocr.service.OcrTaskService;
import com.iflytek.jzcpx.procuracy.ocr.service.RecognizeFileService;
import com.iflytek.jzcpx.procuracy.ocr.service.RecognizeTaskService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 定时清理数据库表数据
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2022/6/13
 */
@Slf4j
@Component
public class CleanDataJob {

    @Value("${job.cleandata.tables:}")
    private String[] tables;
    @Value("${job.cleandata.keepdays:365}")
    private int keepDays;

    @Value("${job.cleandata.enable:false}")
    private boolean enable;
    @Value("${job.cleandata.cleandb.enable:false}")
    private boolean enableCleanDB;
    @Value("${job.cleandata.cleanfdfs.enable:false}")
    private boolean enableCleanFdfs;

    @Autowired
    private OcrFileService ocrFileService;
    @Autowired
    private OcrTaskService ocrTaskService;
    @Autowired
    private RecognizeFileService recognizeFileService;
    @Autowired
    private RecognizeTaskService recognizeTaskService;
    @Autowired
    private CardFileService cardFileService;
    @Autowired
    private CardFormFieldService cardFormFieldService;

    @Scheduled(cron = "${job.cleandata.cron:0 0 1 * * ?}")
    public void cleanData() {
        if (!enable) {
            log.info("数据清理定时任务未启用, 任务结束");
            return;
        }

        log.info("开始执行数据清理定时任务, 待清理的表: {}, 数据保留天数: {}", JSONUtil.toStrDefault(tables), keepDays);
        if (ArrayUtils.isEmpty(tables)) {
            log.info("没有待清理的表, 任务结束");
            return;
        }
        if (keepDays <= 0) {
            log.info("保留天数不是正数, 任务结束");
            return;
        }

        for (String table : tables) {
            Date now = new Date();
            Date endDatetime = DateUtils.addDays(now, -1 * keepDays);
            cleanTableData(table, null, endDatetime, enableCleanDB, enableCleanFdfs);
        }
    }

    public int cleanTableData(String table, Date startDatetime, Date endDatetime, boolean cleanDB, boolean cleanFdfs) {
        log.info("开始清理表: {}, 截止时间: {}", table, endDatetime);
        int cleanCount = 0;
        try {
            switch (table) {
                case "t_ocr_file":
                    cleanCount = ocrFileService.cleanData(startDatetime, endDatetime, cleanDB, cleanFdfs);
                    break;
                case "t_ocr_task":
                    if (cleanDB) {
                        cleanCount = ocrTaskService.cleanData(startDatetime, endDatetime);
                    }
                    break;
                case "t_recognize_file":
                    cleanCount = recognizeFileService.cleanData(startDatetime, endDatetime, cleanDB, cleanFdfs);
                    break;
                case "t_recognize_task":
                    if (cleanDB) {
                        cleanCount = recognizeTaskService.cleanData(startDatetime, endDatetime);
                    }
                    break;
                case "t_card_file":
                    cleanCount = cardFileService.cleanData(startDatetime, endDatetime, cleanDB, cleanFdfs);
                    break;
                case "t_card_form_field":
                    if (cleanDB) {
                        cleanCount = cardFormFieldService.cleanData(startDatetime, endDatetime);
                    }
                    break;
                default:
                    log.warn("暂不支持该清理该表数据: {}", table);
            }
            log.info("结束清理表: {}, 清理条数: {}", table, cleanCount);
        }
        catch (Exception e) {
            log.warn("数据表清理任务执行异常, table: {}", table, e);
        }
        return cleanCount;
    }

}
