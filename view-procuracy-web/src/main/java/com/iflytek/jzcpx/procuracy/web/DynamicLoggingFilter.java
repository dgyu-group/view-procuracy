package com.iflytek.jzcpx.procuracy.web;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.turbo.TurboFilter;
import ch.qos.logback.core.spi.FilterReply;
import org.slf4j.Marker;

/**
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2020/1/19 09:23
 */
public class DynamicLoggingFilter extends TurboFilter {

    @Override
    public FilterReply decide(Marker marker, Logger logger, Level level, String format, Object[] params, Throwable t) {
        String loggerName = logger.getName();
        if (loggerName.startsWith("org.springframework")) {
            return FilterReply.NEUTRAL;
        }


        return FilterReply.NEUTRAL;
    }
}
