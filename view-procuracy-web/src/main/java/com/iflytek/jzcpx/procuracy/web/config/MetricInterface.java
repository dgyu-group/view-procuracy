package com.iflytek.jzcpx.procuracy.web.config;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 指标统计注解
 * 
 * @author dgyu 作用于类、接口等与方法上
 *
 */
@Target({ ElementType.TYPE, ElementType.METHOD }) //// 作用于类、接口等与方法上
@Retention(value = RetentionPolicy.RUNTIME)
public @interface MetricInterface {
    int intVal() default 5;

    String stringVal() default "";
}
