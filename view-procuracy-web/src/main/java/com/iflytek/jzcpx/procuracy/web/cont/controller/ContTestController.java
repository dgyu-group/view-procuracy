package com.iflytek.jzcpx.procuracy.web.cont.controller;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;

@Api(value = "自动编目", tags = "自动编目")
@RestController
@RequestMapping("/card")
public class ContTestController {

    @GetMapping("/test/data")
    public String contResult() {
        String resultPath = "C:\\Users\\Administrator\\Desktop\\ssju\\mulu\\result.json";
        File file = new File(resultPath);

        try {

            FileReader fr = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fr);

            StringBuffer sb = new StringBuffer();
            String str = "";
            while ((str = bufferedReader.readLine()) != null) {
                sb.append(str);
            }

            return sb.toString();

        } catch (Exception ex) {

        }
        return "";
    }
}
