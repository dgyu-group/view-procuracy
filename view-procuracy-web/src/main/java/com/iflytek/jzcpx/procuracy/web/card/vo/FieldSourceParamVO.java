package com.iflytek.jzcpx.procuracy.web.card.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 字段来源信息
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019/9/16 15:40
 */
@Data
@ApiModel("字段来源信息")
public class FieldSourceParamVO {

    @ApiModelProperty("案件信息")
    private String ajxx;

    @ApiModelProperty("用户信息")
    private String yhxx;

    @ApiModelProperty("访问时间")
    private String fwsj;

    @ApiModelProperty("实体表名")
    private String stbm;

    @ApiModelProperty("实体字段")
    private String stzd;

    @ApiModelProperty("数据唯一标识")
    private String id;
}
