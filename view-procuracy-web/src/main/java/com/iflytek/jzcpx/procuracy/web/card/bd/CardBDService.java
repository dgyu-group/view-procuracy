package com.iflytek.jzcpx.procuracy.web.card.bd;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.iflytek.jzcpx.procuracy.card.entity.CardFile;
import com.iflytek.jzcpx.procuracy.card.entity.CardFormField;
import com.iflytek.jzcpx.procuracy.card.pojo.ElleFileInHandleParam;
import com.iflytek.jzcpx.procuracy.card.pojo.ElleFullParam;
import com.iflytek.jzcpx.procuracy.card.pojo.ElleImgInAcceptParam;
import com.iflytek.jzcpx.procuracy.card.pojo.ElleInFolderParam;
import com.iflytek.jzcpx.procuracy.card.pojo.ElleOuterInHandleParam;
import com.iflytek.jzcpx.procuracy.card.pojo.ElleOuterSyncInHandleParam;
import com.iflytek.jzcpx.procuracy.card.pojo.ElleWordInAcceptParam;
import com.iflytek.jzcpx.procuracy.common.result.Result;

/**
 * 案卡业务代理接口
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-06 19:55
 */
public interface CardBDService {

    /**
     * 受理阶段高拍文件案卡提取
     *
     * @param imgExtractParam 高拍文件参数
     *
     * @return
     */
    Result<JSONObject> extractBase64ImgInAccept(ElleImgInAcceptParam imgExtractParam);

    Result<JSONObject> extractWordInAccept(ElleWordInAcceptParam wordInAcceptParam);

    /**
     * 文书编写后单一文书案卡数据即时提取
     *
     * @param elleFileInHandleParam
     * @return
     */
    Result<JSONObject> extractFileInHandle(ElleFileInHandleParam elleFileInHandleParam);

    /**
     * 入卷文书案卡数据提取
     *
     * @param elleInFolderParam
     * @return
     */
    Result inFolder(ElleInFolderParam elleInFolderParam);

    /**
     * 外来文书案卡数据提取（同步）
     *
     * @param elleOuterSyncInHandleParam
     * @return
     */
    Result<JSONObject> handleOuterSync(ElleOuterSyncInHandleParam elleOuterSyncInHandleParam);

    /**
     * 外来文书案卡数据提取（异步）
     *
     * @param elleOuterInHandleParam
     * @return
     */
    Result handleOuterAsync(ElleOuterInHandleParam elleOuterInHandleParam);

    /**
     * 全案卡提取
     *
     * @param elleFullParam
     * @return
     */
    Result<JSONObject> handlFull(ElleFullParam elleFullParam);

    /**
     * 查询案卡字段, 差异化比对页面展示使用
     *
     * @param bmsah 部门受案号
     * @param stbm  实体表名
     * @param stzd  实体字段
     * @param id    数据唯一标识
     *
     * @return
     */
    Map<CardFile, List<CardFormField>> listFieldsForDiff(String bmsah, String stbm, String stzd, String id);
    /**
     * 更新部门受案号
     * @param wjbm
     * @param bmsah
     * @return
     */
    
    public boolean updateBmsahByWjbm(String wjbm, String bmsah) ;
}
