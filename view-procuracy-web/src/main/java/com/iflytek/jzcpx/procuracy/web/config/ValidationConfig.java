package com.iflytek.jzcpx.procuracy.web.config;

import com.iflytek.jzcpx.procuracy.common.aspect.ValidationAspect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-06 15:23
 */
@Configuration
public class ValidationConfig {

    @Bean
    public ValidationAspect aspect() {
        return null;
    }

}
