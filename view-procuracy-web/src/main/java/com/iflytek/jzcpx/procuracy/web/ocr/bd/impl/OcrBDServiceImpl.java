package com.iflytek.jzcpx.procuracy.web.ocr.bd.impl;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.iflytek.jzcpx.procuracy.common.enums.CommonResultEnum;
import com.iflytek.jzcpx.procuracy.common.result.Result;
import com.iflytek.jzcpx.procuracy.common.result.WebResult;
import com.iflytek.jzcpx.procuracy.ocr.common.constant.OcrConstants;
import com.iflytek.jzcpx.procuracy.ocr.common.enums.RecognizeFuncEnum;
import com.iflytek.jzcpx.procuracy.ocr.entity.OcrFile;
import com.iflytek.jzcpx.procuracy.ocr.entity.swx.BatchOcrVO;
import com.iflytek.jzcpx.procuracy.ocr.entity.swx.OcrTxsbVo;
import com.iflytek.jzcpx.procuracy.ocr.entity.swx.OcrWJsbSingleResult;
import com.iflytek.jzcpx.procuracy.ocr.entity.txsb.PicFileRecognizeReqVo;
import com.iflytek.jzcpx.procuracy.ocr.entity.txsb.PicFileRecognizeResult;
import com.iflytek.jzcpx.procuracy.ocr.recognize.model.OcrResultPdfQueryDto;
import com.iflytek.jzcpx.procuracy.ocr.recognize.model.Wjtz;
import com.iflytek.jzcpx.procuracy.ocr.service.OcrFileService;
import com.iflytek.jzcpx.procuracy.ocr.service.OcrService;
import com.iflytek.jzcpx.procuracy.web.ocr.bd.OcrBDService;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * ocr 业务代理类
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-08 17:14
 */
@Service
public class OcrBDServiceImpl implements OcrBDService {
	private static final Logger logger = LoggerFactory.getLogger(OcrBDServiceImpl.class);

	@Autowired
	private OcrService ocrService;

	@Autowired
	private OcrFileService ocrFileService;

	/**
	 * ocr 单个文件识别
	 * @param ocrParamVO
	 *
	 * @return
	 */
	@Override
	public Result<OcrWJsbSingleResult> ocrToPdf(OcrResultPdfQueryDto ocrParamVO) {
		try {
			byte[] bytes =null;
			//bytes=Base64.decodeBase64(ocrParamVO.getWjnr());

			String fileBase64=ocrParamVO.getWjnr();


			//测试数据，获取文件base64编码
			//fileBase64=TestTool.getBase64String();

			bytes =Base64.decodeBase64(fileBase64);

			Result<OcrWJsbSingleResult> result = ocrService.ocrToPdfDetail(bytes, ocrParamVO.getWjbh(), ocrParamVO.getWjhz());
			return result;
		} catch (Exception e) {
			logger.warn("ocr识别抛出未知异常", e);
			return Result.failed(CommonResultEnum.FAILED);
		}
	}

	@Override
	public Result batchOcrToPdf(BatchOcrVO batchOcrVO,JSONObject obj) {
		ocrService.submitBatchOcrToPdf(batchOcrVO,obj);

		return Result.success("ok");
	}

	@Override
	public Result batchRecognize(BatchOcrVO batchOcrVO, JSONObject obj) {
		 ocrService.submitBatchRecognize(batchOcrVO, obj);
	    return null;
	}

	@Override
	public Result<List<Wjtz>> recognize(OcrTxsbVo recognizeVO) {
		List<RecognizeFuncEnum> funcList = new ArrayList<>();
		// 是否识别标题
		if (isFuncEnable(recognizeVO.getSfbt())) {
			funcList.add(RecognizeFuncEnum.TITLE);
		}
		// 是否识别手写签名
		if (isFuncEnable(recognizeVO.getSfsx())) {
			funcList.add(RecognizeFuncEnum.SIGNATURE);
		}
		// 是否识别印章
		if (isFuncEnable(recognizeVO.getSfyz())) {
			funcList.add(RecognizeFuncEnum.Seal);
		}
		// 是否识别指纹
		if (isFuncEnable(recognizeVO.getSfzw())) {
			funcList.add(RecognizeFuncEnum.Fingerprint);
		}

		byte[] fileBytes = Base64.decodeBase64(recognizeVO.getWjnr());

		return ocrService.recognize(fileBytes, recognizeVO.getWjhz(), funcList);
	}
	
	@Override
	public WebResult<?> recognize(PicFileRecognizeReqVo picFileRecognizeReqVo) {
		List<RecognizeFuncEnum> funcList = new ArrayList<>();
		// 是否识别标题
		if (isFuncEnable(picFileRecognizeReqVo.getSfbt())) {
			funcList.add(RecognizeFuncEnum.TITLE);
		}
		// 是否识别手写签名
		if (isFuncEnable(picFileRecognizeReqVo.getSfsx())) {
			funcList.add(RecognizeFuncEnum.SIGNATURE);
		}
		// 是否识别印章
		if (isFuncEnable(picFileRecognizeReqVo.getSfyz())) {
			funcList.add(RecognizeFuncEnum.Seal);
		}
		// 是否识别指纹
		if (isFuncEnable(picFileRecognizeReqVo.getSfzw())) {
			funcList.add(RecognizeFuncEnum.Fingerprint);
		}

		byte[] fileBytes = Base64.decodeBase64(picFileRecognizeReqVo.getWjnr());

		Result<List<Wjtz>> wjtzResult = ocrService.recognize(fileBytes, "", funcList);
		
		if(wjtzResult.isSuccess() && CollectionUtils.isNotEmpty(wjtzResult.getData())) {
			PicFileRecognizeResult picFileRecognizeResult = new PicFileRecognizeResult();
			picFileRecognizeResult.setWjtz(wjtzResult.getData());
			picFileRecognizeResult.setSfkby("true");
			WebResult<PicFileRecognizeResult> result= WebResult.success(picFileRecognizeResult);
			result.setFileId(wjtzResult.getFileId());
			return result;
		}
		return WebResult.failed(wjtzResult.getError().getMsg());
	}

	private boolean isFuncEnable(String sfbt) {
		return StringUtils.isNotBlank(sfbt) && OcrConstants.RecognizeFuncFlag.ENABLE.equals(sfbt);
	}

	@Override
	public List<OcrFile> getOcrByBSBH(String bsbh) {

		return ocrFileService.getOcrByBSBH(bsbh);
	}

	/**
	 * @param bsbh
	 * @param taskid
	 * @param wjxh
	 * @return
	 */
	@Override
	public byte[] getDZJZJPG(String bsbh, String taskid, String wjxh) {
		return ocrFileService.getDZJZJPG(bsbh,taskid,wjxh);
	}
}
