package com.iflytek.jzcpx.procuracy.web.sharding;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Properties;

import lombok.Generated;
import org.apache.commons.lang3.StringUtils;
import org.apache.shardingsphere.sharding.api.sharding.ShardingAutoTableAlgorithm;
import org.apache.shardingsphere.sharding.api.sharding.standard.PreciseShardingValue;
import org.apache.shardingsphere.sharding.api.sharding.standard.RangeShardingValue;
import org.apache.shardingsphere.sharding.api.sharding.standard.StandardShardingAlgorithm;
import org.springframework.stereotype.Component;

/**
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2022/7/22
 */
@Component
public class OcrShardingAlgorithm implements StandardShardingAlgorithm<String>, ShardingAutoTableAlgorithm {

    private Properties props = new Properties();
    private int shardingCount;


    @Override
    public void init() {
        this.shardingCount = this.getShardingCount();
    }

    private int getShardingCount() {
        if (this.props.containsKey("sharding-count")) {
            return Integer.parseInt(this.props.get("sharding-count").toString());
        }
        return 0;
    }

    @Override
    public String getType() {
        return "HEX_MOD";
    }

    public Collection<String> getAllPropertyKeys() {
        return Collections.singletonList("sharding-count");
    }

    public int getAutoTablesAmount() {
        return this.shardingCount;
    }

    @Generated
    public Properties getProps() {
        return this.props;
    }

    @Generated
    public void setProps(Properties props) {
        this.props = props;
    }

    @Generated
    public void setShardingCount(int shardingCount) {
        this.shardingCount = shardingCount;
    }

    @Override
    public String doSharding(Collection<String> availableTargetNames, PreciseShardingValue<String> shardingValue) {
        String suffix = String.valueOf(getLongValue(shardingValue.getValue()) % shardingCount);
        return findMatchedTargetName(availableTargetNames, suffix, shardingValue.getDataNodeInfo()).orElse(null);
    }

    @Override
    public Collection<String> doSharding(Collection<String> availableTargetNames,
            RangeShardingValue<String> shardingValue) {
        return isContainAllTargets(shardingValue) ? availableTargetNames : getAvailableTargetNames(availableTargetNames,
                                                                                                   shardingValue);
    }

    private boolean isContainAllTargets(final RangeShardingValue<String> shardingValue) {
        return !shardingValue.getValueRange().hasUpperBound() ||
               shardingValue.getValueRange().hasLowerBound() && getLongValue(
                       shardingValue.getValueRange().upperEndpoint()) - getLongValue(
                       shardingValue.getValueRange().lowerEndpoint()) >= shardingCount - 1;
    }

    private Collection<String> getAvailableTargetNames(final Collection<String> availableTargetNames,
            final RangeShardingValue<String> shardingValue) {
        Collection<String> result = new LinkedHashSet<>(availableTargetNames.size());
        for (long i = getLongValue(shardingValue.getValueRange().lowerEndpoint()); i <= getLongValue(
                shardingValue.getValueRange().upperEndpoint()); i++) {
            String suffix = String.valueOf(i % shardingCount);
            findMatchedTargetName(availableTargetNames, suffix, shardingValue.getDataNodeInfo()).ifPresent(result::add);
        }
        return result;
    }

    private long getLongValue(final String value) {
        if (StringUtils.isBlank(value)) {
            return 0;
        }
        char ch = value.charAt(value.length());
        if (Character.digit(ch, 16) == -1) {
            return 0;
        }
        return Integer.parseInt(String.valueOf(ch), 16);
    }

}
