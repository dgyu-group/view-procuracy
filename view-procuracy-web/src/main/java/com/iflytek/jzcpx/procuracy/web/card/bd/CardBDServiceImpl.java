package com.iflytek.jzcpx.procuracy.web.card.bd;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.iflytek.jzcpx.procuracy.card.entity.CardFile;
import com.iflytek.jzcpx.procuracy.card.entity.CardFormField;
import com.iflytek.jzcpx.procuracy.card.pojo.ElleFileInHandleParam;
import com.iflytek.jzcpx.procuracy.card.pojo.ElleFullParam;
import com.iflytek.jzcpx.procuracy.card.pojo.ElleImgInAcceptParam;
import com.iflytek.jzcpx.procuracy.card.pojo.ElleInFolderParam;
import com.iflytek.jzcpx.procuracy.card.pojo.ElleOuterInHandleParam;
import com.iflytek.jzcpx.procuracy.card.pojo.ElleOuterSyncInHandleParam;
import com.iflytek.jzcpx.procuracy.card.pojo.ElleWordInAcceptParam;
import com.iflytek.jzcpx.procuracy.card.service.CardFileService;
import com.iflytek.jzcpx.procuracy.card.service.CardFormFieldService;
import com.iflytek.jzcpx.procuracy.card.service.CardService;
import com.iflytek.jzcpx.procuracy.common.result.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019/8/20 19:14
 */
@Service
public class CardBDServiceImpl implements CardBDService {
    public static final Logger logger = LoggerFactory.getLogger(CardBDServiceImpl.class);

    @Autowired
    private CardService cardService;
    @Autowired
    private CardFormFieldService formFieldService;
    @Autowired
    private CardFileService cardFileService;

    @Override
    public Result<JSONObject> extractBase64ImgInAccept(ElleImgInAcceptParam imgExtractParam) {
        return cardService.extractFromImgInAccept(imgExtractParam);
    }

    @Override
    public Result<JSONObject> extractWordInAccept(ElleWordInAcceptParam wordInAcceptParam) {
        return cardService.extractFromWordInAccept(wordInAcceptParam);
    }


    @Override
    public Result<JSONObject> extractFileInHandle(ElleFileInHandleParam elleFileInHandleParam) {
        return cardService.extractFileInHandle(elleFileInHandleParam);
    }

    @Override
    public Result inFolder(ElleInFolderParam elleInFolderParam) {
        return cardService.inFolder(elleInFolderParam);
    }

    @Override
    public Result<JSONObject> handleOuterSync(ElleOuterSyncInHandleParam elleOuterSyncInHandleParam) {
        return cardService.handleOuterSync(elleOuterSyncInHandleParam);
    }

    @Override
    public Result handleOuterAsync(ElleOuterInHandleParam elleOuterInHandleParam) {
        return cardService.handleOuterAsync(elleOuterInHandleParam);
    }

    @Override
    public Result<JSONObject> handlFull(ElleFullParam elleFullParam) {
        return cardService.handleFull(elleFullParam);
    }

    @Override
    public Map<CardFile, List<CardFormField>> listFieldsForDiff(String bmsah, String stbm, String stzd, String id) {
        return formFieldService.listFieldsForDiff(bmsah, stbm, stzd, id);
    }

	@Override
	public boolean updateBmsahByWjbm(String wjbm, String bmsah) {
		return this.cardFileService.updateBmsahByWjbm(wjbm, bmsah);
	}
}
