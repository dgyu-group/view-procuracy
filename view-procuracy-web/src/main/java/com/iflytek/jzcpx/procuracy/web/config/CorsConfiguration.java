package com.iflytek.jzcpx.procuracy.web.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 跨域配置
 * 
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-06 16:30
 */
@Configuration
public class CorsConfiguration implements WebMvcConfigurer {
    /*
     * @Bean public WebMvcConfigurer corsConfigurer() { return new
     * WebMvcConfigurer() {
     * 
     * @Override public void addCorsMappings(CorsRegistry registry) {
     * registry.addMapping("/**"); } }; }
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
	registry.addMapping("/**")
		// 放行哪些原始域
		.allowedOrigins("*").allowedMethods(new String[] { "GET", "POST", "PUT", "DELETE" }).allowedHeaders("*").exposedHeaders("access-control-allow-headers", "access-control-allow-methods", "access-control-allow-origin", "access-control-max-age", "X-Frame-Options");
    }
}