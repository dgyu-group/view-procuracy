package com.iflytek.jzcpx.procuracy.web.sharding;

import java.util.Properties;

import org.apache.shardingsphere.sharding.spi.KeyGenerateAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2022/7/22
 */
public class MyKeyGenerateAlgorithm implements KeyGenerateAlgorithm {
    protected static final Logger logger = LoggerFactory.getLogger(MyKeyGenerateAlgorithm.class);

    private Properties props = new Properties();
    private GuidGenerator guidGenerator;

    @Override
    public void init() {
        guidGenerator = new GuidGenerator();
    }

    @Override
    public synchronized Comparable<?> generateKey() {
        return guidGenerator.nextId();
    }

    @Override
    public boolean isDefault() {
        return true;
    }

    @Override
    public String getType() {
        return "mysnowflake";
    }

    @Override
    public Properties getProps() {
        return props;
    }

    @Override
    public void setProps(Properties props) {
        this.props = props;
    }
}
