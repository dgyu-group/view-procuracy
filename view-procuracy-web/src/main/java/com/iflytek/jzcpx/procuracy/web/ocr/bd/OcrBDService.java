package com.iflytek.jzcpx.procuracy.web.ocr.bd;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.iflytek.jzcpx.procuracy.common.result.Result;
import com.iflytek.jzcpx.procuracy.common.result.WebResult;
import com.iflytek.jzcpx.procuracy.ocr.entity.OcrFile;
import com.iflytek.jzcpx.procuracy.ocr.entity.swx.BatchOcrVO;
import com.iflytek.jzcpx.procuracy.ocr.entity.swx.OcrTxsbVo;
import com.iflytek.jzcpx.procuracy.ocr.entity.swx.OcrWJsbSingleResult;
import com.iflytek.jzcpx.procuracy.ocr.entity.txsb.PicFileRecognizeReqVo;
import com.iflytek.jzcpx.procuracy.ocr.recognize.model.OcrResultPdfQueryDto;
import com.iflytek.jzcpx.procuracy.ocr.recognize.model.Wjtz;

/**
 * OCR业务代理接口
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-06 19:55
 */
public interface OcrBDService {

    /**
     * ocr 识别并生成 pdf 文件
     *
     * @param ocrParamVO
     * @return
     */
    Result<OcrWJsbSingleResult> ocrToPdf(OcrResultPdfQueryDto ocrParamVO);

    /**
     * 异步批量 ocr 识别并生成 pdf 文件
     *
     * @param batchOcrVO
     * @return
     */
    Result batchOcrToPdf(BatchOcrVO batchOcrVO,JSONObject obj);

    /**
     * 异步批量图像识别
     *
     * @param batchOcrVO
     * @return
     */
    Result batchRecognize(BatchOcrVO batchOcrVO,JSONObject obj);

    /**
     * 图像识别
     *
     * @param recognizeVO
     * @return
     */
    Result<List<Wjtz>> recognize(OcrTxsbVo recognizeVO);

    /**
     * @param bsbh
     * @return
     */
    List<OcrFile> getOcrByBSBH(String bsbh);

    /**
     * @param bsbh
     * @param taskid
     * @param wjxh
     * @return
     */
    byte[] getDZJZJPG(String bsbh, String taskid, String wjxh);
    /**
     * 
     * @param picFileRecognizeReqVo
     * @return
     */
    WebResult<?> recognize(PicFileRecognizeReqVo picFileRecognizeReqVo);
}
