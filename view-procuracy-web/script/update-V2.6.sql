ALTER TABLE `t_recognize_task` ADD INDEX idx_recognize_task_create_time ( `create_time` ) ;
ALTER TABLE `t_recognize_file` ADD INDEX idx_recognize_file_create_time ( `create_time` ) ;
ALTER TABLE `t_ocr_task` ADD INDEX idx_ocr_task_create_time ( `create_time` ) ;
ALTER TABLE `t_ocr_file` ADD INDEX idx_ocr_file_create_time ( `create_time` ) ;

ALTER TABLE `t_ocr_task` ADD INDEX idx_ocr_task_bsbh ( `bsbh` ) ;
ALTER TABLE `t_ocr_file` ADD INDEX idx_ocr_file_bsbh ( `bsbh` ) ;
ALTER TABLE `t_recognize_task` ADD INDEX idx_recognize_task_bsbh ( `bsbh` ) ;
ALTER TABLE `t_recognize_file` ADD INDEX idx_bsbh ( `bsbh` ) ;
ALTER TABLE `t_recognize_file` ADD INDEX idx_recognize_task_id ( `recognize_task_id` ) ;
ALTER TABLE `t_recognize_file` ADD INDEX idx_recognize_file_wjxh ( `wjxh` ) ;
ALTER TABLE `t_ocr_file` ADD INDEX idx_ocr_file_wjxh ( `wjxh` ) ;
