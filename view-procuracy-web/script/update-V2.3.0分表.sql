CREATE TABLE `t_metric_0`
(
    `id`             bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `mrtric_type`    varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '指标类型ocr:图文识别  card:案卡回填 cont：编目',
    `data_id`        varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '数据标识',
    `create_time`    datetime                                                       NULL DEFAULT NULL COMMENT '创建时间',
    `update_time`    datetime                                                       NULL DEFAULT NULL COMMENT '更新时间',
    `caller_ip`      varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '调用方IP',
    `service_ip`     varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '服务提供方IP',
    `systemid`       varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '应用和系统唯一id',
    `interface_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '接口名',
    `wjxh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件编号',
    `file_id`        bigint(20) UNSIGNED                                            NULL DEFAULT NULL COMMENT '文件id 存储ocrFileId或者cardFileId',
    `ocr_start_time` datetime                                                       NULL DEFAULT NULL COMMENT '调用OCR开始时间',
    `ocr_end_time`   datetime                                                       NULL DEFAULT NULL COMMENT '调用OCR结束时间',
    `start_millis`   bigint(20) UNSIGNED                                            NOT NULL COMMENT '开始时间 单位毫秒',
    `end_millis`     bigint(20) UNSIGNED                                            NULL DEFAULT NULL COMMENT '结束时间 单位毫秒',
    `cost_time`      bigint(20)                                                     NULL DEFAULT NULL COMMENT '耗时 单位毫秒 原长度32',
    `ocr_result`     varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '调用OCR结果 succes或者fail',
    `remark`         varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `dwbm`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '单位编码',
    `bmsah`          varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '部门受案号',
    `ajlbbm`         varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '案件类别编码',
    `bsbh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`         varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务编号',
    `jzbh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `jzmc`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗名称',
    `mlbh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '目录编号',
    `wslx`           varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '文书类型',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_data_id` (`data_id`) USING BTREE,
    INDEX `idx_wjxh` (`wjxh`) USING BTREE,
    INDEX `idx_create_time` (`create_time`) USING BTREE,
    INDEX `idx_file_id` (`file_id`) USING BTREE,
    INDEX `idx_dwbm` (`dwbm`) USING BTREE,
    INDEX `idx_bmsah` (`bmsah`) USING BTREE,
    INDEX `idx_ajlbbm` (`ajlbbm`) USING BTREE,
    INDEX `idx_bsbh` (`bsbh`) USING BTREE,
    INDEX `idx_jzbh` (`jzbh`) USING BTREE,
    INDEX `idx_mlbh` (`mlbh`) USING BTREE,
    INDEX `idx_wslx` (`wslx`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '指标统计表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_metric_1`
(
    `id`             bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `mrtric_type`    varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '指标类型ocr:图文识别  card:案卡回填 cont：编目',
    `data_id`        varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '数据标识',
    `create_time`    datetime                                                       NULL DEFAULT NULL COMMENT '创建时间',
    `update_time`    datetime                                                       NULL DEFAULT NULL COMMENT '更新时间',
    `caller_ip`      varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '调用方IP',
    `service_ip`     varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '服务提供方IP',
    `systemid`       varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '应用和系统唯一id',
    `interface_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '接口名',
    `wjxh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件编号',
    `file_id`        bigint(20) UNSIGNED                                            NULL DEFAULT NULL COMMENT '文件id 存储ocrFileId或者cardFileId',
    `ocr_start_time` datetime                                                       NULL DEFAULT NULL COMMENT '调用OCR开始时间',
    `ocr_end_time`   datetime                                                       NULL DEFAULT NULL COMMENT '调用OCR结束时间',
    `start_millis`   bigint(20) UNSIGNED                                            NOT NULL COMMENT '开始时间 单位毫秒',
    `end_millis`     bigint(20) UNSIGNED                                            NULL DEFAULT NULL COMMENT '结束时间 单位毫秒',
    `cost_time`      bigint(20)                                                     NULL DEFAULT NULL COMMENT '耗时 单位毫秒 原长度32',
    `ocr_result`     varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '调用OCR结果 succes或者fail',
    `remark`         varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `dwbm`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '单位编码',
    `bmsah`          varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '部门受案号',
    `ajlbbm`         varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '案件类别编码',
    `bsbh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`         varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务编号',
    `jzbh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `jzmc`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗名称',
    `mlbh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '目录编号',
    `wslx`           varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '文书类型',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_data_id` (`data_id`) USING BTREE,
    INDEX `idx_wjxh` (`wjxh`) USING BTREE,
    INDEX `idx_create_time` (`create_time`) USING BTREE,
    INDEX `idx_file_id` (`file_id`) USING BTREE,
    INDEX `idx_dwbm` (`dwbm`) USING BTREE,
    INDEX `idx_bmsah` (`bmsah`) USING BTREE,
    INDEX `idx_ajlbbm` (`ajlbbm`) USING BTREE,
    INDEX `idx_bsbh` (`bsbh`) USING BTREE,
    INDEX `idx_jzbh` (`jzbh`) USING BTREE,
    INDEX `idx_mlbh` (`mlbh`) USING BTREE,
    INDEX `idx_wslx` (`wslx`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '指标统计表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_metric_2`
(
    `id`             bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `mrtric_type`    varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '指标类型ocr:图文识别  card:案卡回填 cont：编目',
    `data_id`        varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '数据标识',
    `create_time`    datetime                                                       NULL DEFAULT NULL COMMENT '创建时间',
    `update_time`    datetime                                                       NULL DEFAULT NULL COMMENT '更新时间',
    `caller_ip`      varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '调用方IP',
    `service_ip`     varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '服务提供方IP',
    `systemid`       varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '应用和系统唯一id',
    `interface_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '接口名',
    `wjxh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件编号',
    `file_id`        bigint(20) UNSIGNED                                            NULL DEFAULT NULL COMMENT '文件id 存储ocrFileId或者cardFileId',
    `ocr_start_time` datetime                                                       NULL DEFAULT NULL COMMENT '调用OCR开始时间',
    `ocr_end_time`   datetime                                                       NULL DEFAULT NULL COMMENT '调用OCR结束时间',
    `start_millis`   bigint(20) UNSIGNED                                            NOT NULL COMMENT '开始时间 单位毫秒',
    `end_millis`     bigint(20) UNSIGNED                                            NULL DEFAULT NULL COMMENT '结束时间 单位毫秒',
    `cost_time`      bigint(20)                                                     NULL DEFAULT NULL COMMENT '耗时 单位毫秒 原长度32',
    `ocr_result`     varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '调用OCR结果 succes或者fail',
    `remark`         varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `dwbm`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '单位编码',
    `bmsah`          varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '部门受案号',
    `ajlbbm`         varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '案件类别编码',
    `bsbh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`         varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务编号',
    `jzbh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `jzmc`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗名称',
    `mlbh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '目录编号',
    `wslx`           varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '文书类型',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_data_id` (`data_id`) USING BTREE,
    INDEX `idx_wjxh` (`wjxh`) USING BTREE,
    INDEX `idx_create_time` (`create_time`) USING BTREE,
    INDEX `idx_file_id` (`file_id`) USING BTREE,
    INDEX `idx_dwbm` (`dwbm`) USING BTREE,
    INDEX `idx_bmsah` (`bmsah`) USING BTREE,
    INDEX `idx_ajlbbm` (`ajlbbm`) USING BTREE,
    INDEX `idx_bsbh` (`bsbh`) USING BTREE,
    INDEX `idx_jzbh` (`jzbh`) USING BTREE,
    INDEX `idx_mlbh` (`mlbh`) USING BTREE,
    INDEX `idx_wslx` (`wslx`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '指标统计表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_metric_3`
(
    `id`             bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `mrtric_type`    varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '指标类型ocr:图文识别  card:案卡回填 cont：编目',
    `data_id`        varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '数据标识',
    `create_time`    datetime                                                       NULL DEFAULT NULL COMMENT '创建时间',
    `update_time`    datetime                                                       NULL DEFAULT NULL COMMENT '更新时间',
    `caller_ip`      varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '调用方IP',
    `service_ip`     varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '服务提供方IP',
    `systemid`       varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '应用和系统唯一id',
    `interface_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '接口名',
    `wjxh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件编号',
    `file_id`        bigint(20) UNSIGNED                                            NULL DEFAULT NULL COMMENT '文件id 存储ocrFileId或者cardFileId',
    `ocr_start_time` datetime                                                       NULL DEFAULT NULL COMMENT '调用OCR开始时间',
    `ocr_end_time`   datetime                                                       NULL DEFAULT NULL COMMENT '调用OCR结束时间',
    `start_millis`   bigint(20) UNSIGNED                                            NOT NULL COMMENT '开始时间 单位毫秒',
    `end_millis`     bigint(20) UNSIGNED                                            NULL DEFAULT NULL COMMENT '结束时间 单位毫秒',
    `cost_time`      bigint(20)                                                     NULL DEFAULT NULL COMMENT '耗时 单位毫秒 原长度32',
    `ocr_result`     varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '调用OCR结果 succes或者fail',
    `remark`         varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `dwbm`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '单位编码',
    `bmsah`          varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '部门受案号',
    `ajlbbm`         varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '案件类别编码',
    `bsbh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`         varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务编号',
    `jzbh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `jzmc`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗名称',
    `mlbh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '目录编号',
    `wslx`           varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '文书类型',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_data_id` (`data_id`) USING BTREE,
    INDEX `idx_wjxh` (`wjxh`) USING BTREE,
    INDEX `idx_create_time` (`create_time`) USING BTREE,
    INDEX `idx_file_id` (`file_id`) USING BTREE,
    INDEX `idx_dwbm` (`dwbm`) USING BTREE,
    INDEX `idx_bmsah` (`bmsah`) USING BTREE,
    INDEX `idx_ajlbbm` (`ajlbbm`) USING BTREE,
    INDEX `idx_bsbh` (`bsbh`) USING BTREE,
    INDEX `idx_jzbh` (`jzbh`) USING BTREE,
    INDEX `idx_mlbh` (`mlbh`) USING BTREE,
    INDEX `idx_wslx` (`wslx`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '指标统计表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_metric_4`
(
    `id`             bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `mrtric_type`    varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '指标类型ocr:图文识别  card:案卡回填 cont：编目',
    `data_id`        varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '数据标识',
    `create_time`    datetime                                                       NULL DEFAULT NULL COMMENT '创建时间',
    `update_time`    datetime                                                       NULL DEFAULT NULL COMMENT '更新时间',
    `caller_ip`      varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '调用方IP',
    `service_ip`     varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '服务提供方IP',
    `systemid`       varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '应用和系统唯一id',
    `interface_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '接口名',
    `wjxh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件编号',
    `file_id`        bigint(20) UNSIGNED                                            NULL DEFAULT NULL COMMENT '文件id 存储ocrFileId或者cardFileId',
    `ocr_start_time` datetime                                                       NULL DEFAULT NULL COMMENT '调用OCR开始时间',
    `ocr_end_time`   datetime                                                       NULL DEFAULT NULL COMMENT '调用OCR结束时间',
    `start_millis`   bigint(20) UNSIGNED                                            NOT NULL COMMENT '开始时间 单位毫秒',
    `end_millis`     bigint(20) UNSIGNED                                            NULL DEFAULT NULL COMMENT '结束时间 单位毫秒',
    `cost_time`      bigint(20)                                                     NULL DEFAULT NULL COMMENT '耗时 单位毫秒 原长度32',
    `ocr_result`     varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '调用OCR结果 succes或者fail',
    `remark`         varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `dwbm`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '单位编码',
    `bmsah`          varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '部门受案号',
    `ajlbbm`         varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '案件类别编码',
    `bsbh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`         varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务编号',
    `jzbh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `jzmc`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗名称',
    `mlbh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '目录编号',
    `wslx`           varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '文书类型',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_data_id` (`data_id`) USING BTREE,
    INDEX `idx_wjxh` (`wjxh`) USING BTREE,
    INDEX `idx_create_time` (`create_time`) USING BTREE,
    INDEX `idx_file_id` (`file_id`) USING BTREE,
    INDEX `idx_dwbm` (`dwbm`) USING BTREE,
    INDEX `idx_bmsah` (`bmsah`) USING BTREE,
    INDEX `idx_ajlbbm` (`ajlbbm`) USING BTREE,
    INDEX `idx_bsbh` (`bsbh`) USING BTREE,
    INDEX `idx_jzbh` (`jzbh`) USING BTREE,
    INDEX `idx_mlbh` (`mlbh`) USING BTREE,
    INDEX `idx_wslx` (`wslx`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '指标统计表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_metric_5`
(
    `id`             bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `mrtric_type`    varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '指标类型ocr:图文识别  card:案卡回填 cont：编目',
    `data_id`        varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '数据标识',
    `create_time`    datetime                                                       NULL DEFAULT NULL COMMENT '创建时间',
    `update_time`    datetime                                                       NULL DEFAULT NULL COMMENT '更新时间',
    `caller_ip`      varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '调用方IP',
    `service_ip`     varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '服务提供方IP',
    `systemid`       varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '应用和系统唯一id',
    `interface_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '接口名',
    `wjxh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件编号',
    `file_id`        bigint(20) UNSIGNED                                            NULL DEFAULT NULL COMMENT '文件id 存储ocrFileId或者cardFileId',
    `ocr_start_time` datetime                                                       NULL DEFAULT NULL COMMENT '调用OCR开始时间',
    `ocr_end_time`   datetime                                                       NULL DEFAULT NULL COMMENT '调用OCR结束时间',
    `start_millis`   bigint(20) UNSIGNED                                            NOT NULL COMMENT '开始时间 单位毫秒',
    `end_millis`     bigint(20) UNSIGNED                                            NULL DEFAULT NULL COMMENT '结束时间 单位毫秒',
    `cost_time`      bigint(20)                                                     NULL DEFAULT NULL COMMENT '耗时 单位毫秒 原长度32',
    `ocr_result`     varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '调用OCR结果 succes或者fail',
    `remark`         varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `dwbm`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '单位编码',
    `bmsah`          varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '部门受案号',
    `ajlbbm`         varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '案件类别编码',
    `bsbh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`         varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务编号',
    `jzbh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `jzmc`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗名称',
    `mlbh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '目录编号',
    `wslx`           varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '文书类型',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_data_id` (`data_id`) USING BTREE,
    INDEX `idx_wjxh` (`wjxh`) USING BTREE,
    INDEX `idx_create_time` (`create_time`) USING BTREE,
    INDEX `idx_file_id` (`file_id`) USING BTREE,
    INDEX `idx_dwbm` (`dwbm`) USING BTREE,
    INDEX `idx_bmsah` (`bmsah`) USING BTREE,
    INDEX `idx_ajlbbm` (`ajlbbm`) USING BTREE,
    INDEX `idx_bsbh` (`bsbh`) USING BTREE,
    INDEX `idx_jzbh` (`jzbh`) USING BTREE,
    INDEX `idx_mlbh` (`mlbh`) USING BTREE,
    INDEX `idx_wslx` (`wslx`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '指标统计表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_metric_6`
(
    `id`             bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `mrtric_type`    varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '指标类型ocr:图文识别  card:案卡回填 cont：编目',
    `data_id`        varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '数据标识',
    `create_time`    datetime                                                       NULL DEFAULT NULL COMMENT '创建时间',
    `update_time`    datetime                                                       NULL DEFAULT NULL COMMENT '更新时间',
    `caller_ip`      varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '调用方IP',
    `service_ip`     varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '服务提供方IP',
    `systemid`       varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '应用和系统唯一id',
    `interface_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '接口名',
    `wjxh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件编号',
    `file_id`        bigint(20) UNSIGNED                                            NULL DEFAULT NULL COMMENT '文件id 存储ocrFileId或者cardFileId',
    `ocr_start_time` datetime                                                       NULL DEFAULT NULL COMMENT '调用OCR开始时间',
    `ocr_end_time`   datetime                                                       NULL DEFAULT NULL COMMENT '调用OCR结束时间',
    `start_millis`   bigint(20) UNSIGNED                                            NOT NULL COMMENT '开始时间 单位毫秒',
    `end_millis`     bigint(20) UNSIGNED                                            NULL DEFAULT NULL COMMENT '结束时间 单位毫秒',
    `cost_time`      bigint(20)                                                     NULL DEFAULT NULL COMMENT '耗时 单位毫秒 原长度32',
    `ocr_result`     varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '调用OCR结果 succes或者fail',
    `remark`         varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `dwbm`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '单位编码',
    `bmsah`          varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '部门受案号',
    `ajlbbm`         varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '案件类别编码',
    `bsbh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`         varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务编号',
    `jzbh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `jzmc`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗名称',
    `mlbh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '目录编号',
    `wslx`           varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '文书类型',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_data_id` (`data_id`) USING BTREE,
    INDEX `idx_wjxh` (`wjxh`) USING BTREE,
    INDEX `idx_create_time` (`create_time`) USING BTREE,
    INDEX `idx_file_id` (`file_id`) USING BTREE,
    INDEX `idx_dwbm` (`dwbm`) USING BTREE,
    INDEX `idx_bmsah` (`bmsah`) USING BTREE,
    INDEX `idx_ajlbbm` (`ajlbbm`) USING BTREE,
    INDEX `idx_bsbh` (`bsbh`) USING BTREE,
    INDEX `idx_jzbh` (`jzbh`) USING BTREE,
    INDEX `idx_mlbh` (`mlbh`) USING BTREE,
    INDEX `idx_wslx` (`wslx`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '指标统计表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_metric_7`
(
    `id`             bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `mrtric_type`    varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '指标类型ocr:图文识别  card:案卡回填 cont：编目',
    `data_id`        varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '数据标识',
    `create_time`    datetime                                                       NULL DEFAULT NULL COMMENT '创建时间',
    `update_time`    datetime                                                       NULL DEFAULT NULL COMMENT '更新时间',
    `caller_ip`      varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '调用方IP',
    `service_ip`     varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '服务提供方IP',
    `systemid`       varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '应用和系统唯一id',
    `interface_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '接口名',
    `wjxh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件编号',
    `file_id`        bigint(20) UNSIGNED                                            NULL DEFAULT NULL COMMENT '文件id 存储ocrFileId或者cardFileId',
    `ocr_start_time` datetime                                                       NULL DEFAULT NULL COMMENT '调用OCR开始时间',
    `ocr_end_time`   datetime                                                       NULL DEFAULT NULL COMMENT '调用OCR结束时间',
    `start_millis`   bigint(20) UNSIGNED                                            NOT NULL COMMENT '开始时间 单位毫秒',
    `end_millis`     bigint(20) UNSIGNED                                            NULL DEFAULT NULL COMMENT '结束时间 单位毫秒',
    `cost_time`      bigint(20)                                                     NULL DEFAULT NULL COMMENT '耗时 单位毫秒 原长度32',
    `ocr_result`     varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '调用OCR结果 succes或者fail',
    `remark`         varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `dwbm`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '单位编码',
    `bmsah`          varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '部门受案号',
    `ajlbbm`         varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '案件类别编码',
    `bsbh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`         varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务编号',
    `jzbh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `jzmc`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗名称',
    `mlbh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '目录编号',
    `wslx`           varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '文书类型',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_data_id` (`data_id`) USING BTREE,
    INDEX `idx_wjxh` (`wjxh`) USING BTREE,
    INDEX `idx_create_time` (`create_time`) USING BTREE,
    INDEX `idx_file_id` (`file_id`) USING BTREE,
    INDEX `idx_dwbm` (`dwbm`) USING BTREE,
    INDEX `idx_bmsah` (`bmsah`) USING BTREE,
    INDEX `idx_ajlbbm` (`ajlbbm`) USING BTREE,
    INDEX `idx_bsbh` (`bsbh`) USING BTREE,
    INDEX `idx_jzbh` (`jzbh`) USING BTREE,
    INDEX `idx_mlbh` (`mlbh`) USING BTREE,
    INDEX `idx_wslx` (`wslx`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '指标统计表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_metric_8`
(
    `id`             bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `mrtric_type`    varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '指标类型ocr:图文识别  card:案卡回填 cont：编目',
    `data_id`        varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '数据标识',
    `create_time`    datetime                                                       NULL DEFAULT NULL COMMENT '创建时间',
    `update_time`    datetime                                                       NULL DEFAULT NULL COMMENT '更新时间',
    `caller_ip`      varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '调用方IP',
    `service_ip`     varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '服务提供方IP',
    `systemid`       varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '应用和系统唯一id',
    `interface_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '接口名',
    `wjxh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件编号',
    `file_id`        bigint(20) UNSIGNED                                            NULL DEFAULT NULL COMMENT '文件id 存储ocrFileId或者cardFileId',
    `ocr_start_time` datetime                                                       NULL DEFAULT NULL COMMENT '调用OCR开始时间',
    `ocr_end_time`   datetime                                                       NULL DEFAULT NULL COMMENT '调用OCR结束时间',
    `start_millis`   bigint(20) UNSIGNED                                            NOT NULL COMMENT '开始时间 单位毫秒',
    `end_millis`     bigint(20) UNSIGNED                                            NULL DEFAULT NULL COMMENT '结束时间 单位毫秒',
    `cost_time`      bigint(20)                                                     NULL DEFAULT NULL COMMENT '耗时 单位毫秒 原长度32',
    `ocr_result`     varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '调用OCR结果 succes或者fail',
    `remark`         varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `dwbm`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '单位编码',
    `bmsah`          varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '部门受案号',
    `ajlbbm`         varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '案件类别编码',
    `bsbh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`         varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务编号',
    `jzbh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `jzmc`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗名称',
    `mlbh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '目录编号',
    `wslx`           varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '文书类型',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_data_id` (`data_id`) USING BTREE,
    INDEX `idx_wjxh` (`wjxh`) USING BTREE,
    INDEX `idx_create_time` (`create_time`) USING BTREE,
    INDEX `idx_file_id` (`file_id`) USING BTREE,
    INDEX `idx_dwbm` (`dwbm`) USING BTREE,
    INDEX `idx_bmsah` (`bmsah`) USING BTREE,
    INDEX `idx_ajlbbm` (`ajlbbm`) USING BTREE,
    INDEX `idx_bsbh` (`bsbh`) USING BTREE,
    INDEX `idx_jzbh` (`jzbh`) USING BTREE,
    INDEX `idx_mlbh` (`mlbh`) USING BTREE,
    INDEX `idx_wslx` (`wslx`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '指标统计表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_metric_9`
(
    `id`             bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `mrtric_type`    varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '指标类型ocr:图文识别  card:案卡回填 cont：编目',
    `data_id`        varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '数据标识',
    `create_time`    datetime                                                       NULL DEFAULT NULL COMMENT '创建时间',
    `update_time`    datetime                                                       NULL DEFAULT NULL COMMENT '更新时间',
    `caller_ip`      varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '调用方IP',
    `service_ip`     varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '服务提供方IP',
    `systemid`       varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '应用和系统唯一id',
    `interface_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '接口名',
    `wjxh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件编号',
    `file_id`        bigint(20) UNSIGNED                                            NULL DEFAULT NULL COMMENT '文件id 存储ocrFileId或者cardFileId',
    `ocr_start_time` datetime                                                       NULL DEFAULT NULL COMMENT '调用OCR开始时间',
    `ocr_end_time`   datetime                                                       NULL DEFAULT NULL COMMENT '调用OCR结束时间',
    `start_millis`   bigint(20) UNSIGNED                                            NOT NULL COMMENT '开始时间 单位毫秒',
    `end_millis`     bigint(20) UNSIGNED                                            NULL DEFAULT NULL COMMENT '结束时间 单位毫秒',
    `cost_time`      bigint(20)                                                     NULL DEFAULT NULL COMMENT '耗时 单位毫秒 原长度32',
    `ocr_result`     varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '调用OCR结果 succes或者fail',
    `remark`         varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `dwbm`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '单位编码',
    `bmsah`          varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '部门受案号',
    `ajlbbm`         varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '案件类别编码',
    `bsbh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`         varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务编号',
    `jzbh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `jzmc`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗名称',
    `mlbh`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '目录编号',
    `wslx`           varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '文书类型',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_data_id` (`data_id`) USING BTREE,
    INDEX `idx_wjxh` (`wjxh`) USING BTREE,
    INDEX `idx_create_time` (`create_time`) USING BTREE,
    INDEX `idx_file_id` (`file_id`) USING BTREE,
    INDEX `idx_dwbm` (`dwbm`) USING BTREE,
    INDEX `idx_bmsah` (`bmsah`) USING BTREE,
    INDEX `idx_ajlbbm` (`ajlbbm`) USING BTREE,
    INDEX `idx_bsbh` (`bsbh`) USING BTREE,
    INDEX `idx_jzbh` (`jzbh`) USING BTREE,
    INDEX `idx_mlbh` (`mlbh`) USING BTREE,
    INDEX `idx_wslx` (`wslx`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '指标统计表'
  ROW_FORMAT = DYNAMIC;


CREATE TABLE `t_ocr_file_0`
(
    `id`                 bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `ocr_task_id`        bigint(20)                                                     NULL DEFAULT NULL COMMENT '图文识别批量任务 id',
    `taskid`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务编号',
    `bsbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `wjxh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件序号',
    `bmsah`              varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门受案号',
    `ocr_result_path`    varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '识别结果存储路径',
    `engine_result_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `type`               varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '识别类型，单文件或批量任务',
    `status`             varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '状态',
    `fail_info`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务失败信息',
    `remark`             varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `state`              varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_ocr_file_create_time` (`create_time`) USING BTREE,
    INDEX `idx_ocr_file_bsbh` (`bsbh`) USING BTREE,
    INDEX `idx_ocr_task_id` (`ocr_task_id`) USING BTREE,
    INDEX `idx_ocr_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = 'ocr识别任务单元表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_ocr_file_1`
(
    `id`                 bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `ocr_task_id`        bigint(20)                                                     NULL DEFAULT NULL COMMENT '图文识别批量任务 id',
    `taskid`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务编号',
    `bsbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `wjxh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件序号',
    `bmsah`              varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门受案号',
    `ocr_result_path`    varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '识别结果存储路径',
    `engine_result_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `type`               varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '识别类型，单文件或批量任务',
    `status`             varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '状态',
    `fail_info`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务失败信息',
    `remark`             varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `state`              varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_ocr_file_create_time` (`create_time`) USING BTREE,
    INDEX `idx_ocr_file_bsbh` (`bsbh`) USING BTREE,
    INDEX `idx_ocr_task_id` (`ocr_task_id`) USING BTREE,
    INDEX `idx_ocr_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = 'ocr识别任务单元表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_ocr_file_2`
(
    `id`                 bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `ocr_task_id`        bigint(20)                                                     NULL DEFAULT NULL COMMENT '图文识别批量任务 id',
    `taskid`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务编号',
    `bsbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `wjxh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件序号',
    `bmsah`              varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门受案号',
    `ocr_result_path`    varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '识别结果存储路径',
    `engine_result_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `type`               varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '识别类型，单文件或批量任务',
    `status`             varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '状态',
    `fail_info`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务失败信息',
    `remark`             varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `state`              varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_ocr_file_create_time` (`create_time`) USING BTREE,
    INDEX `idx_ocr_file_bsbh` (`bsbh`) USING BTREE,
    INDEX `idx_ocr_task_id` (`ocr_task_id`) USING BTREE,
    INDEX `idx_ocr_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = 'ocr识别任务单元表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_ocr_file_3`
(
    `id`                 bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `ocr_task_id`        bigint(20)                                                     NULL DEFAULT NULL COMMENT '图文识别批量任务 id',
    `taskid`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务编号',
    `bsbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `wjxh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件序号',
    `bmsah`              varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门受案号',
    `ocr_result_path`    varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '识别结果存储路径',
    `engine_result_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `type`               varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '识别类型，单文件或批量任务',
    `status`             varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '状态',
    `fail_info`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务失败信息',
    `remark`             varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `state`              varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_ocr_file_create_time` (`create_time`) USING BTREE,
    INDEX `idx_ocr_file_bsbh` (`bsbh`) USING BTREE,
    INDEX `idx_ocr_task_id` (`ocr_task_id`) USING BTREE,
    INDEX `idx_ocr_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = 'ocr识别任务单元表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_ocr_file_4`
(
    `id`                 bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `ocr_task_id`        bigint(20)                                                     NULL DEFAULT NULL COMMENT '图文识别批量任务 id',
    `taskid`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务编号',
    `bsbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `wjxh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件序号',
    `bmsah`              varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门受案号',
    `ocr_result_path`    varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '识别结果存储路径',
    `engine_result_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `type`               varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '识别类型，单文件或批量任务',
    `status`             varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '状态',
    `fail_info`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务失败信息',
    `remark`             varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `state`              varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_ocr_file_create_time` (`create_time`) USING BTREE,
    INDEX `idx_ocr_file_bsbh` (`bsbh`) USING BTREE,
    INDEX `idx_ocr_task_id` (`ocr_task_id`) USING BTREE,
    INDEX `idx_ocr_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = 'ocr识别任务单元表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_ocr_file_5`
(
    `id`                 bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `ocr_task_id`        bigint(20)                                                     NULL DEFAULT NULL COMMENT '图文识别批量任务 id',
    `taskid`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务编号',
    `bsbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `wjxh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件序号',
    `bmsah`              varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门受案号',
    `ocr_result_path`    varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '识别结果存储路径',
    `engine_result_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `type`               varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '识别类型，单文件或批量任务',
    `status`             varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '状态',
    `fail_info`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务失败信息',
    `remark`             varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `state`              varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_ocr_file_create_time` (`create_time`) USING BTREE,
    INDEX `idx_ocr_file_bsbh` (`bsbh`) USING BTREE,
    INDEX `idx_ocr_task_id` (`ocr_task_id`) USING BTREE,
    INDEX `idx_ocr_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = 'ocr识别任务单元表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_ocr_file_6`
(
    `id`                 bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `ocr_task_id`        bigint(20)                                                     NULL DEFAULT NULL COMMENT '图文识别批量任务 id',
    `taskid`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务编号',
    `bsbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `wjxh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件序号',
    `bmsah`              varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门受案号',
    `ocr_result_path`    varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '识别结果存储路径',
    `engine_result_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `type`               varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '识别类型，单文件或批量任务',
    `status`             varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '状态',
    `fail_info`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务失败信息',
    `remark`             varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `state`              varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_ocr_file_create_time` (`create_time`) USING BTREE,
    INDEX `idx_ocr_file_bsbh` (`bsbh`) USING BTREE,
    INDEX `idx_ocr_task_id` (`ocr_task_id`) USING BTREE,
    INDEX `idx_ocr_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = 'ocr识别任务单元表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_ocr_file_7`
(
    `id`                 bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `ocr_task_id`        bigint(20)                                                     NULL DEFAULT NULL COMMENT '图文识别批量任务 id',
    `taskid`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务编号',
    `bsbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `wjxh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件序号',
    `bmsah`              varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门受案号',
    `ocr_result_path`    varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '识别结果存储路径',
    `engine_result_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `type`               varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '识别类型，单文件或批量任务',
    `status`             varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '状态',
    `fail_info`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务失败信息',
    `remark`             varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `state`              varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_ocr_file_create_time` (`create_time`) USING BTREE,
    INDEX `idx_ocr_file_bsbh` (`bsbh`) USING BTREE,
    INDEX `idx_ocr_task_id` (`ocr_task_id`) USING BTREE,
    INDEX `idx_ocr_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = 'ocr识别任务单元表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_ocr_file_8`
(
    `id`                 bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `ocr_task_id`        bigint(20)                                                     NULL DEFAULT NULL COMMENT '图文识别批量任务 id',
    `taskid`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务编号',
    `bsbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `wjxh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件序号',
    `bmsah`              varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门受案号',
    `ocr_result_path`    varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '识别结果存储路径',
    `engine_result_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `type`               varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '识别类型，单文件或批量任务',
    `status`             varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '状态',
    `fail_info`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务失败信息',
    `remark`             varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `state`              varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_ocr_file_create_time` (`create_time`) USING BTREE,
    INDEX `idx_ocr_file_bsbh` (`bsbh`) USING BTREE,
    INDEX `idx_ocr_task_id` (`ocr_task_id`) USING BTREE,
    INDEX `idx_ocr_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = 'ocr识别任务单元表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_ocr_file_9`
(
    `id`                 bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `ocr_task_id`        bigint(20)                                                     NULL DEFAULT NULL COMMENT '图文识别批量任务 id',
    `taskid`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务编号',
    `bsbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `wjxh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件序号',
    `bmsah`              varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门受案号',
    `ocr_result_path`    varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '识别结果存储路径',
    `engine_result_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `type`               varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '识别类型，单文件或批量任务',
    `status`             varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '状态',
    `fail_info`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务失败信息',
    `remark`             varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `state`              varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_ocr_file_create_time` (`create_time`) USING BTREE,
    INDEX `idx_ocr_file_bsbh` (`bsbh`) USING BTREE,
    INDEX `idx_ocr_task_id` (`ocr_task_id`) USING BTREE,
    INDEX `idx_ocr_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = 'ocr识别任务单元表'
  ROW_FORMAT = DYNAMIC;


CREATE TABLE `t_ocr_task_0`
(
    `id`               bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门受案号',
    `status`           varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `jzbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `fail_nums`        int(10)                                                        NULL DEFAULT NULL COMMENT '失败次数',
    `stat`             varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_ocr_task_create_time` (`create_time`) USING BTREE,
    INDEX `idx_ocr_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = 'ocr识别任务表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_ocr_task_1`
(
    `id`               bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门受案号',
    `status`           varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `jzbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `fail_nums`        int(10)                                                        NULL DEFAULT NULL COMMENT '失败次数',
    `stat`             varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_ocr_task_create_time` (`create_time`) USING BTREE,
    INDEX `idx_ocr_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = 'ocr识别任务表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_ocr_task_2`
(
    `id`               bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门受案号',
    `status`           varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `jzbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `fail_nums`        int(10)                                                        NULL DEFAULT NULL COMMENT '失败次数',
    `stat`             varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_ocr_task_create_time` (`create_time`) USING BTREE,
    INDEX `idx_ocr_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = 'ocr识别任务表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_ocr_task_3`
(
    `id`               bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门受案号',
    `status`           varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `jzbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `fail_nums`        int(10)                                                        NULL DEFAULT NULL COMMENT '失败次数',
    `stat`             varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_ocr_task_create_time` (`create_time`) USING BTREE,
    INDEX `idx_ocr_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = 'ocr识别任务表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_ocr_task_4`
(
    `id`               bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门受案号',
    `status`           varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `jzbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `fail_nums`        int(10)                                                        NULL DEFAULT NULL COMMENT '失败次数',
    `stat`             varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_ocr_task_create_time` (`create_time`) USING BTREE,
    INDEX `idx_ocr_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = 'ocr识别任务表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_ocr_task_5`
(
    `id`               bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门受案号',
    `status`           varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `jzbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `fail_nums`        int(10)                                                        NULL DEFAULT NULL COMMENT '失败次数',
    `stat`             varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_ocr_task_create_time` (`create_time`) USING BTREE,
    INDEX `idx_ocr_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = 'ocr识别任务表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_ocr_task_6`
(
    `id`               bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门受案号',
    `status`           varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `jzbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `fail_nums`        int(10)                                                        NULL DEFAULT NULL COMMENT '失败次数',
    `stat`             varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_ocr_task_create_time` (`create_time`) USING BTREE,
    INDEX `idx_ocr_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = 'ocr识别任务表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_ocr_task_7`
(
    `id`               bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门受案号',
    `status`           varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `jzbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `fail_nums`        int(10)                                                        NULL DEFAULT NULL COMMENT '失败次数',
    `stat`             varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_ocr_task_create_time` (`create_time`) USING BTREE,
    INDEX `idx_ocr_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = 'ocr识别任务表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_ocr_task_8`
(
    `id`               bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门受案号',
    `status`           varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `jzbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `fail_nums`        int(10)                                                        NULL DEFAULT NULL COMMENT '失败次数',
    `stat`             varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_ocr_task_create_time` (`create_time`) USING BTREE,
    INDEX `idx_ocr_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = 'ocr识别任务表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_ocr_task_9`
(
    `id`               bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门受案号',
    `status`           varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `jzbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `fail_nums`        int(10)                                                        NULL DEFAULT NULL COMMENT '失败次数',
    `stat`             varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_ocr_task_create_time` (`create_time`) USING BTREE,
    INDEX `idx_ocr_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = 'ocr识别任务表'
  ROW_FORMAT = DYNAMIC;


CREATE TABLE `t_recognize_file_0`
(
    `id`                 bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `bsbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务编号',
    `bmsah`              varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门受案号',
    `jzbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `mlbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '目录编号',
    `wjxh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件序号',
    `engine_result_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `ocr_result_path`    varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '识别结果存储路径',
    `status`             varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '识别状态',
    `type`               varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '任务类型，单文件或批量任务',
    `func_list`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '待提取数据类型',
    `fail_info`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '失败信息',
    `remark`             varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `recognize_task_id`  bigint(20)                                                     NULL DEFAULT NULL,
    `state`              varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_recognize_file_create_time` (`create_time`) USING BTREE,
    INDEX `idx_bsbh` (`bsbh`) USING BTREE,
    INDEX `idx_recognize_task_id` (`recognize_task_id`) USING BTREE,
    INDEX `idx_recognize_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '图像识别任务单元表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_recognize_file_1`
(
    `id`                 bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `bsbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务编号',
    `bmsah`              varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门受案号',
    `jzbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `mlbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '目录编号',
    `wjxh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件序号',
    `engine_result_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `ocr_result_path`    varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '识别结果存储路径',
    `status`             varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '识别状态',
    `type`               varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '任务类型，单文件或批量任务',
    `func_list`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '待提取数据类型',
    `fail_info`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '失败信息',
    `remark`             varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `recognize_task_id`  bigint(20)                                                     NULL DEFAULT NULL,
    `state`              varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_recognize_file_create_time` (`create_time`) USING BTREE,
    INDEX `idx_bsbh` (`bsbh`) USING BTREE,
    INDEX `idx_recognize_task_id` (`recognize_task_id`) USING BTREE,
    INDEX `idx_recognize_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '图像识别任务单元表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_recognize_file_2`
(
    `id`                 bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `bsbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务编号',
    `bmsah`              varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门受案号',
    `jzbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `mlbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '目录编号',
    `wjxh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件序号',
    `engine_result_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `ocr_result_path`    varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '识别结果存储路径',
    `status`             varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '识别状态',
    `type`               varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '任务类型，单文件或批量任务',
    `func_list`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '待提取数据类型',
    `fail_info`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '失败信息',
    `remark`             varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `recognize_task_id`  bigint(20)                                                     NULL DEFAULT NULL,
    `state`              varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_recognize_file_create_time` (`create_time`) USING BTREE,
    INDEX `idx_bsbh` (`bsbh`) USING BTREE,
    INDEX `idx_recognize_task_id` (`recognize_task_id`) USING BTREE,
    INDEX `idx_recognize_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '图像识别任务单元表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_recognize_file_3`
(
    `id`                 bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `bsbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务编号',
    `bmsah`              varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门受案号',
    `jzbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `mlbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '目录编号',
    `wjxh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件序号',
    `engine_result_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `ocr_result_path`    varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '识别结果存储路径',
    `status`             varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '识别状态',
    `type`               varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '任务类型，单文件或批量任务',
    `func_list`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '待提取数据类型',
    `fail_info`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '失败信息',
    `remark`             varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `recognize_task_id`  bigint(20)                                                     NULL DEFAULT NULL,
    `state`              varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_recognize_file_create_time` (`create_time`) USING BTREE,
    INDEX `idx_bsbh` (`bsbh`) USING BTREE,
    INDEX `idx_recognize_task_id` (`recognize_task_id`) USING BTREE,
    INDEX `idx_recognize_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '图像识别任务单元表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_recognize_file_4`
(
    `id`                 bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `bsbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务编号',
    `bmsah`              varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门受案号',
    `jzbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `mlbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '目录编号',
    `wjxh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件序号',
    `engine_result_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `ocr_result_path`    varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '识别结果存储路径',
    `status`             varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '识别状态',
    `type`               varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '任务类型，单文件或批量任务',
    `func_list`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '待提取数据类型',
    `fail_info`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '失败信息',
    `remark`             varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `recognize_task_id`  bigint(20)                                                     NULL DEFAULT NULL,
    `state`              varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_recognize_file_create_time` (`create_time`) USING BTREE,
    INDEX `idx_bsbh` (`bsbh`) USING BTREE,
    INDEX `idx_recognize_task_id` (`recognize_task_id`) USING BTREE,
    INDEX `idx_recognize_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '图像识别任务单元表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_recognize_file_5`
(
    `id`                 bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `bsbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务编号',
    `bmsah`              varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门受案号',
    `jzbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `mlbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '目录编号',
    `wjxh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件序号',
    `engine_result_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `ocr_result_path`    varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '识别结果存储路径',
    `status`             varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '识别状态',
    `type`               varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '任务类型，单文件或批量任务',
    `func_list`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '待提取数据类型',
    `fail_info`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '失败信息',
    `remark`             varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `recognize_task_id`  bigint(20)                                                     NULL DEFAULT NULL,
    `state`              varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_recognize_file_create_time` (`create_time`) USING BTREE,
    INDEX `idx_bsbh` (`bsbh`) USING BTREE,
    INDEX `idx_recognize_task_id` (`recognize_task_id`) USING BTREE,
    INDEX `idx_recognize_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '图像识别任务单元表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_recognize_file_6`
(
    `id`                 bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `bsbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务编号',
    `bmsah`              varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门受案号',
    `jzbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `mlbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '目录编号',
    `wjxh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件序号',
    `engine_result_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `ocr_result_path`    varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '识别结果存储路径',
    `status`             varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '识别状态',
    `type`               varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '任务类型，单文件或批量任务',
    `func_list`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '待提取数据类型',
    `fail_info`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '失败信息',
    `remark`             varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `recognize_task_id`  bigint(20)                                                     NULL DEFAULT NULL,
    `state`              varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_recognize_file_create_time` (`create_time`) USING BTREE,
    INDEX `idx_bsbh` (`bsbh`) USING BTREE,
    INDEX `idx_recognize_task_id` (`recognize_task_id`) USING BTREE,
    INDEX `idx_recognize_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '图像识别任务单元表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_recognize_file_7`
(
    `id`                 bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `bsbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务编号',
    `bmsah`              varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门受案号',
    `jzbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `mlbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '目录编号',
    `wjxh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件序号',
    `engine_result_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `ocr_result_path`    varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '识别结果存储路径',
    `status`             varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '识别状态',
    `type`               varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '任务类型，单文件或批量任务',
    `func_list`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '待提取数据类型',
    `fail_info`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '失败信息',
    `remark`             varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `recognize_task_id`  bigint(20)                                                     NULL DEFAULT NULL,
    `state`              varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_recognize_file_create_time` (`create_time`) USING BTREE,
    INDEX `idx_bsbh` (`bsbh`) USING BTREE,
    INDEX `idx_recognize_task_id` (`recognize_task_id`) USING BTREE,
    INDEX `idx_recognize_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '图像识别任务单元表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_recognize_file_8`
(
    `id`                 bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `bsbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务编号',
    `bmsah`              varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门受案号',
    `jzbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `mlbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '目录编号',
    `wjxh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件序号',
    `engine_result_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `ocr_result_path`    varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '识别结果存储路径',
    `status`             varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '识别状态',
    `type`               varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '任务类型，单文件或批量任务',
    `func_list`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '待提取数据类型',
    `fail_info`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '失败信息',
    `remark`             varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `recognize_task_id`  bigint(20)                                                     NULL DEFAULT NULL,
    `state`              varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_recognize_file_create_time` (`create_time`) USING BTREE,
    INDEX `idx_bsbh` (`bsbh`) USING BTREE,
    INDEX `idx_recognize_task_id` (`recognize_task_id`) USING BTREE,
    INDEX `idx_recognize_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '图像识别任务单元表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_recognize_file_9`
(
    `id`                 bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `bsbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务编号',
    `bmsah`              varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门受案号',
    `jzbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `mlbh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '目录编号',
    `wjxh`               varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件序号',
    `engine_result_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `ocr_result_path`    varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '识别结果存储路径',
    `status`             varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '识别状态',
    `type`               varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '任务类型，单文件或批量任务',
    `func_list`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '待提取数据类型',
    `fail_info`          varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '失败信息',
    `remark`             varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `recognize_task_id`  bigint(20)                                                     NULL DEFAULT NULL,
    `state`              varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci    NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_recognize_file_create_time` (`create_time`) USING BTREE,
    INDEX `idx_bsbh` (`bsbh`) USING BTREE,
    INDEX `idx_recognize_task_id` (`recognize_task_id`) USING BTREE,
    INDEX `idx_recognize_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '图像识别任务单元表'
  ROW_FORMAT = DYNAMIC;



CREATE TABLE `t_recognize_task_0`
(
    `id`               bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '部门受案号',
    `jzbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `status`           varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `fail_nums`        int(10)                                                        NULL DEFAULT NULL COMMENT '失败次数',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_recognize_task_create_time` (`create_time`) USING BTREE,
    INDEX `idx_recognize_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '图像识别任务表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_recognize_task_1`
(
    `id`               bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '部门受案号',
    `jzbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `status`           varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `fail_nums`        int(10)                                                        NULL DEFAULT NULL COMMENT '失败次数',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_recognize_task_create_time` (`create_time`) USING BTREE,
    INDEX `idx_recognize_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '图像识别任务表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_recognize_task_2`
(
    `id`               bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '部门受案号',
    `jzbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `status`           varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `fail_nums`        int(10)                                                        NULL DEFAULT NULL COMMENT '失败次数',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_recognize_task_create_time` (`create_time`) USING BTREE,
    INDEX `idx_recognize_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '图像识别任务表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_recognize_task_3`
(
    `id`               bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '部门受案号',
    `jzbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `status`           varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `fail_nums`        int(10)                                                        NULL DEFAULT NULL COMMENT '失败次数',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_recognize_task_create_time` (`create_time`) USING BTREE,
    INDEX `idx_recognize_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '图像识别任务表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_recognize_task_4`
(
    `id`               bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '部门受案号',
    `jzbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `status`           varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `fail_nums`        int(10)                                                        NULL DEFAULT NULL COMMENT '失败次数',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_recognize_task_create_time` (`create_time`) USING BTREE,
    INDEX `idx_recognize_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '图像识别任务表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_recognize_task_5`
(
    `id`               bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '部门受案号',
    `jzbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `status`           varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `fail_nums`        int(10)                                                        NULL DEFAULT NULL COMMENT '失败次数',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_recognize_task_create_time` (`create_time`) USING BTREE,
    INDEX `idx_recognize_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '图像识别任务表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_recognize_task_6`
(
    `id`               bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '部门受案号',
    `jzbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `status`           varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `fail_nums`        int(10)                                                        NULL DEFAULT NULL COMMENT '失败次数',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_recognize_task_create_time` (`create_time`) USING BTREE,
    INDEX `idx_recognize_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '图像识别任务表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_recognize_task_7`
(
    `id`               bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '部门受案号',
    `jzbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `status`           varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `fail_nums`        int(10)                                                        NULL DEFAULT NULL COMMENT '失败次数',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_recognize_task_create_time` (`create_time`) USING BTREE,
    INDEX `idx_recognize_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '图像识别任务表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_recognize_task_8`
(
    `id`               bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '部门受案号',
    `jzbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `status`           varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `fail_nums`        int(10)                                                        NULL DEFAULT NULL COMMENT '失败次数',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_recognize_task_create_time` (`create_time`) USING BTREE,
    INDEX `idx_recognize_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '图像识别任务表'
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_recognize_task_9`
(
    `id`               bigint(20) UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)                                                    NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '部门受案号',
    `jzbh`             varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '卷宗编号',
    `status`           varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci   NULL DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预留字段',
    `fail_nums`        int(10)                                                        NULL DEFAULT NULL COMMENT '失败次数',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `idx_recognize_task_create_time` (`create_time`) USING BTREE,
    INDEX `idx_recognize_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '图像识别任务表'
  ROW_FORMAT = DYNAMIC;