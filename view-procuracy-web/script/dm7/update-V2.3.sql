alter table "VIEW_PROCURACY"."T_RECOGNIZE_TASK" add column("FAIL_NUMS" INT default (0));
comment on column "VIEW_PROCURACY"."T_RECOGNIZE_TASK"."FAIL_NUMS" is '失败次数';

alter table "VIEW_PROCURACY"."T_OCR_TASK" add column("FAIL_NUMS" INT default (0));
comment on column "VIEW_PROCURACY"."T_OCR_TASK"."FAIL_NUMS" is '失败次数';