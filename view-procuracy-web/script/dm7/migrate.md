替换步骤

1. 停止服务

   - 使用bin/stop.sh停止服务

2. 迁移数据

   - 使用达梦数据库的迁移工具迁移数据，将mysql数据迁移到达梦数据库

3. 修改配置文件

   - 修改applications-dev.properties中数据库的配置，修改为达梦数据库配置

     ```
     # 数据库ip和端口
     datasource.server=LOCALHOST:5236
     #数据库用户名称
     spring.datasource.username=VIEW_PROCURACY
     #数据库用户密码
     spring.datasource.password=iflytek!!
     ```

   - 修改applications.properties文件

     ```
     # dm 驱动设置
     spring.datasource.driver-class-name=dm.jdbc.driver.DmDriver
     # dm 数据源设置
     spring.datasource.url=jdbc:dm://${datasource.server}/VIEW_PROCURACY?STU&zeroDateTimeBehavior=convertToNull&useUnicode=true&characterEncoding=utf-8
     ```

4. 启动服务

   使用bin/start.sh启动服务

5. 注意事项

   - 迁移数据时达梦数据库需提前创建用户VIEW_PROCURACY，并赋予权限
