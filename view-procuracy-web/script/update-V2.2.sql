ALTER TABLE `t_card_file` MODIFY COLUMN   ocr_result_path VARCHAR(500) DEFAULT NULL  COMMENT 'ocr识别结果存储路径';
ALTER TABLE `t_card_form_field` ADD `primary_cb_key`  VARCHAR(200) DEFAULT NULL COMMENT '从表主键值';
---统计指标新增
ALTER TABLE `t_metric` ADD `dwbm`  VARCHAR(200) DEFAULT NULL COMMENT '单位编码';
ALTER TABLE `t_metric` ADD `bmsah`  VARCHAR(200) DEFAULT NULL COMMENT '部门受案号';
ALTER TABLE `t_metric` ADD `ajlbbm`  VARCHAR(10) DEFAULT NULL COMMENT '案件类别编码';
ALTER TABLE `t_metric` ADD `bsbh`  VARCHAR(200) DEFAULT NULL COMMENT '标识编号';
ALTER TABLE `t_metric` ADD `taskid`  VARCHAR(200) DEFAULT NULL COMMENT '任务编号';
ALTER TABLE `t_metric` ADD `jzbh`  VARCHAR(200) DEFAULT NULL COMMENT '卷宗编号';
ALTER TABLE `t_metric` ADD `jzmc`  VARCHAR(200) DEFAULT NULL COMMENT '卷宗名称';
ALTER TABLE `t_metric` ADD `mlbh`  VARCHAR(200) DEFAULT NULL COMMENT '目录编号';
ALTER TABLE `t_metric` ADD `wslx`  VARCHAR(10) DEFAULT NULL COMMENT '文书类型';



ALTER TABLE `t_metric` ADD INDEX idx_dwbm ( `dwbm` ) ;
ALTER TABLE `t_metric` ADD INDEX idx_bmsah_metric ( `bmsah` ) ;
ALTER TABLE `t_metric` ADD INDEX idx_ajlbbm ( `ajlbbm` ) ;
ALTER TABLE `t_metric` ADD INDEX idx_bsbh ( `bsbh` ) ;
ALTER TABLE `t_metric` ADD INDEX idx_taskid ( `taskid` ) ;
ALTER TABLE `t_metric` ADD INDEX idx_jzbh ( `jzbh` ) ;
ALTER TABLE `t_metric` ADD INDEX idx_jzmc ( `jzmc` ) ;
ALTER TABLE `t_metric` ADD INDEX idx_mlbh ( `mlbh` ) ;
ALTER TABLE `t_metric` ADD INDEX idx_wslx ( `wslx` ) ;

