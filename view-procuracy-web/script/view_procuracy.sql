# ************************************************************
# Sequel Ace SQL dump
# 版本号： 20019
#
# https://sequel-ace.com/
# https://github.com/Sequel-Ace/Sequel-Ace
#
# 主机: 172.31.160.210 (MySQL 5.7.16)
# 数据库: view_procuracy_shard
# 生成时间: 2022-08-03 09:52:51 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO', SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

CREATE DATABASE `view_procuracy` DEFAULT CHARACTER SET utf8mb4;

USE view_procuracy;

# 转储表 t_card_file
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_card_file`;

CREATE TABLE `t_card_file`
(
    `id`               bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `create_time`      datetime      DEFAULT NULL COMMENT '创建时间',
    `update_time`      datetime      DEFAULT NULL COMMENT '更新时间',
    `bmsah`            varchar(200)  DEFAULT NULL COMMENT '部门受案号',
    `process`          varchar(10)   DEFAULT NULL COMMENT '文书所在阶段',
    `wsmbbm`           varchar(200)  DEFAULT NULL COMMENT '文书模板编码',
    `wsslbh`           varchar(200)  DEFAULT NULL COMMENT '文书实例编号',
    `wsmc`             varchar(200)  DEFAULT NULL COMMENT '文件名称',
    `wjbm`             varchar(200)  DEFAULT NULL COMMENT '文件编码',
    `jzwjh`            varchar(200)  DEFAULT NULL COMMENT '卷宗文件号',
    `type`             varchar(100)  DEFAULT NULL COMMENT '文书类型(起诉书,提取逮捕书)',
    `source`           varchar(100)  DEFAULT NULL COMMENT '来源(外来,内部)',
    `status`           varchar(30)   DEFAULT NULL COMMENT '状态',
    `fail_info`        varchar(500)  DEFAULT NULL COMMENT '错误信息',
    `ocr_result_path`  varchar(500)  DEFAULT NULL COMMENT 'ocr识别结果存储路径',
    `elle_result_path` varchar(200)  DEFAULT NULL COMMENT '要素抽取引擎结果存储路径',
    `file_content`     text COMMENT '文档文本内容',
    `remark`           varchar(2000) DEFAULT NULL COMMENT '预留字段',
    PRIMARY KEY (`id`),
    KEY `idx_bmsah` (`bmsah`),
    KEY `idx_wjbm` (`wjbm`),
    KEY `idx_zwjh` (`jzwjh`),
    KEY `idx_wsslbh` (`wsslbh`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='案卡文件表';



# 转储表 t_card_form_field
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_card_form_field`;

CREATE TABLE `t_card_form_field`
(
    `id`             bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`    datetime      DEFAULT NULL COMMENT '创建时间',
    `update_time`    datetime      DEFAULT NULL COMMENT '更新时间',
    `card_file_id`   bigint(20)    DEFAULT NULL COMMENT '案卡文书id',
    `stbm`           varchar(50)   DEFAULT NULL COMMENT '实体表名',
    `zdmc`           varchar(50)   DEFAULT NULL COMMENT '字段名称',
    `zdlx`           varchar(30)   DEFAULT NULL COMMENT '字段类型',
    `zdbmz`          varchar(50)   DEFAULT NULL COMMENT '字段编码值',
    `bdjg_id`        varchar(128)  DEFAULT NULL,
    `zdwbz`          text COMMENT '字段文本值',
    `primary_key`    varchar(200)  DEFAULT NULL COMMENT '数据主键值',
    `primary_cb_key` varchar(200)  DEFAULT NULL COMMENT '从表主键值',
    `remark`         varchar(2000) DEFAULT NULL COMMENT '预留字段',
    PRIMARY KEY (`id`),
    KEY `idx_card_file_id` (`card_file_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='案卡表单字段表';



# 转储表 t_metric_0
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_metric_0`;

CREATE TABLE `t_metric_0`
(
    `id`             bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `mrtric_type`    varchar(30)         DEFAULT NULL COMMENT '指标类型ocr:图文识别  card:案卡回填 cont：编目',
    `data_id`        varchar(100)        DEFAULT NULL COMMENT '数据标识',
    `create_time`    datetime            DEFAULT NULL COMMENT '创建时间',
    `update_time`    datetime            DEFAULT NULL COMMENT '更新时间',
    `caller_ip`      varchar(30)         DEFAULT NULL COMMENT '调用方IP',
    `service_ip`     varchar(30)         DEFAULT NULL COMMENT '服务提供方IP',
    `systemid`       varchar(100)        DEFAULT NULL COMMENT '应用和系统唯一id',
    `interface_name` varchar(100)        DEFAULT NULL COMMENT '接口名',
    `wjxh`           varchar(200)        DEFAULT NULL COMMENT '文件编号',
    `file_id`        bigint(20) unsigned DEFAULT NULL COMMENT '文件id 存储ocrFileId或者cardFileId',
    `ocr_start_time` datetime            DEFAULT NULL COMMENT '调用OCR开始时间',
    `ocr_end_time`   datetime            DEFAULT NULL COMMENT '调用OCR结束时间',
    `start_millis`   bigint(20) unsigned NOT NULL COMMENT '开始时间 单位毫秒',
    `end_millis`     bigint(20) unsigned DEFAULT NULL COMMENT '结束时间 单位毫秒',
    `cost_time`      bigint(20)          DEFAULT NULL COMMENT '耗时 单位毫秒 原长度32',
    `ocr_result`     varchar(30)         DEFAULT NULL COMMENT '调用OCR结果 succes或者fail',
    `remark`         varchar(2000)       DEFAULT NULL COMMENT '预留字段',
    `dwbm`           varchar(200)        DEFAULT NULL COMMENT '单位编码',
    `bmsah`          varchar(200)        DEFAULT NULL COMMENT '部门受案号',
    `ajlbbm`         varchar(10)         DEFAULT NULL COMMENT '案件类别编码',
    `bsbh`           varchar(200)        DEFAULT NULL COMMENT '标识编号',
    `taskid`         varchar(200)        DEFAULT NULL COMMENT '任务编号',
    `jzbh`           varchar(200)        DEFAULT NULL COMMENT '卷宗编号',
    `jzmc`           varchar(200)        DEFAULT NULL COMMENT '卷宗名称',
    `mlbh`           varchar(200)        DEFAULT NULL COMMENT '目录编号',
    `wslx`           varchar(10)         DEFAULT NULL COMMENT '文书类型',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_data_id` (`data_id`) USING BTREE,
    KEY `idx_wjxh` (`wjxh`) USING BTREE,
    KEY `idx_create_time` (`create_time`) USING BTREE,
    KEY `idx_file_id` (`file_id`) USING BTREE,
    KEY `idx_dwbm` (`dwbm`) USING BTREE,
    KEY `idx_bmsah` (`bmsah`) USING BTREE,
    KEY `idx_ajlbbm` (`ajlbbm`) USING BTREE,
    KEY `idx_bsbh` (`bsbh`) USING BTREE,
    KEY `idx_jzbh` (`jzbh`) USING BTREE,
    KEY `idx_mlbh` (`mlbh`) USING BTREE,
    KEY `idx_wslx` (`wslx`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='指标统计表';



# 转储表 t_metric_1
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_metric_1`;

CREATE TABLE `t_metric_1`
(
    `id`             bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `mrtric_type`    varchar(30)         DEFAULT NULL COMMENT '指标类型ocr:图文识别  card:案卡回填 cont：编目',
    `data_id`        varchar(100)        DEFAULT NULL COMMENT '数据标识',
    `create_time`    datetime            DEFAULT NULL COMMENT '创建时间',
    `update_time`    datetime            DEFAULT NULL COMMENT '更新时间',
    `caller_ip`      varchar(30)         DEFAULT NULL COMMENT '调用方IP',
    `service_ip`     varchar(30)         DEFAULT NULL COMMENT '服务提供方IP',
    `systemid`       varchar(100)        DEFAULT NULL COMMENT '应用和系统唯一id',
    `interface_name` varchar(100)        DEFAULT NULL COMMENT '接口名',
    `wjxh`           varchar(200)        DEFAULT NULL COMMENT '文件编号',
    `file_id`        bigint(20) unsigned DEFAULT NULL COMMENT '文件id 存储ocrFileId或者cardFileId',
    `ocr_start_time` datetime            DEFAULT NULL COMMENT '调用OCR开始时间',
    `ocr_end_time`   datetime            DEFAULT NULL COMMENT '调用OCR结束时间',
    `start_millis`   bigint(20) unsigned NOT NULL COMMENT '开始时间 单位毫秒',
    `end_millis`     bigint(20) unsigned DEFAULT NULL COMMENT '结束时间 单位毫秒',
    `cost_time`      bigint(20)          DEFAULT NULL COMMENT '耗时 单位毫秒 原长度32',
    `ocr_result`     varchar(30)         DEFAULT NULL COMMENT '调用OCR结果 succes或者fail',
    `remark`         varchar(2000)       DEFAULT NULL COMMENT '预留字段',
    `dwbm`           varchar(200)        DEFAULT NULL COMMENT '单位编码',
    `bmsah`          varchar(200)        DEFAULT NULL COMMENT '部门受案号',
    `ajlbbm`         varchar(10)         DEFAULT NULL COMMENT '案件类别编码',
    `bsbh`           varchar(200)        DEFAULT NULL COMMENT '标识编号',
    `taskid`         varchar(200)        DEFAULT NULL COMMENT '任务编号',
    `jzbh`           varchar(200)        DEFAULT NULL COMMENT '卷宗编号',
    `jzmc`           varchar(200)        DEFAULT NULL COMMENT '卷宗名称',
    `mlbh`           varchar(200)        DEFAULT NULL COMMENT '目录编号',
    `wslx`           varchar(10)         DEFAULT NULL COMMENT '文书类型',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_data_id` (`data_id`) USING BTREE,
    KEY `idx_wjxh` (`wjxh`) USING BTREE,
    KEY `idx_create_time` (`create_time`) USING BTREE,
    KEY `idx_file_id` (`file_id`) USING BTREE,
    KEY `idx_dwbm` (`dwbm`) USING BTREE,
    KEY `idx_bmsah` (`bmsah`) USING BTREE,
    KEY `idx_ajlbbm` (`ajlbbm`) USING BTREE,
    KEY `idx_bsbh` (`bsbh`) USING BTREE,
    KEY `idx_jzbh` (`jzbh`) USING BTREE,
    KEY `idx_mlbh` (`mlbh`) USING BTREE,
    KEY `idx_wslx` (`wslx`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='指标统计表';



# 转储表 t_metric_2
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_metric_2`;

CREATE TABLE `t_metric_2`
(
    `id`             bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `mrtric_type`    varchar(30)         DEFAULT NULL COMMENT '指标类型ocr:图文识别  card:案卡回填 cont：编目',
    `data_id`        varchar(100)        DEFAULT NULL COMMENT '数据标识',
    `create_time`    datetime            DEFAULT NULL COMMENT '创建时间',
    `update_time`    datetime            DEFAULT NULL COMMENT '更新时间',
    `caller_ip`      varchar(30)         DEFAULT NULL COMMENT '调用方IP',
    `service_ip`     varchar(30)         DEFAULT NULL COMMENT '服务提供方IP',
    `systemid`       varchar(100)        DEFAULT NULL COMMENT '应用和系统唯一id',
    `interface_name` varchar(100)        DEFAULT NULL COMMENT '接口名',
    `wjxh`           varchar(200)        DEFAULT NULL COMMENT '文件编号',
    `file_id`        bigint(20) unsigned DEFAULT NULL COMMENT '文件id 存储ocrFileId或者cardFileId',
    `ocr_start_time` datetime            DEFAULT NULL COMMENT '调用OCR开始时间',
    `ocr_end_time`   datetime            DEFAULT NULL COMMENT '调用OCR结束时间',
    `start_millis`   bigint(20) unsigned NOT NULL COMMENT '开始时间 单位毫秒',
    `end_millis`     bigint(20) unsigned DEFAULT NULL COMMENT '结束时间 单位毫秒',
    `cost_time`      bigint(20)          DEFAULT NULL COMMENT '耗时 单位毫秒 原长度32',
    `ocr_result`     varchar(30)         DEFAULT NULL COMMENT '调用OCR结果 succes或者fail',
    `remark`         varchar(2000)       DEFAULT NULL COMMENT '预留字段',
    `dwbm`           varchar(200)        DEFAULT NULL COMMENT '单位编码',
    `bmsah`          varchar(200)        DEFAULT NULL COMMENT '部门受案号',
    `ajlbbm`         varchar(10)         DEFAULT NULL COMMENT '案件类别编码',
    `bsbh`           varchar(200)        DEFAULT NULL COMMENT '标识编号',
    `taskid`         varchar(200)        DEFAULT NULL COMMENT '任务编号',
    `jzbh`           varchar(200)        DEFAULT NULL COMMENT '卷宗编号',
    `jzmc`           varchar(200)        DEFAULT NULL COMMENT '卷宗名称',
    `mlbh`           varchar(200)        DEFAULT NULL COMMENT '目录编号',
    `wslx`           varchar(10)         DEFAULT NULL COMMENT '文书类型',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_data_id` (`data_id`) USING BTREE,
    KEY `idx_wjxh` (`wjxh`) USING BTREE,
    KEY `idx_create_time` (`create_time`) USING BTREE,
    KEY `idx_file_id` (`file_id`) USING BTREE,
    KEY `idx_dwbm` (`dwbm`) USING BTREE,
    KEY `idx_bmsah` (`bmsah`) USING BTREE,
    KEY `idx_ajlbbm` (`ajlbbm`) USING BTREE,
    KEY `idx_bsbh` (`bsbh`) USING BTREE,
    KEY `idx_jzbh` (`jzbh`) USING BTREE,
    KEY `idx_mlbh` (`mlbh`) USING BTREE,
    KEY `idx_wslx` (`wslx`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='指标统计表';



# 转储表 t_metric_3
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_metric_3`;

CREATE TABLE `t_metric_3`
(
    `id`             bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `mrtric_type`    varchar(30)         DEFAULT NULL COMMENT '指标类型ocr:图文识别  card:案卡回填 cont：编目',
    `data_id`        varchar(100)        DEFAULT NULL COMMENT '数据标识',
    `create_time`    datetime            DEFAULT NULL COMMENT '创建时间',
    `update_time`    datetime            DEFAULT NULL COMMENT '更新时间',
    `caller_ip`      varchar(30)         DEFAULT NULL COMMENT '调用方IP',
    `service_ip`     varchar(30)         DEFAULT NULL COMMENT '服务提供方IP',
    `systemid`       varchar(100)        DEFAULT NULL COMMENT '应用和系统唯一id',
    `interface_name` varchar(100)        DEFAULT NULL COMMENT '接口名',
    `wjxh`           varchar(200)        DEFAULT NULL COMMENT '文件编号',
    `file_id`        bigint(20) unsigned DEFAULT NULL COMMENT '文件id 存储ocrFileId或者cardFileId',
    `ocr_start_time` datetime            DEFAULT NULL COMMENT '调用OCR开始时间',
    `ocr_end_time`   datetime            DEFAULT NULL COMMENT '调用OCR结束时间',
    `start_millis`   bigint(20) unsigned NOT NULL COMMENT '开始时间 单位毫秒',
    `end_millis`     bigint(20) unsigned DEFAULT NULL COMMENT '结束时间 单位毫秒',
    `cost_time`      bigint(20)          DEFAULT NULL COMMENT '耗时 单位毫秒 原长度32',
    `ocr_result`     varchar(30)         DEFAULT NULL COMMENT '调用OCR结果 succes或者fail',
    `remark`         varchar(2000)       DEFAULT NULL COMMENT '预留字段',
    `dwbm`           varchar(200)        DEFAULT NULL COMMENT '单位编码',
    `bmsah`          varchar(200)        DEFAULT NULL COMMENT '部门受案号',
    `ajlbbm`         varchar(10)         DEFAULT NULL COMMENT '案件类别编码',
    `bsbh`           varchar(200)        DEFAULT NULL COMMENT '标识编号',
    `taskid`         varchar(200)        DEFAULT NULL COMMENT '任务编号',
    `jzbh`           varchar(200)        DEFAULT NULL COMMENT '卷宗编号',
    `jzmc`           varchar(200)        DEFAULT NULL COMMENT '卷宗名称',
    `mlbh`           varchar(200)        DEFAULT NULL COMMENT '目录编号',
    `wslx`           varchar(10)         DEFAULT NULL COMMENT '文书类型',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_data_id` (`data_id`) USING BTREE,
    KEY `idx_wjxh` (`wjxh`) USING BTREE,
    KEY `idx_create_time` (`create_time`) USING BTREE,
    KEY `idx_file_id` (`file_id`) USING BTREE,
    KEY `idx_dwbm` (`dwbm`) USING BTREE,
    KEY `idx_bmsah` (`bmsah`) USING BTREE,
    KEY `idx_ajlbbm` (`ajlbbm`) USING BTREE,
    KEY `idx_bsbh` (`bsbh`) USING BTREE,
    KEY `idx_jzbh` (`jzbh`) USING BTREE,
    KEY `idx_mlbh` (`mlbh`) USING BTREE,
    KEY `idx_wslx` (`wslx`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='指标统计表';



# 转储表 t_metric_4
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_metric_4`;

CREATE TABLE `t_metric_4`
(
    `id`             bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `mrtric_type`    varchar(30)         DEFAULT NULL COMMENT '指标类型ocr:图文识别  card:案卡回填 cont：编目',
    `data_id`        varchar(100)        DEFAULT NULL COMMENT '数据标识',
    `create_time`    datetime            DEFAULT NULL COMMENT '创建时间',
    `update_time`    datetime            DEFAULT NULL COMMENT '更新时间',
    `caller_ip`      varchar(30)         DEFAULT NULL COMMENT '调用方IP',
    `service_ip`     varchar(30)         DEFAULT NULL COMMENT '服务提供方IP',
    `systemid`       varchar(100)        DEFAULT NULL COMMENT '应用和系统唯一id',
    `interface_name` varchar(100)        DEFAULT NULL COMMENT '接口名',
    `wjxh`           varchar(200)        DEFAULT NULL COMMENT '文件编号',
    `file_id`        bigint(20) unsigned DEFAULT NULL COMMENT '文件id 存储ocrFileId或者cardFileId',
    `ocr_start_time` datetime            DEFAULT NULL COMMENT '调用OCR开始时间',
    `ocr_end_time`   datetime            DEFAULT NULL COMMENT '调用OCR结束时间',
    `start_millis`   bigint(20) unsigned NOT NULL COMMENT '开始时间 单位毫秒',
    `end_millis`     bigint(20) unsigned DEFAULT NULL COMMENT '结束时间 单位毫秒',
    `cost_time`      bigint(20)          DEFAULT NULL COMMENT '耗时 单位毫秒 原长度32',
    `ocr_result`     varchar(30)         DEFAULT NULL COMMENT '调用OCR结果 succes或者fail',
    `remark`         varchar(2000)       DEFAULT NULL COMMENT '预留字段',
    `dwbm`           varchar(200)        DEFAULT NULL COMMENT '单位编码',
    `bmsah`          varchar(200)        DEFAULT NULL COMMENT '部门受案号',
    `ajlbbm`         varchar(10)         DEFAULT NULL COMMENT '案件类别编码',
    `bsbh`           varchar(200)        DEFAULT NULL COMMENT '标识编号',
    `taskid`         varchar(200)        DEFAULT NULL COMMENT '任务编号',
    `jzbh`           varchar(200)        DEFAULT NULL COMMENT '卷宗编号',
    `jzmc`           varchar(200)        DEFAULT NULL COMMENT '卷宗名称',
    `mlbh`           varchar(200)        DEFAULT NULL COMMENT '目录编号',
    `wslx`           varchar(10)         DEFAULT NULL COMMENT '文书类型',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_data_id` (`data_id`) USING BTREE,
    KEY `idx_wjxh` (`wjxh`) USING BTREE,
    KEY `idx_create_time` (`create_time`) USING BTREE,
    KEY `idx_file_id` (`file_id`) USING BTREE,
    KEY `idx_dwbm` (`dwbm`) USING BTREE,
    KEY `idx_bmsah` (`bmsah`) USING BTREE,
    KEY `idx_ajlbbm` (`ajlbbm`) USING BTREE,
    KEY `idx_bsbh` (`bsbh`) USING BTREE,
    KEY `idx_jzbh` (`jzbh`) USING BTREE,
    KEY `idx_mlbh` (`mlbh`) USING BTREE,
    KEY `idx_wslx` (`wslx`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='指标统计表';



# 转储表 t_metric_5
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_metric_5`;

CREATE TABLE `t_metric_5`
(
    `id`             bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `mrtric_type`    varchar(30)         DEFAULT NULL COMMENT '指标类型ocr:图文识别  card:案卡回填 cont：编目',
    `data_id`        varchar(100)        DEFAULT NULL COMMENT '数据标识',
    `create_time`    datetime            DEFAULT NULL COMMENT '创建时间',
    `update_time`    datetime            DEFAULT NULL COMMENT '更新时间',
    `caller_ip`      varchar(30)         DEFAULT NULL COMMENT '调用方IP',
    `service_ip`     varchar(30)         DEFAULT NULL COMMENT '服务提供方IP',
    `systemid`       varchar(100)        DEFAULT NULL COMMENT '应用和系统唯一id',
    `interface_name` varchar(100)        DEFAULT NULL COMMENT '接口名',
    `wjxh`           varchar(200)        DEFAULT NULL COMMENT '文件编号',
    `file_id`        bigint(20) unsigned DEFAULT NULL COMMENT '文件id 存储ocrFileId或者cardFileId',
    `ocr_start_time` datetime            DEFAULT NULL COMMENT '调用OCR开始时间',
    `ocr_end_time`   datetime            DEFAULT NULL COMMENT '调用OCR结束时间',
    `start_millis`   bigint(20) unsigned NOT NULL COMMENT '开始时间 单位毫秒',
    `end_millis`     bigint(20) unsigned DEFAULT NULL COMMENT '结束时间 单位毫秒',
    `cost_time`      bigint(20)          DEFAULT NULL COMMENT '耗时 单位毫秒 原长度32',
    `ocr_result`     varchar(30)         DEFAULT NULL COMMENT '调用OCR结果 succes或者fail',
    `remark`         varchar(2000)       DEFAULT NULL COMMENT '预留字段',
    `dwbm`           varchar(200)        DEFAULT NULL COMMENT '单位编码',
    `bmsah`          varchar(200)        DEFAULT NULL COMMENT '部门受案号',
    `ajlbbm`         varchar(10)         DEFAULT NULL COMMENT '案件类别编码',
    `bsbh`           varchar(200)        DEFAULT NULL COMMENT '标识编号',
    `taskid`         varchar(200)        DEFAULT NULL COMMENT '任务编号',
    `jzbh`           varchar(200)        DEFAULT NULL COMMENT '卷宗编号',
    `jzmc`           varchar(200)        DEFAULT NULL COMMENT '卷宗名称',
    `mlbh`           varchar(200)        DEFAULT NULL COMMENT '目录编号',
    `wslx`           varchar(10)         DEFAULT NULL COMMENT '文书类型',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_data_id` (`data_id`) USING BTREE,
    KEY `idx_wjxh` (`wjxh`) USING BTREE,
    KEY `idx_create_time` (`create_time`) USING BTREE,
    KEY `idx_file_id` (`file_id`) USING BTREE,
    KEY `idx_dwbm` (`dwbm`) USING BTREE,
    KEY `idx_bmsah` (`bmsah`) USING BTREE,
    KEY `idx_ajlbbm` (`ajlbbm`) USING BTREE,
    KEY `idx_bsbh` (`bsbh`) USING BTREE,
    KEY `idx_jzbh` (`jzbh`) USING BTREE,
    KEY `idx_mlbh` (`mlbh`) USING BTREE,
    KEY `idx_wslx` (`wslx`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='指标统计表';



# 转储表 t_metric_6
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_metric_6`;

CREATE TABLE `t_metric_6`
(
    `id`             bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `mrtric_type`    varchar(30)         DEFAULT NULL COMMENT '指标类型ocr:图文识别  card:案卡回填 cont：编目',
    `data_id`        varchar(100)        DEFAULT NULL COMMENT '数据标识',
    `create_time`    datetime            DEFAULT NULL COMMENT '创建时间',
    `update_time`    datetime            DEFAULT NULL COMMENT '更新时间',
    `caller_ip`      varchar(30)         DEFAULT NULL COMMENT '调用方IP',
    `service_ip`     varchar(30)         DEFAULT NULL COMMENT '服务提供方IP',
    `systemid`       varchar(100)        DEFAULT NULL COMMENT '应用和系统唯一id',
    `interface_name` varchar(100)        DEFAULT NULL COMMENT '接口名',
    `wjxh`           varchar(200)        DEFAULT NULL COMMENT '文件编号',
    `file_id`        bigint(20) unsigned DEFAULT NULL COMMENT '文件id 存储ocrFileId或者cardFileId',
    `ocr_start_time` datetime            DEFAULT NULL COMMENT '调用OCR开始时间',
    `ocr_end_time`   datetime            DEFAULT NULL COMMENT '调用OCR结束时间',
    `start_millis`   bigint(20) unsigned NOT NULL COMMENT '开始时间 单位毫秒',
    `end_millis`     bigint(20) unsigned DEFAULT NULL COMMENT '结束时间 单位毫秒',
    `cost_time`      bigint(20)          DEFAULT NULL COMMENT '耗时 单位毫秒 原长度32',
    `ocr_result`     varchar(30)         DEFAULT NULL COMMENT '调用OCR结果 succes或者fail',
    `remark`         varchar(2000)       DEFAULT NULL COMMENT '预留字段',
    `dwbm`           varchar(200)        DEFAULT NULL COMMENT '单位编码',
    `bmsah`          varchar(200)        DEFAULT NULL COMMENT '部门受案号',
    `ajlbbm`         varchar(10)         DEFAULT NULL COMMENT '案件类别编码',
    `bsbh`           varchar(200)        DEFAULT NULL COMMENT '标识编号',
    `taskid`         varchar(200)        DEFAULT NULL COMMENT '任务编号',
    `jzbh`           varchar(200)        DEFAULT NULL COMMENT '卷宗编号',
    `jzmc`           varchar(200)        DEFAULT NULL COMMENT '卷宗名称',
    `mlbh`           varchar(200)        DEFAULT NULL COMMENT '目录编号',
    `wslx`           varchar(10)         DEFAULT NULL COMMENT '文书类型',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_data_id` (`data_id`) USING BTREE,
    KEY `idx_wjxh` (`wjxh`) USING BTREE,
    KEY `idx_create_time` (`create_time`) USING BTREE,
    KEY `idx_file_id` (`file_id`) USING BTREE,
    KEY `idx_dwbm` (`dwbm`) USING BTREE,
    KEY `idx_bmsah` (`bmsah`) USING BTREE,
    KEY `idx_ajlbbm` (`ajlbbm`) USING BTREE,
    KEY `idx_bsbh` (`bsbh`) USING BTREE,
    KEY `idx_jzbh` (`jzbh`) USING BTREE,
    KEY `idx_mlbh` (`mlbh`) USING BTREE,
    KEY `idx_wslx` (`wslx`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='指标统计表';



# 转储表 t_metric_7
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_metric_7`;

CREATE TABLE `t_metric_7`
(
    `id`             bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `mrtric_type`    varchar(30)         DEFAULT NULL COMMENT '指标类型ocr:图文识别  card:案卡回填 cont：编目',
    `data_id`        varchar(100)        DEFAULT NULL COMMENT '数据标识',
    `create_time`    datetime            DEFAULT NULL COMMENT '创建时间',
    `update_time`    datetime            DEFAULT NULL COMMENT '更新时间',
    `caller_ip`      varchar(30)         DEFAULT NULL COMMENT '调用方IP',
    `service_ip`     varchar(30)         DEFAULT NULL COMMENT '服务提供方IP',
    `systemid`       varchar(100)        DEFAULT NULL COMMENT '应用和系统唯一id',
    `interface_name` varchar(100)        DEFAULT NULL COMMENT '接口名',
    `wjxh`           varchar(200)        DEFAULT NULL COMMENT '文件编号',
    `file_id`        bigint(20) unsigned DEFAULT NULL COMMENT '文件id 存储ocrFileId或者cardFileId',
    `ocr_start_time` datetime            DEFAULT NULL COMMENT '调用OCR开始时间',
    `ocr_end_time`   datetime            DEFAULT NULL COMMENT '调用OCR结束时间',
    `start_millis`   bigint(20) unsigned NOT NULL COMMENT '开始时间 单位毫秒',
    `end_millis`     bigint(20) unsigned DEFAULT NULL COMMENT '结束时间 单位毫秒',
    `cost_time`      bigint(20)          DEFAULT NULL COMMENT '耗时 单位毫秒 原长度32',
    `ocr_result`     varchar(30)         DEFAULT NULL COMMENT '调用OCR结果 succes或者fail',
    `remark`         varchar(2000)       DEFAULT NULL COMMENT '预留字段',
    `dwbm`           varchar(200)        DEFAULT NULL COMMENT '单位编码',
    `bmsah`          varchar(200)        DEFAULT NULL COMMENT '部门受案号',
    `ajlbbm`         varchar(10)         DEFAULT NULL COMMENT '案件类别编码',
    `bsbh`           varchar(200)        DEFAULT NULL COMMENT '标识编号',
    `taskid`         varchar(200)        DEFAULT NULL COMMENT '任务编号',
    `jzbh`           varchar(200)        DEFAULT NULL COMMENT '卷宗编号',
    `jzmc`           varchar(200)        DEFAULT NULL COMMENT '卷宗名称',
    `mlbh`           varchar(200)        DEFAULT NULL COMMENT '目录编号',
    `wslx`           varchar(10)         DEFAULT NULL COMMENT '文书类型',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_data_id` (`data_id`) USING BTREE,
    KEY `idx_wjxh` (`wjxh`) USING BTREE,
    KEY `idx_create_time` (`create_time`) USING BTREE,
    KEY `idx_file_id` (`file_id`) USING BTREE,
    KEY `idx_dwbm` (`dwbm`) USING BTREE,
    KEY `idx_bmsah` (`bmsah`) USING BTREE,
    KEY `idx_ajlbbm` (`ajlbbm`) USING BTREE,
    KEY `idx_bsbh` (`bsbh`) USING BTREE,
    KEY `idx_jzbh` (`jzbh`) USING BTREE,
    KEY `idx_mlbh` (`mlbh`) USING BTREE,
    KEY `idx_wslx` (`wslx`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='指标统计表';



# 转储表 t_metric_8
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_metric_8`;

CREATE TABLE `t_metric_8`
(
    `id`             bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `mrtric_type`    varchar(30)         DEFAULT NULL COMMENT '指标类型ocr:图文识别  card:案卡回填 cont：编目',
    `data_id`        varchar(100)        DEFAULT NULL COMMENT '数据标识',
    `create_time`    datetime            DEFAULT NULL COMMENT '创建时间',
    `update_time`    datetime            DEFAULT NULL COMMENT '更新时间',
    `caller_ip`      varchar(30)         DEFAULT NULL COMMENT '调用方IP',
    `service_ip`     varchar(30)         DEFAULT NULL COMMENT '服务提供方IP',
    `systemid`       varchar(100)        DEFAULT NULL COMMENT '应用和系统唯一id',
    `interface_name` varchar(100)        DEFAULT NULL COMMENT '接口名',
    `wjxh`           varchar(200)        DEFAULT NULL COMMENT '文件编号',
    `file_id`        bigint(20) unsigned DEFAULT NULL COMMENT '文件id 存储ocrFileId或者cardFileId',
    `ocr_start_time` datetime            DEFAULT NULL COMMENT '调用OCR开始时间',
    `ocr_end_time`   datetime            DEFAULT NULL COMMENT '调用OCR结束时间',
    `start_millis`   bigint(20) unsigned NOT NULL COMMENT '开始时间 单位毫秒',
    `end_millis`     bigint(20) unsigned DEFAULT NULL COMMENT '结束时间 单位毫秒',
    `cost_time`      bigint(20)          DEFAULT NULL COMMENT '耗时 单位毫秒 原长度32',
    `ocr_result`     varchar(30)         DEFAULT NULL COMMENT '调用OCR结果 succes或者fail',
    `remark`         varchar(2000)       DEFAULT NULL COMMENT '预留字段',
    `dwbm`           varchar(200)        DEFAULT NULL COMMENT '单位编码',
    `bmsah`          varchar(200)        DEFAULT NULL COMMENT '部门受案号',
    `ajlbbm`         varchar(10)         DEFAULT NULL COMMENT '案件类别编码',
    `bsbh`           varchar(200)        DEFAULT NULL COMMENT '标识编号',
    `taskid`         varchar(200)        DEFAULT NULL COMMENT '任务编号',
    `jzbh`           varchar(200)        DEFAULT NULL COMMENT '卷宗编号',
    `jzmc`           varchar(200)        DEFAULT NULL COMMENT '卷宗名称',
    `mlbh`           varchar(200)        DEFAULT NULL COMMENT '目录编号',
    `wslx`           varchar(10)         DEFAULT NULL COMMENT '文书类型',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_data_id` (`data_id`) USING BTREE,
    KEY `idx_wjxh` (`wjxh`) USING BTREE,
    KEY `idx_create_time` (`create_time`) USING BTREE,
    KEY `idx_file_id` (`file_id`) USING BTREE,
    KEY `idx_dwbm` (`dwbm`) USING BTREE,
    KEY `idx_bmsah` (`bmsah`) USING BTREE,
    KEY `idx_ajlbbm` (`ajlbbm`) USING BTREE,
    KEY `idx_bsbh` (`bsbh`) USING BTREE,
    KEY `idx_jzbh` (`jzbh`) USING BTREE,
    KEY `idx_mlbh` (`mlbh`) USING BTREE,
    KEY `idx_wslx` (`wslx`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='指标统计表';



# 转储表 t_metric_9
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_metric_9`;

CREATE TABLE `t_metric_9`
(
    `id`             bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `mrtric_type`    varchar(30)         DEFAULT NULL COMMENT '指标类型ocr:图文识别  card:案卡回填 cont：编目',
    `data_id`        varchar(100)        DEFAULT NULL COMMENT '数据标识',
    `create_time`    datetime            DEFAULT NULL COMMENT '创建时间',
    `update_time`    datetime            DEFAULT NULL COMMENT '更新时间',
    `caller_ip`      varchar(30)         DEFAULT NULL COMMENT '调用方IP',
    `service_ip`     varchar(30)         DEFAULT NULL COMMENT '服务提供方IP',
    `systemid`       varchar(100)        DEFAULT NULL COMMENT '应用和系统唯一id',
    `interface_name` varchar(100)        DEFAULT NULL COMMENT '接口名',
    `wjxh`           varchar(200)        DEFAULT NULL COMMENT '文件编号',
    `file_id`        bigint(20) unsigned DEFAULT NULL COMMENT '文件id 存储ocrFileId或者cardFileId',
    `ocr_start_time` datetime            DEFAULT NULL COMMENT '调用OCR开始时间',
    `ocr_end_time`   datetime            DEFAULT NULL COMMENT '调用OCR结束时间',
    `start_millis`   bigint(20) unsigned NOT NULL COMMENT '开始时间 单位毫秒',
    `end_millis`     bigint(20) unsigned DEFAULT NULL COMMENT '结束时间 单位毫秒',
    `cost_time`      bigint(20)          DEFAULT NULL COMMENT '耗时 单位毫秒 原长度32',
    `ocr_result`     varchar(30)         DEFAULT NULL COMMENT '调用OCR结果 succes或者fail',
    `remark`         varchar(2000)       DEFAULT NULL COMMENT '预留字段',
    `dwbm`           varchar(200)        DEFAULT NULL COMMENT '单位编码',
    `bmsah`          varchar(200)        DEFAULT NULL COMMENT '部门受案号',
    `ajlbbm`         varchar(10)         DEFAULT NULL COMMENT '案件类别编码',
    `bsbh`           varchar(200)        DEFAULT NULL COMMENT '标识编号',
    `taskid`         varchar(200)        DEFAULT NULL COMMENT '任务编号',
    `jzbh`           varchar(200)        DEFAULT NULL COMMENT '卷宗编号',
    `jzmc`           varchar(200)        DEFAULT NULL COMMENT '卷宗名称',
    `mlbh`           varchar(200)        DEFAULT NULL COMMENT '目录编号',
    `wslx`           varchar(10)         DEFAULT NULL COMMENT '文书类型',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_data_id` (`data_id`) USING BTREE,
    KEY `idx_wjxh` (`wjxh`) USING BTREE,
    KEY `idx_create_time` (`create_time`) USING BTREE,
    KEY `idx_file_id` (`file_id`) USING BTREE,
    KEY `idx_dwbm` (`dwbm`) USING BTREE,
    KEY `idx_bmsah` (`bmsah`) USING BTREE,
    KEY `idx_ajlbbm` (`ajlbbm`) USING BTREE,
    KEY `idx_bsbh` (`bsbh`) USING BTREE,
    KEY `idx_jzbh` (`jzbh`) USING BTREE,
    KEY `idx_mlbh` (`mlbh`) USING BTREE,
    KEY `idx_wslx` (`wslx`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='指标统计表';



# 转储表 t_ocr_file_0
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_ocr_file_0`;

CREATE TABLE `t_ocr_file_0`
(
    `id`                 bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `ocr_task_id`        bigint(20)    DEFAULT NULL COMMENT '图文识别批量任务 id',
    `taskid`             varchar(200)  DEFAULT NULL COMMENT '任务编号',
    `bsbh`               varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `wjxh`               varchar(200)  DEFAULT NULL COMMENT '文件序号',
    `bmsah`              varchar(1024) DEFAULT NULL COMMENT '部门受案号',
    `ocr_result_path`    varchar(200)  DEFAULT NULL COMMENT '识别结果存储路径',
    `engine_result_path` varchar(200)  DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `type`               varchar(20)   DEFAULT NULL COMMENT '识别类型，单文件或批量任务',
    `status`             varchar(30)   DEFAULT NULL COMMENT '状态',
    `fail_info`          varchar(500)  DEFAULT NULL COMMENT '任务失败信息',
    `remark`             varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `state`              varchar(1)    DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_ocr_file_create_time` (`create_time`) USING BTREE,
    KEY `idx_ocr_file_bsbh` (`bsbh`) USING BTREE,
    KEY `idx_ocr_task_id` (`ocr_task_id`) USING BTREE,
    KEY `idx_ocr_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='ocr识别任务单元表';



# 转储表 t_ocr_file_1
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_ocr_file_1`;

CREATE TABLE `t_ocr_file_1`
(
    `id`                 bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `ocr_task_id`        bigint(20)    DEFAULT NULL COMMENT '图文识别批量任务 id',
    `taskid`             varchar(200)  DEFAULT NULL COMMENT '任务编号',
    `bsbh`               varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `wjxh`               varchar(200)  DEFAULT NULL COMMENT '文件序号',
    `bmsah`              varchar(1024) DEFAULT NULL COMMENT '部门受案号',
    `ocr_result_path`    varchar(200)  DEFAULT NULL COMMENT '识别结果存储路径',
    `engine_result_path` varchar(200)  DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `type`               varchar(20)   DEFAULT NULL COMMENT '识别类型，单文件或批量任务',
    `status`             varchar(30)   DEFAULT NULL COMMENT '状态',
    `fail_info`          varchar(500)  DEFAULT NULL COMMENT '任务失败信息',
    `remark`             varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `state`              varchar(1)    DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_ocr_file_create_time` (`create_time`) USING BTREE,
    KEY `idx_ocr_file_bsbh` (`bsbh`) USING BTREE,
    KEY `idx_ocr_task_id` (`ocr_task_id`) USING BTREE,
    KEY `idx_ocr_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='ocr识别任务单元表';



# 转储表 t_ocr_file_2
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_ocr_file_2`;

CREATE TABLE `t_ocr_file_2`
(
    `id`                 bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `ocr_task_id`        bigint(20)    DEFAULT NULL COMMENT '图文识别批量任务 id',
    `taskid`             varchar(200)  DEFAULT NULL COMMENT '任务编号',
    `bsbh`               varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `wjxh`               varchar(200)  DEFAULT NULL COMMENT '文件序号',
    `bmsah`              varchar(1024) DEFAULT NULL COMMENT '部门受案号',
    `ocr_result_path`    varchar(200)  DEFAULT NULL COMMENT '识别结果存储路径',
    `engine_result_path` varchar(200)  DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `type`               varchar(20)   DEFAULT NULL COMMENT '识别类型，单文件或批量任务',
    `status`             varchar(30)   DEFAULT NULL COMMENT '状态',
    `fail_info`          varchar(500)  DEFAULT NULL COMMENT '任务失败信息',
    `remark`             varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `state`              varchar(1)    DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_ocr_file_create_time` (`create_time`) USING BTREE,
    KEY `idx_ocr_file_bsbh` (`bsbh`) USING BTREE,
    KEY `idx_ocr_task_id` (`ocr_task_id`) USING BTREE,
    KEY `idx_ocr_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='ocr识别任务单元表';



# 转储表 t_ocr_file_3
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_ocr_file_3`;

CREATE TABLE `t_ocr_file_3`
(
    `id`                 bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `ocr_task_id`        bigint(20)    DEFAULT NULL COMMENT '图文识别批量任务 id',
    `taskid`             varchar(200)  DEFAULT NULL COMMENT '任务编号',
    `bsbh`               varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `wjxh`               varchar(200)  DEFAULT NULL COMMENT '文件序号',
    `bmsah`              varchar(1024) DEFAULT NULL COMMENT '部门受案号',
    `ocr_result_path`    varchar(200)  DEFAULT NULL COMMENT '识别结果存储路径',
    `engine_result_path` varchar(200)  DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `type`               varchar(20)   DEFAULT NULL COMMENT '识别类型，单文件或批量任务',
    `status`             varchar(30)   DEFAULT NULL COMMENT '状态',
    `fail_info`          varchar(500)  DEFAULT NULL COMMENT '任务失败信息',
    `remark`             varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `state`              varchar(1)    DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_ocr_file_create_time` (`create_time`) USING BTREE,
    KEY `idx_ocr_file_bsbh` (`bsbh`) USING BTREE,
    KEY `idx_ocr_task_id` (`ocr_task_id`) USING BTREE,
    KEY `idx_ocr_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='ocr识别任务单元表';



# 转储表 t_ocr_file_4
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_ocr_file_4`;

CREATE TABLE `t_ocr_file_4`
(
    `id`                 bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `ocr_task_id`        bigint(20)    DEFAULT NULL COMMENT '图文识别批量任务 id',
    `taskid`             varchar(200)  DEFAULT NULL COMMENT '任务编号',
    `bsbh`               varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `wjxh`               varchar(200)  DEFAULT NULL COMMENT '文件序号',
    `bmsah`              varchar(1024) DEFAULT NULL COMMENT '部门受案号',
    `ocr_result_path`    varchar(200)  DEFAULT NULL COMMENT '识别结果存储路径',
    `engine_result_path` varchar(200)  DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `type`               varchar(20)   DEFAULT NULL COMMENT '识别类型，单文件或批量任务',
    `status`             varchar(30)   DEFAULT NULL COMMENT '状态',
    `fail_info`          varchar(500)  DEFAULT NULL COMMENT '任务失败信息',
    `remark`             varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `state`              varchar(1)    DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_ocr_file_create_time` (`create_time`) USING BTREE,
    KEY `idx_ocr_file_bsbh` (`bsbh`) USING BTREE,
    KEY `idx_ocr_task_id` (`ocr_task_id`) USING BTREE,
    KEY `idx_ocr_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='ocr识别任务单元表';



# 转储表 t_ocr_file_5
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_ocr_file_5`;

CREATE TABLE `t_ocr_file_5`
(
    `id`                 bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `ocr_task_id`        bigint(20)    DEFAULT NULL COMMENT '图文识别批量任务 id',
    `taskid`             varchar(200)  DEFAULT NULL COMMENT '任务编号',
    `bsbh`               varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `wjxh`               varchar(200)  DEFAULT NULL COMMENT '文件序号',
    `bmsah`              varchar(1024) DEFAULT NULL COMMENT '部门受案号',
    `ocr_result_path`    varchar(200)  DEFAULT NULL COMMENT '识别结果存储路径',
    `engine_result_path` varchar(200)  DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `type`               varchar(20)   DEFAULT NULL COMMENT '识别类型，单文件或批量任务',
    `status`             varchar(30)   DEFAULT NULL COMMENT '状态',
    `fail_info`          varchar(500)  DEFAULT NULL COMMENT '任务失败信息',
    `remark`             varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `state`              varchar(1)    DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_ocr_file_create_time` (`create_time`) USING BTREE,
    KEY `idx_ocr_file_bsbh` (`bsbh`) USING BTREE,
    KEY `idx_ocr_task_id` (`ocr_task_id`) USING BTREE,
    KEY `idx_ocr_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='ocr识别任务单元表';



# 转储表 t_ocr_file_6
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_ocr_file_6`;

CREATE TABLE `t_ocr_file_6`
(
    `id`                 bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `ocr_task_id`        bigint(20)    DEFAULT NULL COMMENT '图文识别批量任务 id',
    `taskid`             varchar(200)  DEFAULT NULL COMMENT '任务编号',
    `bsbh`               varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `wjxh`               varchar(200)  DEFAULT NULL COMMENT '文件序号',
    `bmsah`              varchar(1024) DEFAULT NULL COMMENT '部门受案号',
    `ocr_result_path`    varchar(200)  DEFAULT NULL COMMENT '识别结果存储路径',
    `engine_result_path` varchar(200)  DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `type`               varchar(20)   DEFAULT NULL COMMENT '识别类型，单文件或批量任务',
    `status`             varchar(30)   DEFAULT NULL COMMENT '状态',
    `fail_info`          varchar(500)  DEFAULT NULL COMMENT '任务失败信息',
    `remark`             varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `state`              varchar(1)    DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_ocr_file_create_time` (`create_time`) USING BTREE,
    KEY `idx_ocr_file_bsbh` (`bsbh`) USING BTREE,
    KEY `idx_ocr_task_id` (`ocr_task_id`) USING BTREE,
    KEY `idx_ocr_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='ocr识别任务单元表';



# 转储表 t_ocr_file_7
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_ocr_file_7`;

CREATE TABLE `t_ocr_file_7`
(
    `id`                 bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `ocr_task_id`        bigint(20)    DEFAULT NULL COMMENT '图文识别批量任务 id',
    `taskid`             varchar(200)  DEFAULT NULL COMMENT '任务编号',
    `bsbh`               varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `wjxh`               varchar(200)  DEFAULT NULL COMMENT '文件序号',
    `bmsah`              varchar(1024) DEFAULT NULL COMMENT '部门受案号',
    `ocr_result_path`    varchar(200)  DEFAULT NULL COMMENT '识别结果存储路径',
    `engine_result_path` varchar(200)  DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `type`               varchar(20)   DEFAULT NULL COMMENT '识别类型，单文件或批量任务',
    `status`             varchar(30)   DEFAULT NULL COMMENT '状态',
    `fail_info`          varchar(500)  DEFAULT NULL COMMENT '任务失败信息',
    `remark`             varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `state`              varchar(1)    DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_ocr_file_create_time` (`create_time`) USING BTREE,
    KEY `idx_ocr_file_bsbh` (`bsbh`) USING BTREE,
    KEY `idx_ocr_task_id` (`ocr_task_id`) USING BTREE,
    KEY `idx_ocr_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='ocr识别任务单元表';



# 转储表 t_ocr_file_8
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_ocr_file_8`;

CREATE TABLE `t_ocr_file_8`
(
    `id`                 bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `ocr_task_id`        bigint(20)    DEFAULT NULL COMMENT '图文识别批量任务 id',
    `taskid`             varchar(200)  DEFAULT NULL COMMENT '任务编号',
    `bsbh`               varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `wjxh`               varchar(200)  DEFAULT NULL COMMENT '文件序号',
    `bmsah`              varchar(1024) DEFAULT NULL COMMENT '部门受案号',
    `ocr_result_path`    varchar(200)  DEFAULT NULL COMMENT '识别结果存储路径',
    `engine_result_path` varchar(200)  DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `type`               varchar(20)   DEFAULT NULL COMMENT '识别类型，单文件或批量任务',
    `status`             varchar(30)   DEFAULT NULL COMMENT '状态',
    `fail_info`          varchar(500)  DEFAULT NULL COMMENT '任务失败信息',
    `remark`             varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `state`              varchar(1)    DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_ocr_file_create_time` (`create_time`) USING BTREE,
    KEY `idx_ocr_file_bsbh` (`bsbh`) USING BTREE,
    KEY `idx_ocr_task_id` (`ocr_task_id`) USING BTREE,
    KEY `idx_ocr_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='ocr识别任务单元表';



# 转储表 t_ocr_file_9
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_ocr_file_9`;

CREATE TABLE `t_ocr_file_9`
(
    `id`                 bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `ocr_task_id`        bigint(20)    DEFAULT NULL COMMENT '图文识别批量任务 id',
    `taskid`             varchar(200)  DEFAULT NULL COMMENT '任务编号',
    `bsbh`               varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `wjxh`               varchar(200)  DEFAULT NULL COMMENT '文件序号',
    `bmsah`              varchar(1024) DEFAULT NULL COMMENT '部门受案号',
    `ocr_result_path`    varchar(200)  DEFAULT NULL COMMENT '识别结果存储路径',
    `engine_result_path` varchar(200)  DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `type`               varchar(20)   DEFAULT NULL COMMENT '识别类型，单文件或批量任务',
    `status`             varchar(30)   DEFAULT NULL COMMENT '状态',
    `fail_info`          varchar(500)  DEFAULT NULL COMMENT '任务失败信息',
    `remark`             varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `state`              varchar(1)    DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_ocr_file_create_time` (`create_time`) USING BTREE,
    KEY `idx_ocr_file_bsbh` (`bsbh`) USING BTREE,
    KEY `idx_ocr_task_id` (`ocr_task_id`) USING BTREE,
    KEY `idx_ocr_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='ocr识别任务单元表';



# 转储表 t_ocr_task_0
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_ocr_task_0`;

CREATE TABLE `t_ocr_task_0`
(
    `id`               bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100)  DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100)  DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200)  DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(1024) DEFAULT NULL COMMENT '部门受案号',
    `status`           varchar(30)   DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500)  DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `jzbh`             varchar(200)  DEFAULT NULL COMMENT '卷宗编号',
    `fail_nums`        int(10)       DEFAULT NULL COMMENT '失败次数',
    `stat`             varchar(1)    DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_ocr_task_create_time` (`create_time`) USING BTREE,
    KEY `idx_ocr_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='ocr识别任务表';



# 转储表 t_ocr_task_1
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_ocr_task_1`;

CREATE TABLE `t_ocr_task_1`
(
    `id`               bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100)  DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100)  DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200)  DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(1024) DEFAULT NULL COMMENT '部门受案号',
    `status`           varchar(30)   DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500)  DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `jzbh`             varchar(200)  DEFAULT NULL COMMENT '卷宗编号',
    `fail_nums`        int(10)       DEFAULT NULL COMMENT '失败次数',
    `stat`             varchar(1)    DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_ocr_task_create_time` (`create_time`) USING BTREE,
    KEY `idx_ocr_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='ocr识别任务表';



# 转储表 t_ocr_task_2
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_ocr_task_2`;

CREATE TABLE `t_ocr_task_2`
(
    `id`               bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100)  DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100)  DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200)  DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(1024) DEFAULT NULL COMMENT '部门受案号',
    `status`           varchar(30)   DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500)  DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `jzbh`             varchar(200)  DEFAULT NULL COMMENT '卷宗编号',
    `fail_nums`        int(10)       DEFAULT NULL COMMENT '失败次数',
    `stat`             varchar(1)    DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_ocr_task_create_time` (`create_time`) USING BTREE,
    KEY `idx_ocr_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='ocr识别任务表';



# 转储表 t_ocr_task_3
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_ocr_task_3`;

CREATE TABLE `t_ocr_task_3`
(
    `id`               bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100)  DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100)  DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200)  DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(1024) DEFAULT NULL COMMENT '部门受案号',
    `status`           varchar(30)   DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500)  DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `jzbh`             varchar(200)  DEFAULT NULL COMMENT '卷宗编号',
    `fail_nums`        int(10)       DEFAULT NULL COMMENT '失败次数',
    `stat`             varchar(1)    DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_ocr_task_create_time` (`create_time`) USING BTREE,
    KEY `idx_ocr_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='ocr识别任务表';



# 转储表 t_ocr_task_4
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_ocr_task_4`;

CREATE TABLE `t_ocr_task_4`
(
    `id`               bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100)  DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100)  DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200)  DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(1024) DEFAULT NULL COMMENT '部门受案号',
    `status`           varchar(30)   DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500)  DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `jzbh`             varchar(200)  DEFAULT NULL COMMENT '卷宗编号',
    `fail_nums`        int(10)       DEFAULT NULL COMMENT '失败次数',
    `stat`             varchar(1)    DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_ocr_task_create_time` (`create_time`) USING BTREE,
    KEY `idx_ocr_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='ocr识别任务表';



# 转储表 t_ocr_task_5
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_ocr_task_5`;

CREATE TABLE `t_ocr_task_5`
(
    `id`               bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100)  DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100)  DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200)  DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(1024) DEFAULT NULL COMMENT '部门受案号',
    `status`           varchar(30)   DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500)  DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `jzbh`             varchar(200)  DEFAULT NULL COMMENT '卷宗编号',
    `fail_nums`        int(10)       DEFAULT NULL COMMENT '失败次数',
    `stat`             varchar(1)    DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_ocr_task_create_time` (`create_time`) USING BTREE,
    KEY `idx_ocr_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='ocr识别任务表';



# 转储表 t_ocr_task_6
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_ocr_task_6`;

CREATE TABLE `t_ocr_task_6`
(
    `id`               bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100)  DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100)  DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200)  DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(1024) DEFAULT NULL COMMENT '部门受案号',
    `status`           varchar(30)   DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500)  DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `jzbh`             varchar(200)  DEFAULT NULL COMMENT '卷宗编号',
    `fail_nums`        int(10)       DEFAULT NULL COMMENT '失败次数',
    `stat`             varchar(1)    DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_ocr_task_create_time` (`create_time`) USING BTREE,
    KEY `idx_ocr_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='ocr识别任务表';



# 转储表 t_ocr_task_7
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_ocr_task_7`;

CREATE TABLE `t_ocr_task_7`
(
    `id`               bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100)  DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100)  DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200)  DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(1024) DEFAULT NULL COMMENT '部门受案号',
    `status`           varchar(30)   DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500)  DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `jzbh`             varchar(200)  DEFAULT NULL COMMENT '卷宗编号',
    `fail_nums`        int(10)       DEFAULT NULL COMMENT '失败次数',
    `stat`             varchar(1)    DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_ocr_task_create_time` (`create_time`) USING BTREE,
    KEY `idx_ocr_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='ocr识别任务表';



# 转储表 t_ocr_task_8
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_ocr_task_8`;

CREATE TABLE `t_ocr_task_8`
(
    `id`               bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100)  DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100)  DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200)  DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(1024) DEFAULT NULL COMMENT '部门受案号',
    `status`           varchar(30)   DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500)  DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `jzbh`             varchar(200)  DEFAULT NULL COMMENT '卷宗编号',
    `fail_nums`        int(10)       DEFAULT NULL COMMENT '失败次数',
    `stat`             varchar(1)    DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_ocr_task_create_time` (`create_time`) USING BTREE,
    KEY `idx_ocr_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='ocr识别任务表';



# 转储表 t_ocr_task_9
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_ocr_task_9`;

CREATE TABLE `t_ocr_task_9`
(
    `id`               bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100)  DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100)  DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200)  DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(1024) DEFAULT NULL COMMENT '部门受案号',
    `status`           varchar(30)   DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500)  DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `jzbh`             varchar(200)  DEFAULT NULL COMMENT '卷宗编号',
    `fail_nums`        int(10)       DEFAULT NULL COMMENT '失败次数',
    `stat`             varchar(1)    DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_ocr_task_create_time` (`create_time`) USING BTREE,
    KEY `idx_ocr_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='ocr识别任务表';



# 转储表 t_recognize_file_0
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_recognize_file_0`;

CREATE TABLE `t_recognize_file_0`
(
    `id`                 bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `bsbh`               varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `taskid`             varchar(200)  DEFAULT NULL COMMENT '任务编号',
    `bmsah`              varchar(1024) DEFAULT NULL COMMENT '部门受案号',
    `jzbh`               varchar(200)  DEFAULT NULL COMMENT '卷宗编号',
    `mlbh`               varchar(200)  DEFAULT NULL COMMENT '目录编号',
    `wjxh`               varchar(200)  DEFAULT NULL COMMENT '文件序号',
    `engine_result_path` varchar(200)  DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `ocr_result_path`    varchar(200)  DEFAULT NULL COMMENT '识别结果存储路径',
    `status`             varchar(30)   DEFAULT NULL COMMENT '识别状态',
    `type`               varchar(30)   DEFAULT NULL COMMENT '任务类型，单文件或批量任务',
    `func_list`          varchar(500)  DEFAULT NULL COMMENT '待提取数据类型',
    `fail_info`          varchar(500)  DEFAULT NULL COMMENT '失败信息',
    `remark`             varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `recognize_task_id`  bigint(20)    DEFAULT NULL,
    `state`              varchar(1)    DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_recognize_file_create_time` (`create_time`) USING BTREE,
    KEY `idx_bsbh` (`bsbh`) USING BTREE,
    KEY `idx_recognize_task_id` (`recognize_task_id`) USING BTREE,
    KEY `idx_recognize_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='图像识别任务单元表';



# 转储表 t_recognize_file_1
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_recognize_file_1`;

CREATE TABLE `t_recognize_file_1`
(
    `id`                 bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `bsbh`               varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `taskid`             varchar(200)  DEFAULT NULL COMMENT '任务编号',
    `bmsah`              varchar(1024) DEFAULT NULL COMMENT '部门受案号',
    `jzbh`               varchar(200)  DEFAULT NULL COMMENT '卷宗编号',
    `mlbh`               varchar(200)  DEFAULT NULL COMMENT '目录编号',
    `wjxh`               varchar(200)  DEFAULT NULL COMMENT '文件序号',
    `engine_result_path` varchar(200)  DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `ocr_result_path`    varchar(200)  DEFAULT NULL COMMENT '识别结果存储路径',
    `status`             varchar(30)   DEFAULT NULL COMMENT '识别状态',
    `type`               varchar(30)   DEFAULT NULL COMMENT '任务类型，单文件或批量任务',
    `func_list`          varchar(500)  DEFAULT NULL COMMENT '待提取数据类型',
    `fail_info`          varchar(500)  DEFAULT NULL COMMENT '失败信息',
    `remark`             varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `recognize_task_id`  bigint(20)    DEFAULT NULL,
    `state`              varchar(1)    DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_recognize_file_create_time` (`create_time`) USING BTREE,
    KEY `idx_bsbh` (`bsbh`) USING BTREE,
    KEY `idx_recognize_task_id` (`recognize_task_id`) USING BTREE,
    KEY `idx_recognize_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='图像识别任务单元表';



# 转储表 t_recognize_file_2
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_recognize_file_2`;

CREATE TABLE `t_recognize_file_2`
(
    `id`                 bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `bsbh`               varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `taskid`             varchar(200)  DEFAULT NULL COMMENT '任务编号',
    `bmsah`              varchar(1024) DEFAULT NULL COMMENT '部门受案号',
    `jzbh`               varchar(200)  DEFAULT NULL COMMENT '卷宗编号',
    `mlbh`               varchar(200)  DEFAULT NULL COMMENT '目录编号',
    `wjxh`               varchar(200)  DEFAULT NULL COMMENT '文件序号',
    `engine_result_path` varchar(200)  DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `ocr_result_path`    varchar(200)  DEFAULT NULL COMMENT '识别结果存储路径',
    `status`             varchar(30)   DEFAULT NULL COMMENT '识别状态',
    `type`               varchar(30)   DEFAULT NULL COMMENT '任务类型，单文件或批量任务',
    `func_list`          varchar(500)  DEFAULT NULL COMMENT '待提取数据类型',
    `fail_info`          varchar(500)  DEFAULT NULL COMMENT '失败信息',
    `remark`             varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `recognize_task_id`  bigint(20)    DEFAULT NULL,
    `state`              varchar(1)    DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_recognize_file_create_time` (`create_time`) USING BTREE,
    KEY `idx_bsbh` (`bsbh`) USING BTREE,
    KEY `idx_recognize_task_id` (`recognize_task_id`) USING BTREE,
    KEY `idx_recognize_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='图像识别任务单元表';



# 转储表 t_recognize_file_3
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_recognize_file_3`;

CREATE TABLE `t_recognize_file_3`
(
    `id`                 bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `bsbh`               varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `taskid`             varchar(200)  DEFAULT NULL COMMENT '任务编号',
    `bmsah`              varchar(1024) DEFAULT NULL COMMENT '部门受案号',
    `jzbh`               varchar(200)  DEFAULT NULL COMMENT '卷宗编号',
    `mlbh`               varchar(200)  DEFAULT NULL COMMENT '目录编号',
    `wjxh`               varchar(200)  DEFAULT NULL COMMENT '文件序号',
    `engine_result_path` varchar(200)  DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `ocr_result_path`    varchar(200)  DEFAULT NULL COMMENT '识别结果存储路径',
    `status`             varchar(30)   DEFAULT NULL COMMENT '识别状态',
    `type`               varchar(30)   DEFAULT NULL COMMENT '任务类型，单文件或批量任务',
    `func_list`          varchar(500)  DEFAULT NULL COMMENT '待提取数据类型',
    `fail_info`          varchar(500)  DEFAULT NULL COMMENT '失败信息',
    `remark`             varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `recognize_task_id`  bigint(20)    DEFAULT NULL,
    `state`              varchar(1)    DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_recognize_file_create_time` (`create_time`) USING BTREE,
    KEY `idx_bsbh` (`bsbh`) USING BTREE,
    KEY `idx_recognize_task_id` (`recognize_task_id`) USING BTREE,
    KEY `idx_recognize_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='图像识别任务单元表';



# 转储表 t_recognize_file_4
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_recognize_file_4`;

CREATE TABLE `t_recognize_file_4`
(
    `id`                 bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `bsbh`               varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `taskid`             varchar(200)  DEFAULT NULL COMMENT '任务编号',
    `bmsah`              varchar(1024) DEFAULT NULL COMMENT '部门受案号',
    `jzbh`               varchar(200)  DEFAULT NULL COMMENT '卷宗编号',
    `mlbh`               varchar(200)  DEFAULT NULL COMMENT '目录编号',
    `wjxh`               varchar(200)  DEFAULT NULL COMMENT '文件序号',
    `engine_result_path` varchar(200)  DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `ocr_result_path`    varchar(200)  DEFAULT NULL COMMENT '识别结果存储路径',
    `status`             varchar(30)   DEFAULT NULL COMMENT '识别状态',
    `type`               varchar(30)   DEFAULT NULL COMMENT '任务类型，单文件或批量任务',
    `func_list`          varchar(500)  DEFAULT NULL COMMENT '待提取数据类型',
    `fail_info`          varchar(500)  DEFAULT NULL COMMENT '失败信息',
    `remark`             varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `recognize_task_id`  bigint(20)    DEFAULT NULL,
    `state`              varchar(1)    DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_recognize_file_create_time` (`create_time`) USING BTREE,
    KEY `idx_bsbh` (`bsbh`) USING BTREE,
    KEY `idx_recognize_task_id` (`recognize_task_id`) USING BTREE,
    KEY `idx_recognize_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='图像识别任务单元表';



# 转储表 t_recognize_file_5
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_recognize_file_5`;

CREATE TABLE `t_recognize_file_5`
(
    `id`                 bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `bsbh`               varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `taskid`             varchar(200)  DEFAULT NULL COMMENT '任务编号',
    `bmsah`              varchar(1024) DEFAULT NULL COMMENT '部门受案号',
    `jzbh`               varchar(200)  DEFAULT NULL COMMENT '卷宗编号',
    `mlbh`               varchar(200)  DEFAULT NULL COMMENT '目录编号',
    `wjxh`               varchar(200)  DEFAULT NULL COMMENT '文件序号',
    `engine_result_path` varchar(200)  DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `ocr_result_path`    varchar(200)  DEFAULT NULL COMMENT '识别结果存储路径',
    `status`             varchar(30)   DEFAULT NULL COMMENT '识别状态',
    `type`               varchar(30)   DEFAULT NULL COMMENT '任务类型，单文件或批量任务',
    `func_list`          varchar(500)  DEFAULT NULL COMMENT '待提取数据类型',
    `fail_info`          varchar(500)  DEFAULT NULL COMMENT '失败信息',
    `remark`             varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `recognize_task_id`  bigint(20)    DEFAULT NULL,
    `state`              varchar(1)    DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_recognize_file_create_time` (`create_time`) USING BTREE,
    KEY `idx_bsbh` (`bsbh`) USING BTREE,
    KEY `idx_recognize_task_id` (`recognize_task_id`) USING BTREE,
    KEY `idx_recognize_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='图像识别任务单元表';



# 转储表 t_recognize_file_6
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_recognize_file_6`;

CREATE TABLE `t_recognize_file_6`
(
    `id`                 bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `bsbh`               varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `taskid`             varchar(200)  DEFAULT NULL COMMENT '任务编号',
    `bmsah`              varchar(1024) DEFAULT NULL COMMENT '部门受案号',
    `jzbh`               varchar(200)  DEFAULT NULL COMMENT '卷宗编号',
    `mlbh`               varchar(200)  DEFAULT NULL COMMENT '目录编号',
    `wjxh`               varchar(200)  DEFAULT NULL COMMENT '文件序号',
    `engine_result_path` varchar(200)  DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `ocr_result_path`    varchar(200)  DEFAULT NULL COMMENT '识别结果存储路径',
    `status`             varchar(30)   DEFAULT NULL COMMENT '识别状态',
    `type`               varchar(30)   DEFAULT NULL COMMENT '任务类型，单文件或批量任务',
    `func_list`          varchar(500)  DEFAULT NULL COMMENT '待提取数据类型',
    `fail_info`          varchar(500)  DEFAULT NULL COMMENT '失败信息',
    `remark`             varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `recognize_task_id`  bigint(20)    DEFAULT NULL,
    `state`              varchar(1)    DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_recognize_file_create_time` (`create_time`) USING BTREE,
    KEY `idx_bsbh` (`bsbh`) USING BTREE,
    KEY `idx_recognize_task_id` (`recognize_task_id`) USING BTREE,
    KEY `idx_recognize_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='图像识别任务单元表';



# 转储表 t_recognize_file_7
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_recognize_file_7`;

CREATE TABLE `t_recognize_file_7`
(
    `id`                 bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `bsbh`               varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `taskid`             varchar(200)  DEFAULT NULL COMMENT '任务编号',
    `bmsah`              varchar(1024) DEFAULT NULL COMMENT '部门受案号',
    `jzbh`               varchar(200)  DEFAULT NULL COMMENT '卷宗编号',
    `mlbh`               varchar(200)  DEFAULT NULL COMMENT '目录编号',
    `wjxh`               varchar(200)  DEFAULT NULL COMMENT '文件序号',
    `engine_result_path` varchar(200)  DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `ocr_result_path`    varchar(200)  DEFAULT NULL COMMENT '识别结果存储路径',
    `status`             varchar(30)   DEFAULT NULL COMMENT '识别状态',
    `type`               varchar(30)   DEFAULT NULL COMMENT '任务类型，单文件或批量任务',
    `func_list`          varchar(500)  DEFAULT NULL COMMENT '待提取数据类型',
    `fail_info`          varchar(500)  DEFAULT NULL COMMENT '失败信息',
    `remark`             varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `recognize_task_id`  bigint(20)    DEFAULT NULL,
    `state`              varchar(1)    DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_recognize_file_create_time` (`create_time`) USING BTREE,
    KEY `idx_bsbh` (`bsbh`) USING BTREE,
    KEY `idx_recognize_task_id` (`recognize_task_id`) USING BTREE,
    KEY `idx_recognize_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='图像识别任务单元表';



# 转储表 t_recognize_file_8
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_recognize_file_8`;

CREATE TABLE `t_recognize_file_8`
(
    `id`                 bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `bsbh`               varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `taskid`             varchar(200)  DEFAULT NULL COMMENT '任务编号',
    `bmsah`              varchar(1024) DEFAULT NULL COMMENT '部门受案号',
    `jzbh`               varchar(200)  DEFAULT NULL COMMENT '卷宗编号',
    `mlbh`               varchar(200)  DEFAULT NULL COMMENT '目录编号',
    `wjxh`               varchar(200)  DEFAULT NULL COMMENT '文件序号',
    `engine_result_path` varchar(200)  DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `ocr_result_path`    varchar(200)  DEFAULT NULL COMMENT '识别结果存储路径',
    `status`             varchar(30)   DEFAULT NULL COMMENT '识别状态',
    `type`               varchar(30)   DEFAULT NULL COMMENT '任务类型，单文件或批量任务',
    `func_list`          varchar(500)  DEFAULT NULL COMMENT '待提取数据类型',
    `fail_info`          varchar(500)  DEFAULT NULL COMMENT '失败信息',
    `remark`             varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `recognize_task_id`  bigint(20)    DEFAULT NULL,
    `state`              varchar(1)    DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_recognize_file_create_time` (`create_time`) USING BTREE,
    KEY `idx_bsbh` (`bsbh`) USING BTREE,
    KEY `idx_recognize_task_id` (`recognize_task_id`) USING BTREE,
    KEY `idx_recognize_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='图像识别任务单元表';



# 转储表 t_recognize_file_9
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_recognize_file_9`;

CREATE TABLE `t_recognize_file_9`
(
    `id`                 bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`        datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `bsbh`               varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `taskid`             varchar(200)  DEFAULT NULL COMMENT '任务编号',
    `bmsah`              varchar(1024) DEFAULT NULL COMMENT '部门受案号',
    `jzbh`               varchar(200)  DEFAULT NULL COMMENT '卷宗编号',
    `mlbh`               varchar(200)  DEFAULT NULL COMMENT '目录编号',
    `wjxh`               varchar(200)  DEFAULT NULL COMMENT '文件序号',
    `engine_result_path` varchar(200)  DEFAULT NULL COMMENT 'ocr引擎结果存储路径',
    `ocr_result_path`    varchar(200)  DEFAULT NULL COMMENT '识别结果存储路径',
    `status`             varchar(30)   DEFAULT NULL COMMENT '识别状态',
    `type`               varchar(30)   DEFAULT NULL COMMENT '任务类型，单文件或批量任务',
    `func_list`          varchar(500)  DEFAULT NULL COMMENT '待提取数据类型',
    `fail_info`          varchar(500)  DEFAULT NULL COMMENT '失败信息',
    `remark`             varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `recognize_task_id`  bigint(20)    DEFAULT NULL,
    `state`              varchar(1)    DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_recognize_file_create_time` (`create_time`) USING BTREE,
    KEY `idx_bsbh` (`bsbh`) USING BTREE,
    KEY `idx_recognize_task_id` (`recognize_task_id`) USING BTREE,
    KEY `idx_recognize_file_wjxh` (`wjxh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='图像识别任务单元表';



# 转储表 t_recognize_task_0
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_recognize_task_0`;

CREATE TABLE `t_recognize_task_0`
(
    `id`               bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100)  DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100)  DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200)  DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(200)  DEFAULT NULL COMMENT '部门受案号',
    `jzbh`             varchar(200)  DEFAULT NULL COMMENT '卷宗编号',
    `status`           varchar(30)   DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500)  DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `fail_nums`        int(10)       DEFAULT NULL COMMENT '失败次数',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_recognize_task_create_time` (`create_time`) USING BTREE,
    KEY `idx_recognize_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='图像识别任务表';



# 转储表 t_recognize_task_1
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_recognize_task_1`;

CREATE TABLE `t_recognize_task_1`
(
    `id`               bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100)  DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100)  DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200)  DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(200)  DEFAULT NULL COMMENT '部门受案号',
    `jzbh`             varchar(200)  DEFAULT NULL COMMENT '卷宗编号',
    `status`           varchar(30)   DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500)  DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `fail_nums`        int(10)       DEFAULT NULL COMMENT '失败次数',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_recognize_task_create_time` (`create_time`) USING BTREE,
    KEY `idx_recognize_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='图像识别任务表';



# 转储表 t_recognize_task_2
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_recognize_task_2`;

CREATE TABLE `t_recognize_task_2`
(
    `id`               bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100)  DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100)  DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200)  DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(200)  DEFAULT NULL COMMENT '部门受案号',
    `jzbh`             varchar(200)  DEFAULT NULL COMMENT '卷宗编号',
    `status`           varchar(30)   DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500)  DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `fail_nums`        int(10)       DEFAULT NULL COMMENT '失败次数',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_recognize_task_create_time` (`create_time`) USING BTREE,
    KEY `idx_recognize_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='图像识别任务表';



# 转储表 t_recognize_task_3
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_recognize_task_3`;

CREATE TABLE `t_recognize_task_3`
(
    `id`               bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100)  DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100)  DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200)  DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(200)  DEFAULT NULL COMMENT '部门受案号',
    `jzbh`             varchar(200)  DEFAULT NULL COMMENT '卷宗编号',
    `status`           varchar(30)   DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500)  DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `fail_nums`        int(10)       DEFAULT NULL COMMENT '失败次数',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_recognize_task_create_time` (`create_time`) USING BTREE,
    KEY `idx_recognize_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='图像识别任务表';



# 转储表 t_recognize_task_4
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_recognize_task_4`;

CREATE TABLE `t_recognize_task_4`
(
    `id`               bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100)  DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100)  DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200)  DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(200)  DEFAULT NULL COMMENT '部门受案号',
    `jzbh`             varchar(200)  DEFAULT NULL COMMENT '卷宗编号',
    `status`           varchar(30)   DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500)  DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `fail_nums`        int(10)       DEFAULT NULL COMMENT '失败次数',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_recognize_task_create_time` (`create_time`) USING BTREE,
    KEY `idx_recognize_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='图像识别任务表';



# 转储表 t_recognize_task_5
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_recognize_task_5`;

CREATE TABLE `t_recognize_task_5`
(
    `id`               bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100)  DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100)  DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200)  DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(200)  DEFAULT NULL COMMENT '部门受案号',
    `jzbh`             varchar(200)  DEFAULT NULL COMMENT '卷宗编号',
    `status`           varchar(30)   DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500)  DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `fail_nums`        int(10)       DEFAULT NULL COMMENT '失败次数',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_recognize_task_create_time` (`create_time`) USING BTREE,
    KEY `idx_recognize_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='图像识别任务表';



# 转储表 t_recognize_task_6
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_recognize_task_6`;

CREATE TABLE `t_recognize_task_6`
(
    `id`               bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100)  DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100)  DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200)  DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(200)  DEFAULT NULL COMMENT '部门受案号',
    `jzbh`             varchar(200)  DEFAULT NULL COMMENT '卷宗编号',
    `status`           varchar(30)   DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500)  DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `fail_nums`        int(10)       DEFAULT NULL COMMENT '失败次数',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_recognize_task_create_time` (`create_time`) USING BTREE,
    KEY `idx_recognize_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='图像识别任务表';



# 转储表 t_recognize_task_7
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_recognize_task_7`;

CREATE TABLE `t_recognize_task_7`
(
    `id`               bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100)  DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100)  DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200)  DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(200)  DEFAULT NULL COMMENT '部门受案号',
    `jzbh`             varchar(200)  DEFAULT NULL COMMENT '卷宗编号',
    `status`           varchar(30)   DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500)  DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `fail_nums`        int(10)       DEFAULT NULL COMMENT '失败次数',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_recognize_task_create_time` (`create_time`) USING BTREE,
    KEY `idx_recognize_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='图像识别任务表';



# 转储表 t_recognize_task_8
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_recognize_task_8`;

CREATE TABLE `t_recognize_task_8`
(
    `id`               bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100)  DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100)  DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200)  DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(200)  DEFAULT NULL COMMENT '部门受案号',
    `jzbh`             varchar(200)  DEFAULT NULL COMMENT '卷宗编号',
    `status`           varchar(30)   DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500)  DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `fail_nums`        int(10)       DEFAULT NULL COMMENT '失败次数',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_recognize_task_create_time` (`create_time`) USING BTREE,
    KEY `idx_recognize_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='图像识别任务表';



# 转储表 t_recognize_task_9
# ------------------------------------------------------------

DROP TABLE IF EXISTS `t_recognize_task_9`;

CREATE TABLE `t_recognize_task_9`
(
    `id`               bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
    `create_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) COMMENT '创建时间',
    `update_time`      datetime(3)   DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `systemid`         varchar(100)  DEFAULT NULL COMMENT '应用和系统唯一id',
    `dwbm`             varchar(100)  DEFAULT NULL COMMENT '单位编号',
    `bsbh`             varchar(200)  DEFAULT NULL COMMENT '标识编号',
    `taskid`           varchar(200)  DEFAULT NULL COMMENT '任务id',
    `bmsah`            varchar(200)  DEFAULT NULL COMMENT '部门受案号',
    `jzbh`             varchar(200)  DEFAULT NULL COMMENT '卷宗编号',
    `status`           varchar(30)   DEFAULT NULL COMMENT '任务状态',
    `result_push_info` varchar(500)  DEFAULT NULL COMMENT '结果推送信息',
    `remark`           varchar(2000) DEFAULT NULL COMMENT '预留字段',
    `fail_nums`        int(10)       DEFAULT NULL COMMENT '失败次数',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_recognize_task_create_time` (`create_time`) USING BTREE,
    KEY `idx_recognize_task_bsbh` (`bsbh`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='图像识别任务表';



/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;