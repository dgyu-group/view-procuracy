## 项目简介
高检 2.0 对接项目

## 技术选型
基于 springboot-2.1.6.RELEASE, JDK1.8, lombok-1.18.8


### 

## 本地构建
 
## 外部依赖
项目运行时所依赖的外部集成方，比如订单系统会依赖于会员系统；
 
## 环境信息
各个环境的访问方式，数据库连接等；
 
## 编码实践
统一的编码实践，比如异常处理原则、分页封装等；
1. view-procuracy-web工程按功能模块划分, ocr包处理 ocr 相关业务, card 处理案卡业务, catalog 处理编目业务. 每个包下又

## FAQ
开发过程中常见问题的解答。
1. view-procuracy-web 工程里的 bd 包什么意思?
  bd: business delegate, 业务代理层, 用来封装对下层 service(view-procuracy-ocr, view-procuracy-card等) 的调用

## 模块说明

- `view-procuracy` : 父工程, 定义通用依赖及配置
- `view-procuracy-common` : 通用组件工程, 包含: 工具类, 公共异常类, 结果码, POJO 等
    ```
    ├── aspect: 通用切面逻辑
    │── enums: 通用枚举类
    │── exception: 工程统一异常封装, 每个子模块可以继承后定义自己的异常
    │── result: 统一的结果对象, 包括分页结果对象, 统一 web 响应对象
    │── util: 通用的工具类
    ```
- `view-procuracy-tools`: 底层引擎调用的简单封装, 不包含业务逻辑    
    ```
    ├── cont: 编目能力
    │── elle: 要素抽取能力
    │── ocr: 图文识别能力
    ```
- `view-procuracy-ocr` : OCR工程
- `view-procuracy-card` : 案卡工程
- `view-procuracy-cont` : 编目工程
- `view-procuracy-web` : 项目入口 web 工程


## 通用逻辑说明
- 工具类
- 结果码
- 异常结构
- 参数验证
- web 层统一异常处理
## 海南现场问题解决
对应版本号2.2.7
## 湖南识别失败导致的同一标识编号无法再次识别情况
解决方法：通过判断数据库中待识别任务执行时间是否超过3分钟去删除失败任务
对应版本号2.2.16

## 问题排查记录
### 云南合成双层PDF文件的问题排查
#### test code
```text
//通过ImageMagic工具将失真的jpg(有损压缩)转成png（无损压缩）后可成功生成pdf
    
    public static void main(String[] args) throws Exception {
        String filePath = "D:\\test\\15.jpg";
        File file = new File(filePath);
        byte[] fileBytes = FileUtil.readBytes(file);
        String outPath =  "D:\\test\\15_result_pdf.pdf";
        String SERVER_URL = "http://172.31.96.176:8085/"+"/tuling/ocr/v2/file/{trackId}";
        Result<String> ocrResult = null;
        String trackId = UUID.randomUUID().toString();
        String serviceUrl = StringUtils.replace(SERVER_URL, "{trackId}", trackId);
        serviceUrl=serviceUrl +"?params={params}";
        JSONObject params = new JSONObject();
        params.put("pagetype", "page");
        params.put("funclist", "");
        params.put("resultlevel", "3");

        params.put("pagetype", "page");
        //DEFAULT_PARAMS_JSON.put("funclist", "ocr,od");
        params.put("funclist", "");
        //od 朝向检测 ；ic 图片分类； er 证据检出（签章、收印、插图、手写体）
        params.put("resultlevel", "3");

        HttpHeaders headers = new HttpHeaders();
        //headers.setContentType(MediaType.MULTIPART_FORM_DATA_VALUE.toString());
        headers.add("Content-Type", MediaType.MULTIPART_FORM_DATA_VALUE);
        FileSystemResource resource = new FileSystemResource(file);
        MultiValueMap<String, Object> form = new LinkedMultiValueMap<>();
        form.add("picFile", resource);

        HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(form, headers);
        String finalServiceUrl = serviceUrl;
        String paramJson = params.toJSONString();
        RestTemplate restTemplate = new RestTemplate();
        String resp = restTemplate.postForObject(finalServiceUrl, httpEntity, String.class, paramJson);
        Result<String> body = Result.success(JSONUtil.getProp(JSONUtil.toJsonNode(resp), "body", String.class));
        String ocrJson = body.getData();
        byte[] bytes = new PdfComposeUtil().handleCompose(fileBytes, ocrJson);
        FileUtil.writeBytes(bytes,outPath);
    }
      /*  Image image = Image.getInstance(filePath);
        image.getPlainHeight();*/
    }
```