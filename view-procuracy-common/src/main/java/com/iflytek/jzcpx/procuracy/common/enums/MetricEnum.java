package com.iflytek.jzcpx.procuracy.common.enums;

/**
 * 指标类型枚举
 * 
 * @author dgyu
 *
 */
public enum MetricEnum {
    OCR, CARD, CONT, RECOGNIZE;

    public static MetricEnum getMetricEnum(String uri) {
	if (uri.indexOf(MetricEnum.CARD.name().toLowerCase()) > -1) {
	    return MetricEnum.CARD;
	} else if (uri.indexOf(MetricEnum.OCR.name().toLowerCase()) > -1 || uri.indexOf("/iflytek/api") > -1) {
	    return MetricEnum.OCR;
	} else if (uri.indexOf(MetricEnum.CONT.name().toLowerCase()) > -1) {
	    return MetricEnum.CONT;
	} else {
	    return MetricEnum.RECOGNIZE;
	}
    }
}
