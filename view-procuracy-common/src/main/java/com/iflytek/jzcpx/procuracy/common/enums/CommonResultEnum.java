package com.iflytek.jzcpx.procuracy.common.enums;

/**
 * 通用结果枚举, 3 位
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-06 11:37
 */
public enum CommonResultEnum implements ResultType {
    /** 成功 */
    SUCCESS(1, "成功"),

    /** 失败 */
    FAILED(500, "失败"),
    EXCEPTION(101, "服务异常"),

    INVALID_PARAMETER(201, "参数不合法"),
    PARAMETER_MISSING(202, "参数缺失"),



    ;

    private Integer code;
    private String desc;

    CommonResultEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static CommonResultEnum getByCode(Integer code) {
        if (code == null) {
            return null;
        }

        for (CommonResultEnum anEnum : values()) {
            if (anEnum.getCode().equals(code)) {
                return anEnum;
            }
        }

        return null;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getDesc() {
        return desc;
    }}