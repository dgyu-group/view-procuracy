package com.iflytek.jzcpx.procuracy.common.model;

import lombok.Data;

@Data
public class HandContResult {

    public HandContResult()
    {
        success=true;
        message="成功";
    }

    private String code;

    private  boolean success;

    private String message;

    private HandContResultInfo data;
}
