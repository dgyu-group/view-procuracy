package com.iflytek.jzcpx.procuracy.common.result;

import java.util.List;

import lombok.Data;

/**
 * 分页结果
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-06 11:31
 */
@Data
public class PageResult<T> {

    /** 页码, 从 1 开始 */
    private int page;

    /** 分页条数 */
    private int size;

    /** 总条数 */
    private long total;

    /** 分页数据 */
    private List<T> data;

    public PageResult() {
    }

    public PageResult(int page, int size) {
        this.page = page;
        this.size = size;
    }
}
