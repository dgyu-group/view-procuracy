package com.iflytek.jzcpx.procuracy.common.model;

import lombok.Data;

@Data
public class HandContResultInfoJzjbxx {

    private String  bmsah;// "汉东省院起诉受[2016]77000000103号",#部门受案号
    private String  tysah;// "77000020160055700",#统一受案号
  private String  jzmc;// "123",#卷宗名称
     private String  jzlj;// null,#卷宗存储路径
     private String jzbh;// "77000000000001",#卷宗编号
     private String jzms;// null,#卷宗描述
     private   String jzscsj;// "2019-01-11T08:50:35.000+0000",#卷宗上传时间
     private String  dwbm;// "770000"#单位编码

     private  String cjsj;
     private String zhxgsj;
     private String sjbsbh;
     private String sfsc;
     private String sjly;
     private String sjlybs;
     private String rybm;
     private String rymc;
     private String zzzt;
     private String jzxh;
     private String jzsd;
     private String nfbd;
     private Integer jzcs;
     private Integer jzwjs;
     private Integer jzys;
     private String jzzzr;
     private String jzzzrbm;
     private String ajlbbm;
     private String ajlbmc;
     private String slrq;
}
