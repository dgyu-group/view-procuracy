package com.iflytek.jzcpx.procuracy.common.enums;

/**
 * 接口状态
 *
 * 1 位: 成功(1)
 * 3 位: 通用错误
 * 4 位: 引擎错误 11：ocr引擎， 13：编目引擎，14：要素抽取引擎
 * 5 位: 业务类错误, 11xxx:ocr，12:图像识别， 13xxx:编目， 14xxx:案卡
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-07 09:01
 */
public interface ResultType {

    int SUCCESS = 1;

    default boolean isSuccess() {
        return this.getCode() == ResultType.SUCCESS;
    }

    Integer getCode();

    String getDesc();
}
