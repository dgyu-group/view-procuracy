package com.iflytek.jzcpx.procuracy.common.exception;

import com.iflytek.jzcpx.procuracy.common.enums.ResultType;

/**
 * 参数验证不合法异常
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-06 13:48
 */
public class InvalidParameterException extends ViewException {
    private static final long serialVersionUID = -220674549834770492L;

    public InvalidParameterException() {
        super();
    }

    public InvalidParameterException(ResultType code, String message) {
        super(code, message);
    }
}
