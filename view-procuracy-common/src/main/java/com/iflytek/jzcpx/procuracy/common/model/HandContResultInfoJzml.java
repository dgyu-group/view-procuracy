package com.iflytek.jzcpx.procuracy.common.model;//

import lombok.Data;

/**
 *
 */
@Data
public class HandContResultInfoJzml {
    private String bmsah;// 汉东省院起诉受[2016]77000000103号,#部门受案号
    private String jzbh;// 77000000000001,#卷宗编号
    private String dwbm;// 770000,#单位编码
    private String mlsxh;// 1,#目录顺序号
    private String sslbbm;// 000000-000001,#所属类别编码
    private String mlxsmc;// 诉讼文书卷,#目录显示名称
    private String sslbmc;// 诉讼文书卷,#所属类别名称
    private String mlbm;// null,#目录编码
    private String mlbh;// 4865AE7A9EDD4B53BB50749391C0EBEB,#目录编号
    private String mllx;// 1,#目录类型
    private String fmlbh;// null,#父目录编号
    private String mlxx;// null#目录详细信息
    private String cjsj;
    private String zhxgsj;
    private String sjbsbh;
    private String sfsc;
    private String sjly;
    private String mlbs;
    private String taskid;

}
