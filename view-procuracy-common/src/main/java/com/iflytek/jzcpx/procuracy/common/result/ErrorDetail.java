package com.iflytek.jzcpx.procuracy.common.result;

import java.util.HashMap;
import java.util.Map;

import com.iflytek.jzcpx.procuracy.common.enums.ResultType;
import lombok.Data;

/**
 * 失败请求错误详情
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-07 08:39
 */
@Data
public class ErrorDetail {
    /** 结果码, 值为:{@link ResultType#getCode()} */
    private int code;

    /** 结果描述, 值为:{@link ResultType#getDesc()} */
    private String msg;

    /** 结果类型, 值为:{@link ResultType#toString()} */
    private String name;

    private Map<String, Object> data = new HashMap<>();

    public ErrorDetail(ResultType resultType) {
        this.name = resultType.toString();
        this.code = resultType.getCode();
        this.msg = resultType.getDesc();
    }
}
