package com.iflytek.jzcpx.procuracy.common.model;

import lombok.Data;

@Data
public class HandContResultInfoJzmlwj {

    private String wjmc;// 7FF12A291C704D61AD05773FD1016D8E,#文件名称
    private String wjlj;// \\770000\\2019\\0301\\汉东省院起诉受[2016]37000000103号,#文件路径
    private String wjhz;// .pdf,#文件后缀
    private String wjxh;// 231829D094C94EFF98382B4CE5F857B7,#文件序号
    private String wjzdy;// null,#文件自定义（Y：识别成功）
    private String bmsah;// 汉东省院起诉受[2016]37000000103号,#部门受案号
    private String jzbh;// 77000000000001,#卷宗编号
    private String dwbm;// 770000,#单位编码
    private Integer wjsxh;// 0,#文件顺序号
    private String sslbbm;// null,# 所属类别编码
    private Long wjdx;// 0,#文件大小
    private String sslbmc;// null,#所属类别名称
    private Integer wjksy;// 1,#文件开始页
    private String wjbzxx;// null,#文件备注信息
    private String wjym;// null,#文件页码
    private String wjlx;// null,#文件类型
    private String mlbh;// 4865AE7A9EDD4B53BB50749391C0EBEB,#目录编号
    private String wjxsmc;// 第 1 页,#文件显示名称
    private Integer wjjsy;// 1,#文件结束页
    private String wjyzbz;// Y#文件验证标识（MD5值）
    private String cjsj;
    private String zhxgsj;
    private String sjbsbh;
    private String sfsc;
    private String sjly;
    private String wjscbz;
    private String ywjlj;
    private String sltwjlj;
    private String pdfwjlj;
    private String jpgwjlj;
    private String wjbs;
    private String taskid;
    private String wjid;
    private String ocrsbzt;
    private String wjocrsbzt;
    private String jpgnrwjlj;
    private String syzbwjlj;
    private String mhzbwjlj;
}
