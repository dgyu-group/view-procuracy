package com.iflytek.jzcpx.procuracy.common.exception;

import java.util.HashMap;
import java.util.Map;

import com.iflytek.jzcpx.procuracy.common.enums.ResultType;

import static com.iflytek.jzcpx.procuracy.common.enums.CommonResultEnum.FAILED;

/**
 * 基础异常
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-06 11:38
 */
public class ViewException extends RuntimeException {
    private static final long serialVersionUID = 8832650084951672891L;

    private final ResultType resultType;

    private final Map<String, Object> data = new HashMap();

    public ViewException() {
        this.resultType = FAILED;
    }

    public ViewException(ResultType resultType) {
        this.resultType = resultType;
    }

    public ViewException(ResultType resultType, String message) {
        super(message);
        this.resultType = resultType;
    }

    public ViewException(ResultType resultType, Throwable cause) {
        super(cause);
        this.resultType = resultType;
    }

    public ViewException(ResultType resultType, String message, Throwable cause) {
        super(message, cause);
        this.resultType = resultType;
    }

    public Map<String, Object> addData(String key, Object value) {
        data.put(key, value);
        return this.data;
    }

    public Map<String, Object> getData() {
        return this.data;
    }

    public ResultType getResultType() {
        return resultType;
    }
}
