package com.iflytek.jzcpx.procuracy.common.util;

/**
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2022/7/15
 */
public class Logs {

    public static int maxLength = -1;

    public static String chop(Object obj) {
        if (obj == null) {
            return null;
        }
        String s = obj.toString();
        return maxLength == -1 ? s : s.substring(0, maxLength);
    }
}

