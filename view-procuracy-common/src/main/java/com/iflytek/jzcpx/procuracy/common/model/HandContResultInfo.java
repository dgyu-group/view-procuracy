package com.iflytek.jzcpx.procuracy.common.model;

import lombok.Data;

import java.util.List;

@Data
public class HandContResultInfo {

    /**
     * 卷宗编号
     */
    private String jzbh;

    /**
     * 标志编号
     */
    private String bsbh;

    /**
     * 卷宗基本信息
     */
    private HandContResultInfoJzjbxx jzjbxx;

    /**
     * 卷宗目录
     */
    private List<HandContResultInfoJzml> jzml;

    /**
     * 卷宗目录文件
     */
    private List<HandContResultInfoJzmlwj> jzmlwj;

}
