package com.iflytek.jzcpx.procuracy.common.util;

import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ID 生成器
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019/5/27 15:36
 */
public final class IdWokerUtil {
    private static final Logger logger = LoggerFactory.getLogger(IdWokerUtil.class);

    private static final Sequence SEQUENCE = new Sequence();

    private IdWokerUtil() {
    }

    public static long nextId() {
        return SEQUENCE.nextId();
    }

    public static String decode(long id) {
        return SEQUENCE.decode(id);
    }

    public static void main(String[] args) {
        // todo 解析id
    }

    private static class Sequence {
        /*
         * <br>
         * 0 - 0000000000 0000000000 0000000000 0000000000 0 - 00000 - 00000 - 000000000000 <br>
         * <br>
         * 1位符号位 + 41位时间戳(69年) + 5位节点标识(32) + 5位进程标识(32) + 12位毫秒内序列(4096/ms)
         */

        /**
         * 时间起始标记点 2019-01-01 00:00:00.000，作为基准，一旦确定不能变动
         */
        private final long twepoch = 1546272000000L;
        /**
         * 节点标识位数
         */
        private final long datacenterIdBits = 5L;
        /**
         * 进程标识位数
         */
        private final long workerIdBits = 5L;
        private final long maxWorkerId = -1L ^ (-1L << workerIdBits);
        private final long maxDatacenterId = -1L ^ (-1L << datacenterIdBits);
        /**
         * 毫秒内自增位
         */
        private final long sequenceBits = 12L;
        private final long workerIdShift = sequenceBits;
        private final long datacenterIdShift = sequenceBits + workerIdBits;
        /**
         * 时间戳左移动位
         */
        private final long timestampLeftShift = sequenceBits + workerIdBits + datacenterIdBits;
        private final long sequenceMask = -1L ^ (-1L << sequenceBits);

        /**
         * 节点标识 ID
         */
        private final long datacenterId;

        /**
         * 进程标识 ID
         */
        private final long workerId;

        /**
         * 毫秒内序列
         */
        private long sequence = 0L;

        /**
         * 上次生产 ID 时间戳
         */
        private long lastTimestamp = -1L;

        public Sequence() {
            this.datacenterId = getDatacenterId(maxDatacenterId);
            this.workerId = getMaxWorkerId(datacenterId, maxWorkerId);
        }

        /**
         * 有参构造器
         *
         * @param workerId     工作机器 ID
         * @param datacenterId 序列号
         */
        public Sequence(long workerId, long datacenterId) {
            if (workerId > maxWorkerId || workerId < 0) {
                throw new RuntimeException(
                        String.format("worker Id can't be greater than %d or less than 0", maxWorkerId));
            }
            if (datacenterId > maxDatacenterId || datacenterId < 0) {
                throw new RuntimeException(
                        String.format("datacenter Id can't be greater than %d or less than 0", maxDatacenterId));
            }
            this.workerId = workerId;
            this.datacenterId = datacenterId;
        }

        /**
         * 获取 maxWorkerId
         */
        protected static long getMaxWorkerId(long datacenterId, long maxWorkerId) {
            StringBuilder mpid = new StringBuilder();
            mpid.append(datacenterId);
            String name = ManagementFactory.getRuntimeMXBean().getName();
            if (StringUtils.isNotEmpty(name)) {
                /*
                 * GET jvmPid
                 */
                mpid.append(name.split("@")[0]);
            }
            /*
             * MAC + PID 的 hashcode 获取16个低位
             */
            return (mpid.toString().hashCode() & 0xffff) % (maxWorkerId + 1);
        }

        /**
         * 数据标识id部分
         */
        protected static long getDatacenterId(long maxDatacenterId) {
            long id = 0L;
            try {
                InetAddress ip = InetAddress.getLocalHost();
                NetworkInterface network = NetworkInterface.getByInetAddress(ip);
                if (network == null) {
                    id = 1L;
                }
                else {
                    byte[] mac = network.getHardwareAddress();
                    if (null != mac) {
                        id = ((0x000000FF & (long) mac[mac.length - 1]) |
                                (0x0000FF00 & (((long) mac[mac.length - 2]) << 8))) >> 6;
                        id = id % (maxDatacenterId + 1);
                    }
                }
            }
            catch (Exception e) {
                logger.warn(" getDatacenterId: " + e.getMessage());
            }
            return id;
        }

        /**
         * 获取下一个 ID
         *
         * @return 下一个 ID
         */
        public synchronized long nextId() {
            long timestamp = timeGen();
            //闰秒
            if (timestamp < lastTimestamp) {
                long offset = lastTimestamp - timestamp;
                if (offset <= 5) {
                    try {
                        wait(offset << 1);
                        timestamp = timeGen();
                        if (timestamp < lastTimestamp) {
                            throw new RuntimeException(String.format("Clock moved backwards.  Refusing to generate id for %d milliseconds",
                                    offset));
                        }
                    }
                    catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
                else {
                    throw new RuntimeException(
                            String.format("Clock moved backwards.  Refusing to generate id for %d milliseconds", offset));
                }
            }

            if (lastTimestamp == timestamp) {
                // 相同毫秒内，序列号自增
                sequence = (sequence + 1) & sequenceMask;
                if (sequence == 0) {
                    // 同一毫秒的序列数已经达到最大
                    timestamp = tilNextMillis(lastTimestamp);
                }
            }
            else {
                // 不同毫秒内，序列号置为 1 - 3 随机数
                sequence = ThreadLocalRandom.current().nextLong(1, 3);
            }

            lastTimestamp = timestamp;

            // 时间戳部分 | 数据中心部分 | 机器标识部分 | 序列号部分
            return ((timestamp - twepoch) << timestampLeftShift) |
                    (datacenterId << datacenterIdShift) |
                    (workerId << workerIdShift) |
                    sequence;
        }

        protected long tilNextMillis(long lastTimestamp) {
            long timestamp = timeGen();
            while (timestamp <= lastTimestamp) {
                timestamp = timeGen();
            }
            return timestamp;
        }

        protected long timeGen() {
            return SystemClock.now();
        }

        public String decode(long id) {
            long date = (id >> timestampLeftShift) + twepoch;
            String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date(date));

            return time;
        }
    }

    private static class SystemClock {

        private final long period;
        private final AtomicLong now;

        public static final SystemClock INSTANCE = new SystemClock(1);

        private SystemClock(long period) {
            this.period = period;
            this.now = new AtomicLong(System.currentTimeMillis());
            scheduleClockUpdating();
        }

        public static long now() {
            return INSTANCE.currentTimeMillis();
        }

        private void scheduleClockUpdating() {
            ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor(runnable -> {
                Thread thread = new Thread(runnable, "System Clock");
                thread.setDaemon(true);
                return thread;
            });
            scheduler.scheduleAtFixedRate(() -> now.set(System.currentTimeMillis()), period, period, TimeUnit.MILLISECONDS);
        }

        private long currentTimeMillis() {
            return now.get();
        }
    }

}
