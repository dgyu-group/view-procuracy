package com.iflytek.jzcpx.procuracy.card.pojo;

import lombok.Data;

/**
 * 入卷文书
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019/8/20 14:31
 */
@Data
public class InFolderFile {
	/** 文书模板编号 */
    private String wsmbbh;
  
    /** 文书实例编号 */
    private String wsslbh;
    
    /** 关联自然人编号 */
    private String glzrrbh;
    
    /** 文书名称*/
    private String wsmc;
    
    /** 文号*/
    private String wh;
}
