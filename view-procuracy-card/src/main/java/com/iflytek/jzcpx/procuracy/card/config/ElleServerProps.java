package com.iflytek.jzcpx.procuracy.card.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-07 09:48
 */
@ConfigurationProperties(prefix = "skynet.elle")
@Data
public class ElleServerProps {

    private String server;

    private String path;

}
