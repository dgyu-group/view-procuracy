package com.iflytek.jzcpx.procuracy.card.pojo;

import java.util.List;

import lombok.Data;

/**
 * 入卷文书案卡提取参数
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019/8/20 14:31
 */
@Data
public class ElleInFolderParam {
	/** 部门受案号 */
    private String bmsah;
  
    /** 入卷人编号 */
    private String rjybh;
    
    /** 入卷人姓名 */
    private String rjyxm;
    
    /** 文件列表*/
    private List<InFolderFile> wslb;
}
