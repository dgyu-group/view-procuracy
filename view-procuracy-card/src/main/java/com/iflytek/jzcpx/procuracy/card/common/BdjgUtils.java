package com.iflytek.jzcpx.procuracy.card.common;

import com.alibaba.fastjson.JSONObject;

/**
 * 表单结构工具类
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019/10/15 14:51
 */
public final class BdjgUtils {
    // TODO 2019/10/15 表单操作抽取
    private static final String stbm = "stbm";
    private static final String cbsj = "cbsj";
    private static final String zdsj = "zdsj";
    private static final String zdmc = "zdmc";
    private static final String sjbs = "sjbs";
    private static final String zdlx = "zdlx";
    private static final String bghzdwbz = "bghzdwbz";
    private static final String bghzdbmz = "bghzdbmz";
    private static final String bgqzdwbz = "bgqzdwbz";
    private static final String bgqzdbmz = "bgqzdbmz";

    /**
     * 标识表单数据为新增
     * @param bdjg
     */
    public void markAdd(JSONObject bdjg) {
        // 01新增 11修改 12删除
        bdjg.put("sjbs", SJBS.ADD.getMark());
    }

    /** 数据标识 */
    private enum SJBS {
        /** 新增 */
        ADD("01"),
        /** 修改 */
        MODIFY("11"),
        /** 删除 */
        DELETE("12"),
        ;

        private String mark;
        SJBS(String mark) {
            this.mark = mark;
        }

        public String getMark() {
            return mark;
        }
    }

}
