package com.iflytek.jzcpx.procuracy.card.pojo;

import lombok.Data;

/**
 * 要素抽取前期参数
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019/8/20 14:31
 */
@Data
public class ElleOuterInHandleParam {
	/** 部门受案号 */
    private String bmsah;
  
    /** 卷宗文件号 */
    private String jzwjh;
   
}
