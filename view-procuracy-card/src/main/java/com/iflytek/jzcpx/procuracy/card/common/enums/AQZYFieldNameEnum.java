package com.iflytek.jzcpx.procuracy.card.common.enums;

/**
 * 案情摘要表单字段枚举
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-29 17:22
 */
public enum AQZYFieldNameEnum {
    AQZY("案情摘要"),

    ;

    /** 字段说明 */
    private String desc;

    private AQZYFieldNameEnum(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }}

