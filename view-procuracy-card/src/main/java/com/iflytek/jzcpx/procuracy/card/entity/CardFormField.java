package com.iflytek.jzcpx.procuracy.card.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 案卡表单字段表
 * 
 * @author 
 * @date 2019/08/27
 */
@Data
@TableName("t_card_form_field")
public class CardFormField implements Serializable {
    private static final long serialVersionUID = 1;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;

    /**
     * 案卡文书id
     */
    @TableField("card_file_id")
    private Long cardFileId;

    /**
     * 实体表名
     */
    @TableField("stbm")
    private String stbm;

    /**
     * 字段名称
     */
    @TableField("zdmc")
    private String zdmc;

    /**
     * 字段类型
     */
    @TableField("zdlx")
    private String zdlx;

    /**
     * 字段编码值
     */
    @TableField("zdbmz")
    private String zdbmz;

    /**
     * 数据主键值
     */
    @TableField("primary_key")
    private String primaryKey;

    /**
     * 字段文本值
     */
    @TableField("zdwbz")
    private String zdwbz;

    /**
     * 表单结构 id
     */
    @TableField("bdjg_id")
    private String bdjgId;

    /**
     * 预留字段
     */
    @TableField("remark")
    private String remark;
    
    /**
     * 从表主键值
     */
    @TableField("primary_cb_key")
    private String primaryCbkey;
    
}