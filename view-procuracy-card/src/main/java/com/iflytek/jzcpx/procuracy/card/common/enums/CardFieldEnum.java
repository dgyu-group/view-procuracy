package com.iflytek.jzcpx.procuracy.card.common.enums;

import java.util.Collection;
import java.util.function.Function;

import com.google.common.collect.Lists;
import com.iflytek.jzcpx.procuracy.card.common.CardConstants;
import com.iflytek.jzcpx.procuracy.card.component.ElleFieldExtractFunctionFactory;
import com.iflytek.jzcpx.procuracy.card.component.FieldConvertors;
import com.iflytek.jzcpx.procuracy.card.config.ZdbmConfig;
import com.iflytek.jzcpx.procuracy.card.entity.CardFormField;
import com.iflytek.jzcpx.procuracy.card.pojo.ElleResultData;
import com.iflytek.jzcpx.procuracy.tools.elle.ElleTextTypeEnum;
import org.apache.commons.lang3.StringUtils;

import static com.iflytek.jzcpx.procuracy.card.common.CardConstants.SLRZ_TABLENAME;
import static com.iflytek.jzcpx.procuracy.card.common.CardConstants.XBS_AJ_TABLENAME;
import static com.iflytek.jzcpx.procuracy.card.common.CardConstants.XBS_XYR_TABLENAME;
import static com.iflytek.jzcpx.procuracy.card.common.CardConstants.YSGS_AJ_TABLENAME;
import static com.iflytek.jzcpx.procuracy.card.common.CardConstants.YSGS_XYR_TABLENAME;
import static com.iflytek.jzcpx.procuracy.tools.elle.ElleTextTypeEnum.bpzdbjds;
import static com.iflytek.jzcpx.procuracy.tools.elle.ElleTextTypeEnum.bqsjds;
import static com.iflytek.jzcpx.procuracy.tools.elle.ElleTextTypeEnum.pjs;
import static com.iflytek.jzcpx.procuracy.tools.elle.ElleTextTypeEnum.pzdbjds;
import static com.iflytek.jzcpx.procuracy.tools.elle.ElleTextTypeEnum.qss;
import static com.iflytek.jzcpx.procuracy.tools.elle.ElleTextTypeEnum.qsyjs;
import static com.iflytek.jzcpx.procuracy.tools.elle.ElleTextTypeEnum.tqpzdbs;

/**
 * 案卡其他表单字段
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019/9/26 15:17
 */
public enum CardFieldEnum {
	/* ----------------------------- 案情摘要 ----------------------------- */
	/** 案情摘要 - 案情摘要字段 */
	AQZY_AQZY(Lists.newArrayList(CardConstants.AQZY_TABLENAME), "AQZY", Integer.MAX_VALUE, Lists.newArrayList(qsyjs, tqpzdbs), ElleFieldExtractFunctionFactory.buildAQZYFunc()),

	/*
	 * ----------------------------- 案件(审查起诉表/审查逮捕案件表) -----------------------------
	 */
	/** 案件 - 案件名称 */
	AJ_AJMC(CardConstants.AJ_TABLENAME.values(),"AJMC", "案件名称", true, ElleFieldExtractFunctionFactory.builajmcFunc(), "通过计算嫌疑人+案由+犯罪人数得出", Lists.newArrayList(qsyjs, tqpzdbs)),

	/** 案件 - 移送案由 */
	AJ_YSAY(CardConstants.AJ_TABLENAME.values(),"YSAY", "移诉罪名", ElleFieldExtractFunctionFactory.buildysayFunc(), "通过嫌疑人移送罪名累加，需第一个为该人主要审结案由，剩下的为此人其他案由", Lists.newArrayList(qsyjs, tqpzdbs),"案由"),
	
	/** 案件 - 移送其他案由 */
	AJ_YSQTAY(CardConstants.AJ_TABLENAME.values(),"YSQTAY", "移诉其他罪名", ElleFieldExtractFunctionFactory.buildysqtayFunc(), "通过嫌疑人移送罪名累加，需第一个为该人主要审结案由，剩下的为此人其他案由", Lists.newArrayList(qsyjs, tqpzdbs),"案由"),
	
	/** 案件 - 移送意见 */
	AJ_YSYJ(CardConstants.AJ_TABLENAME.values(), "YSYJ", Lists.newArrayList(qsyjs, tqpzdbs), ElleFieldExtractFunctionFactory.buildYSYJFunc(), "移送意见"),

	/** 案件 - 移送单位 */
	AJ_YSDW(CardConstants.AJ_TABLENAME.values(), "YSDW", Lists.newArrayList(qsyjs, tqpzdbs), ElleFieldExtractFunctionFactory.buildYSDWFunc(),"机构/单位"),

	/** 案件 - 侦（调）查机关 */
	AJ_ZCJG(CardConstants.AJ_TABLENAME.values(), "ZCJG", Lists.newArrayList(qsyjs, tqpzdbs), ElleFieldExtractFunctionFactory.buildZCJGFunc(), "机构/单位"),

	/** 审查起诉 - (不)起诉意见书文号 */
	AJ_YSWSWH(CardConstants.AJ_TABLENAME.values(), "YSWSWH", Lists.newArrayList(qsyjs, tqpzdbs), ElleFieldExtractFunctionFactory.buildWHFunc()),

	/** 审查起诉 - 审结日期 */
	AJ_SJRQ(CardConstants.AJ_TABLENAME.values(), "SJRQ", Lists.newArrayList(pzdbjds, bpzdbjds), ElleFieldExtractFunctionFactory.buildSJRQFunc(), FieldConvertors.dateContactConvert(" 00:00:00")),

	/** 审查起诉 - 审结处理结果 */
	AJ_SJCLJG(Lists.newArrayList(XBS_AJ_TABLENAME), "SJCLJG", Lists.newArrayList(pzdbjds, bpzdbjds), ElleFieldExtractFunctionFactory.buildSJCLJGFunc(), "审结处理结果"),

	/** 审查起诉 - 审结情况 */
	AJ_SJQK(Lists.newArrayList(YSGS_AJ_TABLENAME), "SJQK", Lists.newArrayList(pzdbjds, bpzdbjds), ElleFieldExtractFunctionFactory.buildSJQKFunc(), "审结情况"),

	/** 审查起诉 - 一审判决日期 */
	AJ_YSPJRQ(Lists.newArrayList(YSGS_AJ_TABLENAME), "YSPJRQ", Lists.newArrayList(pjs), ElleFieldExtractFunctionFactory.buildYSPJRQFunc(), FieldConvertors.dateContactConvert(" 00:00:00")),

	/** 审查起诉 - 一审裁判书文号 */
	AJ_YSCPSWH(Lists.newArrayList(YSGS_AJ_TABLENAME), "YSCPSWH", Lists.newArrayList(pjs), ElleFieldExtractFunctionFactory.buildWHFunc()),

	/** 审查起诉 - 检察机关是否适用认罪认罚 */
	AJ_JCJGSFSYRZRF(Lists.newArrayList(YSGS_AJ_TABLENAME), "JCJGSFSYRZRF", Lists.newArrayList(qss, bqsjds), ElleFieldExtractFunctionFactory.buildJCJGSFSYRZRFFunc()),

	/** 审查起诉 - 认罪认罚适用程序 */
	AJ_RZRFSYCX(Lists.newArrayList(YSGS_AJ_TABLENAME), "RZRFSYCX", Lists.newArrayList(qss, bqsjds), ElleFieldExtractFunctionFactory.buildRZRFSYCXFunc(), "认罪认罚适用程序"),

	/** 审查起诉 - 案件造成伤亡情况 */
	AJ_AJZCSWQK(CardConstants.AJ_TABLENAME.values(), "AJZCSWQK", Lists.newArrayList(qss, bqsjds), ElleFieldExtractFunctionFactory.buildAJZCSWQKFunc(), "案件造成伤亡情况"),

	/** 审查起诉 - 移送法院日期 */
	AJ_YSFYRQ(Lists.newArrayList(YSGS_AJ_TABLENAME), "YSFYRQ", Lists.newArrayList(qss), ElleFieldExtractFunctionFactory.buildYSFYRQFunc(), FieldConvertors.dateContactConvert(" 00:00:00")),

	/** 审查起诉 - 移送何法院 */
	AJ_YSHFY(Lists.newArrayList(YSGS_AJ_TABLENAME), "YSHFY", Lists.newArrayList(qss), ElleFieldExtractFunctionFactory.buildYSHFYFunc()),

	/** 审查起诉 - 审结数额 */
	AJ_SJSE(Lists.newArrayList(YSGS_AJ_TABLENAME), "SJSE", Lists.newArrayList(qss, bqsjds), ElleFieldExtractFunctionFactory.buildSJSEFunc()),
	
	AJ_SFDWFZ(Lists.newArrayList(YSGS_AJ_TABLENAME), "SFDWFZ", "是否单位犯罪", true, ElleFieldExtractFunctionFactory.buildsfdwfzFunc(), "通过嫌疑人是否为单位获取", Lists.newArrayList(qsyjs)),
	
	AJ_GTFZRS(CardConstants.AJ_TABLENAME.values(), "GTFZRS", "共同犯罪人数", true, ElleFieldExtractFunctionFactory.buildGTFZRSFunc(), "通过计算嫌疑人数量得出", Lists.newArrayList(qsyjs, tqpzdbs)),
	
	/** 侦查机关是否建议适用认罪认罚 **/
	AJ_ZCJGSFJYSYRZRF(Lists.newArrayList(YSGS_AJ_TABLENAME), "ZCJGSFJYSYRZRF", "侦(调)查机关建议是否建议适用认罪认罚", "N", ElleFieldExtractFunctionFactory.buildZCJGSFJYSYRZRFFunc(), Lists.newArrayList(qsyjs)),
	
	/** 共同犯罪性质 一审公诉案件 **/
	AJ_GGFZXZ(CardConstants.AJ_TABLENAME.values(),"GGFZXZ", "共同犯罪性质", true, ElleFieldExtractFunctionFactory.buildGTFZXZFunc(), FieldConvertors.gtfzxzConvert(), Lists.newArrayList(qsyjs),"共同犯罪性质"),
	
	/** 共同犯罪性质 审查逮捕案件 **/
	AJ_GTFZXZ(CardConstants.AJ_TABLENAME.values(),"GTFZXZ", "共同犯罪性质", true, ElleFieldExtractFunctionFactory.buildGTFZXZFunc(), FieldConvertors.gtfzxzConvert(), Lists.newArrayList(tqpzdbs),"共同犯罪性质"),
	
	/** 是否涉及恶势力犯罪 **/
	AJ_SFSJESLFZ(CardConstants.AJ_TABLENAME.values(), "SFSJESLFZ", "是否涉及恶势力犯罪", "N", ElleFieldExtractFunctionFactory.buildSFSJESLFZFunc(), FieldConvertors.whetherSfeslConvert(), Lists.newArrayList(qsyjs)),
    
	/** 犯罪案件是否涉及恶势力 **/
	AJ_FZAJSFSJESL(CardConstants.AJ_TABLENAME.values(), "FZAJSFSJESL", "犯罪案件是否涉及恶势力", "N", ElleFieldExtractFunctionFactory.buildSFSJESLFZFunc(), FieldConvertors.whetherSfeslConvert(), Lists.newArrayList(qsyjs)),
	/* ----------------------------- 嫌疑人 ----------------------------- */
	/** 嫌疑人 - (不)逮捕书文号 */
	XYR_DBSWH(Lists.newArrayList(XBS_XYR_TABLENAME), "DBSWH", Lists.newArrayList(qss, bqsjds), ElleFieldExtractFunctionFactory.buildWHFunc()),

	/** 嫌疑人 - (不)起诉书文号 */
	XYR_BQSSWH(Lists.newArrayList(YSGS_XYR_TABLENAME), "BQSSWH", Lists.newArrayList(qss, bqsjds), ElleFieldExtractFunctionFactory.buildWHFunc()),

	/** 嫌疑人 - 一审判决书文号 */
	XYR_YSPJSWH(Lists.newArrayList(YSGS_XYR_TABLENAME), "YSPJSWH", Lists.newArrayList(pjs), ElleFieldExtractFunctionFactory.buildWHFunc()),
	/** 嫌疑人 - 一审判决日期 */
	XYR_YSPJRQ(Lists.newArrayList(YSGS_XYR_TABLENAME), "YYSPJRQ", Lists.newArrayList(pjs), ElleFieldExtractFunctionFactory.buildYSPJRQFunc(), FieldConvertors.dateContactConvert(" 00:00:00")),

	/* ----------------------------- 受理日志 ----------------------------- */
	/** 受理日志 - 案件名称 */
	//ALRZ_AJMC(Lists.newArrayList(SLRZ_TABLENAME), "AJMC", Lists.newArrayList(qsyjs, tqpzdbs), ElleFieldExtractFunctionFactory.buildAJMCFunc()),
	ALRZ_AJMC(Lists.newArrayList(SLRZ_TABLENAME),"AJMC", "案件名称", true, ElleFieldExtractFunctionFactory.builajmcFunc(), "通过计算嫌疑人+案由+犯罪人数得出", Lists.newArrayList(qsyjs, tqpzdbs)),

	/** 受理日志 - 移送单位 */
	ALRZ_YSDW(Lists.newArrayList(SLRZ_TABLENAME), "YSDW", Lists.newArrayList(qsyjs, tqpzdbs), ElleFieldExtractFunctionFactory.buildYSDWFunc(), "机构/单位"),

	/** 受理日志 - 移送案由 */
	ALRZ_YSAY(Lists.newArrayList(SLRZ_TABLENAME),"YSAY", "移诉罪名", ElleFieldExtractFunctionFactory.buildysayFunc(), "通过嫌疑人移送罪名累加，需第一个为该人主要审结案由，剩下的为此人其他案由", Lists.newArrayList(qsyjs, tqpzdbs),"案由"),
	
	/** 受理日志 - 移送其他案由 */
	ALRZ_YSQTAY(Lists.newArrayList(SLRZ_TABLENAME),"YSQTAY", "移诉其他罪名", ElleFieldExtractFunctionFactory.buildysqtayFunc(), "通过嫌疑人移送罪名累加，需第一个为该人主要审结案由，剩下的为此人其他案由", Lists.newArrayList(qsyjs, tqpzdbs),"案由"),

	/** 受理日志 - 卷宗册数 */
	ALRZ_JZCS(Lists.newArrayList(SLRZ_TABLENAME), "JZCS", Lists.newArrayList(qsyjs, tqpzdbs), ElleFieldExtractFunctionFactory.buildJZCSFunc(), FieldConvertors.columnConvert()),

	/** 受理日志 - 光盘数量 */
	ALRZ_GPSL(Lists.newArrayList(SLRZ_TABLENAME), "GPSL", Lists.newArrayList(qsyjs, tqpzdbs), ElleFieldExtractFunctionFactory.buildGPSLFunc(), FieldConvertors.columnConvert()),

	ALRZ_SFDWFZ(Lists.newArrayList(SLRZ_TABLENAME), "SFDWFZ", "是否单位犯罪", true, ElleFieldExtractFunctionFactory.buildsfdwfzFunc(), "通过嫌疑人是否为单位获取", Lists.newArrayList(qsyjs)),
	
	/** 审查起诉 - (不)起诉意见书文号 */
	ALRZ_YSWSWH(Lists.newArrayList(SLRZ_TABLENAME), "YSWSWH", Lists.newArrayList(qsyjs,  tqpzdbs), ElleFieldExtractFunctionFactory.buildWHFunc()),
	
	/** 案件 - 侦（调）查机关 */
	ALRZ_ZCJG(Lists.newArrayList(SLRZ_TABLENAME), "ZCJG", Lists.newArrayList(qsyjs, tqpzdbs), ElleFieldExtractFunctionFactory.buildZCJGFunc(),
              "机构/单位"),
	
	;

	/** 实体表名 */
	private Collection<String> stbm;

	/** 字段名称 */
	private String zdmc;

	/** 字段中文名称 */
	private String zdzwmc;

	/** 类别名称 */
	private String lbmc;

	/** 字段来自哪种文书, 只有当文书是这种类型时, 这个字段才需要回填 */
	private Collection<ElleTextTypeEnum> supportElleTextType;

	/** 字段文本值长度限制, 需要对照赛威讯案卡数据结构赋值 */
	private int zdwbzLimitSize = 300;

	/** 用来获取引擎对应字段值的函数 */
	private Function<ElleResultData, ElleResultData.Item> elleFieldExtractFunc;

	/** 字段文本值格式转换方法 */
	private Function<String, String> convertFunc;

	/** 是否有映射函数 **/
	private boolean hasMapFunc = false;

	/** 映射方法描述 **/
	private String mapFuncNote;

	/** 默认值 **/
	private String defaultValue;

	/** 通过嫌疑人数据获取 */
	private Function<ElleResultData, ElleResultData.Item[]> xyrFieldExtractFunc;

	/** 用来获取引擎映射字段值的函数 */
	private Function<ElleResultData, String> mappingFunc;
	
	
	
	
	CardFieldEnum(Collection<String> stbm, String zdmc, String zdzwmc, Function<ElleResultData, ElleResultData.Item[]> xyrFieldExtractFunc, String mapFuncNote, Collection<ElleTextTypeEnum> supportElleTextType,String lbmc){
		this.stbm=stbm;
		this.zdmc=zdmc;
		this.xyrFieldExtractFunc=xyrFieldExtractFunc;
		this.mapFuncNote=mapFuncNote;
		this.supportElleTextType=supportElleTextType;
		this.lbmc=lbmc;
	}

	CardFieldEnum(Collection<String> stbm, String zdmc, Collection<ElleTextTypeEnum> supportElleTextType, Function<ElleResultData, ElleResultData.Item[]> xyrFieldExtractFunc, String lbmc, String mapFuncNote) {
		this.stbm = stbm;
		this.zdmc = zdmc;
		this.supportElleTextType = supportElleTextType;
		this.xyrFieldExtractFunc = xyrFieldExtractFunc;
		this.lbmc = lbmc;
		this.mapFuncNote = mapFuncNote;
	}

	CardFieldEnum(Collection<String> stbm, String zdmc, String zdzwmc, String defaultValue, Function<ElleResultData, ElleResultData.Item> elleFieldExtractFunc, Function<String, String> convertFunc, Collection<ElleTextTypeEnum> supportElleTextType) {
		this.stbm = stbm;
		this.zdmc = zdmc;
		this.zdzwmc = zdzwmc;
		this.elleFieldExtractFunc = elleFieldExtractFunc;
		this.defaultValue = defaultValue;
		this.supportElleTextType = supportElleTextType;
		this.convertFunc = convertFunc;
	}

	CardFieldEnum(Collection<String> stbm, String zdmc, String zdzwmc, boolean hasMapFunc, Function<ElleResultData, ElleResultData.Item> elleFieldExtractFunc, Function<ElleResultData, String> mappingFunc, Collection<ElleTextTypeEnum> supportElleTextType) {
		this.stbm = stbm;
		this.zdmc = zdmc;
		this.zdzwmc = zdzwmc;
		this.elleFieldExtractFunc = elleFieldExtractFunc;
		this.mappingFunc = mappingFunc;
		this.supportElleTextType = supportElleTextType;
		this.hasMapFunc=hasMapFunc;
	}
	
	CardFieldEnum(Collection<String> stbm, String zdmc, String zdzwmc, boolean hasMapFunc, Function<ElleResultData, ElleResultData.Item> elleFieldExtractFunc, Function<ElleResultData, String> mappingFunc, Collection<ElleTextTypeEnum> supportElleTextType
			,String lbmc) {
		this.stbm = stbm;
		this.zdmc = zdmc;
		this.zdzwmc = zdzwmc;
		this.elleFieldExtractFunc = elleFieldExtractFunc;
		this.mappingFunc = mappingFunc;
		this.supportElleTextType = supportElleTextType;
		this.hasMapFunc=hasMapFunc;
		this.lbmc=lbmc;
	}

	CardFieldEnum(Collection<String> stbm, String zdmc, String zdzwmc, String defaultValue, Function<ElleResultData, ElleResultData.Item> elleFieldExtractFunc, Collection<ElleTextTypeEnum> supportElleTextType) {
		this.stbm = stbm;
		this.zdmc = zdmc;
		this.zdzwmc = zdzwmc;
		this.elleFieldExtractFunc = elleFieldExtractFunc;
		this.defaultValue = defaultValue;
		this.supportElleTextType = supportElleTextType;
	}

	CardFieldEnum(Collection<String> stbm, String zdmc, String zdzwmc, boolean hasMapFunc, Function<ElleResultData, String> mappingFunc, String mapFuncNote, Collection<ElleTextTypeEnum> supportElleTextType) {
		this.stbm = stbm;
		this.zdmc = zdmc;
		this.zdzwmc = zdzwmc;
		this.hasMapFunc = hasMapFunc;
		this.mappingFunc = mappingFunc;
		this.mapFuncNote = mapFuncNote;
		this.supportElleTextType = supportElleTextType;
	}
	
	CardFieldEnum(Collection<String> stbm, String zdmc, String zdzwmc, boolean hasMapFunc, Function<ElleResultData, String> mappingFunc, String mapFuncNote, Collection<ElleTextTypeEnum> supportElleTextType,String lbmc) {
		this.stbm = stbm;
		this.zdmc = zdmc;
		this.zdzwmc = zdzwmc;
		this.hasMapFunc = hasMapFunc;
		this.mappingFunc = mappingFunc;
		this.mapFuncNote = mapFuncNote;
		this.supportElleTextType = supportElleTextType;
		this.lbmc=lbmc;
	}

	CardFieldEnum(Collection<String> stbm, String zdmc, Collection<ElleTextTypeEnum> supportElleTextType, Function<ElleResultData, ElleResultData.Item> elleFieldExtractFunc, Function<String, String> convertFunc) {
		this.stbm = stbm;
		this.zdmc = zdmc;
		this.supportElleTextType = supportElleTextType;
		this.elleFieldExtractFunc = elleFieldExtractFunc;
		this.convertFunc = convertFunc;
	}

	CardFieldEnum(Collection<String> stbm, String zdmc, Integer zdwbzLimitSize, Collection<ElleTextTypeEnum> supportElleTextType, Function<ElleResultData, ElleResultData.Item> elleFieldExtractFunc) {
		this.stbm = stbm;
		this.zdmc = zdmc;
		this.zdwbzLimitSize = zdwbzLimitSize;
		this.supportElleTextType = supportElleTextType;
		this.elleFieldExtractFunc = elleFieldExtractFunc;
	}

	CardFieldEnum(Collection<String> stbm, String zdmc, Collection<ElleTextTypeEnum> supportElleTextType, Function<ElleResultData, ElleResultData.Item> elleFieldExtractFunc) {
		this.stbm = stbm;
		this.zdmc = zdmc;
		this.supportElleTextType = supportElleTextType;
		this.elleFieldExtractFunc = elleFieldExtractFunc;
	}

	CardFieldEnum(Collection<String> stbm, String zdmc, Collection<ElleTextTypeEnum> supportElleTextType, Function<ElleResultData, ElleResultData.Item> elleFieldExtractFunc, String lbmc) {
		this.stbm = stbm;
		this.zdmc = zdmc;
		this.supportElleTextType = supportElleTextType;
		this.elleFieldExtractFunc = elleFieldExtractFunc;
		this.lbmc = lbmc;
	}

	public Collection<String> getStbm() {
		return stbm;
	}

	public String getZdmc() {
		return zdmc;
	}

	public Collection<ElleTextTypeEnum> getSupportElleTextType() {
		return supportElleTextType;
	}

	public Function<ElleResultData, ElleResultData.Item> getElleFieldExtractor() {
		return elleFieldExtractFunc;
	}

	/** 字段文本值转换处理 */
	public String convertZdwbz(String input) {
		if (null != convertFunc) {
			String zdwbz = convertFunc.apply(input);
			// 截断, 防止文本超长
			return StringUtils.substring(zdwbz, 0, zdwbzLimitSize);
		}
		return input;
	}

	/** 字段编码值转换处理, 在回填到表单结构时调用 */
	public String convertZdbmzForBdjg(String zdwbz, ZdbmConfig zdbmConfig) {
		// 解析字段编码值，如果字段没有配置编码值，则把文本值填入编码值字段
		// 如果有配置编码值，且能解析出对应编码值则填入编码值字段，否则不填
		if (StringUtils.isBlank(zdwbz) || zdbmConfig == null || StringUtils.isBlank(this.lbmc)) {
			return zdwbz;
		}

		return zdbmConfig.getZdbm(this.lbmc, zdwbz);
	}

	/** 根据已保存的字段数据解析字段编码值 */
	public static String convertZdbmzForBdjg(CardFormField field) {
		if (field == null) {
			return "";
		}

		String zdwbz = field.getZdwbz();
		String zdbmz = field.getZdbmz();
		String stbm = field.getStbm();
		String zdmc = field.getZdmc();
		CardFieldEnum fieldEnum = parseByStbmAndZdmc(stbm, zdmc);
		if (fieldEnum != null) {
			// 字段有类别名称配置, 说明字段有编码值配置, 因此需要返回字段编码值, 否则将字段文本值作为编码值返回
			return StringUtils.isNotBlank(fieldEnum.getLbmc()) ? zdbmz : zdwbz;
		} else {
			return StringUtils.isNotBlank(zdbmz) ? zdbmz : zdwbz;
		}
	}

	private static CardFieldEnum parseByStbmAndZdmc(String stbm, String zdmc) {
		if (StringUtils.isAnyBlank(stbm, zdmc)) {
			return null;
		}
		for (CardFieldEnum fieldEnum : values()) {
			if (fieldEnum.getStbm().contains(stbm) && fieldEnum.getZdmc().equals(zdmc)) {
				return fieldEnum;
			}
		}
		return null;
	}

	private String getLbmc() {
		return this.lbmc;
	}

	/** 字段编码值转换处理, 在保存字段到数据库时调用 */
	public String convertZdbmzForStore(String zdwbz, ZdbmConfig zdbmConfig) {
		// 解析字段编码值，如果字段没有配置编码值，则把文本值填入编码值字段
		// 但为了防止字段文本值超长导致存储失败, 因此使用空字符而不是文本值保存到编码值字段
		if (StringUtils.isBlank(zdwbz) || zdbmConfig == null || StringUtils.isBlank(this.lbmc)) {
			return "";
		}

		return zdbmConfig.getZdbm(this.lbmc, zdwbz);
	}

	public String getZdzwmc() {
		return zdzwmc;
	}

	public boolean isHasMapFunc() {
		return hasMapFunc;
	}

	public String getMapFuncNote() {
		return mapFuncNote;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public Function<ElleResultData, ElleResultData.Item[]> getXyrFieldExtractFunc() {
		return xyrFieldExtractFunc;
	}

	public Function<ElleResultData, String> getMappingFunc() {
		return mappingFunc;
	}

	public Function<String, String> getConvertFunc() {
		return convertFunc;
	}

	public int getZdwbzLimitSize() {
		return zdwbzLimitSize;
	}

	public Function<ElleResultData, ElleResultData.Item> getElleFieldExtractFunc() {
		return elleFieldExtractFunc;
	}
}
