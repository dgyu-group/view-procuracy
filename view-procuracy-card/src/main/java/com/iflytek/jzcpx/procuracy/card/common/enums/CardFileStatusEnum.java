package com.iflytek.jzcpx.procuracy.card.common.enums;

/**
 * 案卡任务状态枚举
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-29 09:47
 */
public enum CardFileStatusEnum {

    INIT,

    GOT_FILE_STREAM,

    /** 请求ocr引擎结束 */
    REQUESTED_OCR_ENGINE,

    /** ocr引擎结果存储完成 */
    OCR_ENGINE_RESULT_STORED,

    /** 请求elle引擎结束 */
    REQUESTED_ELLE_ENGINE,

    /** elle引擎结果存储完成 */
    ELLE_ENGINE_RESULT_STORED,

    /** 结束 */
    END,

}

