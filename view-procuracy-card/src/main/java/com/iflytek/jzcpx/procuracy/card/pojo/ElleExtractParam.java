package com.iflytek.jzcpx.procuracy.card.pojo;

import lombok.Data;

/**
 * 要素抽取前期参数
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019/8/20 14:31
 */
@Data
public class ElleExtractParam {
    /** 文件编码（标识文件唯一值）*/
    private String wjbm;

    /** 文书类型 */
    private String wslx;

    /** 文件类型 */
    private String wjlx;

    /** 表单结构 */
    private String bdjg;

    /** 案件类别编码 */
    private String ajlbbm;

}
