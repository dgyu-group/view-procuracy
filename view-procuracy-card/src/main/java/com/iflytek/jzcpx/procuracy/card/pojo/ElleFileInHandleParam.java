package com.iflytek.jzcpx.procuracy.card.pojo;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

/**
 * 办理阶段-获取办理文书案卡提取结果请求参数
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019/8/20 14:31
 */
@Data
public class ElleFileInHandleParam {
    /** 部门受案号 */
    private String bmsah;

    /** 案件类别编码 */
    private String ajlbbm;

    /** 文书实例编号 */
    private String wsslbh;

    /** 文书模板编码 */
    private String wsmbbm;

    /** 文书名称 */
    private String wsmc;

    /** 文件扩展名 */
    private String wjkzm;

    /** 承办检察官编码 */
    private String cbjcgbm;

    /** 表单结构 */
    private JSONObject bdjg;

    /** 文件流 */
    private byte[] wswj;

}
