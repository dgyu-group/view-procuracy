package com.iflytek.jzcpx.procuracy.card.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 案卡文件表
 * 
 * @author 
 * @date 2019/08/27
 */
@Data
@TableName("t_card_file")
public class CardFile implements Serializable {
    private static final long serialVersionUID = 1;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;

    /**
     * 部门受案号
     */
    @TableField("bmsah")
    private String bmsah;

    /**
     * 文书所在阶段
     */
    @TableField("process")
    private String process;

    /**
     * 文书模板编码
     */
    @TableField("wsmbbm")
    private String wsmbbm;

    /**
     * 文书实例编号
     */
    @TableField("wsslbh")
    private String wsslbh;

    /**
     * 文件名称
     */
    @TableField("wsmc")
    private String wsmc;

    /**
     * 文件编码
     */
    @TableField("wjbm")
    private String wjbm;

    /**
     * 卷宗文件号
     */
    @TableField("jzwjh")
    private String jzwjh;

    /**
     * 文书类型(起诉书,提取逮捕书)
     */
    @TableField("type")
    private String type;

    /**
     * 状态
     */
    @TableField("status")
    private String status;

    /**
     * 来源(外来,内部)
     */
    @TableField("source")
    private String source;

    /**
     * 错误信息
     */
    @TableField("fail_info")
    private String failInfo;

    /**
     * ocr识别结果存储路径
     */
    @TableField("ocr_result_path")
    private String ocrResultPath;

    /**
     * 要素抽取引擎结果存储路径
     */
    @TableField("elle_result_path")
    private String elleResultPath;

    /**
     * 文档文本内容
     */
    @TableField("file_content")
    private String fileContent;

    /**
     * 预留字段
     */
    @TableField("remark")
    private String remark;
}