package com.iflytek.jzcpx.procuracy.card.service;

import com.alibaba.fastjson.JSONObject;
import com.iflytek.jzcpx.procuracy.card.pojo.ElleFileInHandleParam;
import com.iflytek.jzcpx.procuracy.card.pojo.ElleFullParam;
import com.iflytek.jzcpx.procuracy.card.pojo.ElleImgInAcceptParam;
import com.iflytek.jzcpx.procuracy.card.pojo.ElleInFolderParam;
import com.iflytek.jzcpx.procuracy.card.pojo.ElleOuterInHandleParam;
import com.iflytek.jzcpx.procuracy.card.pojo.ElleOuterSyncInHandleParam;
import com.iflytek.jzcpx.procuracy.card.pojo.ElleWordInAcceptParam;
import com.iflytek.jzcpx.procuracy.common.result.Result;

/**
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-06 19:46
 */
public interface CardService {

    /**
     * 受理文书案卡提取 - 高拍文件
     *
     * @param param
     * @return
     */
    Result<JSONObject> extractFromImgInAccept(ElleImgInAcceptParam param);

    /**
     * 受理文书案卡提取 - word文件
     *
     * @param param
     * @return
     */
    Result<JSONObject> extractFromWordInAccept(ElleWordInAcceptParam param);

    /**
     * 办理文书案卡提取
     *
     * @param elleFileInHandleParam
     * @return
     */
    Result<JSONObject> extractFileInHandle(ElleFileInHandleParam elleFileInHandleParam);

    /**
     * 入卷文书案卡提取
     *
     * @param elleInFolderParam
     * @return
     */
    Result inFolder(ElleInFolderParam elleInFolderParam);

    /**
     * 外来文书案卡数据提取（同步）
     *
     * @param elleOuterSyncInHandleParam
     * @return
     */
    Result<JSONObject> handleOuterSync(ElleOuterSyncInHandleParam elleOuterSyncInHandleParam);

    /**
     * 外来文书案卡数据提取（异步）
     *
     * @param elleOuterInHandleParam
     * @return
     */
    Result handleOuterAsync(ElleOuterInHandleParam elleOuterInHandleParam);

    /**
     * 全案卡数据提取
     *
     * @param elleFullParam
     * @return
     */
    Result<JSONObject> handleFull(ElleFullParam elleFullParam);
}
