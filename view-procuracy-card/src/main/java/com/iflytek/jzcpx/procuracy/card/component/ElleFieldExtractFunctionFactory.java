package com.iflytek.jzcpx.procuracy.card.component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.iflytek.jzcpx.procuracy.card.common.CardConstants;
import com.iflytek.jzcpx.procuracy.card.common.enums.XYRFieldNameEnum;
import com.iflytek.jzcpx.procuracy.card.pojo.ElleResultData;
import com.iflytek.jzcpx.procuracy.card.pojo.ElleResultData.Suspect;
import com.iflytek.jzcpx.procuracy.tools.elle.ElleTextTypeEnum;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * elle 引擎结果字段抽取方法
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019/10/14 11:08
 */
public class ElleFieldExtractFunctionFactory {

	// 刑事拘留：执行机关或批准机关（公安），引擎返回个值，另一个补取这个值；决定日期与执行日期，引擎返回1个，另一个值补取。
	// 逮捕：引擎返回批准机关（检察院），执行机关补取侦（调）查机关；决定日期与执行日期，引擎返回1个，另一个值补取。
	public static BiFunction<ElleResultData.CompulsoryMeasureCondition, ElleResultData, ElleResultData.Item> builExecutiveOrganFunc() {
		return new BiFunction<ElleResultData.CompulsoryMeasureCondition, ElleResultData, ElleResultData.Item>() {
			@Override
			public ElleResultData.Item apply(ElleResultData.CompulsoryMeasureCondition compulsoryMeasure,
					ElleResultData elleResultData) {
				if (null == compulsoryMeasure) {
					return null;
				}
				ElleResultData.Item compulsoryMeasureItem = compulsoryMeasure.getCompulsoryMeasure();
				ElleResultData.Item executiveOrganItem = compulsoryMeasure.getExecutiveOrgan();
				if (null != compulsoryMeasureItem) {
					String qzcsMean = compulsoryMeasureItem.getMean();
					if (StringUtils.isNotEmpty(qzcsMean)) {
						if (CardConstants.QZCS_XSJL.equals(qzcsMean)) {
							if (null == executiveOrganItem) {
								ElleResultData.Item approvalAuthorityItem = compulsoryMeasure.getApprovalAuthority();
								if (null == approvalAuthorityItem) {
									return elleResultData.getExtractInfoVec().get(0).getExtractInfo()
											.getInvestigationOrgan();
								}
								return approvalAuthorityItem;
							}
							return elleResultData.getExtractInfoVec().get(0).getExtractInfo().getInvestigationOrgan();
						} else if (CardConstants.QZCS_DB.equals(qzcsMean)) {
							return elleResultData.getExtractInfoVec().get(0).getExtractInfo().getInvestigationOrgan();
						}
					}
					if (null != executiveOrganItem && (executiveOrganItem.getMean().contains("我局")
							|| executiveOrganItem.getMean().contains("我分局")
							|| executiveOrganItem.getMean().contains("本局"))) {
						return elleResultData.getExtractInfoVec().get(0).getExtractInfo().getInvestigationOrgan();
					}
					if (null == executiveOrganItem && null != elleResultData.getExtractInfoVec().get(0).getExtractInfo()
							.getInvestigationOrgan()) {
						return elleResultData.getExtractInfoVec().get(0).getExtractInfo().getInvestigationOrgan();
					}
					return executiveOrganItem;
				}
				return executiveOrganItem;
			}
		};
	}

	// 刑事拘留：执行机关或批准机关（公安），引擎返回个值，另一个补取这个值；决定日期与执行日期，引擎返回1个，另一个值补取。
	// 逮捕：引擎返回批准机关（检察院），执行机关补取侦（调）查机关；决定日期与执行日期，引擎返回1个，另一个值补取。
	public static BiFunction<ElleResultData.CompulsoryMeasureCondition, ElleResultData, ElleResultData.Item> builApprovalAuthorityFunc() {
		return new BiFunction<ElleResultData.CompulsoryMeasureCondition, ElleResultData, ElleResultData.Item>() {
			@Override
			public ElleResultData.Item apply(ElleResultData.CompulsoryMeasureCondition compulsoryMeasure,
					ElleResultData elleResultData) {
				if (null == compulsoryMeasure) {
					return null;
				}
				ElleResultData.Item compulsoryMeasureItem = compulsoryMeasure.getCompulsoryMeasure();
				ElleResultData.Item approvalAuthorityItem = compulsoryMeasure.getApprovalAuthority();
				if (null != compulsoryMeasureItem) {
					if (null == approvalAuthorityItem && null != elleResultData.getExtractInfoVec().get(0)
							.getExtractInfo().getInvestigationOrgan()) {
						return elleResultData.getExtractInfoVec().get(0).getExtractInfo().getInvestigationOrgan();
					}
					String qzcsMean = compulsoryMeasureItem.getMean();
					if (StringUtils.isNotEmpty(qzcsMean)) {
						if (CardConstants.QZCS_XSJL.equals(qzcsMean)) {
							return elleResultData.getExtractInfoVec().get(0).getExtractInfo().getInvestigationOrgan();
						}
					}
					if (null != approvalAuthorityItem && (approvalAuthorityItem.getMean().contains("我局")
							|| approvalAuthorityItem.getMean().contains("我分局")
							|| approvalAuthorityItem.getMean().contains("本局"))) {
						return elleResultData.getExtractInfoVec().get(0).getExtractInfo().getInvestigationOrgan();
					}
					return approvalAuthorityItem;
				}
				return approvalAuthorityItem;
			}
		};
	}

	/** 案情摘要 抽取方法 */
	public static Function<ElleResultData, ElleResultData.Item> buildAQZYFunc() {
		return new Function<ElleResultData, ElleResultData.Item>() {

			@Override
			public ElleResultData.Item apply(ElleResultData elleResultData) {
				ElleResultData.Item item = elleResultData.getExtractInfoVec().get(0).getExtractInfo().getCaseX();
				if (null != item) {
					item.setMean(item.getOrg_text());
					return item;
				}
				return null;
			}
		};
	}

	/** 批准逮捕决定书文号/不批准逮捕决定书文号/起诉书文号/不起诉决定书文号/判决书文号 抽取方法 */
	public static Function<ElleResultData, ElleResultData.Item> buildWHFunc() {
		return new Function<ElleResultData, ElleResultData.Item>() {

			@Override
			public ElleResultData.Item apply(ElleResultData elleResultData) {
				return elleResultData.getExtractInfoVec().get(0).getExtractInfo().getCaseNumber();
			}
		};
	}

	/** 审结日期 抽取方法 */
	public static Function<ElleResultData, ElleResultData.Item> buildSJRQFunc() {
		return new Function<ElleResultData, ElleResultData.Item>() {

			@Override
			public ElleResultData.Item apply(ElleResultData elleResultData) {
				return elleResultData.getExtractInfoVec().get(0).getExtractInfo().getConcludedDate();
			}
		};
	}

	/** 一审判决日期 抽取方法 */
	public static Function<ElleResultData, ElleResultData.Item> buildYSPJRQFunc() {
		return new Function<ElleResultData, ElleResultData.Item>() {

			@Override
			public ElleResultData.Item apply(ElleResultData elleResultData) {
				return elleResultData.getExtractInfoVec().get(0).getExtractInfo().getJudgeDateOfFI();
			}
		};
	}

	/** 案件 审结处理结果 抽取方法 */
	public static Function<ElleResultData, ElleResultData.Item> buildSJCLJGFunc() {
		return new Function<ElleResultData, ElleResultData.Item>() {

			@Override
			public ElleResultData.Item apply(ElleResultData elleResultData) {
				// 引擎抽取结果中, 每个嫌疑人下都有审结处理结果
				ElleResultData.ExtractInfo extractInfo = elleResultData.getExtractInfoVec().get(0).getExtractInfo();
				List<Suspect> suspects = extractInfo.getSuspect();
				// 审结处理结果 只要有一个嫌疑人信息是逮捕 则案件审结处理结果就是逮捕
				if (CollectionUtils.isEmpty(suspects)) {
					return null;
				}
				for (Suspect suspect : suspects) {
					ElleResultData.Label label = suspect.getLabel();
					ElleResultData.Item[] items = XYRFieldNameEnum.SJCLJG.getElleFieldExtractor().apply(label);
					if (ArrayUtils.isNotEmpty(items)) {
						for (ElleResultData.Item item : items) {
							String result = FieldConvertors.whetherConvert().apply(item.getMean());
							if ("批准逮捕".equals(result)) {
								return item;
							}
						}
					}
				}
				for (Suspect suspect : suspects) {
					ElleResultData.Label label = suspect.getLabel();
					ElleResultData.Item[] items = XYRFieldNameEnum.SJCLJG.getElleFieldExtractor().apply(label);
					if (ArrayUtils.isNotEmpty(items)) {
						return items[0];
					}
				}
				return null;
			}
		};
	}

	private static ElleResultData.Suspect getSingleSuspect(ElleResultData elleResultData) {
		ElleResultData.ExtractInfo extractInfo = elleResultData.getExtractInfoVec().get(0).getExtractInfo();
		List<ElleResultData.Suspect> suspects = extractInfo.getSuspect();
		return CollectionUtils.size(suspects) == 1 ? suspects.get(0) : null;
	}

	/** 案件 检察机关是否适用认罪认罚 抽取方法 */
	public static Function<ElleResultData, ElleResultData.Item> buildJCJGSFSYRZRFFunc() {
		return new Function<ElleResultData, ElleResultData.Item>() {

			@Override
			public ElleResultData.Item apply(ElleResultData elleResultData) {
				// 引擎抽取结果中, 每个嫌疑人下都有检察机关是否适用认罪认罚,
				ElleResultData.ExtractInfo extractInfo = elleResultData.getExtractInfoVec().get(0).getExtractInfo();
				List<Suspect> suspects = extractInfo.getSuspect();
				// 检察机关适用认罪认罚这个字段，只要该案中有一个人的该字段为“是”，那么该案件的此字段就为 “是”
				if (CollectionUtils.isEmpty(suspects)) {
					return null;
				}
				for (Suspect suspect : suspects) {
					ElleResultData.Label label = suspect.getLabel();
					ElleResultData.Item[] items = XYRFieldNameEnum.JCJGSFSYRZRF.getElleFieldExtractor().apply(label);
					if (ArrayUtils.isNotEmpty(items)) {
						for (ElleResultData.Item item : items) {
							String result = FieldConvertors.whetherConvert().apply(item.getMean());
							if ("是".equals(result)) {
								return item;
							}
						}
					}
				}
				for (Suspect suspect : suspects) {
					ElleResultData.Label label = suspect.getLabel();
					ElleResultData.Item[] items = XYRFieldNameEnum.JCJGSFSYRZRF.getElleFieldExtractor().apply(label);
					if (ArrayUtils.isNotEmpty(items)) {
						return items[0];
					}
				}
				ElleResultData.Item item = new ElleResultData.Item();
				item.setMean(XYRFieldNameEnum.JCJGSFSYRZRF.getDefaultValue());
				return item;
			}
		};
	}

	/** 案件 认罪认罚适用程序 抽取方法 */
	public static Function<ElleResultData, ElleResultData.Item> buildRZRFSYCXFunc() {
		return new Function<ElleResultData, ElleResultData.Item>() {

			@Override
			public ElleResultData.Item apply(ElleResultData elleResultData) {
				return elleResultData.getExtractInfoVec().get(0).getExtractInfo().getApplicableProceduresOfGuiltyPlea();
			}
		};
	}

	/** 案件 案件造成伤亡情况 抽取方法 */
	public static Function<ElleResultData, ElleResultData.Item> buildAJZCSWQKFunc() {
		return new Function<ElleResultData, ElleResultData.Item>() {

			@Override
			public ElleResultData.Item apply(ElleResultData elleResultData) {
				ElleResultData.Suspect suspect = getSingleSuspect(elleResultData);
				return suspect == null ? null : getFirstItem(suspect.getLabel().getInjuriesAndDeaths());
			}
		};
	}

	/** 案件 审结情况 抽取方法 */
	public static Function<ElleResultData, ElleResultData.Item> buildSJQKFunc() {
		return new Function<ElleResultData, ElleResultData.Item>() {

			@Override
			public ElleResultData.Item apply(ElleResultData elleResultData) {
				ElleResultData.ExtractInfo extractInfo = elleResultData.getExtractInfoVec().get(0).getExtractInfo();
				List<Suspect> suspects = extractInfo.getSuspect();
				// 只要有一个嫌疑人 审结情况是“起诉” 则案件就是起诉
				if (CollectionUtils.isEmpty(suspects)) {
					return null;
				}
				for (Suspect suspect : suspects) {
					ElleResultData.Label label = suspect.getLabel();
					ElleResultData.Item[] items = XYRFieldNameEnum.SJCLQK.getElleFieldExtractor().apply(label);
					if (ArrayUtils.isNotEmpty(items)) {
						for (ElleResultData.Item item : items) {
							String result = item.getMean();
							if ("起诉".equals(result)) {
								return item;
							}
						}
					}
				}
				// 若取不到起诉 就取第一个出现嫌疑人审结情况作为返回
				for (Suspect suspect : suspects) {
					ElleResultData.Label label = suspect.getLabel();
					ElleResultData.Item[] items = XYRFieldNameEnum.SJCLQK.getElleFieldExtractor().apply(label);
					if (ArrayUtils.isNotEmpty(items)) {
						return items[0];
					}
				}
				ElleResultData.Suspect suspect = getSingleSuspect(elleResultData);
				return suspect == null ? null : getFirstItem(suspect.getLabel().getConcludedSituation());
			}
		};
	}

	private static ElleResultData.Item getFirstItem(ElleResultData.Item[] items) {
		return ArrayUtils.isEmpty(items) ? null : items[0];
	}

	/** 案件 移送法院日期 抽取方法 */
	public static Function<ElleResultData, ElleResultData.Item> buildYSFYRQFunc() {
		return new Function<ElleResultData, ElleResultData.Item>() {

			@Override
			public ElleResultData.Item apply(ElleResultData elleResultData) {
				List<ElleResultData.Suspect> suspects = elleResultData.getExtractInfoVec().get(0).getExtractInfo()
						.getSuspect();
				if (CollectionUtils.isEmpty(suspects)) {
					return null;
				}

				return getFirstItem(suspects.get(0).getLabel().getTransferDateToCourt());
			}
		};
	}

	/** 案件 移送何法院 抽取方法 */
	public static Function<ElleResultData, ElleResultData.Item> buildYSHFYFunc() {
		return new Function<ElleResultData, ElleResultData.Item>() {

			@Override
			public ElleResultData.Item apply(ElleResultData elleResultData) {
				return elleResultData.getExtractInfoVec().get(0).getExtractInfo().getCourtTransferredTo();
			}
		};
	}

	/** 案件 审结数额 抽取方法 */
	public static Function<ElleResultData, ElleResultData.Item> buildSJSEFunc() {
		return new Function<ElleResultData, ElleResultData.Item>() {

			@Override
			public ElleResultData.Item apply(ElleResultData elleResultData) {
				// 引擎抽取结果中, 每个嫌疑人下都有审结数额,
				// 所以当只有一个嫌疑人时才把该嫌疑人下的字段作为案件的字段返回
				ElleResultData.Suspect suspect = getSingleSuspect(elleResultData);
				return suspect == null ? null : getFirstItem(suspect.getLabel().getConcludedAmount());
			}
		};
	}

	/** 案件 案件名称 抽取方法 */
	public static Function<ElleResultData, ElleResultData.Item> buildAJMCFunc() {
		return new Function<ElleResultData, ElleResultData.Item>() {

			@Override
			public ElleResultData.Item apply(ElleResultData elleResultData) {
				return elleResultData.getExtractInfoVec().get(0).getExtractInfo().getCaseName();
			}
		};
	}

	/** 案件 移送意见 抽取方法 */
	public static Function<ElleResultData, ElleResultData.Item> buildYSYJFunc() {
		return new Function<ElleResultData, ElleResultData.Item>() {

			@Override
			public ElleResultData.Item apply(ElleResultData elleResultData) {
				return elleResultData.getExtractInfoVec().get(0).getExtractInfo().getTransferOpinion();
			}
		};
	}

	/** 案件 移送单位 抽取方法 */
	public static Function<ElleResultData, ElleResultData.Item> buildYSDWFunc() {
		return new Function<ElleResultData, ElleResultData.Item>() {

			@Override
			public ElleResultData.Item apply(ElleResultData elleResultData) {
				// return
				// elleResultData.getExtractInfoVec().get(0).getExtractInfo().getTransferUnit();
				return elleResultData.getExtractInfoVec().get(0).getExtractInfo().getInvestigationOrgan();
			}
		};
	}

	/** 案件 侦（调）查机关 抽取方法 */
	public static Function<ElleResultData, ElleResultData.Item> buildZCJGFunc() {
		return new Function<ElleResultData, ElleResultData.Item>() {

			@Override
			public ElleResultData.Item apply(ElleResultData elleResultData) {
				return elleResultData.getExtractInfoVec().get(0).getExtractInfo().getInvestigationOrgan();
			}
		};
	}

	/** 受理日志 受理日志 抽取方法 */
	public static Function<ElleResultData, ElleResultData.Item> buildJZCSFunc() {
		return new Function<ElleResultData, ElleResultData.Item>() {

			@Override
			public ElleResultData.Item apply(ElleResultData elleResultData) {
				ElleResultData.Item dossierVolumes = elleResultData.getExtractInfoVec().get(0).getExtractInfo()
						.getDossierVolumes();
				// todo 中文转阿拉伯数字

				return dossierVolumes;
			}
		};
	}

	/** 受理日志 光盘数量 抽取方法 */
	public static Function<ElleResultData, ElleResultData.Item> buildGPSLFunc() {
		return new Function<ElleResultData, ElleResultData.Item>() {

			@Override
			public ElleResultData.Item apply(ElleResultData elleResultData) {
				ElleResultData.Item cdNumber = elleResultData.getExtractInfoVec().get(0).getExtractInfo().getCDNumber();
				// todo 中文转阿拉伯数字

				return cdNumber;
			}
		};
	}

	/** 侦查机关是否建议适用认罪认罚 抽取方法 */
	public static Function<ElleResultData, ElleResultData.Item> buildZCJGSFJYSYRZRFFunc() {
		return new Function<ElleResultData, ElleResultData.Item>() {

			@Override
			public ElleResultData.Item apply(ElleResultData elleResultData) {
				List<Suspect> suspects = elleResultData.getExtractInfoVec().get(0).getExtractInfo().getSuspect();
				if (CollectionUtils.isEmpty(suspects)) {
					return null;
				}
				for (Suspect suspect : suspects) {
					ElleResultData.Label label = suspect.getLabel();
					ElleResultData.Item[] items = XYRFieldNameEnum.ZCJGSFJYSYRZRF.getElleFieldExtractor().apply(label);
					if (ArrayUtils.isNotEmpty(items)) {
						for (ElleResultData.Item zcjgsfjysyrzrfItem : items) {
							String mean = zcjgsfjysyrzrfItem.getMean();
							if (StringUtils.isNotBlank(mean) && mean.contains("认罪认罚")) {
								zcjgsfjysyrzrfItem.setMean("是");
							} else {
								zcjgsfjysyrzrfItem.setMean("否");
							}
							return zcjgsfjysyrzrfItem;
						}
					}
				}
				for (Suspect suspect : suspects) {
					ElleResultData.Label label = suspect.getLabel();
					ElleResultData.Item[] items = XYRFieldNameEnum.ZCJGSFJYSYRZRF.getElleFieldExtractor().apply(label);
					if (ArrayUtils.isNotEmpty(items)) {
						ElleResultData.Item item = items[0];
						item.setMean("否");
						return item;
					}
				}
				return null;
			}
		};
	}

	/** 共同犯罪性质 抽取方法 */
	public static Function<ElleResultData, ElleResultData.Item> buildGTFZXZFunc() {
		return new Function<ElleResultData, ElleResultData.Item>() {

			@Override
			public ElleResultData.Item apply(ElleResultData elleResultData) {
				ElleResultData.Item GTFZXZ = elleResultData.getExtractInfoVec().get(0).getExtractInfo()
						.getNatureOfJointCrime();
				return GTFZXZ;
			}
		};
	}

	/** 是否涉及恶势力犯罪 抽取方法 */
	public static Function<ElleResultData, ElleResultData.Item> buildSFSJESLFZFunc() {
		return new Function<ElleResultData, ElleResultData.Item>() {

			@Override
			public ElleResultData.Item apply(ElleResultData elleResultData) {
				ElleResultData.Item SFSJESLFZ = elleResultData.getExtractInfoVec().get(0).getExtractInfo()
						.getViciousPower();
				return SFSJESLFZ;
			}
		};
	}

	/** 共同犯罪人数 抽取方法 */
	public static Function<ElleResultData, String> buildGTFZRSFunc() {
		return new Function<ElleResultData, String>() {

			@Override
			public String apply(ElleResultData elleResultData) {
				Integer suspectCnt = 0;
				List<ElleResultData.Suspect> suspects = elleResultData.getExtractInfoVec().get(0).getExtractInfo()
						.getSuspect();
				if (CollectionUtils.isEmpty(suspects) || suspects.size() <= 1) {
					return "0";
				}
				suspectCnt = suspects.size();
				return String.valueOf(suspectCnt);
			}
		};
	}

	/** 是否单位犯罪 抽取方法 */
	public static Function<ElleResultData, String> buildsfdwfzFunc() {
		return new Function<ElleResultData, String>() {

			@Override
			public String apply(ElleResultData elleResultData) {
				String result = "N";
				List<ElleResultData.Suspect> suspects = elleResultData.getExtractInfoVec().get(0).getExtractInfo()
						.getSuspect();
				if (org.springframework.util.CollectionUtils.isEmpty(suspects)) {
					return result;
				}
				ElleResultData.Label label = suspects.get(0).getLabel();
				// 获取单位名称
				ElleResultData.Item[] dwmcItems = XYRFieldNameEnum.DWMC.getElleFieldExtractor().apply(label);
				if (ArrayUtils.isEmpty(dwmcItems)) {
					return result;
				}
				String dwmc = dwmcItems[0].getMean();
				if (StringUtils.isEmpty(dwmc)) {
					return result;
				}
				result = "Y";
				return result;
			}
		};
	}

	/** 移送案由（移诉罪名）： 抽取方法 通过嫌疑人移送罪名累加，需第一个为该人主要审结案由，剩下的为此人其他案由 */
	public static Function<ElleResultData, ElleResultData.Item[]> buildysayFunc() {
		return new Function<ElleResultData, ElleResultData.Item[]>() {

			@Override
			public ElleResultData.Item[] apply(ElleResultData elleResultData) {
				List<ElleResultData.Suspect> suspects = elleResultData.getExtractInfoVec().get(0).getExtractInfo()
						.getSuspect();
				if (org.springframework.util.CollectionUtils.isEmpty(suspects)) {
					return null;
				}
				for (int i = 0; i < suspects.size(); i++) {
					ElleResultData.Label label = suspects.get(i).getLabel();
					ElleResultData.Item[] ysayItems = XYRFieldNameEnum.YS.getElleFieldExtractor().apply(label);
					if (ArrayUtils.isNotEmpty(ysayItems)) {
						ElleResultData.Item[] temp = new ElleResultData.Item[1];
						System.arraycopy(ysayItems, 0, temp, 0, 1);
						return temp;
					}
				}
				return null;
			}
		};
	}

	/** 移送其他案由（移诉其他罪名）： 抽取方法 通过嫌疑人移送罪名累加，需第一个为该人主要审结案由，剩下的为此人其他案由 */
	public static Function<ElleResultData, ElleResultData.Item[]> buildysqtayFunc() {
		return new Function<ElleResultData, ElleResultData.Item[]>() {

			@Override
			public ElleResultData.Item[] apply(ElleResultData elleResultData) {
				List<ElleResultData.Suspect> suspects = elleResultData.getExtractInfoVec().get(0).getExtractInfo()
						.getSuspect();
				if (org.springframework.util.CollectionUtils.isEmpty(suspects)) {
					return null;
				}
				// 主案由
				ElleResultData.Item[] mainItem = buildysayFunc().apply(elleResultData);
				final String mainItemMean = ArrayUtils.isNotEmpty(mainItem) ? mainItem[0].getMean() : StringUtils.EMPTY;

				List<ElleResultData.Item> itemList = new ArrayList<ElleResultData.Item>();
				SetList<String> setList = new SetList<>();
				for (int i = 0; i < suspects.size(); i++) {
					ElleResultData.Label label = suspects.get(i).getLabel();
					ElleResultData.Item[] ysqtayItems = null;

					if (StringUtils.isNotEmpty(mainItemMean)) {
						ysqtayItems = XYRFieldNameEnum.YS.getElleFieldExtractor().apply(label);
						if (ArrayUtils.isNotEmpty(ysqtayItems)) {
							List<ElleResultData.Item> list = Arrays.asList(ysqtayItems);
							list = list.stream().filter(v -> !mainItemMean.equals(v.getMean()))
									.collect(Collectors.toList());
							if (CollectionUtils.isNotEmpty(list)) {
								for (ElleResultData.Item item : list) {
									String mean = item.getMean();
									if (StringUtils.isNotBlank(mean) && !setList.contains(mean)) {
										setList.add(mean);
										itemList.add(item);
									}
								}
							}

						}
					}

					ysqtayItems = XYRFieldNameEnum.YSQT.getElleFieldExtractor().apply(label);
					if (ArrayUtils.isNotEmpty(ysqtayItems)) {
						for (ElleResultData.Item item : ysqtayItems) {
							String mean = item.getMean();
							if (StringUtils.isNotBlank(mean) && !setList.contains(mean)) {
								setList.add(mean);
								itemList.add(item);
							}
						}
					}

				}
				if (CollectionUtils.isNotEmpty(itemList)) {
					ElleResultData.Item[] items = itemList.toArray(new ElleResultData.Item[itemList.size()]);
					return items;
				}
				return null;
			}
		};
	}

	/** 审结罪名： 抽取方法 以文书中列明的首个案由为主，其它案由归入“审结其他罪名 */
	public static Function<ElleResultData, ElleResultData.Item[]> buildzjzmFunc() {
		return new Function<ElleResultData, ElleResultData.Item[]>() {

			@Override
			public ElleResultData.Item[] apply(ElleResultData elleResultData) {
				List<ElleResultData.Suspect> suspects = elleResultData.getExtractInfoVec().get(0).getExtractInfo()
						.getSuspect();
				if (org.springframework.util.CollectionUtils.isEmpty(suspects)) {
					return null;
				}
				for (int i = 0; i < suspects.size(); i++) {
					ElleResultData.Label label = suspects.get(0).getLabel();
					ElleResultData.Item[] zdayItems = XYRFieldNameEnum.SDAY.getElleFieldExtractor().apply(label);
					if (ArrayUtils.isNotEmpty(zdayItems)) {
						ElleResultData.Item[] temp = new ElleResultData.Item[1];
						System.arraycopy(zdayItems, 0, temp, 0, 1);
						return temp;
					}
				}
				return null;
			}
		};
	}

	/** 审结其他罪名： 抽取方法 以文书中列明的首个案由为主，其它案由归入“审结其他罪名 */
	public static Function<ElleResultData, ElleResultData.Item[]> buildzjqtzmFunc() {
		return new Function<ElleResultData, ElleResultData.Item[]>() {

			@Override
			public ElleResultData.Item[] apply(ElleResultData elleResultData) {
				List<ElleResultData.Suspect> suspects = elleResultData.getExtractInfoVec().get(0).getExtractInfo()
						.getSuspect();
				if (org.springframework.util.CollectionUtils.isEmpty(suspects)) {
					return null;
				}
				// 主审结罪名
				ElleResultData.Item[] mainItem = buildzjzmFunc().apply(elleResultData);
				final String mainItemMean = ArrayUtils.isNotEmpty(mainItem) ? mainItem[0].getMean() : StringUtils.EMPTY;

				List<ElleResultData.Item> itemList = new ArrayList<ElleResultData.Item>();
				SetList<String> setList = new SetList<>();
				for (int i = 0; i < suspects.size(); i++) {
					ElleResultData.Label label = suspects.get(i).getLabel();
					ElleResultData.Item[] ysqtayItems = null;

					if (StringUtils.isNotEmpty(mainItemMean)) {
						ysqtayItems = XYRFieldNameEnum.SDAY.getElleFieldExtractor().apply(label);
						if (ArrayUtils.isNotEmpty(ysqtayItems)) {
							List<ElleResultData.Item> list = Arrays.asList(ysqtayItems);
							list = list.stream().filter(v -> !mainItemMean.equals(v.getMean()))
									.collect(Collectors.toList());
							if (CollectionUtils.isNotEmpty(list)) {
								for (ElleResultData.Item item : list) {
									String mean = item.getMean();
									if (StringUtils.isNotBlank(mean) && !setList.contains(mean)) {
										setList.add(mean);
										itemList.add(item);
									}
								}
							}

						}
					}
					ysqtayItems = XYRFieldNameEnum.SDQTAY.getElleFieldExtractor().apply(label);
					if (ArrayUtils.isNotEmpty(ysqtayItems)) {
						for (ElleResultData.Item item : ysqtayItems) {
							String mean = item.getMean();
							if (StringUtils.isNotBlank(mean) && !setList.contains(mean)) {
								setList.add(mean);
								itemList.add(item);
							}
						}
					}
				}
				if (CollectionUtils.isNotEmpty(itemList)) {
					ElleResultData.Item[] items = itemList.toArray(new ElleResultData.Item[itemList.size()]);
					return items;
				}
				return null;
			}
		};
	}

	/**
	 * 案件名称处理规则 审查逮捕案件：犯罪嫌疑人姓名+涉嫌+案由+案。例如：王小三涉嫌故意杀人案。
	 * 对于嫌疑人超过2人，涉嫌案由超过2个的罪名的，取前2名嫌疑人姓名+等N人+涉嫌+前2个案由+案。例如：王小三、李小二等5人涉嫌故意杀人、抢劫等案。
	 * 一审公诉案件：犯罪嫌疑人姓名+案由+案。例如：王小三故意杀人案。
	 * 对于犯罪嫌疑人超过2人，涉嫌案由超过2个罪名的，取前2个犯罪嫌疑人姓名+等N人+前2个案由+等案。例如：王小三、李小二等5人故意杀人、抢劫等案。
	 */
	public static Function<ElleResultData, String> builajmcFunc() {
		return new Function<ElleResultData, String>() {

			@Override
			public String apply(ElleResultData elleResultData) {
				ElleResultData.Item item = elleResultData.getExtractInfoVec().get(0).getExtractInfo().getCaseName();
				String result = StringUtils.EMPTY;
				// 默认案件名称取elle返回的值
				if (null != item) {
					result = item.getMean();
				}
				// 案卡类型 一审公诉 审查逮捕
				String type = elleResultData.getExtractInfoVec().get(0).getType();

				ElleTextTypeEnum elleTextTypeEnum = ElleTextTypeEnum.fromName(type);
				// 获取案卡类型
				int caseCatrgory = elleTextTypeEnum.getCaseCatrgory();
				List<ElleResultData.Suspect> suspects = elleResultData.getExtractInfoVec().get(0).getExtractInfo()
						.getSuspect();
				if (org.springframework.util.CollectionUtils.isEmpty(suspects)) {
					return result;
				}
				// 嫌疑人数量
				int suspectNum = suspects.size();
				String model1 = "%s涉嫌%s案";
				String model2 = "%s%s%d人涉嫌%s%s案";
				String model3 = "%s%s案";
				String model4 = "%s%s%d人%s%s案";

				SetList<String> ayList = new SetList<>();
				SetList<String> xyrxmList = new SetList<String>();
				for (int i = 0; i < suspects.size(); i++) {
					ElleResultData.Label label = suspects.get(i).getLabel();
					// 案由
					ElleResultData.Item[] ysayItems = XYRFieldNameEnum.YS.getElleFieldExtractor().apply(label);
					if (ArrayUtils.isNotEmpty(ysayItems)) {
						List<ElleResultData.Item> listItem = Arrays.asList(ysayItems);
						List<String> listStr = listItem.stream().map(ElleResultData.Item::getOrg_text)
								.filter(v -> StringUtils.isNotBlank(v)).collect(Collectors.toList());
						for (String s : listStr) {
							ayList.add(s);
						}
					}
					// 嫌疑人姓名
					ElleResultData.Item[] xyrxmItems = XYRFieldNameEnum.XM.getElleFieldExtractor().apply(label);
					if (ArrayUtils.isNotEmpty(xyrxmItems)) {
						List<ElleResultData.Item> listItem = Arrays.asList(xyrxmItems);
						List<String> listStr = listItem.stream().map(ElleResultData.Item::getMean)
								.filter(v -> StringUtils.isNotBlank(v)).collect(Collectors.toList());
						for (String s : listStr) {
							xyrxmList.add(s);
						}
					} else {
						// 嫌疑人姓名
						String xm = suspects.get(i).getLabel().getSuspectName()[0].getMean();
						xyrxmList.add(xm);
					}

				}

				for (int i = 0; i < suspects.size(); i++) {
					ElleResultData.Label label = suspects.get(i).getLabel();
					// 移送其他案由
					ElleResultData.Item[] qtayItems = XYRFieldNameEnum.YSQT.getElleFieldExtractor().apply(label);
					if (ArrayUtils.isNotEmpty(qtayItems)) {
						List<ElleResultData.Item> qtlistItem = Arrays.asList(qtayItems);
						List<String> qtlistStr = qtlistItem.stream().map(ElleResultData.Item::getOrg_text)
								.filter(v -> StringUtils.isNotBlank(v)).collect(Collectors.toList());
						for (String s : qtlistStr) {
							ayList.add(s);
						}
					}
				}
				String ay = StringUtils.EMPTY;
				if (!org.springframework.util.CollectionUtils.isEmpty(ayList)) {
					ay = ayList.stream().collect(Collectors.joining("、"));
					if (ay.endsWith("罪")) {
						ay = ay.substring(0, ay.length() - 1);
					}
				} else {
					return result;
				}
				String xm = StringUtils.EMPTY;
				if (!org.springframework.util.CollectionUtils.isEmpty(xyrxmList)) {
					xm = xyrxmList.stream().collect(Collectors.joining("、"));
				}
				switch (caseCatrgory) {
				case 1:// 一审公诉
					if (suspectNum <= 2) {
						result = String.format(model3, xm, ay);
					} else {
						xm = xyrxmList.subList(0, 2).stream().collect(Collectors.joining("、"));
						if (ayList.size() > 2) {
							ay = ayList.subList(0, 2).stream().collect(Collectors.joining("、"));
						}
						result = String.format(model4, xm, suspectNum <= 2 ? "" : "等", suspectNum, ay,
								ayList.size() <= 2 ? "" : "等");
					}
					break;
				case 2:// 审查逮捕
					if (suspectNum <= 2) {
						result = String.format(model1, xm, ay);
					} else {
						xm = xyrxmList.subList(0, 2).stream().collect(Collectors.joining("、"));
						if (ayList.size() > 2) {
							ay = ayList.subList(0, 2).stream().collect(Collectors.joining("、"));
						}
						result = String.format(model2, xm, suspectNum <= 2 ? "" : "等", suspectNum, ay,
								ayList.size() <= 2 ? "" : "等");
					}
					break;
				default:
					return result;
				}
				return result;

			}
		};
	}

	/** 案件受理向导-是否涉及未成年人案件 抽取方法 */
	public static Function<ElleResultData, String> buildSfswcnajFunc() {
		return new Function<ElleResultData, String>() {

			@Override
			public String apply(ElleResultData elleResultData) {
				String result = "否";
				List<ElleResultData.Suspect> suspects = elleResultData.getExtractInfoVec().get(0).getExtractInfo()
						.getSuspect();
				if (org.springframework.util.CollectionUtils.isEmpty(suspects)) {
					return result;
				}
				String time = StringUtils.EMPTY;// 作案时间
				ElleResultData.Item caseTime = elleResultData.getExtractInfoVec().get(0).getExtractInfo().getCaseTime();// 案情摘要时间
				if (null != caseTime && StringUtils.isNotEmpty(caseTime.getMean())) {
					String caseTimeStr = caseTime.getMean();

					String[] YMD = caseTimeStr.split("-", -1);
					// 缺失年的直接过滤
					if (YMD.length > 1 && StringUtils.isNotEmpty(YMD[0])) {
						for (int i = 0; i < YMD.length; i++) {
							if (StringUtils.isEmpty(YMD[i])) {
								YMD[i] = "01";
							}
						}
						List<String> newList = Arrays.asList(YMD);
						caseTimeStr = newList.stream().collect(Collectors.joining("-"));
						time = caseTimeStr;
					}

				}
				for (int ii = 0; ii < suspects.size(); ii++) {
					ElleResultData.Label label = suspects.get(ii).getLabel();
					String birthday = StringUtils.EMPTY;// 生日
					if (StringUtils.isEmpty(time)) {
						ElleResultData.Item[] timeItems = XYRFieldNameEnum.ZASJ.getElleFieldExtractor().apply(label);
						if (ArrayUtils.isNotEmpty(timeItems)) {
							List<ElleResultData.Item> arr = Arrays.asList(timeItems);
							// 作案时间集合
							List<String> list = arr.stream().map(ElleResultData.Item::getMean)
									.filter(v -> StringUtils.isNotBlank(v)).collect(Collectors.toList());
							List<String> standList = new ArrayList<>();
							for (String xx : list) {
								String[] YMD = xx.split("-", -1);
								// 缺失年的直接过滤
								if (YMD.length < 1 || StringUtils.isEmpty(YMD[0])) {
									continue;
								}
								for (int i = 0; i < YMD.length; i++) {
									if (StringUtils.isEmpty(YMD[i])) {
										YMD[i] = "01";
									}
								}
								List<String> newList = Arrays.asList(YMD);
								xx = newList.stream().collect(Collectors.joining("-"));
								standList.add(xx);
							}
							// 时间正序排序
							DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
							standList.sort((String m1, String m2) -> {
								try {
									return sdf.parse(m1).compareTo(sdf.parse(m2));
								} catch (ParseException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								return 0;
							});
							// 取最早作案时间
							if (!CollectionUtils.isEmpty(standList)) {
								time = standList.get(0);
							}
						}
					}

					ElleResultData.Item[] csrqItems = XYRFieldNameEnum.CSRQ.getElleFieldExtractor().apply(label);
					if (ArrayUtils.isNotEmpty(csrqItems)) {
						birthday = csrqItems[0].getMean();
					}
					if (StringUtils.isNotEmpty(birthday) && StringUtils.isNotEmpty(time)) {
						int age = FieldConvertors.getAge(birthday, time);
						if (age < 18) {
							result = "是";
							return result;
						}
					}
				}
				return result;

			}
		};
	}

	public static void main(String[] args) {
		SetList<String> ayList = new SetList<>();
		List<String> list = new ArrayList<>();
		ayList.add("开设赌场罪");
		ayList.add("走私、贩卖、运输、制造毒品罪");
		ayList.add("开设赌场罪");
		ayList.add("走私、贩卖、运输、制造毒品罪");
		ayList.addAll(list);
		for (String message : ayList) {
			System.out.println(message);
		}
	}
}
