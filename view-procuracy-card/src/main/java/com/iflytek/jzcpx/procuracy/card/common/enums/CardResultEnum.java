package com.iflytek.jzcpx.procuracy.card.common.enums;

import com.iflytek.jzcpx.procuracy.common.enums.ResultType;

/**
 * 案卡接口结果枚举, 14xxx
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019/8/22 9:20
 */
public enum CardResultEnum implements ResultType {

    ELLE_REQUEST_EXCEPTION(14001, "elle引擎调用异常"),
    ELLE_REQUEST_FAILED(14002, "elle引擎调用失败"),
    ELLE_RESULT_PARSE_FAILED(14003, "elle引擎结果解析失败"),

    FILE_CONTENT_IS_BLANK(14010, "文本内容抽取结果为空"),
    FILE_TYPE_NOT_SUPPORT(14102, "文件类型不支持"),

    ELLE_FILE_TYPE_CONVERT_FAILED(14101, "没有对应的elle引擎文书类型"),

    DOWNLOAD_FILE_STREAM_EXCEPTION(14110, "文件下载异常"),
    DOWNLOAD_FILE_STREAM_FAILED(14111, "文件下载失败"),

    CSB_REQUEST_FAILED(14201, "CSB调用失败");

    private Integer code;
    private String desc;

    CardResultEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static CardResultEnum getByCode(Integer code) {
        if (code == null) {
            return null;
        }

        for (CardResultEnum anEnum : values()) {
            if (anEnum.getCode().equals(code)) {
                return anEnum;
            }
        }

        return null;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getDesc() {
        return desc;
    }}