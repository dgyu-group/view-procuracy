package com.iflytek.jzcpx.procuracy.card.common.enums;

/**
 * 案卡文件所在阶段枚举
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-27 15:51
 */
public enum CardFileProcessEnum {
    /** 受理阶段 */
    ACCEPT,
    /** 办理阶段 */
    HANDLE,

    ;
}