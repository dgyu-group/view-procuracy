package com.iflytek.jzcpx.procuracy.card.component;

import com.alibaba.fastjson.JSONObject;
import com.iflytek.jzcpx.procuracy.card.pojo.ElleResultData;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * 案卡字段表 - remark 扩展字段实体
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019/9/30 14:11
 */
@Data
public class CardFormFieldRemark {

    private int beginPosition;

    private int endPisiton;

    public CardFormFieldRemark() {
    }

    public CardFormFieldRemark(ElleResultData.Item item) {
        if (item != null) {
            this.beginPosition = item.getPos_begin();
            this.endPisiton = item.getPos_end();
        }
    }

    public static CardFormFieldRemark fromRemark(String remark) {
        if (StringUtils.isNotBlank(remark)) {
            return JSONObject.parseObject(remark, CardFormFieldRemark.class);
        }
        return null;
    }

    public String toRemarkString() {
        return JSONObject.toJSONString(this);
    }

}
