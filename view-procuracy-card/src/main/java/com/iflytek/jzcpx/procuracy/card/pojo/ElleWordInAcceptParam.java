package com.iflytek.jzcpx.procuracy.card.pojo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Word文件要素抽取参数
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019/8/21 14:00
 */
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Data
public class ElleWordInAcceptParam extends ElleExtractParam {

    /** 文档文件流 */
    private byte[] wdwjl;

}
