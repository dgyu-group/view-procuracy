package com.iflytek.jzcpx.procuracy.card.config;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.iflytek.jzcpx.procuracy.tools.elle.ElleTextTypeEnum;
import org.apache.commons.lang3.StringUtils;

/**
 * 赛威讯文书模块编码与elle引擎文书类型对应关系
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019/9/8 11:29
 */
public class WsmbbmConfig {

    private final Map<String, ElleTextTypeEnum> storage = new ConcurrentHashMap<>();

    /**
     * 添加对应关系配置
     *
     * @param wsmbbm           赛威讯文书模板编码
     * @param elleTextTypeEnum elle文书类型
     */
    public void addConfig(String wsmbbm, ElleTextTypeEnum elleTextTypeEnum) {
        if (StringUtils.isNotBlank(wsmbbm) && elleTextTypeEnum != null) {
            storage.put(wsmbbm, elleTextTypeEnum);
        }
    }

    /**
     * 获取对应的elle文书类型
     * @param wsmbbm 赛威讯文书模板编码
     * @return null：如果没有对应的elle文书类型或者入参为空
     */
    public ElleTextTypeEnum getElleTextType(String wsmbbm) {
        return StringUtils.isNotBlank(wsmbbm) ? storage.get(wsmbbm) : null;
    }

}
