package com.iflytek.jzcpx.procuracy.card.config;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * 字段编码值配置
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019/9/5 11:39
 */
public class ZdbmConfig {
    /**
     * 保存类别名称及其下名称与编码值对应关系
     * 例如:
     * <pre>
     * 性别:
     *      男性: 9909180000001
     *      女性: 9909180000002
     * 受教育状况:
     *      中专: 9915180600006
     *      高职: 9915180600007
     *      专科毕业: 9915180600008
     *      大学本科: 9915180600009
     * </pre>
     *
     * @return
     */
    private Map<String, Map<String, String>> zdbmMap = new ConcurrentHashMap<>();;

    /**
     * 添加字段类别配置
     *
     * @param lbmc      类别名称
     * @param wbzBmzMap 文本值与编码值对应关系
     */
    public void add(String lbmc, Map<String, String> wbzBmzMap) {
        this.zdbmMap.put(lbmc, wbzBmzMap);
    }


	/**
     * 某个字段是否存在配置
     *
     * @param zdmc 字段名称
     *
     * @return
     */
    public boolean containsLbmc(String zdmc) {
        return StringUtils.isNotBlank(zdmc) && this.zdbmMap.containsKey(zdmc);
    }

    /**
     * 获取某个字段文本值对应的的字段编码值
     *
     * @param lbmc  类别名称，如：性别
     * @param zdwbz 字段文本值，如：男
     *
     * @return
     */
    public String getZdbm(String lbmc, String zdwbz) {
        Map<String, String> wbzBmzMap = this.zdbmMap.get(lbmc);
        if (MapUtils.isEmpty(wbzBmzMap)) {
            return "";
        }

        for (Map.Entry<String, String> entry : wbzBmzMap.entrySet()) {
            String wbz = entry.getKey();
            String bmz = entry.getValue();
            if (wbz.equals(zdwbz) || wbz.startsWith(zdwbz) || wbz.endsWith(zdwbz) || zdwbz.startsWith(wbz) ||
                    zdwbz.endsWith(wbz) || wbz.contains(zdwbz) || zdwbz.contains(wbz)) {
                return bmz;
            }
        }

        return "";
    }
}
