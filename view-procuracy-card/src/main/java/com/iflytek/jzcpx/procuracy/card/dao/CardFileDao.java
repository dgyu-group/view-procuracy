package com.iflytek.jzcpx.procuracy.card.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.iflytek.jzcpx.procuracy.card.entity.CardFile;

public interface CardFileDao extends BaseMapper<CardFile> {

}