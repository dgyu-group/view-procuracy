package com.iflytek.jzcpx.procuracy.card.pojo;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * 图片文件要素抽取参数
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019/8/20 14:36
 */
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Data
public class ElleImgInAcceptParam extends ElleExtractParam {

    /** 文档文件流, base64格式的文件内容 */
    private List<byte[]> wdwjl;

}
