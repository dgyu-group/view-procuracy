package com.iflytek.jzcpx.procuracy.card.pojo;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

/**
 * 要素抽取前期参数
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019/8/20 14:31
 */
@Data
public class ElleOuterSyncInHandleParam {
	/** 部门受案号 */
    private String bmsah;

    /** 案件类别编码 */
    private String ajlbbm;

    /** 卷宗文件号 */
    private String jzwjh;

    /** 卷宗文件名称 */
    private String jzwjmc;

    /** 卷宗文书类型 */
    private String jzwjlx;
    
    /** 表单结构 */
    private JSONObject bdjg;
    
    /** 卷宗文件流 */
    private byte[] jzwswj;
}
