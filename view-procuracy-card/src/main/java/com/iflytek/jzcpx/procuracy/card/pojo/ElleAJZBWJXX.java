package com.iflytek.jzcpx.procuracy.card.pojo;

import java.util.List;

import lombok.Data;

/**
 * 案件在办文件信息
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-30 16:38
 */
@Data
public class ElleAJZBWJXX {
    /** 同级索引：0开头，前端使用 */
    private String index;

    /** 同级数量，前端使用 */
    private int count;

    /** 服务器文件名 */
    private Object fwqwjmc;

    /** 父文书编号 */
    private Object fwsbh;

    /** 加载方式 0:word 1:form */
    private String jzfs;

    /** 文书拟置人单位编码 */
    private String nzrdwbm;

    /** 拟制日期 */
    private String nzrq;

    /** 拟制人姓名 */
    private String nzrxm;

    /** 入卷目录编号 */
    private Object rjmlbh;

    /** 入卷目录名称 */
    private Object rjmlmc;

    /** 锁定人员名称 */
    private Object sdry;

    /** 是否可覆盖 */
    private String sfkfg;

    /** 是否锁定 0 未锁定 1 锁定 */
    private String sfsd;

    /** 是否要好 0 不能要号 1 未要号 2 已要号 */
    private String sfyh;

    /** 是否用印 0 不用印 1 未用印 2 已用印 */
    private String sfyy;

    /** 附件扩展名 */
    private String wjkzm;

    /** 文件路径 */
    private String wjlj;

    /** 文书类型 0 表示文书 -1表示审批表（包括自带审批表） -2表示附件 1 表示自带审批表的文书 */
    private String wslx;

    /** 文书模板编号 */
    private String wsmbbh;

    /** 文书名称 */
    private String wsmc;

    /** 文书实例编号 */
    private String wsslbh;

    /** 文书实例单位编码 */
    private String wssldwbm;

    /** 文书文号 */
    private Object wswh;

    /** 文书运行状态（拟制中、审批中） */
    private String wsyxzt;

    /** 文书运行状态编码 */
    private int wsyxztbm;

    /** 序号 */
    private Object xh;

    /** 文书关联的自然人集合 */
    private List<PeopleBean> people;

    /** 子文书集合 */
    private List<ElleAJZBWJXX> children;

    @Data
    public static class PeopleBean {
        /** 自然人编码 */
        private String zrrbm;

        /** 自然人类别编码 */
        private String zrrlbbm;

        /** 自然人类别名称 */
        private String zrrlbmc;

        /** 自然人名称 */
        private String zrrmc;
    }
}
