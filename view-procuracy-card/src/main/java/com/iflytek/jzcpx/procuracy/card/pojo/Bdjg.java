package com.iflytek.jzcpx.procuracy.card.pojo;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * 表单结构
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019/8/20 16:41
 */
@Data
public class Bdjg implements Serializable {
    private static final long serialVersionUID = -3051474057305414589L;

    /** 实体表名 */
    private String stbm;

    /** 类别编码 */
    private String lbbm;

    /** 动态条件 */
    private String dttj;
    private String id;
    private String fid;

    /** 数据标识 */
    private String sjbs;

    /** 字段数据集合 */
    private List<Zdsj> zdsj;

    /** 从表数据 */
    private List<Bdjg> cbsj;

    @Data
    public static class Zdsj {
        /** 字段名称 */
        private String zdmc;

        /** 是否主键 */
        private String sfzj;

        /** 主键生成策略 */
        private  String zjsccl;

        /** 字段类型 */
        private String zdlx;

        /** 变更前字段编码值 */
        private String bgqzdbmz;

        /** 变更钱字段文本值 */
        private String bgqzdwbz;

        /** 变更后字段编码值 */
        private String bghzdbmz;

        /** 变更后字段文本值 */
        private String bghzdwbz;

        /** 变更时间, 毫秒时间戳 */
        private String bgsj;

        /** 智能回填 */
        private List<Znht> znht;
    }

    @Data
    public static class Znht {
        /** 文件类型 */
        private String wjlx;

        /** wjbm */
        private String wjbm;

        /** 文件名称 */
        private String wjmc;

        /** 数据项 */
        private List<Sjx> sjx;
    }

    @Data
    public static class Sjx {
        /** 编码值 */
        private String bm;

        /** 文本值 */
        private String wb;
    }

}
