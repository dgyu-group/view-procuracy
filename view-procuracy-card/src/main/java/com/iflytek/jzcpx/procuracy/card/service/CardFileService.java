package com.iflytek.jzcpx.procuracy.card.service;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.iflytek.jzcpx.procuracy.card.common.enums.CardFileStatusEnum;
import com.iflytek.jzcpx.procuracy.card.entity.CardFile;

/**
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-27 10:17
 */
public interface CardFileService extends IService<CardFile> {

    /**
     * 案卡任务异常结束
     *
     * @param id        案卡文件 id
     * @param errorInfo 错误信息
     *
     * @return
     */
    boolean abnormalEnd(Long id, String errorInfo);

    /**
     * 更新任务状态为 ocr 结果保存完成
     *
     * @param id           案卡文件 id
     * @param ocrStorePath ocr 结果存储路径, 多个路径以逗号(,)分隔
     *
     * @return
     */
    boolean updateByStoreOcrResult(Long id, String ocrStorePath);

    /**
     * 更新案卡任务状态
     *
     * @param id             案卡文件 id
     * @param fileStatusEnum 更新为该状态
     *
     * @return
     */
    boolean updateStatus(Long id, CardFileStatusEnum fileStatusEnum);

    /**
     * 更新任务状态为 elle 结果保存完成
     *
     * @param id            案卡文件 id
     * @param elleStorePath elle 结果存储位置
     *
     * @return
     */
    boolean updateByStoreElleResult(Long id, String elleStorePath);

    /**
     * 获取办理阶段已处理的文件
     *
     * @param bmsah 部门受案号
     *
     * @return
     */
    List<CardFile> findInHandleFiles(String bmsah);

    /**
     * 根据部门受案号和文书实例编号查询案卡文件
     *
     * @param bmsah  部门受案号
     * @param wsslbh 文书实例编号
     *
     * @return
     */
    List<CardFile> findByWsslbh(String bmsah, String wsslbh);

    /**
     * 根据部门受案号和卷宗文件号查询案卡文件
     *
     * @param bmsah 部门受案号
     * @param jzwjh 卷宗文件号
     *
     * @return
     */
    List<CardFile> findByJzwjh(String bmsah, String jzwjh);

    /**
     * 根据部门受案号查询案卡文件
     *
     * @param bmsah 部门受案号
     *
     * @return
     */
    List<CardFile> listByBmsah(String bmsah);

    /**
     * 更新案卡任务状态并保存文件内容
     *
     * @param cardFileId     案卡文件 id
     * @param fileStatusEnum 更新为该状态
     * @param fileContent    文档文本内容
     *
     * @return
     */
    boolean updateStatusAndFileContent(Long cardFileId, CardFileStatusEnum fileStatusEnum, String fileContent);
    
    /**
     * 根据文件编码更新部门受案号
     * @param wjbm 文件编码
     * @param bmsah 部门受案号
     * @return
     */
    boolean updateBmsahByWjbm(String wjbm,String bmsah);

    int cleanData(Date startDatetime, Date endDatetime, boolean cleanDB, boolean cleanFdfs);
}
