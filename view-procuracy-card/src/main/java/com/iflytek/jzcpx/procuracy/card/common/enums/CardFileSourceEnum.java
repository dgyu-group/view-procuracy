package com.iflytek.jzcpx.procuracy.card.common.enums;

/**
 * 案卡文件来源枚举
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-27 15:51
 */
public enum CardFileSourceEnum {
    /** 检察院内部文件 */
    INNER,
    /** 外部文件 */
    OUTER,

    ;

}