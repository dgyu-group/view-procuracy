package com.iflytek.jzcpx.procuracy.card.common;

import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * 案卡业务常量类
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-29 11:32
 */
public final class CardConstants {

    public static final String FASTDFS_SUFFIX_TXT = "txt";

    /** 案情摘要 表名 */
    public static final String AQZY_TABLENAME = "T_TYYW_GG_AQZY";

    /** 案件 表名配置(案件类别编码:案件表名) */
    public static final Map<String, String> AJ_TABLENAME = Maps.newHashMap();

    /** 一审公诉案件 审查起诉表 */
    public static final String YSGS_AJ_TABLENAME = "T_TYYW_XJ_YSGS_AJ";
    /** 审查逮捕案件 审查逮捕案件表  */
    public static final String XBS_AJ_TABLENAME = "T_TYYW_XJ_XBS_AJ";
    static {
        AJ_TABLENAME.put("2001", YSGS_AJ_TABLENAME);
        AJ_TABLENAME.put("2031", XBS_AJ_TABLENAME);
    }

    /** 受理日志表 表名 */
    public static final String SLRZ_TABLENAME = "AJ_YX_SLRZ";

    /** 一审单位犯罪 表名 */
    public static final String DWFZ_TABLENAME = "T_TYYW_XJ_YSGS_DWFZXX";

    /** 一审公诉案件 嫌疑人表名 */
    public static final String YSGS_XYR_TABLENAME = "T_TYYW_XJ_YSGS_XYR";
    /** 审查逮捕案件 嫌疑人表名 */
    public static final String XBS_XYR_TABLENAME = "T_TYYW_XJ_XBS_XYR";

    /** 嫌疑人 表名配置(案件类别编码:嫌疑人表名) */
    public static final Map<String, String> XYR_TABLENAMES = Maps.newHashMap();
    static {
        XYR_TABLENAMES.put("2001", YSGS_XYR_TABLENAME);
        XYR_TABLENAMES.put("2031", XBS_XYR_TABLENAME);
    }

    /** 嫌疑人-强制措施情况 表名 */
    public static final String QZCSQK_TABLENAME = "T_TYYW_GG_QZCSQK";


    /** 智能回填时, 如果字段类型在如下范围, 则需要将对应编码值填入bm字段, 对应文本值填入wb字段，其他类型均将值填入bm字段 */
    public static final List<String> FILL_BM_ZDLX = Lists.newArrayList("31", "32", "33", "34", "35", "36");

    private CardConstants() {
    }
    
    //强制措施-刑事拘留
    public static final String QZCS_XSJL="刑事拘留";
    //强制措施-逮捕
    public static final String QZCS_DB="逮捕";
    
    //SWX表单结构字段类型 文本类型 
    //    00 其它
	//    01 String
	//    02 Date
	//    03 Time
	//    04 Number
	//    11 Image
	//    12 File
	//    13 Blob
	//    31 DM
	//    32 DMS
	//    33 AYDM
	//    34 AYDMS
	//    35 DWDM
	//    36 DWDMS
	//    41 BM
	//    42 BMS
    public static final List<String> TEXT_ZDLX=Lists.newArrayList("00", "01","02","03","04","11","12","13");
}
