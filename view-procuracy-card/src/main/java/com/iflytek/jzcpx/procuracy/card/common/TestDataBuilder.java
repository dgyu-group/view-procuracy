package com.iflytek.jzcpx.procuracy.card.common;

import java.util.ArrayList;
import java.util.List;

import com.iflytek.jzcpx.procuracy.card.pojo.ElleResultData;
import com.iflytek.jzcpx.procuracy.common.result.Result;
import org.apache.commons.collections4.CollectionUtils;

/**
 * 伪造数据，方便调试，正式运行时不要使用
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019/9/8 15:25
 */
public class TestDataBuilder {

    /** 构造办理阶段-文书编写Elle结果 */
    public static Result<ElleResultData> buildElleDataInHandleFile(Result<ElleResultData> elleResult) {
        ElleResultData.ExtractInfo info = elleResult.getData().getExtractInfoVec().get(0).getExtractInfo();
        List<ElleResultData.Suspect> suspects = info.getSuspect();
        if (CollectionUtils.isEmpty(suspects)) {
            ElleResultData.Label label = buildLabel();
            ElleResultData.Suspect suspect = new ElleResultData.Suspect();
            suspect.setLabel(label);
            suspects = new ArrayList<>();
            suspects.add(suspect);
            info.setSuspect(suspects);
        }
        else {
            ElleResultData.Label label = suspects.get(0).getLabel();
            modifyLabel(label);
        }
        return elleResult;
    }

    private static ElleResultData.Label buildLabel() {
        ElleResultData.Label label = new ElleResultData.Label();
        modifyLabel(label);
        return label;
    }

    private static void modifyLabel(ElleResultData.Label label) {
        ElleResultData.Item workplace = new ElleResultData.Item();
        workplace.setMean("xxxx公司");
        label.setWorkplace(new ElleResultData.Item[]{workplace});
    }
}
