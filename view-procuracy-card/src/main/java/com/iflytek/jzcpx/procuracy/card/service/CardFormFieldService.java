package com.iflytek.jzcpx.procuracy.card.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;
import com.iflytek.jzcpx.procuracy.card.entity.CardFile;
import com.iflytek.jzcpx.procuracy.card.entity.CardFormField;

/**
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-27 10:17
 */
public interface CardFormFieldService extends IService<CardFormField> {
    /**
     * 根据案卡文件 id 批量查询字段数据
     *
     * @param fileIds 案卡文件 id 集合
     *
     * @return
     */
    Map<Long, List<CardFormField>> findFieldsByFileId(List<Long> fileIds);

    /**
     * 根据案卡文件id批量删除表单字段
     *
     * @param cardFileIds 案卡文件id
     * @return
     */
    boolean removeByCardFileIds(List<Long> cardFileIds);

    /**
     * 更新表单结构 id 到案卡字段
     *
     * @param ids    案卡字段 id
     * @param bdjgId 表单结构 id
     *
     * @return
     */
    boolean updateBdjgId(List<Long> ids, String bdjgId);


    /**
     * 查询案卡字段, 差异化比对页面使用
     *
     * @param bmsah  部门受案号
     * @param stbm   实体表名
     * @param stzd   实体字段
     * @param bdjgId 表单结构 id
     *
     * @return
     */
    Map<CardFile, List<CardFormField>> listFieldsForDiff(String bmsah, String stbm, String stzd, String bdjgId);

    int cleanData(Date startDatetime, Date endDatetime);
}
