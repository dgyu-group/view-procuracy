package com.iflytek.jzcpx.procuracy.card.pojo;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

/**
 * 全案卡提取参数
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019/8/20 14:31
 */
@Data
public class ElleFullParam {
	/** 部门受案号 */
    private String bmsah;
  
    /** 案件类别编码 */
    private String ajlbbm;
    
    /** 登陆人部门编码 */
    private String dlrbmbm;

    /** 登陆人单位编码 */
    private String dlrdwbm;

    /** 登陆人编码 */
    private String dlrybm;

    /** 表单结构 */
    private JSONObject bdjg;

}
