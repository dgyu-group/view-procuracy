package com.iflytek.jzcpx.procuracy.card.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.iflytek.jzcpx.procuracy.card.entity.CardFormField;

public interface CardFormFieldDao extends BaseMapper<CardFormField> {

}