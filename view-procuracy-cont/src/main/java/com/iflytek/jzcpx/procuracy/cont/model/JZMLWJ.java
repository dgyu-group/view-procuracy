package com.iflytek.jzcpx.procuracy.cont.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.File;

/**
 * @Author wwwang7
 * @Date 2019/6/17 18:56
 */
public class JZMLWJ {

    /**
     * 卷宗编号
     */
    @JSONField(name="jzbh")
    private String JZBH;

    /**
     * 目录编号
     */
    @JSONField(name="mlbh")
    private String MLBH;

    /**
     * 文件编号
     */
    @JSONField(name="wjxh")
    private String WJXH;

    /**
     * 文件名称
     */
    @JSONField(name="wjmc")
    private String WJMC;

    /**
     * 文件路径
     */
    @JSONField(name="wjlj")
    private String WJLJ;

    /**
     * 文件识别路径
     */
    @JSONField(name="wjsblj")
    private String WJSBLJ;

    /**
     * 文件后缀
     */
    @JSONField(name="wjhz")
    private String WJHZ;

    /**
     * 文件类型(1：封面，2：目录)
     */
    @JSONField(name="wjlx")
    private String WJLX;

    /**
     * 文件顺序号
     */
    @JSONField(name="wjsxh")
    private int WJSXH;

    /**
     * 文件自定义（Y：识别成功）
     */
    @JSONField(name="wjzdy")
    private String WJZDY;

    /**
     * 文件显示名称
     */
    @JSONField(name="wjxsmc")
    private String WJXSMC;

    /**
     * 文件开始页
     */
    @JSONField(name="wjksy")
    private int WJKSY;

    /**
     * 文件结束页
     */
    @JSONField(name="wjjsy")
    private int WJJSY;

    /**
     * 文件备注信息
     */
    @JSONField(name="wjbzxx")
    private String WJBZXX;

    /**
     * 文件上传验证标识
     */
    @JSONField(name="wjyzbz")
    private String WJYZBZ;

    /**
     * 文本结果
     */
    private String ocrRes;
    
    /**
     * 存储在fdfs上的结果文件
     */
    private String fdfsEngineResultPath;

    private File file;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getOcrRes() {
        return ocrRes;
    }

    public void setOcrRes(String ocrRes) {
        this.ocrRes = ocrRes;
    }

    public String getJZBH() {
        return JZBH;
    }

    public void setJZBH(String JZBH) {
        this.JZBH = JZBH;
    }

    public String getMLBH() {
        return MLBH;
    }

    public void setMLBH(String MLBH) {
        this.MLBH = MLBH;
    }

    public String getWJXH() {
        return WJXH;
    }

    public void setWJXH(String WJXH) {
        this.WJXH = WJXH;
    }

    public String getWJMC() {
        return WJMC;
    }

    public void setWJMC(String WJMC) {
        this.WJMC = WJMC;
    }

    public String getWJLJ() {
        return WJLJ;
    }

    public void setWJLJ(String WJLJ) {
        this.WJLJ = WJLJ;
    }

    public String getWJSBLJ() {
        return WJSBLJ;
    }

    public void setWJSBLJ(String WJSBLJ) {
        this.WJSBLJ = WJSBLJ;
    }

    public String getWJHZ() {
        return WJHZ;
    }

    public void setWJHZ(String WJHZ) {
        this.WJHZ = WJHZ;
    }

    public String getWJLX() {
        return WJLX;
    }

    public void setWJLX(String WJLX) {
        this.WJLX = WJLX;
    }

    public String getWJZDY() {
        return WJZDY;
    }

    public void setWJZDY(String WJZDY) {
        this.WJZDY = WJZDY;
    }

    public int getWJSXH() {
        return WJSXH;
    }

    public void setWJSXH(int WJSXH) {
        this.WJSXH = WJSXH;
    }

    public String getWJXSMC() {
        return WJXSMC;
    }

    public void setWJXSMC(String WJXSMC) {
        this.WJXSMC = WJXSMC;
    }

    public int getWJKSY() {
        return WJKSY;
    }

    public void setWJKSY(int WJKSY) {
        this.WJKSY = WJKSY;
    }

    public int getWJJSY() {
        return WJJSY;
    }

    public void setWJJSY(int WJJSY) {
        this.WJJSY = WJJSY;
    }

    public String getWJBZXX() {
        return WJBZXX;
    }

    public void setWJBZXX(String WJBZXX) {
        this.WJBZXX = WJBZXX;
    }

    public String getWJYZBZ() {
        return WJYZBZ;
    }

    public void setWJYZBZ(String WJYZBZ) {
        this.WJYZBZ = WJYZBZ;
    }

	/**
	 * @return the fdfsEngineResultPath
	 */
	public String getFdfsEngineResultPath() {
		return fdfsEngineResultPath;
	}

	/**
	 * @param fdfsEngineResultPath the fdfsEngineResultPath to set
	 */
	public void setFdfsEngineResultPath(String fdfsEngineResultPath) {
		this.fdfsEngineResultPath = fdfsEngineResultPath;
	}
}
