package com.iflytek.jzcpx.procuracy.cont.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 华宇查询立案号返回的实体
 * 
 * @author lli
 *
 * @version 1.0
 * 
 */
public class AjDto {
    @JSONField(name = "wjid")
    private String wjid;
    @JSONField(name = "s_ah")
    private String s_ah;
    @JSONField(name = "laay")
    private String laay;

    /**
     * @return the wjid
     */
    public String getWjid() {
        return wjid;
    }

    /**
     * @param wjid
     *            the wjid to set
     */
    public void setWjid(String wjid) {
        this.wjid = wjid;
    }

    /**
     * @return the s_ah
     */
    public String getS_ah() {
        return s_ah;
    }

    /**
     * @param s_ah
     *            the s_ah to set
     */
    public void setS_ah(String s_ah) {
        this.s_ah = s_ah;
    }

    /**
     * @return the laay
     */
    public String getLaay() {
        return laay;
    }

    /**
     * @param laay
     *            the laay to set
     */
    public void setLaay(String laay) {
        this.laay = laay;
    }

}
