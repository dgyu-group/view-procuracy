package com.iflytek.jzcpx.procuracy.cont.common.config;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * 配置线程池
 *
 * @author lli
 * @version 1.0
 */
@Configuration
public class ExecutorConfig {

	/**
	 * 抽取OCR和CONT编目引擎线程池
	 * 
	 * @return
	 */
	@Bean
	public Executor dzjzOcrContExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(10);
		executor.setMaxPoolSize(10);
		executor.setQueueCapacity(Integer.MAX_VALUE);
		executor.setThreadNamePrefix("dzjzOcrCont-pool-");
		executor.initialize();
		return executor;
	}
}