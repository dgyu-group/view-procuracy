package com.iflytek.jzcpx.procuracy.cont.enums;

import com.iflytek.jzcpx.procuracy.common.enums.ResultType;

/**
 * 自动编目接口结果枚举，12xxx
 *
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-07 09:15
 */
public enum ContResultEnum implements ResultType {
    

    ;

    private Integer code;
    private String desc;

    ContResultEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static ContResultEnum getByCode(Integer code) {
        if (code == null) {
            return null;
        }

        for (ContResultEnum anEnum : values()) {
            if (anEnum.getCode().equals(code)) {
                return anEnum;
            }
        }

        return null;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getDesc() {
        return desc;
    }}