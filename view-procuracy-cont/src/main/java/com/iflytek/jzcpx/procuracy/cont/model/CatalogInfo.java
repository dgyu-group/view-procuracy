package com.iflytek.jzcpx.procuracy.cont.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CatalogInfo {

    private List<Catalog> catalog=new ArrayList<>();
    @JsonProperty("pages_num")
    private int pagesNum;

    public void setCatalog(List<Catalog> catalog) {
        this.catalog = catalog;
    }

    public List<Catalog> getCatalog() {
        return catalog;
    }

    public void setPagesNum(int pagesNum) {
        this.pagesNum = pagesNum;
    }

    public int getPagesNum() {
        return pagesNum;
    }

}