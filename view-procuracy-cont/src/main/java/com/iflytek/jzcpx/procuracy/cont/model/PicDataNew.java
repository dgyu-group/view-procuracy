package com.iflytek.jzcpx.procuracy.cont.model;

import java.io.File;

/**
 * 
 * @author iFLYREC
 *
 */
public class PicDataNew {

    private byte[]  data;

    private String imageId;

    private File file;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }
}
