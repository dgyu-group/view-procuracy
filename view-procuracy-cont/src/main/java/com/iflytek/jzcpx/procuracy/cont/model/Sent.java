package com.iflytek.jzcpx.procuracy.cont.model;

import com.alibaba.fastjson.annotation.JSONField;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

@Generated("http://www.stay4it.com")
public class Sent {
	
	@JSONField(name="char")
    private List<Char> _char = new ArrayList<Char>();
	
	@JSONField(name="char_count")
    private Integer charCount;
    private Double score;
    private String value;

    public List<Char> getChar() {
        return _char;
    }

    public void setChar(List<Char> _char) {
        this._char = _char;
    }

    public Integer getCharCount() {
        return charCount;
    }

    public void setCharCount(Integer charCount) {
        this.charCount = charCount;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return "Sent [_char=" + _char + ", charCount=" + charCount + ", score=" + score + ", value=" + value + "]";
    }

}
