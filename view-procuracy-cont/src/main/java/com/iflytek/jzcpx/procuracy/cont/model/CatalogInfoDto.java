package com.iflytek.jzcpx.procuracy.cont.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CatalogInfoDto {

    @JsonProperty("catalog_info")
    private CatalogInfo catalogInfo;
    private int rc;

    public void setCatalogInfo(CatalogInfo catalogInfo) {
        this.catalogInfo = catalogInfo;
    }

    public CatalogInfo getCatalogInfo() {
        return catalogInfo;
    }

    public void setRc(int rc) {
        this.rc = rc;
    }

    public int getRc() {
        return rc;
    }

}