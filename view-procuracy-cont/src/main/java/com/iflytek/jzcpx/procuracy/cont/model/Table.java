package com.iflytek.jzcpx.procuracy.cont.model;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

@Generated("http://www.stay4it.com")
public class Table {

    private List<Cell> cell = new ArrayList<Cell>();
    private Integer id;
    private Rect rect;
    private String type;

    public List<Cell> getCell() {
        return cell;
    }

    public void setCell(List<Cell> cell) {
        this.cell = cell;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Rect getRect() {
        return rect;
    }

    public void setRect(Rect rect) {
        this.rect = rect;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
