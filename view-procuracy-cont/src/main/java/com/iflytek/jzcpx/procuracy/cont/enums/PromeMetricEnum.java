package com.iflytek.jzcpx.procuracy.cont.enums;

/**
 * @author mhwang
 * ARGUE参数枚举
 */
public enum PromeMetricEnum {
	CASE_COUNT("iflytek_eds_case","案件数"),
	PIC_COUNT("iflytek_eds_pic","扫描数"),
	OCR_COUNT("iflytek_eds_ocr","扫描分布"),
	MODIFY_COUNT("iflytek_eds_modify","人工修改量"),
	CONT_ERROR_COUNT("iflytek_eds_cont_error","标题错误占比"),
	CONT_COUNT("iflytek_eds_cont","编目总图片数"),
	PIC_DOWN_TIME("iflytek_eds_pic_down_time","下载时长"),
	OCR_TIME("iflytek_eds_ocr_time","ocr时长"),
	CONT_TIME("iflytek_eds_cont_time","编目时长"),
	MODIFY_TIME("iflytek_eds_modify_time","人工核对时长"),
	OCR_FAIL_COUNT("iflytek_eds_ocr_fail","ocr失败数"),
	PIC_DPI("iflytek_eds_ocr_pic_dpi","图片质量分布dpi"),
	DOSSIER_USAGE("iflytek_eds_dossier_usage","电子卷宗使用情况之调用次数"),
	DOSSIER_ELLE_USAGE("iflytek_eds_dossier_elle","电子卷宗使用情况之抽取次数");

	private String name;
	private String description;

	PromeMetricEnum(String name, String description) {
		this.name = name;
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
