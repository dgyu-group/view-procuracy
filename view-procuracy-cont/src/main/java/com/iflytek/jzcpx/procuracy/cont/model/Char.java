package com.iflytek.jzcpx.procuracy.cont.model;

import javax.annotation.Generated;

@Generated("http://www.stay4it.com")
public class Char {

    private Rect rect;
    private Double score;
    private String value;

    public Rect getRect() {
        return rect;
    }

    public void setRect(Rect rect) {
        this.rect = rect;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
