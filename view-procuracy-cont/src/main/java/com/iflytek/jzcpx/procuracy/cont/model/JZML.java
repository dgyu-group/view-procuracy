package com.iflytek.jzcpx.procuracy.cont.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @Author wwwang7
 * @Date 2019/6/17 18:54
 */
public class JZML {

    /**
     * 卷宗编号
     */
    @JSONField(name="jzbh")
    private String JZBH;

    /**
     * 目录编号
     */
    @JSONField(name="mlbh")
    private String MLBH;

    /**
     * 目录类型
     */
    @JSONField(name="mllx")
    private int MLLX;

    /**
     * 父目录编号
     */
    @JSONField(name="fmlbh")
    private String FMLBH;

    /**
     * 目录显示名称
     */
    @JSONField(name="mlxsmc")
    private String MLXSMC;

    /**
     * 目录详细信息
     */
    @JSONField(name="mlxx")
    private String MLXX;

    /**
     * 目录顺序号
     */
    @JSONField(name="mlsxh")
    private int MLSXH;

    /**
     * 所属类别编码
     */
    @JSONField(name="sslbbm")
    private String SSLBBM;

    /**
     * 所属类别名称
     */
    @JSONField(name="sslbmc")
    private String SSLBMC;

    public String getJZBH() {
        return JZBH;
    }

    public void setJZBH(String JZBH) {
        this.JZBH = JZBH;
    }

    public String getMLBH() {
        return MLBH;
    }

    public void setMLBH(String MLBH) {
        this.MLBH = MLBH;
    }

    public int getMLLX() {
        return MLLX;
    }

    public void setMLLX(int MLLX) {
        this.MLLX = MLLX;
    }

    public int getMLSXH() {
        return MLSXH;
    }

    public void setMLSXH(int MLSXH) {
        this.MLSXH = MLSXH;
    }

    public String getFMLBH() {
        return FMLBH;
    }

    public void setFMLBH(String FMLBH) {
        this.FMLBH = FMLBH;
    }

    public String getMLXSMC() {
        return MLXSMC;
    }

    public void setMLXSMC(String MLXSMC) {
        this.MLXSMC = MLXSMC;
    }

    public String getMLXX() {
        return MLXX;
    }

    public void setMLXX(String MLXX) {
        this.MLXX = MLXX;
    }


    public String getSSLBBM() {
        return SSLBBM;
    }

    public void setSSLBBM(String SSLBBM) {
        this.SSLBBM = SSLBBM;
    }

    public String getSSLBMC() {
        return SSLBMC;
    }

    public void setSSLBMC(String SSLBMC) {
        this.SSLBMC = SSLBMC;
    }
}
