package com.iflytek.jzcpx.procuracy.cont.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 
 * @author iFLYREC
 *
 */
public interface RedisDataService {

	/**
	 * redis生成自增主键
	 * @param key
	 * @return
	 */
	String generateKey(String key,int index);

	/**
	 * 
	 * @param dossierId
	 * @return
	 */
	Map<String, Integer> getFailDetails(Integer dossierId);

	/**
	 * 
	 * @param dossierId
	 * @param count
	 * @param isAppend
	 */
	void setDetails(Integer dossierId, Integer count, Integer isAppend);

	/**
	 * 
	 * @param key
	 * @param value
	 */
	void resetKey(String key, Integer value);
	
	/**
	 * 
	 * @param key
	 * @return
	 */
	Integer getKey(String key);
	
	/**
	 * 
	 * @param key
	 * @param value
	 */
	void setLongKey(String key, long value);

	/**
	 * 
	 * @param key
	 * @return
	 */
	Long getLongKey(String key);

	/**
	 * 
	 * @param key
	 * @param delta
	 */
	void dataIncrements(String key, long delta);

	/**
	 * 
	 * @param dossierId
	 * @param isAppend
	 */
	void setScanDetails(Integer dossierId, Integer isAppend);

	/**
	 * 
	 * @param dossierId
	 * @param isAppend
	 * @param count
	 */
	void setUploadDetails(Integer dossierId, Integer isAppend, Integer count);
	
	/**
	 * 
	 * @param key
	 * @param hashKey
	 * @param path
	 */
	void setHashKeyList(String key, String hashKey, String path);
	
	/**
	 * 
	 * @param key
	 * @param hashKey
	 */
	void removeFailPathByHashKey(String key, String hashKey);
	
	/**
	 * 
	 * @param key
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	List getHashKeyList(String key);

	/**
	 * 
	 * @param key
	 * @param hashKey
	 * @param value
	 */
	void setArgueData(String key, String hashKey, String value);

	/**
	 * 
	 * @param key
	 * @param hashKey
	 * @return
	 */
	String getArgueData(String key, String hashKey);

	/**
	 * 
	 * @param key
	 * @param hashKey
	 */
	void setArgueCourtList(String key, String hashKey);

	/**
	 * 
	 * @param key
	 * @return
	 */
	int getArgueCourtCount(String key);

	/**
	 * 
	 * @param key
	 */
    void removeKey(String key);

    /**
     * 
     * @param s
     * @return
     */
    boolean hasKey(String s);

    /**
     * 
     * @param key
     * @param fileId
     * @param ocrJson
     * @param score
     */
    void addZSet(String key, String fileId, String ocrJson, double score);

    /**
     * 
     * @param key
     * @param start
     * @param end
     * @return
     */
	Set getZSet(String key, long start, long end);

	/**
	 * 
	 * @param key
	 * @param min
	 * @param max
	 * @param fileId
	 * @param ocrJson
	 */
    void replaceZSet(String key, double min, double max, String fileId, String ocrJson);

    /**
     * 
     * @param all_ocr_count
     * @param num
     * @param i
     */
    void hashDataIncrements(String all_ocr_count, String num, int i);
}
