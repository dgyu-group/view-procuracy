package com.iflytek.jzcpx.procuracy.cont.model;

import java.util.ArrayList;
import java.util.List;

/**
 * 编目实体模型
 */
public class ContObjModel {

    /**
     * 标题列
     */
    private  int titleLie;

    /**
     * 页码列
     */
    private int yema;

    public int getTitleLie() {
        return titleLie;
    }

    public int getYema() {
        return yema;
    }

    public void setTitleLie(int titleLie) {
        this.titleLie = titleLie;
    }

    public void setYema(int yema) {
        this.yema = yema;
    }

    /**
     * 封面
     */
    private List<JZMLWJ> fm;

    /**
     * 封底
     */
    private List<JZMLWJ> fd;

    /**
     * 目录
     */
    private List<JZMLWJ> mulu;

    /**
     * 正文部分
     */
    private List<JZMLWJ> items;

    public List<JZMLWJ> getFd() {
        return fd;
    }

    public List<JZMLWJ> getFm() {
        return fm;
    }

    public List<JZMLWJ> getItems() {
        return items;
    }

    public List<JZMLWJ> getMulu() {
        return mulu;
    }

    public void setFd(List<JZMLWJ> fd) {
        this.fd = fd;
    }

    public void setFm(List<JZMLWJ> fm) {
        this.fm = fm;
    }

    public void setItems(List<JZMLWJ> items) {
        this.items = items;
    }

    public void setMulu(List<JZMLWJ> mulu) {
        this.mulu = mulu;
    }

    /**
     * 新的顺序，结合人工和编目引擎结果
     * @return
     */
    public List<JZMLWJ> getFileList()
    {
        List<JZMLWJ> list=new ArrayList<>();
        if(fm.size()>0)
        {
            list.addAll(fm);
        }
        if(mulu.size()>0)
        {
            list.addAll(mulu);
        }
        if(items.size()>0)
        {
            list.addAll(items);
        }
        if(fd.size()>0)
        {
            list.addAll(fd);
        }

        return list;
    }
}
