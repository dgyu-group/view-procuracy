package com.iflytek.jzcpx.procuracy.cont.common.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.iflytek.jzcpx.procuracy.cont.model.OcrResult;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author zyzhang15
 *
 * @version 1.0
 *
 */
public class OcrResourcesUtil {

	private static final Logger logger = LoggerFactory.getLogger(OcrResourcesUtil.class);

	/**
	 * 将ocr JsonString 转换为OcrResult对象
	 * 
	 * @author zyzhang15 2018年2月25日下午3:20:22
	 * @param ocrJson
	 *            ocrjson数据
	 * @return ocr对象
	 */
	public static OcrResult getOcrResult(String ocrJson) {

		if (StringUtils.isEmpty(ocrJson)) {
			return new OcrResult();
		}

		OcrResult ocrResult = null;
		try {
			// 将原始json数据中 List转为null 的对象删除 ,将 String为null的对象转为空
			String pureJson = JSON.toJSONString(JSON.parseObject(ocrJson.trim()));
			ocrResult = JSONObject.parseObject(pureJson, OcrResult.class);
			return ocrResult;
		} catch (Exception ex) {
			logger.info("json 转换失败,参数输入:{}", ocrJson);
		}
		return ocrResult;
	}

}
