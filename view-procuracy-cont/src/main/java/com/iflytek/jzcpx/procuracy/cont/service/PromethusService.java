package com.iflytek.jzcpx.procuracy.cont.service;

/**
 * 
 * @author iFLYREC
 *
 */
public interface PromethusService {

	/**
	 * 
	 * @param dossierId
	 * @param metric
	 * @param value
	 * @param tags
	 */
	void counterTypeWithCourt(Integer dossierId, String metric, int value, String tags);

	/**
	 * 
	 * @param dossierId
	 * @param metric
	 * @param value
	 * @param tags
	 */
	void counterType(Integer dossierId, String metric, int value, String tags);
}
