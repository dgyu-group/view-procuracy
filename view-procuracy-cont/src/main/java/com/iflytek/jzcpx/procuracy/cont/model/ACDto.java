package com.iflytek.jzcpx.procuracy.cont.model;

import com.alibaba.fastjson.JSONObject;

/**
 * 自动编目协议json对应实体
 * 
 * @author lli
 *
 * @version 1.0
 * 
 */
public class ACDto implements Comparable<ACDto>  {
    /**
     * 文件id
     */
    private String id;
    /**
     * 文件ocr识别结果
     */
    private JSONObject ocr_result;

    private double acSort;

    public ACDto(String id,double acSort, JSONObject ocr_result) {
        super();
        this.id = id;
        this.acSort = acSort;
        this.ocr_result = ocr_result;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the ocr_result
     */
    public JSONObject getOcr_result() {
        return ocr_result;
    }

    /**
     * @param ocr_result
     *            the ocr_result to set
     */
    public void setOcr_result(JSONObject ocr_result) {
        this.ocr_result = ocr_result;
    }

    public double getAcSort() {
        return acSort;
    }

    public void setAcSort(double acSort) {
        this.acSort = acSort;
    }

    public int compareTo(ACDto o) {
        return this.acSort > o.getAcSort() ? 1 : -1;                //当compareTo方法返回正数的时候集合会怎么存就怎么取
    }
}
