package com.iflytek.jzcpx.procuracy.cont.model;

import com.alibaba.fastjson.annotation.JSONField;
import java.util.ArrayList;
import java.util.List;

public class ArchiveCatalogueResultDto {

    /**
     * 卷宗编号
     */
    private String jzbh;

    /**
     * 部门受案号
     */
    @JSONField(name="bmsah")
    private String BMSAH;

    /**
     * 标识编号
     */
    @JSONField(name="bsbh")
    private String BSBH;

    /**
     * 卷宗目录
     */
    @JSONField(name="jzml")
    private List<JZML> JZML;

    /**
     * 卷宗目录文件
     */
    @JSONField(name="jzmlwj")
    private List<JZMLWJ> JZMLWJ;

    /**
     * 删除的目录编号列表(MLBH)
     */
    @JSONField(name="delmulist")
    private List<String> DELMULIST = new ArrayList<>();

    /**
     * 删除的文件名称列表(WJMC)
     */
    @JSONField(name="delwjlist")
    private List<String> DELWJLIST= new ArrayList<>();

    public String getBMSAH() {
        return BMSAH;
    }

    public void setBMSAH(String BMSAH) {
        this.BMSAH = BMSAH;
    }

    public String getBSBH() {
        return BSBH;
    }

    public void setBSBH(String BSBH) {
        this.BSBH = BSBH;
    }

    public List<JZML> getJZML() {
        return JZML;
    }

    public void setJZML(List<JZML> JZML) {
        this.JZML = JZML;
    }

    public List<JZMLWJ> getJZMLWJ() {
        return JZMLWJ;
    }

    public void setJZMLWJ(List<JZMLWJ> JZMLWJ) {
        this.JZMLWJ = JZMLWJ;
    }

    public List<String> getDELMULIST() {
        return DELMULIST;
    }

    public void setDELMULIST(List<String> DELMULIST) {
        this.DELMULIST = DELMULIST;
    }

    public List<String> getDELWJLIST() {
        return DELWJLIST;
    }

    public void setDELWJLIST(List<String> DELWJLIST) {
        this.DELWJLIST = DELWJLIST;
    }
}
