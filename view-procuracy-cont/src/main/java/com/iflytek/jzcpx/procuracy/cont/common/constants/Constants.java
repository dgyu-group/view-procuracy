package com.iflytek.jzcpx.procuracy.cont.common.constants;

public class Constants {
	
	private Constants() {

    }

    /**
     * http请求前缀
     */
    public static final String HTTP = "http://";

    //定时任务已更新
    public static final String TASK_UPDATE_VALUE = "task_update_value";

    /**
     * 请求参数类型：页码0，文件1，全部2，未处理3,已处理4
     */
    public interface REQUEST_TYPE{
    	public final static int PAGE = 0;
    	public final static int FILE = 1;
    	public final static int CATALOG = 2;
    	public final static int UNPROCESS = 3;
    	public final static int PROCESS = 4;
        public final static int RECYCLEBIN = 5;
    }
    /**
     * 文件夹是否有更新状态
     *
     */
    public interface FOLDER_UPDATE {
        /**
         * 无更新
         */
        public final static int NO = 0;
        /**
         * 有更新
         */
        public final static int YES = 1;
    }

    /**
     * 自动编目未成功的目录名
     */
    public static final String UNKNOWN = "unknown";
    /**
     * 自动编目服务成功返回码
     */
    public static final String SUCCESS = "0";

    /**
     * 扫描文件格式
     */
    public static final String SCAN_PIC_TYPE = "jpg";
    public static final int CatalogStatus_SUCESS =3;
    public static final int CatalogStatus_FAIL =4;

    /** 印章类型*/
    public static final String OCR_SEAL_TYPE = "SEAL";
    /** 打印体*/
    public static final String OCR_MACHINEPRINT_TYPE = "MACHINEPRINT";
    /** 手写体*/
    public static final String OCR_HANDWRITE_TYPE = "HANDWRITE";

    public interface FILE_CAN_EDIT {
        /**
         * 可编辑
         */
        public final static int YES = 0;
        /**
         * 不可编辑
         */
        public final static int NO = 1;
    }

}
