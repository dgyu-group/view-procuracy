package com.iflytek.jzcpx.procuracy.cont.model;

import com.alibaba.fastjson.annotation.JSONField;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

@Generated("http://www.stay4it.com")
public class TextRegion {

    private Integer id;
    private Rect rect;
    private List<Textline> textline = new ArrayList<Textline>();
    
    @JSONField(name="textline_count")
    private Integer textlineCount;
    private String type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Rect getRect() {
        return rect;
    }

    public void setRect(Rect rect) {
        this.rect = rect;
    }

    public List<Textline> getTextline() {
        return textline;
    }

    public void setTextline(List<Textline> textline) {
        this.textline = textline;
    }

    public Integer getTextlineCount() {
        return textlineCount;
    }

    public void setTextlineCount(Integer textlineCount) {
        this.textlineCount = textlineCount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
