package com.iflytek.jzcpx.procuracy.cont.common.constants;

/**
 * 全局静态变量
 */
public class EdsConstants {

    // 地区常量
    public final static String DEFAULT = "default";
    public final static String SHANGHAI = "shanghai";
    public final static String SHANGHAI_PRISON = "shanghai_prison";
    public final static String JIANGXI = "jiangxi";
    public final static String SUZHOU = "suzhou";
    public final static String LANSHAN="lanshan";
    public final static String ZHGX206="zhgs";
    // 
    public final static String SZZY="suzhouzy";
    public final static String BEIJING="beijing";
    public final static String SHENXUN="shenxun";

    // 下载方式
    public final static String HTTP = "http";
    public final static String FTP_DIR = "ftpdir";
    public final static String FTP_FILE = "ftpfile";


    public final static String FDFS_PIC_TYPE = "jpg";
    public final static String SUZHOUZY_BATCHNO="suzhouzy_batchno_";
    public final static String SUZHOUZY_FYDM="suzhouzy_fydm_";

    public final static int HOT_WORD_MAXSIZE=50;
}
