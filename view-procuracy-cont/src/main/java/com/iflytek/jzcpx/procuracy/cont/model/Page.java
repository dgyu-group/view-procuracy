package com.iflytek.jzcpx.procuracy.cont.model;

import com.alibaba.fastjson.annotation.JSONField;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

@Generated("http://www.stay4it.com")
public class Page {

	@JSONField(name = "BlueSeal")
	private Object blueSeal;
	
	@JSONField(name = "Finger")
	private Object finger;
	
	@JSONField(name = "Graphics")
	private Object graphics;
	
	@JSONField(name = "RedSeal")
	private Object redSeal;
	
	@JSONField(name = "Seal")
	private Object seal;
	
	@JSONField(name = "Table")
	private List<Table> table = new ArrayList<Table>();
	
	@JSONField(name = "TextRegion")
	private List<TextRegion> textRegion = new ArrayList<TextRegion>();
	
	private Double angle;
	@JSONField(name = "blue_seal_count")
	private Integer blueSealCount;
	
	@JSONField(name = "finger_count")
	private Integer fingerCount;
	
	@JSONField(name = "graphics_count")
	private Integer graphicsCount;
	private Integer id;
	private String label;
	private Rect rect;
	
	@JSONField(name = "red_seal_count")
	private Integer redSealCount;
	
	@JSONField(name = "seal_count")
	private Integer sealCount;
	
	@JSONField(name = "table_count")
	private Integer tableCount;
	
	@JSONField(name = "text_region_count")
	private Integer textRegionCount;

	public Object getBlueSeal() {
		return blueSeal;
	}

	public void setBlueSeal(Object blueSeal) {
		this.blueSeal = blueSeal;
	}

	public Object getFinger() {
		return finger;
	}

	public void setFinger(Object finger) {
		this.finger = finger;
	}

	public Object getGraphics() {
		return graphics;
	}

	public void setGraphics(Object graphics) {
		this.graphics = graphics;
	}

	public Object getRedSeal() {
		return redSeal;
	}

	public void setRedSeal(Object redSeal) {
		this.redSeal = redSeal;
	}

	public Object getSeal() {
		return seal;
	}

	public void setSeal(Object seal) {
		this.seal = seal;
	}

	public List<Table> getTable() {
		return table;
	}

	public void setTable(List<Table> table) {
		this.table = table;
	}

	public List<TextRegion> getTextRegion() {
		return textRegion;
	}

	public void setTextRegion(List<TextRegion> textRegion) {
		this.textRegion = textRegion;
	}

	public Double getAngle() {
		return angle;
	}

	public void setAngle(Double angle) {
		this.angle = angle;
	}

	public Integer getBlueSealCount() {
		return blueSealCount;
	}

	public void setBlueSealCount(Integer blueSealCount) {
		this.blueSealCount = blueSealCount;
	}

	public Integer getFingerCount() {
		return fingerCount;
	}

	public void setFingerCount(Integer fingerCount) {
		this.fingerCount = fingerCount;
	}

	public Integer getGraphicsCount() {
		return graphicsCount;
	}

	public void setGraphicsCount(Integer graphicsCount) {
		this.graphicsCount = graphicsCount;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Rect getRect() {
		return rect;
	}

	public void setRect(Rect rect) {
		this.rect = rect;
	}

	public Integer getRedSealCount() {
		return redSealCount;
	}

	public void setRedSealCount(Integer redSealCount) {
		this.redSealCount = redSealCount;
	}

	public Integer getSealCount() {
		return sealCount;
	}

	public void setSealCount(Integer sealCount) {
		this.sealCount = sealCount;
	}

	public Integer getTableCount() {
		return tableCount;
	}

	public void setTableCount(Integer tableCount) {
		this.tableCount = tableCount;
	}

	public Integer getTextRegionCount() {
		return textRegionCount;
	}

	public void setTextRegionCount(Integer textRegionCount) {
		this.textRegionCount = textRegionCount;
	}

}
