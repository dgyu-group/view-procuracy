package com.iflytek.jzcpx.procuracy.cont.model;

public class CellData {


    private String value;

    private String pageNumStr;

    private int startNum;

    private int endNum;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getPageNumStr() {
        return pageNumStr;
    }

    public void setPageNumStr(String pageNumStr) {
        this.pageNumStr = pageNumStr;
    }

    public int getStartNum() {
        return startNum;
    }

    public void setStartNum(int startNum) {
        this.startNum = startNum;
    }

    public int getEndNum() {
        return endNum;
    }

    public void setEndNum(int endNum) {
        this.endNum = endNum;
    }
}
