package com.iflytek.jzcpx.procuracy.cont.model;

import com.alibaba.fastjson.annotation.JSONField;
import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

@Generated("http://www.stay4it.com")
public class TaskResult {

    private List<Page> page = new ArrayList<Page>();
    
    @JSONField(name="page_count")
    private Integer pageCount;
    
    @JSONField(name="return")
    private Integer _return;

    public List<Page> getPage() {
        return page;
    }

    public void setPage(List<Page> page) {
        this.page = page;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }

    public Integer getReturn() {
        return _return;
    }

    public void setReturn(Integer _return) {
        this._return = _return;
    }

}
