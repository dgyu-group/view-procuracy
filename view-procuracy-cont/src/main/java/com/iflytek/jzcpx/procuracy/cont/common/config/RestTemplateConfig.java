package com.iflytek.jzcpx.procuracy.cont.common.config;

import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.List;

import com.iflytek.ocr.client.OcrClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfig {

	@Autowired
	private EDSConfig edsConfig;

	@Bean
	public RestTemplate restTemplate(ClientHttpRequestFactory factory) {
		RestTemplate restTemplate = new RestTemplate(factory);
		// 使用 utf-8 编码集的 conver 替换默认的 conver（默认的 string conver
		// 的编码集为"ISO-8859-1"）
		List<HttpMessageConverter<?>> messageConverters = restTemplate.getMessageConverters();
		Iterator<HttpMessageConverter<?>> iterator = messageConverters.iterator();
		while (iterator.hasNext()) {
			HttpMessageConverter<?> converter = iterator.next();
			if (converter instanceof StringHttpMessageConverter) {
				iterator.remove();
			}
		}
		messageConverters.add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
		return restTemplate;
	}

	/*
	 * @Bean public ClientHttpRequestFactory simpleClientHttpRequestFactory() { //
	 * 长连接保持30秒 PoolingHttpClientConnectionManager pollingConnectionManager = new
	 * PoolingHttpClientConnectionManager(30, TimeUnit.SECONDS); // 总连接数
	 * pollingConnectionManager.setMaxTotal(1000); // 同路由的并发数
	 * pollingConnectionManager.setDefaultMaxPerRoute(1000); HttpClientBuilder
	 * httpClientBuilder = HttpClients.custom();
	 * httpClientBuilder.setConnectionManager(pollingConnectionManager); //
	 * 重试次数，默认是3次，没有开启 httpClientBuilder.setRetryHandler(new
	 * DefaultHttpRequestRetryHandler(2, true)); // 保持长连接配置，需要在头添加Keep-Alive
	 * httpClientBuilder.setKeepAliveStrategy(new
	 * DefaultConnectionKeepAliveStrategy()); List<Header> headers = new
	 * ArrayList<>(); headers.add(new BasicHeader("User-Agent",
	 * "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.16 Safari/537.36"
	 * )); headers.add(new BasicHeader("Accept-Encoding", "gzip,deflate"));
	 * headers.add(new BasicHeader("Accept-Language", "zh-CN")); headers.add(new
	 * BasicHeader("Connection", "Keep-Alive"));
	 * httpClientBuilder.setDefaultHeaders(headers); HttpClient httpClient =
	 * httpClientBuilder.build(); HttpComponentsClientHttpRequestFactory
	 * clientHttpRequestFactory = new
	 * HttpComponentsClientHttpRequestFactory(httpClient); // 连接超时
	 * clientHttpRequestFactory.setConnectTimeout(5000); // 数据读取超时时间，即SocketTimeout
	 * clientHttpRequestFactory.setReadTimeout(5000); //
	 * 连接不够用的等待时间，不宜过长，必须设置，比如连接不够用时，时间过长将是灾难性的
	 * clientHttpRequestFactory.setConnectionRequestTimeout(200); //
	 * 缓冲请求数据，默认值是true。通过POST或者PUT大量发送数据时，建议将此属性更改为false，以免耗尽内存。 //
	 * clientHttpRequestFactory.setBufferRequestBody(false); return
	 * clientHttpRequestFactory; }
	 */
	@Bean
	public ClientHttpRequestFactory simpleClientHttpRequestFactory() {
		int restReadTimeout = 60;
		int restConnectTimeout = 10;

		SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
		factory.setReadTimeout(restReadTimeout * 1000);// ms
		factory.setConnectTimeout(restConnectTimeout * 1000);// ms
		return factory;
	}

	@Bean
	public OcrClient getOcrClient() {
		OcrClient ocrClient = new OcrClient("http://" + edsConfig.getOcrIp(), edsConfig.getTimeout() * 100,
				edsConfig.getTimeout() * 1000);
		return ocrClient;
	}
}