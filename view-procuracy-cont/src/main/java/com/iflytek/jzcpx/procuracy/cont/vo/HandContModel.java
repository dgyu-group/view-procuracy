package com.iflytek.jzcpx.procuracy.cont.vo;

import lombok.Data;

/**
 *
 */
@Data
public class HandContModel {

    /**
     * 卷宗编号
     */
    private String jzbh;

    /**
     * 目录编号，一级目录
     */
    private String mlbh;
}
