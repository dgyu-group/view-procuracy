/* 
 *
 * Copyright (C) 1999-2012 IFLYTEK Inc.All Rights Reserved. 
 * 
 * FileName：MsgEnum.java
 * 
 * Description：
 * 
 * History：
 * Version   Author      Date            Operation 
 * 1.0	  lli   2017年8月3日下午2:02:18	       Create	
 */
package com.iflytek.jzcpx.procuracy.cont.model;

/**
 * 业务异常提示信息枚举类
 * 
 * @author lli
 *
 * @version 1.0
 * 
 */
public enum BusinessMsgEnum {
    /**  重复提交*/
    REPEATED_SUBMIT("101", "重复提交"),
    /**  失败任务重试  定时任务重复*/
    SCHEDULE_REPEATED_SUBMIT("114", "定时任务重复执行"),
    /** 参数为空异常 */
    PARMETER_NULL_EXCEPTION("102", "参数为空！"),
    /** 文件重名异常 */
    FILE_REPEAT_EXCEPTION("103", "名称已存在！"),
    /** 密码错误异常 */
    PWD_ERROR("104", "密码错误！"),
    USER_ERROR("105", "用户名错误！"),
    /** 正在进行图文识别 */
    OCR_PROCESSING("106", "正在进行图文识别"),
    /** 不存在未识别的图文 */
    NOT_EXIST_CONT("107", "尚无待编目的图片"),
    /** 编目结果为空 */
    NOT_CONT_RES("1071", "编目结果为空"),
    ROTATE_FAIL("106", "图片旋转失败！"),
    FOLDER_NOT_EXIST("107", "文件夹不存在！"),
    FILE_NOT_EXIST("108", "文件不存在！"),
    EDIT_DEFAULT_FAULT_ERROR("109", "未命名文件夹不可编辑！"),
    DOSSIER_NOT_EXIST("110", "收案材料不存在！"),
    ONLINE_FILE_CLOSE_IN("111", "当前案件网上材料已审核过！"),
    DOSSIER_NO_REPEAT("112", "收案号重复！"),
    PIC_EXCEPTION("113","图片下载为null"),
    /** 超时错误 */
    TIME_OUT_ERROR("499", "处理发生超时！"),
    /** 文件上传存储超时 */
    TIME_OUT_FDFS_ERROR("499", "文件上传存储超时！"),
    /** 文件上传存储超时 */
    TIME_OUT_OCR_ERROR("499", "图像识别超时！"),
    /** 500 : 发生异常 */
    UNEXPECTED_EXCEPTION("500", "系统发生异常，请联系管理员！"),
    /**任务未编目*/
    UNCATALOG_EXCEPTION("114","任务未编目！"),
    OCR_FAILED("300", "OCR识别失败"),

    ALL_PAGE("115", "图片尚未完成编目，请点击自动编目"),
    PARTION_PAGE("116", "文件归目成功，图片尚未完成编目，请点击自动编目"),
    NO_PAGE_AND_FOLDER("117", "尚无待归目的文件"),
    ONEKEYTOTREE_COMPLETED("0","文件归目完成"),
    FTP_PATH_NOFIND("201","FTP文件路径不存在"),
    DOSSIERNO_EXIT("301","当前材料号已存在"),
    SUPPORT_ONE_PDF("302","一次只能上传一份PDF卷宗！"),
    NOT_FINISH_CONT("303", "编目尚未完成"),
    FDFS_DELETE_WRONG("304", "fdfs删除文件失败"),
    NOFILE_DELETE("305", "该时间范围fdfs无文件可清除"),
    NO_OCR_FAILED("306", "不存在OCR识别失败的文件！"),
    FILE_FORMAT_ERROER("307", "请上传正确格式的文件！"),
    NOFILE_IN_DOSSIER_ERROER("308", "该任务下没有文件"),
    HOTWORD_SHOULDNOT_BENULL("401","新增热词不能为空！"),
    HOTWORD_ALEADLY_EXISTS("402","新增自定义热词已经存在！"),
    HOTWORD_NOT_EXISTS("403","自定义热词不存在！"),
    HOTWORD_REACH_MAXSIZE("405","您当前已添加50个热词，达到热词上限，请删除后再试！"),
    ARGUE_EXCEL_EMPTY("600","导出报表数据为空"),
    FOLDER_EXIST("700","文件夹已存在"),
    DEFAULT_FOLDER("701","默认目录不允许修改和删除"),
    FILE_EXIST("702","当前目录存在文件夹"),
    DEFAULT_FOLDER_ERROR("801","默认目录，可重命名，不能删除！"),
    FOLDER_NOT_EMPTY("703","存在非空目录"),
    NO_PIC_EXCEPTION("802","下载内容为空"),
    DOSSIER_ID_NULL("700","案件不存在"),
    DOSSIER_NAME_NULL("701","材料号不能为空"),
    NO_TARGET_SELECTED("901","未命中目标"),
    WORD_TRANSFER_ERROR("333","doc文件转换异常！！！"),
    
	// 接口限流提示
	SERVER_BUSY("503","系统繁忙，稍后重试！");

    /**
     * 消息码
     */
    private String code;
    /**
     * 消息内容
     */
    private String msg;

    private BusinessMsgEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String code() {
        return code;
    }

    public String msg() {
        return msg;
    }

}
