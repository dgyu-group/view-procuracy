package com.iflytek.jzcpx.procuracy.cont.service;

import com.iflytek.jzcpx.procuracy.cont.model.ArchiveCatalogueResultDto;
import com.iflytek.jzcpx.procuracy.cont.model.JZML;
import com.iflytek.jzcpx.procuracy.cont.model.JZMLWJ;
import java.util.List;

public interface ElectronicArchiveService {

    ArchiveCatalogueResultDto ocrAndContFile(List<JZML> mlList, List<JZMLWJ> wjList, String BSBH) throws Exception;
}