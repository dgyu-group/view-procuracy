package com.iflytek.jzcpx.procuracy.cont.model;

import java.util.List;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 华宇查询立案号返回的实体
 * 
 * @author lli
 *
 * @version 1.0
 * 
 */
public class AjDtos {
    
    @JSONField(name = "ajlist")
    private List<AjDto> ajlist;

    /**
     * @return the ajlist
     */
    public List<AjDto> getAjlist() {
        return ajlist;
    }

    /**
     * @param ajlist
     *            the ajlist to set
     */
    public void setAjlist(List<AjDto> ajlist) {
        this.ajlist = ajlist;
    }

}
