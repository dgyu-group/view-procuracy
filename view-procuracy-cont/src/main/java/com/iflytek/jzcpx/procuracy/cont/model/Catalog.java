package com.iflytek.jzcpx.procuracy.cont.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author iFLYREC
 *
 */
public class Catalog {

    @JsonProperty("evidence_id")
    private int evidenceId;
    private String id;
    @JsonProperty("page_num")
    private int pageNum;
    private double score;
    private Object title;
    @JsonProperty("title_score")
    private double titleScore;
    private String type;
    private String content;
//    private List<SelectTitleDto> titles;

    public void setEvidenceId(int evidenceId) {
        this.evidenceId = evidenceId;
    }

    public int getEvidenceId() {
        return evidenceId;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public double getScore() {
        return score;
    }

    public void setTitle(Object title) {
        this.title = title;
    }

    public Object getTitle() {
        return title;
    }

    public void setTitleScore(double titleScore) {
        this.titleScore = titleScore;
    }

    public double getTitleScore() {
        return titleScore;
    }

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

//    public List<SelectTitleDto> getTitles() {
//        return titles;
//    }
//
//    public void setTitles(List<SelectTitleDto> titles) {
//        this.titles = titles;
//    }
}