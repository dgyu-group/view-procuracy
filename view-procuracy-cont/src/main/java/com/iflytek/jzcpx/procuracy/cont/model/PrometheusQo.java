package com.iflytek.jzcpx.procuracy.cont.model;

import lombok.Data;

/**
 * 
 * @author iFLYREC
 *
 */
@Data
public class PrometheusQo {
	
	/**
	 * 
	 */
    private String metric;
    
    /**
     * 
     */
    private String tags;
    
    /**
     * 
     */
    private int value;
}
