package com.iflytek.jzcpx.procuracy.cont.model;

import com.alibaba.fastjson.annotation.JSONField;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

@Generated("http://www.stay4it.com")
public class Textline {

    private Integer id;
    private Rect rect;
    private List<Sent> sent = new ArrayList<Sent>();
    
    @JSONField(name="top_n")
    private Integer topN;
    private String type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Rect getRect() {
        return rect;
    }

    public void setRect(Rect rect) {
        this.rect = rect;
    }

    public List<Sent> getSent() {
        return sent;
    }

    public void setSent(List<Sent> sent) {
        this.sent = sent;
    }

    public Integer getTopN() {
        return topN;
    }

    public void setTopN(Integer topN) {
        this.topN = topN;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return "Textline [id=" + id + ", rect=" + rect.toString() + ", sent=" + sent.toString() + ", topN=" + topN + ", type=" + type + "]";
    }

}
