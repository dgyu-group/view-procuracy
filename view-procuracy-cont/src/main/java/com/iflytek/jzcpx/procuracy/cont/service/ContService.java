package com.iflytek.jzcpx.procuracy.cont.service;

import java.io.File;
import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.iflytek.jzcpx.procuracy.common.model.HandContResultInfo;
import com.iflytek.jzcpx.procuracy.common.model.HandContResultInfoJzml;
import com.iflytek.jzcpx.procuracy.common.model.HandContResultInfoJzmlwj;
import com.iflytek.jzcpx.procuracy.cont.model.JZML;
import com.iflytek.jzcpx.procuracy.cont.model.JZMLWJ;
import com.iflytek.jzcpx.procuracy.cont.model.SuccessJsonResult;
import com.iflytek.jzcpx.procuracy.cont.vo.HandContModel;

/**
 * @author <a href=mailto:ktyi@iflytek.com>伊开堂</a>
 * @date 2019-08-06 19:49
 */
public interface ContService {

	/**
	 * 根据标识编号获取卷宗信息
	 * @param bsbh
	 * @return
	 */
	String getDZJZInfo(String bsbh);
	
	/**
	 * 保存电子卷宗信息
	 * @param body
	 * @return
	 */
	String SaveDZJZInfo(String body);
	
	/**
	 * 保存电子卷宗信息
	 * @param body
	 * @return
	 */
	public String saveDzjzzzInfo(String body);
	
	/**
	 * 根据标志编号和文件序号获取文件的字节流
	 * @param bsbh
	 * @param wjxh
	 * @return
	 */
	byte[] getFileBytes(String bsbh,String wjxh);
	
	/**
	 * 获取解析赛威讯结构的文件
	 * @param bsbh
	 * @param wjxh
	 * @return
	 */
	byte[] getFileBytesBySWX(String bsbh,String wjxh);
	
	/**
	 * 图像识别，去除最外层结构后的结果
	 * @param bsbh
	 * @param wjxh
	 * @return
	 */
	String recognize(String bsbh,String wjxh);

	/**
	 *
	 * @param file
	 * @param params
	 * @param trackId
	 * @return
	 */
	String recognize(File file, JSONObject params, String trackId);

	/**
	 *
	 * @param handContModel
	 * @return
	 */
	SuccessJsonResult<Void> handCont(HandContModel handContModel);

	/**
	 *
	 * @param jzbh
	 * @param mlbh
	 * @return
	 */
    HandContResultInfo getContDataFromSwx(String jzbh, String mlbh);

	/**
	 *
	 * @param handContResultInfoJzml
	 * @return
	 */
	List<JZML> handContJzml2Jzml(HandContResultInfoJzml handContResultInfoJzml);

	/**
	 *
	 * @param handContResultInfoJzmlwjs
	 * @return
	 */
	List<JZMLWJ> HandContJzmlwj(List<HandContResultInfoJzmlwj> handContResultInfoJzmlwjs);
}
