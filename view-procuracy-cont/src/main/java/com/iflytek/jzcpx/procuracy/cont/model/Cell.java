package com.iflytek.jzcpx.procuracy.cont.model;

import com.alibaba.fastjson.annotation.JSONField;
import javax.annotation.Generated;
import java.util.ArrayList;

@Generated("http://www.stay4it.com")
public class Cell {

    private Integer col;
    private Integer colspan;
    private Rect rect;
    private Integer row;
    private Integer rowspan;
    
    private  ArrayList<Textline> textline=new ArrayList<>();
    @JSONField(name="textline_count")
    private Integer textlineCount;

    public Integer getCol() {
        return col;
    }

    public void setCol(Integer col) {
        this.col = col;
    }

    public Integer getColspan() {
        return colspan;
    }

    public void setColspan(Integer colspan) {
        this.colspan = colspan;
    }

    public Rect getRect() {
        return rect;
    }

    public void setRect(Rect rect) {
        this.rect = rect;
    }

    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }

    public Integer getRowspan() {
        return rowspan;
    }

    public void setRowspan(Integer rowspan) {
        this.rowspan = rowspan;
    }

    public ArrayList<Textline> getTextline() {
        return textline;
    }

    public void setTextline(ArrayList<Textline> textline) {
        this.textline = textline;
    }

    public Integer getTextlineCount() {
        return textlineCount;
    }

    public void setTextlineCount(Integer textlineCount) {
        this.textlineCount = textlineCount;
    }

}
