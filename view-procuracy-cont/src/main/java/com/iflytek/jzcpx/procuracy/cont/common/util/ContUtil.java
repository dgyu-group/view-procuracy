package com.iflytek.jzcpx.procuracy.cont.common.util;

import com.iflytek.jzcpx.procuracy.cont.model.Catalog;
import com.iflytek.jzcpx.procuracy.cont.model.CellData;
import com.iflytek.jzcpx.procuracy.cont.model.ContObjModel;
import com.iflytek.jzcpx.procuracy.cont.model.JZMLWJ;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * 编目工具
 */
@Slf4j
public class ContUtil {

    /**
     * 判断集合中是否包含封面、封底
     *
     * @return
     */
    public static List<JZMLWJ> containsFm(List<JZMLWJ> items) {

        List<JZMLWJ> objs = new ArrayList<>();
        for (JZMLWJ jzmlwj : items) {
            if (jzmlwj.getWJMC().contains("封面")) {
                objs.add(jzmlwj);
            }
        }

        return objs;
    }

    /**
     * @param items
     * @return
     */
    public static List<JZMLWJ> containsFd(List<JZMLWJ> items) {

        List<JZMLWJ> objs = new ArrayList<>();
        for (JZMLWJ jzmlwj : items) {
            if (jzmlwj.getWJMC().contains("封底")) {
                objs.add(jzmlwj);
            }
        }

        return objs;
    }

    /**
     * @param items
     * @return
     */
    public static List<JZMLWJ> containsMulu(List<JZMLWJ> items) {

        List<JZMLWJ> objs = new ArrayList<>();

        for (JZMLWJ jzmlwj : items) {
            if (jzmlwj.getWJMC().contains("目录") && !jzmlwj.getWJMC().contains("封面") && !jzmlwj.getWJMC().contains("封底")) {
                objs.add(jzmlwj);
            }
        }

        return objs;
    }

    /**
     * 封底下标
     *
     * @param items
     * @return
     */
    public static int containsFdIndex(List<JZMLWJ> items) {

        List<JZMLWJ> objs = new ArrayList<>();
        for (int i = 0; i < items.size(); i++) {
            JZMLWJ jzmlwj = items.get(i);
            if (jzmlwj.getWJMC().contains("封底")) {
                return i;
            }
        }
        return -1;
    }

    /**
     * @param items
     * @return
     */
    public static int containsMuluBeginIndex(List<JZMLWJ> items) {

        List<JZMLWJ> objs = new ArrayList<>();

        for (int i = 0; i < items.size(); i++) {
            JZMLWJ jzmlwj = items.get(i);
            if (jzmlwj.getWJMC().contains("目录") && !jzmlwj.getWJMC().contains("封面") && !jzmlwj.getWJMC().contains("封底")) {
                return i;
            }
        }

        return -1;
    }

    /**
     * 通过引擎找目录名称
     *
     * @param catalogs
     * @return
     */
    public static String getMuluName(List<Catalog> catalogs) {
        for (int i = 0; i < catalogs.size(); i++) {
            if (catalogs.get(i).getTitle().toString().matches(MATCH_REGEX)) {
                return catalogs.get(i).getTitle().toString();
            }
        }
        return "卷宗目录";
    }

    /**
     * 通过引擎找封面名称
     *
     * @param catalogs
     * @return
     */
    public static String getFmName(List<Catalog> catalogs) {
        for (int i = 0; i < catalogs.size(); i++) {
            if (catalogs.get(i).getTitle().toString().matches("封面")) {
                return catalogs.get(i).getTitle().toString();
            }
        }
        return "封面";
    }

    /**
     * @param catalogs
     * @return
     */
    public static String getFdName(List<Catalog> catalogs) {
        for (int i = 0; i < catalogs.size(); i++) {
            if (catalogs.get(i).getTitle().toString().matches("封底")) {
                return catalogs.get(i).getTitle().toString();
            }
        }
        return "封底";
    }

    /**
     * 排序
     *
     * @param wjList
     */
    public static List<JZMLWJ> listDoOrder(List<JZMLWJ> wjList) {

        JZMLWJ[] dataTemp = new JZMLWJ[wjList.size()];
        wjList.toArray(dataTemp);

        Arrays.sort(dataTemp, new Comparator<JZMLWJ>() {
            public int compare(JZMLWJ o1, JZMLWJ o2) {
                int n1 = extractNumber(o1.getWJMC());
                int n2 = extractNumber(o2.getWJMC());
                return n1 - n2;
            }
        });

        wjList = Arrays.asList(dataTemp);
        //wjList.toString();

        return wjList;
    }

    /**
     * @param name
     * @return
     */
    private static int extractNumber(String name) {
        int i;
        try {
            String number = name.replaceAll("[^\\d]", "");
            i = Integer.parseInt(number);
        } catch (Exception e) {
            i = 0;
        }
        return i;
    }

    /**
     * @param model
     * @param items
     */
    private static void getContent(ContObjModel model, List<JZMLWJ> items) {
        model.setItems(new ArrayList<>());

        for (JZMLWJ jzmlwj : items) {
            int flag = 0;
            for (JZMLWJ fm : model.getFm()) {
                if ((fm.getWJXH().equals(jzmlwj.getWJXH()))) {
                    flag = 1;//在封面里
                    break;
                }
            }
            for (JZMLWJ fd : model.getFd()) {
                if ((fd.getWJXH().equals(jzmlwj.getWJXH()))) {
                    flag = 1;//在封底里
                    break;
                }
            }
            for (JZMLWJ mulu : model.getMulu()) {
                if ((mulu.getWJXH().equals(jzmlwj.getWJXH()))) {
                    flag = 1;//在目录里
                    break;
                }
            }
            if (flag == 0) {
                model.getItems().add(jzmlwj);
            }
        }
    }

    private final static String MATCH_REGEX = ".*(目录).*";

    private final static String NOT_RECOGNIZE_FILE = "未识别材料";

    /**
     * @param catalogs
     * @param items
     */
    public static void contModelFormat(ContObjModel model, List<Catalog> catalogs, List<JZMLWJ> items) {

        //拆分结构，获取 封面、目录、正文、封底、先用汉字匹配的方式拆分

        //取封面
        model.setFm(containsFm((items)));


        //取目录开始下标
        int muluIndex = containsMuluBeginIndex(items);

        //用名字取目录
        List<JZMLWJ> mulusByName = containsMulu(items);
        model.setMulu(mulusByName);

        int muluFindFirst = 0;

        if (mulusByName.size() == 0) {
            model.setMulu(new ArrayList<JZMLWJ>());
            //取引擎返回的目录
            for (int i = 0; i < catalogs.size(); i++) {
                Catalog catalogsItemTemp = catalogs.get(i);
                if (catalogsItemTemp.getTitle().toString().matches(MATCH_REGEX)) {

                    if (muluFindFirst == 0) {
                        //从引擎里找到目录的开始下标
                        muluIndex = i;
                    }
                    model.getMulu().add(items.get(i));
                    muluFindFirst = 1;
                } else {
                    if (muluFindFirst == 1) {
                        //不连续时退掉
                        break;
                    }
                }
            }
        }

        //设置封底
        model.setFd(containsFd(items));

        //如果找到了目录，没有找到封面，则把目录前面的放到封面里去

        if (model.getFm().size() == 0) {
            //判断目录的开始位置，如果不是
            if (muluIndex > 0) {

                model.setFm(new ArrayList<>());

                //目录前面的文件当做封面
                for (int i = 0; i < items.size() && i < muluIndex; i++) {

                    int inFd = 0;
                    for (int j = 0; j < model.getFd().size(); j++) {
                        JZMLWJ jzmlwj_fd = model.getFd().get(j);

                        //这个文件是封底的话，不要放到封面里去
                        if (jzmlwj_fd.getWJXH().equals(items.get(i).getWJXH())) {
                            inFd = 1;
                        }
                    }

                    if (inFd == 1) {
                        continue;
                    }
                    model.getFm().add(items.get(i));
                }
            }
        }


        //剩下的部分是内容
        getContent(model, items);
    }

    /**
     * @param cellList
     * @param items
     * @return
     */
    public static boolean check(List<CellData> cellList, List<JZMLWJ> items) {

        try {
            int size = items.size();

            //如果前一个的结束位置为0，则复制为后一个开始-1
            for (int j = 0; j < cellList.size() - 1; j++) {

                CellData prev = cellList.get(j);
                CellData next = cellList.get(j + 1);
                if (prev.getEndNum() == 0) {
                    prev.setEndNum(next.getStartNum() - 1);
                }

            }
            //最后一个也处理一下
            if (cellList.get(cellList.size() - 1).getEndNum() == 0) {
                cellList.get(cellList.size() - 1).setEndNum(size);
            }

            //目录存在标题为空情况，判定为不合法，开始坐标在不是第一个的时候，如果开始页码是0或者为负
            for (int k = 0; k < cellList.size(); k++) {


                CellData item = cellList.get(k);

                if (item.getValue() == null) {
                    //item.setValue(NOT_RECOGNIZE_FILE);
                    return false;
                }

                if (k != 0 && item.getStartNum() <= 0) {
                    return false;
                }
                if (item.getValue().equals("")) {
                    return false;
                }
            }

            //检查页码交叉的情况
            for (int i = 0; i < cellList.size() - 1; i++) {
                CellData prev = cellList.get(i);
                CellData next = cellList.get(i + 1);

                if (prev.getEndNum() < prev.getStartNum() || next.getEndNum() < next.getStartNum()) {
                    return false;
                }

                if (prev.getStartNum() >= next.getStartNum()) {
                    //不合法
                    return false;
                }
                if (prev.getStartNum() > size || prev.getEndNum() > size || next.getStartNum() > size || next.getEndNum() > size) {
                    return false;
                }
            }
        } catch (Exception ex) {

            log.error(ex.getMessage());
            return false;
        }

        return true;
    }
}