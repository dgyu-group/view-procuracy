/* 
 *
 * Copyright (C) 1999-2012 IFLYTEK Inc.All Rights Reserved. 
 * 
 * FileName：BL_point.java
 * 
 * Description：
 * 
 * History：
 * Version   Author      Date            Operation 
 * 1.0	  zyzhang15   2018年4月3日下午5:31:49	       Create	
 */
package com.iflytek.jzcpx.procuracy.cont.model;

import javax.annotation.Generated;

/**
 * @author zyzhang15 
 *
 * @version 1.0
 * 
 */
@Generated("http://www.stay4it.com")
public class BL_point {
    public Integer x;
    public Integer y;
    /** {@inheritDoc} */
    @Override
    public String toString() {
        return "BL_point [x=" + x + ", y=" + y + "]";
    }
}
