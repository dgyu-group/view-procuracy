/* 
 *                                                                   
 * FileName：ocr.java                       
 *          
 * Description：ocr jni                         
 *                                                                  
 *     
 */
package com.iflytek.jzcpx.procuracy.cont.common.util;

/**
 * 
 * @author manzhang
 *
 * @version 1.0
 *
 */

public class ocr {

    /**
     * 
     * @description 初始化ocr
     * @author manzhang
     * @create 2016年8月17日下午7:13:05
     * @version 1.0
     * @param server
     *            ocr服务
     * @param appid
     *            ocr id
     * @param time_out
     *            超时
     * @return 0:初始化成功,其余失败
     */
    public native static int OCRInitialize(String server, String appid,
            int time_out);

    /**
     * 
     * @description 解除初始化
     * @author manzhang
     * @create 2016年8月17日下午7:14:23
     * @version 1.0
     * @return 0:解除成功，其余失败
     */
    public native static int OCRUninitialize();

    /**
     * 
     * @description ocr识别
     * @author manzhang
     * @create 2016年8月17日下午7:15:01
     * @version 1.0
     * @param param
     *            文件信息
     * @param json_data
     *            图片数据
     * @param json_len
     *            数据长度
     * @param result
     *            结果集
     * @param result_len
     *            结果长度
     * @return 0 初始化成功，其余失败
     */
    public native static int QOCR(String param, String json_data, int json_len,
            byte[] result, int[] result_len);

    /**
     * 
     * @description 获取本次识别的sessionId
     * @author manzhang
     * @create 2016年11月14日下午8:34:36
     * @version 1.0
     * @param param
     *            采用key、value方式传入，以”=”连接，参数之间采用”&”分隔
     * @param errorCode
     *            接口参数中errorCode表示是否出错。如果函数调用成功返回0，否则返回错误代号
     * @return
     */
    public native String OCRSessionBegin(String param, int[] errorCode);

    /**
     * 
     * @description 结束本次图像识别
     * @author manzhang
     * @create 2016年11月14日下午3:47:40
     * @version 1.0
     * @param sessionId
     *            由OCRSessionBegin创建的唯一会话标志
     * @return 0:初始化成功,其余失败
     */
    public native int OCRSessionEnd(String sessionId);

    /**
     * 
     * @description 写入要识别的图像数据
     * @author manzhang
     * @create 2016年11月14日下午8:39:44
     * @version 1.0
     * @param sessionId
     *            session_begin获取的session id
     * @param imageName
     *            图片名
     * @param imageType
     *            类别，为0
     * @param imageData
     *            图片数据
     * @param imageLen
     *            图片数据的长度
     * @param imageStatus
     *            传入图片的的状态，1，代表开始，2代表继续，4代表结束，一个图片时，可以直接传入4。
     * @return 接口参数中ret表示返回值。如果函数调用成功返回0，否则返回错误代号
     */
    public native int OCRImageWrite(String sessionId, String imageName,
            int imageType, byte[] imageData, int imageLen, int imageStatus);

    /**
     * 
     * @description 获取要解析的语义
     * @author manzhang
     * @create 2016年11月14日下午7:00:19
     * @version 1.0
     * @param sessionId
     *            session_begin获取的session id
     * @param globalStat
     *            该参数代表整个（多张）图片获取结果是否结束，开始的时候是2，如果为3代表结束，1代表出错
     * @param rsltStatus
     *            该参数代表获取结果是否结束，0代表继续获取结果，1代表出错，2表示本次调用有结果但是不是全部结果还需要继续获取，3
     *            代表获取结果结束
     * @param errorCode
     *            该参数代表调用该结果是否出错，0代表成功，其他代表出错
     * @return
     */
    public native String OCRGetResult(String sessionId, int[] globalStat,
            int[] rsltStatus, int[] errorCode);

    /**
     * 
     * @description 初始化语义抽取
     * @author manzhang
     * @create 2016年11月14日下午3:38:41
     * @version 1.0
     * @param server
     *            ocr服务
     * @param appid
     *            ocr id
     * @param time_out
     *            超时
     * @return 0:初始化成功,其余失败
     */
    public native static int ELLEInitialize(String server, String appid,
            int time_out);

    /**
     * 
     * @description 释放语义抽取库
     * @author manzhang
     * @create 2016年11月14日下午3:39:55
     * @version 1.0
     * @return 0:初始化成功,其余失败
     */
    public native static int ELLEUninitialize();

    /**
     * 
     * @description 获取本次识别的sessionId
     * @author manzhang
     * @create 2016年11月14日下午3:44:38
     * @version 1.0
     * @return 0:初始化成功,其余失败
     */
    public native String ELLESessionBegin(String param, int[] errorCode);

    /**
     * 
     * @description 结束本次语义抽取
     * @author manzhang
     * @create 2016年11月14日下午3:47:40
     * @version 1.0
     * @param sessionId
     *            由ELLESessionBegin创建的唯一会话标志
     * @return 0:初始化成功,其余失败
     */
    public native int ELLESessionEnd(String sessionId);

    /**
     * 
     * @description 写入要解析的语义文本
     * @author manzhang
     * @create 2016年11月14日下午6:58:22
     * @version 1.0
     * @param sessionId
     *            session_begin获取的session id
     * @param fileName
     *            语义抽取的文本路径名
     * @param fileData
     *            语义抽取的文本数据
     * @param fileLen
     *            语义抽取的文本数据的长度
     * @param fileStatus
     *            传入文本的状态，1，代表开始，2代表继续，4代表结束，一个文件时，可以直接传入4。目前只支持一次性传入所有文本内容
     * @return
     */
    public native int ELLEImageWrite(String sessionId, String fileName,
            byte[] fileData, int fileLen, int fileStatus);

    /**
     * 
     * @description 获取要解析的语义
     * @author manzhang
     * @create 2016年11月14日下午7:00:19
     * @version 1.0
     * @param sessionId
     *            session_begin获取的session id
     * @param rsltStatus
     *            该参数代表获取结果是否结束，0代表继续获取结果，1代表出错，2表示本次调用有结果但是不是全部结果还需要继续获取，3
     *            代表获取结果结束
     * @param errorCode
     *            该参数代表调用该结果是否出错，0代表成功，其他代表出错
     * @return
     */
    public native String ELLEGetResult(String sessionId, int[] rsltStatus,
            int[] errorCode);

}
