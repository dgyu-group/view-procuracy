/**
 * Copyright (C), 2015-2018, iflytek.com
 * FileName: UUIDUtil
 * Author:   pc
 * Date:     2018/9/5 16:22
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号           描述
 */

package com.iflytek.jzcpx.procuracy.cont.common.util;

import java.util.UUID;

/**
 * 
 * @author iFLYREC
 *
 */
public class UUIDUtil {
    public static String getuuid(){
        UUID uuid=UUID.randomUUID();
        String str = uuid.toString().toUpperCase();
        String uuidStr=str.replace("-", "");
        return uuidStr;
    }

    public static String getuuidFromStr(String str){
        UUID uuid=UUID.nameUUIDFromBytes(str.getBytes());
        String string = uuid.toString();
        String uuidStr=string.replace("-", "");
        return uuidStr;
    }

}
