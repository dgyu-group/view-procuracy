package com.iflytek.jzcpx.procuracy.cont.common.constants;

/**
 * 数据埋点类型枚举
 */
public enum StatisticDataType {

    OCR_TIME("ocr_time", "单张图片OCR耗时"),
    CASE_NUM("case_num","收案数量"),
    UPDATE_NUM("update_num","处理图片数量"),
    PIC_SIZE("pic_size","图片大小"),
    TITLE_IDENTIFY_NUM("title_identify_num","标题识别总数"),
    TITLE_IDENTIFY_UNKNOWN("title_identify_unknown","标题未识别数量"),
    SKIP_NUM("skip_num","跳过编目"),
    CATALOG_TIME("catalog_time","编目耗时"),
    MANUAL_TIME("manual_time","人工操作耗时");


    private String dataType;

    private String desc;

    StatisticDataType(String dataType, String desc) {
        this.dataType = dataType;
        this.desc = desc;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
