package com.iflytek.jzcpx.procuracy.cont.model;

import com.alibaba.fastjson.annotation.JSONField;

import javax.annotation.Generated;

@Generated("http://www.stay4it.com")
public class OcrResult {
	
	@JSONField(name="task_result")
    private TaskResult taskResult;

    public TaskResult getTaskResult() {
        return taskResult;
    }

    public void setTaskResult(TaskResult taskResult) {
        this.taskResult = taskResult;
    }

}
