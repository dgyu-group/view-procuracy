package com.iflytek.jzcpx.procuracy.cont.common.constants;

/**
 * 缓存key前缀静态变量
 */
public class CacheKeyPrefixConstants {

    public final static String AVOID_RESUBMIT = "avoid_resubmit_";

    public final static String TASK_STATUS_UPDATE = "task_status_update";

    public final static String CASE_CAUSE_UPDATE = "case_cause_update";

    public final static String DOWNLOAD_FAIL_NUM = "download_fail_num_";
    public final static String UPLOAD_FAIL_NUM = "upload_fail_num_";
    public final static String OCR_FAIL_NUM = "ocr_fail_num_";
    
    public final static String OCR_SUCCESS_NUM = "ocr_success_num_";
    public final static String FILE_NUM = "file_num_";
    public final static String FILE_UPLOAD_NUM = "file_upload_num_";
    public final static String OCR_FAIL_PATH = "ocr_fail_path_";
    
    public final static String TIME_OUT = "time_out_";

    public final static String OCR_ISBLANK_NUM = "ocr_isblank_num_";
    public final static String OCR_NOBLANK_NUM = "ocr_noblank_num_";

    @SuppressWarnings("unused")
	private final static String TASK_QUEUE_NUM = "task_queue_num_";
    
    public final static String CATALOG_TIME = "catalog_time_";
    public final static String MANUAL_TIME = "manual_time_";

    public final static String ARGUE_DATA = "argue_data_";
    public final static String ARGUE_COURT_LIST = "argue_court_list_";

    public final static String OCR_RES = "ocr_res_";
    public final static String HAVE_CHANGE = "have_change_";
}
